# Installation
## Requirements:

This tool makes use of the following dependencies:
- `git`
- `boost` version `>=1.36`. Extra information can be found [Boost.Units](https://www.boost.org/doc/libs/1_65_0/doc/html/boost_units.html);
- `cmake` version `>=3.2`;
- `clang` version `>=14`

## Installation
1. get source files using the following `git` command:

    `git clone https://gitlab.com/micrenda/univec.git`


2. configure the external dependencies

    `git submodule init`

    `git submodule update`


## Example
For example, to calculate the energy of an object, we can use the well-known formulas:

```c++
#include "univec/univec.hpp"

VectorC3D<QtySiVelocity> vec1;

vec1 = VectorC3D<QtySiVelocity> ( 23.5 * si::meter_per_second, 45.0 * si::meter_per_second, 63.0 * si::meter_per_second);
QtySiEnergy kinetic_energy = 1./2. * si::kilogram * vec1.normSquared();
cout << "Kinetic energy: " << kinetic_energy << endl;
QtySiLength height(3. * si::metre);
QtySiAcceleration g(9.8 * si::metre_per_second_squared);
QtySiEnergy gravitational_enegry = mass * height * g;
cout << "Gravitational energy: " << gravitational_enegry << endl;
QtySiVelocity c(299792458. * si::metre_per_second);
QtySiEnergy einstein_energy = mass * c * c;
cout << "Einstein’s energy: " << einstein_energy << endl;
```
---
[< back to main page](../README.md)