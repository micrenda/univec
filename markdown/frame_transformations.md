# Frame transformation
A transformation-matrix can represent any linear transformation: complex transformations can be easily represented and computed by chaining multiple simpler transformations such as translations, rotations, and scaling operations.

One can make a frame transformation using a concrete implementation of the abstract class BaseFrameC3D. The following table presents the ready-to-use implementations:
| Class name       | Description                                               |
| ---------------- | :-------------------------------------------------------- |
| `TranslateFrame` | Linear translation by a compatible vector                 |
| `RotateFrame2D`  | $2D$ Rotation (by angle or complex)                       |
| `RotateFrame3D`  | $3D$ Rotation (by Euler angles or quaternion)             |
| `ScaleFrame`     | Scaling by a scalar or by a different value for each axis |
| `CompositeFrame` | Wrap two transformation in a single one                   |

A vector can be easily transformed into a new reference frame:
```c++
VectorC3D<QtySiLength>        translation(0. * meter, 1. * meter, 0. * meter);
TranslateFrame<3,QtySiLength> frame(translation);
VectorC3D<QtySiLength>        globalPosition;                                // ( 0, 0, 0) m
VectorC3D<QtySiLength>        localPosition = frame.forward(globalPosition); // ( 0,-1, 0) m
localPosition += VectorC3D<QtySiLength>(1. * meter, 1. * meter, 1. * meter); // ( 1, 0, 1) m
globalPosition = frame.backward(localPosition);                              // ( 1, 1, 1) m
```

Multiple transformations can be combined to describe complex scenarios using the syntax:
```c++
VectorC3D<QtySiLength> v1; // (0,0,0) m
VectorC3D<QtySiLength> v2(0. * meter, -sqrt(2) * meter, sqrt(2) * meter);

VectorC3D<QtySiLength>       translation(0. * meter, 1. * meter, 0. * meter);
EulerRotation3D<QtySiLength> rotation(
        45. * degrees, 0. * degrees, 0. * degrees,
        RotationMode::INTRINSIC,
        RotationAxis::X, RotationAxis::Y, RotationAxis::Z);
QtySiDimensionless           scale(0.5);

auto frame = TranslateFrame<3,QtySiLength>(translation)
        >> RotateFrame3D<QtySiLength>(rotation)
        >> ScaleFrame<3,QtySiLength>(scale);

assert(frame.forward(v1).isNear(v2));
assert(frame.backward(v2).isNear(v1));
```

---
[< back to main page](../README.md)