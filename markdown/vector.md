
# Vector operations
`Univec` allows writing a compact and semantically clear code with dimensional-check at compile-time. This is done using a set of classes, shown in next table, representing different types of quantities, such as vectors and matrices.

| Name          | Description                              | Alias of     |
| ------------- | :--------------------------------------- | ------------ |
| `VectorC<N>`  | Generic $n$-dimensional Cartesian vector |              |
| `VectorC1D`   | $1D$ vectors VectorC<1>                  | VectorC<1>   |
| `VectorC2D`   | $2D$ vectors VectorC<2>                  | VectorC<2>   |
| `VectorC3D`   | $3D$ vectors VectorC<3>                  | VectorC<3>   |
| `VectorP2D`   | Vector $2D$ in polar coordinates         |              |
| `VectorS3D`   | Vector $3D$ in spherical coordinates     |              |
| `VectorY3D`   | Vector $3D$ in cylindrical coordinates   |              |
| `Complex`     | Complex quantity    VectorC<-2>          | VectorC<-2>  |
| `Quaternion`  | Quaternion quantity VectorC<-4>          | VectorC<-4>  |
| `Octonion`    | Octonion quantity   VectorC<-8>          | VectorC<-8>  |
| `Sedenion`    | Sedenion quantity   VectorC<-16>         | VectorC<-16> |
| `Matrix2`     | $2 \times 2$ square matrix Matrix<2,2>   | Matrix<2,2>  |
| `Matrix3`     | $3 \times 3$ square matrix Matrix<3,3>   | Matrix<3,3>  |
| `MatrixN<N>`  | $N \times N$ square matrix Matrix<N,N>   | Matrix<N,N>  |
| `Matrix<M,N>` | $M \times N$ generic matrix              |              |

List of functions implemented for each type of vector. Cross product is only implemented for 3𝐷 and 7𝐷 vectors, while the angle is implemented only for 2𝐷 and 3𝐷 vectors.

 | Method                     | Description                                                                                                     |
 | -------------------------- | :-------------------------------------------------------------------------------------------------------------- |
 | `v.isNull()`               | Check if the matrix has only zeros                                                                              |
 | `v.norm()`                 | Calculate the euclidean norm (L2-norm) of the vector                                                            |
 | `v.normL1()`               | Calculate the generic $L_p$ norm of the vector                                                                  |
 | `v.normL(p)`               | Calculate the euclidean squared norm of the vector                                                              |
 | `v.normLInf()`             | Calculate the L1 norm (Manhattan norm) of the vector                                                            |
 | `v.normSquared()`          | Create a unit vector with the same direction of the vector                                                      |
 | `v.dot()`                  | Dot product between two vectors                                                                                 |
 | `v.cross(u)`               | Cross product between two vectors                                                                               |
 | `v.elementwiseProduct(u)`  | Hadamard product between two vectors                                                                            |
 | `v.elementwiseDivision(u)` | Hadamard division between two vectir                                                                            |
 | `v.versor()`               | Create a unit dimensionless vector with the same direction of the vector                                        |
 | `v.isParallel(u)`          | Check if two vector are parallel. Two vector are parallel if they have the same direction or opposite direciton |
 | `v.isPerpendicular(u)`     | Check if two have perpendicular direction. If one vector is zero, then they do not have perpendicular direction |
 | `v.isSameDirection(u)`     | Check if two have same direction. If one vector is zero, then they do not have same direction                   |
 | `v.isOppositeDirection(u)` | Check if two have opposite direction. If one vector is zero, then they do not have opposite direction           |
 | `v.scale(u)`               | Get the ratio between the module of two parallel vectors.                                                       |


To consult the complete list of implemented functions, check the [documentation](https://micrenda.gitlab.io/univec/doc/index.html).

---
## Vector conversion

Another valuable feature of `Univec` is changing the coordinate system of a vector. The conversion from a type to another is made via a constructor, which is explicit when the conversion is computationally expensive (e.g. `VectorP2D` to `VectorC<2>`), and implicit otherwise (e.g. `VectorC<4>` to `Quaternion`).

### Example
These methods can be useful for executing operations on vectors represented in different coordinate systems. For example, for converting a `VectorP2D` into a `VectorC2D`, we can use the following code:

```c++
VectorP2D<QtySiLength> vec_a(5. * meters, 90. * degrees);

// Cartesian coordinates
VectorC2D<QtySiLength> vec_b(vec_a); // ( 0, 5) m
```

while for 3𝐷 vectors, we can use:

```c++
VectorC3D<QtySiLength> vec_a(3. * meters, 4. * meters, 12. * meters);

// Spherical coordinates
VectorS3D<QtySiLength> vec_b(vec_a); // ( 13 m, 22 deg, 53 deg)

// Cylindrical coordinates
VectorY3D<QtySiLength> vec_c(vec_a); // ( 5 m, 53 deg, 12 m)
```

and for complex and quaternions, we have:
```c++
Complex<QtySiFrequency> c(1. * hertz, 2. * hertz);
Quaternion<QtySiLength> q(1. * meters, 2. * meters, 3. * meters, 4. * meters);

// 2x2 matrix
Matrix<2,2,QtySiFrequency> m1 = c.matrix();

// 4x4 matrix
Matrix<4,4,QtySiLength> m2 = q.matrix();
```

---
[< back to main page](../README.md)
