# Complex, Quaternion, Octenion and Sedenions
Below is listed the list of functions implemented for complex, quaternion, octonion and sedenions. These methods came in addition to those presented for the vector operations:
| Operation                          | Method          | Description                                | Note            |
| ---------------------------------- | :-------------- | ------------------------------------------ | :-------------- |
| $q^*$                              | `q.conj()`      | Conjugate                                  |                 |
| $q^{-1}$                           | `q.inv()`       | Reciprocal                                 |                 |
| $0 + b  i +  c  j + d  k + \ldots$ | `q.pure()`      | Vector complex, quaternion, octonion, etc. |                 |
| $a + 0  i +  0  j + 0  k + \ldots$ | `q.scalar()`    | Scalar complex, quaternion, octonion, etc. |                 |
| $(b, c, d, \ldots )$               | `q.vector()`    | Associated $n-1$ vector                    |                 |
| $2 \arctan(q/a)$                   | `q.angle()`     | Rotation angle                             | qaternions only |
| $(b,c,d) / \sin(\theta/2)$         | `q.axis()`      | Rotation axis                              | qaternions only |
| $ q  v  q^{-1}$                    | `q.rotate(v)`   | Rotation of a vector                       |                 |
| $\mathcal{M}$                      | `q.matrix<n>()` | Matrix representation                      |                 |
| $a = 0$                            | `q.isPure()`    | Real part is zero                          |                 |
| $(b, c, d, \ldots ) = 0$           | `q.isScalar()`  | Unreal part is zero                        |                 |

## Opeartors for $n$-dimensional vector, quaternions, octonions, sedenions, and complex numbers

Operator & Operation & Note|

| Operator | Operation                                                                  | Note      |
| -------- | :------------------------------------------------------------------------- | :-------- |
| `u + v`  | $(u_1+v_1, u_2+v_2, \ldots, u_n+v_n)$                                      |           |
| `u - v`  | $(u_1-v_1, u_2-v_2, \ldots, u_n-v_n)$                                      |           |
| `u * v`  | (Hamilton multiplication)                                                  | $\dagger$ |
| `v * k`  | $(v_1\:k,v_2\:k, \ldots, v_n\:k)$                                          |           |
| `k * v`  | $(k\:v_1,k\:v_2, \ldots, k\:v_n)$                                          |           |
| `u / v`  | (Hamilton division)                                                        | $\dagger$ |
| `v / k`  | $(v_1/k, v_2/k, \ldots, v_n/k )$                                           |           |
| `u >  v` | $u_1^2 + u_2^2 + \ldots + u_n^2 > v_1^2 + v_2^2 + \ldots + v_n^2$          |           |
| `u >= v` | $u_1^2 + u_2^2 + \ldots + u_n^2 \ge v_1^2 + v_2^2 + \ldots + v_n^2$        |           |
| `u <= v` | $u_1^2 + u_2^2 + \ldots + u_n^2 \le v_1^2 + v_2^2 + \ldots + v_n^2$        |           |
| `u == v` | $u_1=v_1 \textrm{ and } u_2=v_2 \; \ldots \textrm{ and } u_n=v_n$          |           |
| `u != v` | $u_1 \ne v_1 \textrm{ or } u_2 \ne v_2 \;\ldots \textrm{ or } u_n \ne v_n$ |           |
| `u + v`  | $(u_1+v_1, u_2+v_2, \ldots, u_n+v_n)$                                      |           |
| `u - v`  | $(u_1-v_1, u_2-v_2, \ldots, u_n-v_n)$                                      |           |
| `u * v`  | (Hamilton multiplication)                                                  | $\dagger$ |
| `v * k`  | $(v_1\:k,v_2\:k, \ldots, v_n\:k)$                                          |           |
| `k * v`  | $(k\:v_1,k\:v_2, \ldots, k\:v_n)$                                          |           |
| `u / v`  | (Hamilton division)                                                        | $\dagger$ |
| `v / k`  | $(v_1/k, v_2/k, \ldots, v_n/k )$                                           |           |
| `u >  v` | $u_1^2 + u_2^2 + \ldots + u_n^2 > v_1^2 + v_2^2 + \ldots + v_n^2$          |           |
| `u <  v` | $u_1^2 + u_2^2 + \ldots + u_n^2 < v_1^2 + v_2^2 + \ldots + v_n^2$          |           |
| `u >= v` | $u_1^2 + u_2^2 + \ldots + u_n^2 \ge v_1^2 + v_2^2 + \ldots + v_n^2$        |           |
| `u <= v` | $u_1^2 + u_2^2 + \ldots + u_n^2 \le v_1^2 + v_2^2 + \ldots + v_n^2$        |           |
| `u == v` | $u_1=v_1 \textrm{ and } u_2=v_2 \; \ldots \textrm{ and } u_n=v_n$          |           |
| `u != v` | $u_1 \ne v_1 \textrm{ or } u_2 \ne v_2 \;\ldots \textrm{ or } u_n \ne v_n$ |           |

$\dagger$ - marks hamiltonian multiplication and division, operations available only for quaternions, octonions, sedenions, and complex numbers. Relational operators compare the module of the vectors at the first instance: if the module is equal, then the vector's elements are compared one by one to obtain a strong ordering.

---
[< back to main page](../README.md)