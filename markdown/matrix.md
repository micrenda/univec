# Matrix operations
List of functions implemented for the `Matrix<M,N>` class.
| Operation                                           | Method                    | Description                                | Note       |
| --------------------------------------------------- | :-----------------------  | ------------------------------------------ | :--------- |
| $\mathcal{A}_{m,n}$                                 | `a(m,n)`                  | Element access (1-based)                   |            |
| $\mathcal{A}_{i+1,j+1}$                             | `a[i,j]`                  | Element access (0-based)                   |            |
| $\mathcal{A}(i)$                                    | `a.rowMajor(i)`           | Element access row-major (0-based)         |            |
| $\mathcal{A}(i)$                                    | `a.colMajor(i)`           | Element access col-major (0-based)         |            |
| $m$                                                 | `a.rows`                  | Rows count                                 |            |
| $n$                                                 | `a.cols`                  | Columns count                              |            |
| $m=n$                                               | `a.square`                | Is square                                  |            |
| $o(\mathcal{A})$                                    | `a.order()`               | Order                                      | $\dagger$  |
| $O(\mathcal{A})$                                    | `a.orders()`              | Order in ($m \times n$ form)               |            |
| $\det(\mathcal{A})$                                 | `a.det()`                 | Determinant                                | $\dagger$  |
| $\text{tr}(\mathcal{A})$                            | `a.tr()`                  | Trace                                      | $\dagger$  |
| $\mathcal{A}^T$                                     | `a.t()`                   | Transpose                                  |            |
| $\mathcal{A} \leftarrow \mathcal{A}^T$              | `a.doT()`                 | In-place transpose                         | $\dagger$  |
| $\overline{A}$                                      | `a.conj()`                | Conjugate                                  |            |
| $\mathcal{A} \leftarrow \overline{\mathcal{A}}$     | `a.doConj()`              | In-place conjugate                         |            |
| $\mathcal{A}^H$                                     | `a.conjT()`               | Conjugate transpose                        |            |
| $\mathcal{A} \leftarrow \mathcal{A}^H$              | `a.doConjT()`             | In-place conjugate transpose               | $\dagger$  |
| $\mathcal{A}^{-1}$                                  | `a.inv()`                 | Inverse                                    | $\dagger$  |
| $\mathcal{A}^{+}$                                   | `a.pseudoInv()`           | Moore-Penrose inverse                      |            |
| $\mathcal{A}[m;n]$                                  | `a.sub(n,m)`              | Submatrix                                  |            |
| $\text{adj}(\mathcal{A})$                           | `a.adj()`                 | Ajugate                                    | $\dagger$  |
| $m_{m,n}$                                           | `a.minor(m,n)`            | Minor element                              | $\dagger$  |
| $M_{m,n}$                                           | `a.minors(m,n)`           | Minor matrix                               | $\dagger$  |
| $c_{m,n}$                                           | `a.cofactor(m,n)`         | Cofactor element                           | $\dagger$  |
| $C_{m,n}$                                           | `a.cofactors(m,n)`        | Cofactor matrix                            | $\dagger$  |
| $(\mathcal{A}_{1,1},\mathcal{A}_{2,2}, \ldots)$     | `a.diagonal()`            | Diagonal elements                          | $\dagger$  |
| $\langle \mathcal{A}, \mathcal{B} \rangle_F$        | `a.frobenius(b)`          | Frobenius inner product                    |            |
| $\mathcal{A} \otimes \mathcal{B}$                   | `a.kroneckerProduct(b)`   | Kronecker product                          |            |
| $\mathcal{A} \oplus \mathcal{B}$                    | `a.kroneckerSum(b)`       | Kronecker sum                              | $\dagger$  |
| $\mathcal{A} \oplus \mathcal{B}$                    | `a.directSum(b)`          | Direct sum                                 |            |
| $\mathcal{A} \circ \mathcal{B}$                     | `a.elementwiseProduct(b)` | Hadamard product                           |            |
| $\mathcal{A} \oshash \mathcal{B}$                   | `a.elementwiseDivision(b)`| Hadamard division                          |            |
| $\|\mathcal{A}\|$                                   | `a.norm()`                | Euclidean row or col norm                  | $\ddagger$ |
| $\|\mathcal{A}\|^2$                                 | `a.normSquared()`         | Euclidean row or col norm square           | $\ddagger$ |
| $\|\mathcal{A}\|_{1}$                               | `a.normL1()`              | $L_1$ norm                                 |            |
| $\|\mathcal{A}\|_{2}$                               | `a.normL2()`              | $L_2$ norm                                 |            |
| $\|\mathcal{A}\|_{max}$                             | `a.normLMax()`            | Max norm                                   |            |
| $\|\mathcal{A}\|_{\infty}$                          | `a.normLInf()`            | Infinity norm                              |            |
| $\|\mathcal{A}\|_{p,q}$                             | `a.normL(p,q)`            | $pq$ norm                                  |            |
| $\|\mathcal{A}\|_{F}$                               | `a.normF()`               | Frobenius norm                             |            |
| $\mathcal{A}_{m,n} = 0$                             | `a.isNull()`              | All elements are zero                      |            |
| $\mathcal{A}_{m,n} < \infty$                        | `a.isFinite()`            | All elements are finite                    |            |
| $\mathcal{A}_{m,n} = \pm\infty$                     | `a.isInfinite()`          | Any element is not finite                  |            |
| $\mathcal{A}_{m,n} \in \mathbb{R} \backslash\{0\}$  | `a.isNormal()`            | All elements are finite, real and non-zero |            |
| $\mathcal{A}_{m,n} \notin \mathbb{R}$               | `a.isNan()`               | Any element is a `nan`                     |            |
| $\mathcal{A} \approx \mathcal{B}$                   | `a.isNear(b)`             | Is approx equals                           |            |
| $\mathcal{A} = \mathcal{I}$                         | `a.isIdentity()`          | Is identity                                | $\dagger$  |
| $\det(\mathcal{A}) = 0$                             | `a.isSingular()`          | Is singular                                | $\dagger$  |
| $\mathcal{A}_{m \neq n} = 0$                        | `a.isDiagonal()`          | Is diagonal                                | $\dagger$  |
| $\mathcal{A}_{m,n} = \mathcal{A}_{n,m}$             | `a.isSymmetric()`         | Is symmetric                               | $\dagger$  |
| $\mathcal{A}_{m,n} = -\mathcal{A}_{n,m}$            | `a.isSkewSymmetric()`     | Is skew-symmetric                          | $\dagger$  |
| $\mathcal{A}_{m,n} =  \overline{\mathcal{A}}_{n,m}$ | `a.isHermitian()`         | Is Hermitian                               | $\dagger$  |
| $\mathcal{A}_{m,n} = -\overline{\mathcal{A}}_{n,m}$ | `a.isSkewHermitian()`     | Is skew-hermitian                          | $\dagger$  |
| $\mathcal{A} A^T = \mathcal{I}$                     | `a.isOrthogonal()`        | Is orthogonal                              | $\dagger$  |
| $\mathcal{A}^H = \mathcal{A}^{-1}$                  | `a.isUnitary()`           | Is unitary                                 | $\dagger$  |
| $\mathcal{A}_{m > n} = 0$                           | `a.isUpperTriangular()`   | Is upper-triangular                        | $\dagger$  |
| $\mathcal{A}_{m < n} = 0$                           | `a.isLowerTriangular()`   | Is lower-triangular                        | $\dagger$  |

The $\dagger$ symbol marks operations available for square matrix only, while $\ddagger$ symbols is available for row or col matrix only.


List of functions implemented for the `Matrix<M,N>` class.
| Operator                     | Operation | Note                         |
| ---------------------------- | :-------- | :--------------------------- |
| $\mathcal{A} + \mathcal{B}$  | `a + b`   | Sum                          |
| $\mathcal{A} - \mathcal{B}$  | `A - B`   | Subtraction                  |
| $\mathcal{A} \; \mathcal{B}$ | `a * b`   | Multiplication               |
| $\mathcal{A} \; c$           | `a * c`   | Multiplication with a scalar |
| $c \; \mathcal{A}$           | `c * a`   | Multiplication with a scalar |
| $\mathcal{A} \; \textbf{v}$  | `a * v`   | Multiplication with a vector |
| $\mathcal{A} / c$            | `a / c`   | Division by a scalar         |


---
[< back to main page](../README.md)
