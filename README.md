# Univec
`Univec` - a dimensional-aware vector library

When developing software, it's common to use libraries that provide useful functionalities to save time and effort.
Dimensional analysis is one of the most powerful tools available in physics to verify the correctness of mathematical formulas describing physical processes. `Univec` is a C++ library that offers dimension-aware scalar, vector, matrix, complex, quaternion, octonions, and sedenions operations with a compile-time check of dimensional correctness. `Univec` is performing these operations by integrating [`Boost.Units`](https://www.boost.org/doc/libs/1_80_0/doc/html/boost_units.html), one of the most popular solutions for C++.

Helpful details are presented in the following subpages, with methods, classes, and examples for each of them.

1. [Installation](markdown/installation.md)
2. [Vector operations](markdown/vector.md)
3. [Complex, Quaternion, Octenion and Sedenions](markdown/complex_quaternion_octonions_sedenions.md)
4. [Matrix](markdown/matrix.md)
5. [Frame transformation](markdown/frame_transformations.md)


The complete code documentation can be found [here](https://micrenda.gitlab.io/univec/doc/index.html)







