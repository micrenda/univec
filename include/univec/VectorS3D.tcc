#include <boost/units/cmath.hpp>
#include <boost/units/is_dimensionless.hpp>

namespace dfpe
{
#ifndef GCC
    template<typename T, typename A>
    constexpr VectorS3D<T, A>::VectorS3D() { }
#else
    #ifdef ENABLE_GCC_PROPERTY
        template<typename T, typename A>
        constexpr VectorS3D<T, A>::VectorS3D() : r(this), theta(this), phi(this) { }
    #else
        template<typename T, typename A>
        constexpr VectorS3D<T, A>::VectorS3D() { }
    #endif
#endif

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorS3D<T, A>::VectorS3D(const VectorS3D<U, B> &other): VectorS3D()
    {
        m_r     = T(other.m_r);
        m_theta = A(other.m_theta);
        m_phi   = A(other.m_phi);
    }

    template<typename T, typename A>
    template<typename U, typename B, typename C>
    constexpr VectorS3D<T, A>::VectorS3D(const U &r, const B &theta, const C &phi): VectorS3D()
    {
        m_r     = T(r);
        m_theta = A(theta);
        m_phi   = A(phi);

        if (!isNormalized())
            doNormalize();
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorS3D<T, A> &VectorS3D<T, A>::operator+=(const VectorS3D<U, B> &right)
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        VectorS3D<T, A> value(VectorC<3,T,A>(*this) + VectorC<3,U,B>(right));

        m_r     = value.m_r;
        m_theta = value.m_theta;
        m_phi   = value.m_phi;

        return *this;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorS3D<T, A> &VectorS3D<T, A>::operator-=(const VectorS3D<U, B> &right)
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        VectorS3D<T, A> value(VectorC<3,T,A>(*this) - VectorC<3,U,B>(right));

        m_r     = value.m_r;
        m_theta = value.m_theta;
        m_phi   = value.m_phi;

        return *this;
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorS3D<T, A> &VectorS3D<T, A>::operator*=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        m_r *= right;
        return *this;
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorS3D<T, A> &VectorS3D<T, A>::operator/=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        m_r /= right;
        return *this;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorS3D<ADD(T,U), A> VectorS3D<T, A>::operator+(const VectorS3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return VectorS3D<T,A>(VectorC<3,T,A>(*this) + VectorC<3,U,B>(right));
    }


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorS3D<SUB(T,U), A> VectorS3D<T, A>::operator-(const VectorS3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return VectorS3D<T,A>(VectorC<3,T,A>(*this) - VectorC<3,U,B>(right));
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorS3D<MUL(T,U), A> VectorS3D<T, A>::operator*(const U &right) const
    {
        typedef MUL(T,U) type;
        return VectorS3D<type, A>(m_r * right, m_theta, m_phi);

    }

    template<typename T, typename U, typename B>
    constexpr VectorS3D<MUL(T,U), B> operator*(const T &left, const VectorS3D<U, B> &right)
    {
        typedef MUL(T,U) type;
        return VectorS3D<type, B>(left * right.m_r, right.m_theta, right.m_phi);
    }


    template<typename T, typename A>
    template<typename U>
    constexpr VectorS3D<DIV(T,U), A> VectorS3D<T, A>::operator/(const U &right) const
    {
        typedef DIV(T,U) type;
        return VectorS3D<type, A>(m_r / right, m_theta, m_phi);
    }

    template<typename T, typename A>
    constexpr VectorS3D<UADD(T), A> VectorS3D<T, A>::operator+() const
    {
        typedef UADD(T) type;
        return VectorS3D<type, A>(*this);
    }

    template<typename T, typename A>
    constexpr VectorS3D<USUB(T), A> VectorS3D<T, A>::operator-() const
    {
        typedef USUB(T) type;
        return VectorS3D<type, A>(-m_r, m_theta, m_phi);
    }



    template<typename T, typename A>
    constexpr inline T VectorS3D<T, A>::norm() const
    {
        return m_r;
    }

    template<typename T, typename A>
    constexpr inline POW(T,2) VectorS3D<T, A>::normSquared() const
    {
        return m_r * m_r;
    }


    template<typename T, typename A>
    constexpr VectorS3D<T, A> VectorS3D<T, A>::versor() const
    {
        return VectorS3D<T, A>(T::from_value(1.), m_theta, m_phi);
    }

    template<typename T, typename A>
    constexpr void VectorS3D<T, A>::doVersor()
    {
       m_r = T::from_value(1.);
    }

    template<typename T, typename A>
    constexpr VectorS3D<DL(T), A> VectorS3D<T, A>::versorDl() const
    {
        return VectorS3D<DL(T) , A>(1., m_theta, m_phi);
    }


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr MUL(T,U) VectorS3D<T, A>::dot(const VectorS3D<U, B> &other) const
    {
        check_type_compatibility<A,B>();

        return m_r * other.m_r * (
                boost::units::cos(m_theta) * boost::units::cos(other.m_theta) +
                boost::units::sin(m_theta) * boost::units::sin(other.m_theta) * boost::units::cos(m_phi - other.m_phi));
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorS3D<MUL(T,U), A> VectorS3D<T, A>::cross(const VectorS3D<U, B> &other) const
    {
        check_type_compatibility<A,B>();

        return VectorS3D<MUL(T,U), A>(VectorC<3,T,A>( *this ).cross( VectorC<3,U,B>( other )));
    }


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr MUL(T,U) VectorS3D<T, A>::operator|(const VectorS3D<U, B> &right) const
    {
        check_type_compatibility<A,B>();

        return dot(right);
    }


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorS3D<MUL(T,U), A> VectorS3D<T, A>::operator^(const VectorS3D<U, B> &right) const
    {
        check_type_compatibility<A,B>();

        return cross(right);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr A VectorS3D<T, A>::angle(const VectorS3D<U, B> &right) const
    {
        check_type_compatibility<A,B>();

        return *this == right ? A() : (A)(boost::units::acos(dot(right) / (m_r * right.m_r)));
    }

    template<typename T, typename A>
    template<typename TOL>
    constexpr bool VectorS3D<T, A>::isNull(const TOL& tol) const
    {
        return tol.isZeroQ(m_r);
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorS3D<T, A>::isNear(const VectorS3D<U, B> &other, const TOL& tol) const
    {
        check_type_compatibility<T,U>();

        constexpr A qtyFullPi (M_PI * boost::units::si::radians);

        if (tol.isZeroQ(m_r) && tol.isZeroQ(other.m_r))
            return true;

        if ((tol.isZeroA(m_theta) || tol.isNearA(m_theta, qtyFullPi)) &&
            (tol.isZeroA(other.m_theta) || tol.isNearA(other.m_theta, qtyFullPi)))
            return tol.isNearQ(m_r, other.m_r);

        return tol.isNearQ(m_r, other.m_r) && tol.isNearA(m_phi, other.m_phi) && tol.isNearA(m_theta, other.m_theta);
    }


    template<typename T, typename A>
    constexpr bool VectorS3D<T, A>::isFinite() const
    {
        return isfinite(m_r) && isfinite(m_theta) && isfinite(m_phi);
    }

    template<typename T, typename A>
    constexpr bool VectorS3D<T, A>::isNormal() const
    {
        return isnormal(m_r) && isnormal(m_theta) && isnormal(m_phi);
    }

    template<typename T, typename A>
    constexpr bool VectorS3D<T, A>::isNan() const
    {
        return isnan(m_r) || isnan(m_theta) || isnanl(m_phi);
    }

    template<typename T, typename A>
    constexpr bool VectorS3D<T, A>::isInfinite() const
    {
        return isinf(m_r) || isinf(m_theta) || isinf(m_phi);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr bool VectorS3D<T, A>::operator==(const VectorS3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return m_r == right.m_r && m_theta == right.m_theta && m_phi == right.m_phi;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr bool VectorS3D<T, A>::operator!=(const VectorS3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return m_r != right.m_r || m_theta != right.m_theta || m_phi != right.m_phi;
    }

    template<typename T, typename A>
    constexpr void VectorS3D<T, A>::doNormalize()
    {
        if (!isfinite(m_r) || !isfinite(m_theta) || !isfinite(m_phi))
            return;

        constexpr A qtyFullPi  (M_PI * boost::units::si::radians);
        constexpr A qtyTwicePi (2 * M_PI * boost::units::si::radians);

        if (m_r < T()) // Normalize r to [0,∞]
        {
            m_r = -m_r;
            m_theta += qtyFullPi;
        }


        m_theta -= qtyTwicePi * boost::units::floor(m_theta / qtyTwicePi);    // Normalize theta to [0,2π]

        if (m_theta > qtyFullPi) // Normalize theta to [0,π]
        {
            m_theta = qtyTwicePi - m_theta;
            m_phi   += qtyFullPi;
        }

        m_phi -= qtyTwicePi * boost::units::floor(m_phi / qtyTwicePi);    // Normalize phi to [0,2π]

        if (m_r == T())
        {
            m_theta = A();
        }

        if (m_theta == A() || m_theta == qtyFullPi)
        {
            m_phi = A();
        }

        assert(isNormalized());
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr std::optional<DL(T)>
    VectorS3D<T, A>::scale(const VectorS3D<U, B> &other, const TOL& tol) const
    {
        const A qtyFullPi = static_cast<A>(boost::units::quantity<boost::units::si::plane_angle>(M_PI   * boost::units::si::radians));

        if (!tol.isZeroQ(m_r) && !tol.isZeroQ(other.m_r))
        {
            auto funIsZeroOrPi = [&](const A& v) { return tol.isZeroA(v) || tol.isNearA(v,qtyFullPi); };

            if (tol.isNearA(m_theta, other.m_theta))
            {
                if (funIsZeroOrPi(m_theta) || funIsZeroOrPi(other.m_theta))
                    return other.m_r / m_r;
                if ( tol.isNearA(m_phi, other.m_phi) )
                    return other.m_r / m_r;
                if ( tol.isNearA(abs(m_phi - other.m_phi), qtyFullPi) )
                    return -other.m_r / m_r;
            }
            if (tol.isNearA(abs(m_theta - other.m_theta), qtyFullPi))
            {
                if (tol.isNearA(abs(m_phi - other.m_phi), qtyFullPi) || funIsZeroOrPi(m_theta) || funIsZeroOrPi(other.m_theta))
                    return -other.m_r / m_r;
            }
        }
        return std::optional<DL(T)>();
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorS3D<T, A>::isParallel(const VectorS3D<U, B> &other, const TOL& tol) const
    {
        auto ratio = scale(other, tol);
        return ratio.has_value();
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorS3D<T, A>::isSameDirection(const VectorS3D<U, B> &other, const TOL& tol) const
    {
        auto ratio = scale(other, tol);
        return ratio.has_value() && *ratio > 0.;
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorS3D<T, A>::isOppositeDirection(const VectorS3D<U, B> &other, const TOL& tol) const
    {
        auto ratio = scale(other, tol);
        return ratio.has_value() && *ratio < 0.;
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorS3D<T, A>::isPerpendicular(const VectorS3D<U, B> &other, const TOL& tol) const
    {
        return !isNull(tol) && !other.isNull(tol) && (tol * tol).isZeroQ(dot(other));
    }


    template<typename T, typename A>
    constexpr inline bool VectorS3D<T, A>::isNormalized() const
    {
        if (!isfinite(m_r) || !isfinite(m_theta) || !isfinite(m_phi))
            return true;

        constexpr A qtyFullPi  (M_PI * boost::units::si::radians);
        constexpr A qtyTwicePi (2. * M_PI * boost::units::si::radians);

        if (m_r == T())
            return m_theta == A() && m_phi == A();
        else if (m_theta == A() || m_theta == qtyFullPi)
            return m_r >= T() && m_phi == A();
        else
            return m_r >= T() && m_theta >= A() && m_theta <= qtyFullPi && m_phi >= A() && m_phi < qtyTwicePi;
    }


    /* Comparative operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<typename T, typename A>
    template<typename U, typename B>
    constexpr std::weak_ordering VectorS3D<T, A>::operator<=>(const VectorS3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        assert(isNormalized());
        assert(right.isNormalized());

        if (m_r < right.m_r)
            return std::weak_ordering::less;
        if (m_r > right.m_r)
            return std::weak_ordering::greater;
        if (m_theta < right.m_theta)
            return std::weak_ordering::less;
        if (m_theta > right.m_theta)
            return std::weak_ordering::greater;
        if (m_phi < right.m_phi)
            return std::weak_ordering::less;
        if (m_phi > right.m_phi)
            return std::weak_ordering::greater;

        return std::weak_ordering::equivalent;
    }

}