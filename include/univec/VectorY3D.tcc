#include <boost/units/cmath.hpp>
#include <boost/units/is_dimensionless.hpp>

namespace dfpe
{
#ifndef GCC
    template<typename T, typename A>
    constexpr VectorY3D<T, A>::VectorY3D() { }
#else
    #ifdef ENABLE_GCC_PROPERTY
        template<typename T, typename A>
        constexpr VectorY3D<T, A>::VectorY3D() : rho(this), phi(this), z(this) { }
    #else
        template<typename T, typename A>
        constexpr VectorY3D<T, A>::VectorY3D() { }
    #endif
#endif


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorY3D<T, A>::VectorY3D(const VectorY3D<U, B> &other): VectorY3D()
    {
        //check_type_compatibility<T,U>();
        //check_type_compatibility<A,B>();

        m_rho = T(other.m_rho);
        m_phi = A(other.m_phi);
        m_z   = T(other.m_z);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorY3D<T, A>::VectorY3D(const U &rho, const B &phi, const T &z): VectorY3D()
    {
        //check_type_compatibility<T,U>();
        //check_type_compatibility<A,B>();

        m_rho = T(rho);
        m_phi = A(phi);
        m_z   = T(z);

        if (!isNormalized())
            doNormalize();
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorY3D<T, A> &VectorY3D<T, A>::operator+=(const VectorY3D<U, B> &right)
    {
        //check_type_compatibility<T,U>();
        //check_type_compatibility<A,B>();

        *this = VectorY3D<T, A>(VectorC<3,T,A>(*this) + VectorC<3,U,B>(right));
        return *this;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorY3D<T, A> &VectorY3D<T, A>::operator-=(const VectorY3D<U, B> &right)
    {
        //check_type_compatibility<T,U>();
        //check_type_compatibility<A,B>();

        *this = VectorY3D<T, A>(VectorC<3,T,A>(*this) - VectorC<3,U,B>(right));
        return *this;
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorY3D<T, A> &VectorY3D<T, A>::operator*=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        m_rho *= right;
        m_z *= right;
        return *this;
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorY3D<T, A> &VectorY3D<T, A>::operator/=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        m_rho /= right;
        m_z /= right;
        return *this;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorY3D<ADD(T,U), A>  VectorY3D<T, A>::operator+(const VectorY3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return VectorY3D<ADD(T,U), A>(VectorC<3,T,A>(*this) + VectorC<3,U,B>(right));
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorY3D<SUB(T,U), A>  VectorY3D<T, A>::operator-(const VectorY3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return VectorY3D<ADD(T,U), A>(VectorC<3,T,A>(*this) - VectorC<3,U,B>(right));
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorY3D<MUL(T,U), A>  VectorY3D<T, A>::operator*(const U &right) const
    {
        typedef MUL(T,U) type;
        return VectorY3D<type, A>(m_rho * right, m_phi, m_z * right);

    }

    template<typename T, typename U, typename B>
    constexpr VectorY3D<MUL(T,U), B> operator*(const T &left, const VectorY3D<U, B> &right)
    {
        typedef MUL(T,U) type;
        return VectorY3D<type, B>(left * right.m_rho, right.m_phi, left * right.m_z);
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorY3D<DIV(T,U), A>  VectorY3D<T, A>::operator/(const U &right) const
    {
        typedef DIV(T,U) type;
        return VectorY3D<type, A>(m_rho / right, m_phi, m_z / right);
    }

    template<typename T, typename A>
    constexpr VectorY3D<UADD(T), A>  VectorY3D<T, A>::operator+() const
    {
        typedef UADD(T) type;
        return VectorY3D<type, A>(*this);
    }

    template<typename T, typename A>
    constexpr VectorY3D<USUB(T), A>  VectorY3D<T, A>::operator-() const
    {
        typedef USUB(T) type;
        return VectorY3D<type, A>(-m_rho, m_phi, -m_z);
    }

    template<typename T, typename A>
    constexpr T VectorY3D<T, A>::norm() const
    {
        return boost::units::hypot(m_rho, m_z);
    }

    template<typename T, typename A>
    constexpr POW(T,2) VectorY3D<T, A>::normSquared() const
    {
        return m_rho * m_rho + m_z * m_z;
    }


    template<typename T, typename A>
    constexpr VectorY3D<T, A> VectorY3D<T, A>::versor() const
    {
        VectorY3D<T, A> result = *this;
        result.doVersor();
        return result;
    }

    template<typename T, typename A>
    constexpr void VectorY3D<T, A>::doVersor()
    {
        boost::units::quantity<boost::units::si::dimensionless> scale(VectorY3D<T, A>::norm().value());
        m_rho /= scale;
        m_z /= scale;
    }

    template<typename T, typename A>
    constexpr VectorY3D<boost::units::quantity<boost::units::si::dimensionless>, A> VectorY3D<T, A>::versorDl() const
    {
        //TODO: find a better way to handle the next instruction. This implementation is linked to SI units.
        //      find a general way to do this
        T scale(VectorY3D<T, A>::norm());
        return *this / scale;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr MUL(T,U) VectorY3D<T, A>::dot(const VectorY3D<U, B> &other) const
    {
        check_type_compatibility<A,B>();

        typedef MUL(T,U) type;
        return (type)(m_rho * other.m_rho * boost::units::cos(m_phi - other.m_phi)) + m_z * other.m_z;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorY3D<MUL(T,U), A> VectorY3D<T, A>::cross(const VectorY3D<U, B> &other) const
    {
        check_type_compatibility<A,B>();
        // Is there a better way to do this?
        return VectorY3D<MUL(T,U), A>(VectorC<3,T,A>(*this).cross(VectorC<3,U,B>(other)));
    }


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr MUL(T,U)  VectorY3D<T, A>::operator|(const VectorY3D<U, B> &right) const
    {
        check_type_compatibility<A,B>();

        return dot(right);
    }


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorY3D<MUL(T,U), A>  VectorY3D<T, A>::operator^(const VectorY3D<U, B> &right) const
    {
        check_type_compatibility<A,B>();

        return cross(right);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr A VectorY3D<T, A>::angle(const VectorY3D<U, B> &right) const
    {
        check_type_compatibility<A,B>();

        return *this == right ? A() : A(boost::units::acos( dot(right) / (norm() * right.norm())));
    }

    template<typename T, typename A>
    template<typename TOL>
    constexpr bool VectorY3D<T, A>::isNull(const TOL& tol) const
    {
        return tol.isZeroQ(m_rho) && tol.isZeroQ(m_z);
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorY3D<T, A>::isNear(const VectorY3D<U, B> &other, const TOL& tol) const
    {
        check_type_compatibility<T,U>();

        return tol.isNearQ(m_rho, other.m_rho) && tol.isNearA(m_phi, other.m_phi) && tol.isNearQ(m_z, other.m_z);
    }

    template<typename T, typename A>
    constexpr bool VectorY3D<T, A>::isFinite() const
    {
        return isfinite(m_rho) && isfinite(m_phi) && isfinite(m_z);
    }

    template<typename T, typename A>
    constexpr bool VectorY3D<T, A>::isNormal() const
    {
        return isnormal(m_rho) && isnormal(m_phi) && isnormal(m_z);
    }

    template<typename T, typename A>
    constexpr bool VectorY3D<T, A>::isNan() const
    {
        return isnan(m_rho) || isnan(m_phi) || isnan(m_z);
    }

    template<typename T, typename A>
    constexpr bool VectorY3D<T, A>::isInfinite() const
    {
        return isinf(m_rho) || isinf(m_phi) || isinf(m_z);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr bool VectorY3D<T, A>::operator==(const VectorY3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return m_rho == right.m_rho && m_phi == right.m_phi && m_z == right.m_z;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr bool VectorY3D<T, A>::operator!=(const VectorY3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return m_rho != right.m_rho || m_phi != right.m_phi || m_z != right.m_z;
    }

    template<typename T, typename A>
    constexpr void VectorY3D<T, A>::doNormalize()
    {
        constexpr A qtyFullPi  = A(boost::units::quantity<boost::units::si::plane_angle>(M_PI * boost::units::si::radians));
        constexpr A qtyTwicePi = A(boost::units::quantity<boost::units::si::plane_angle>(2 * M_PI * boost::units::si::radians));

        // Normalize rho to [0,∞]
        if (m_rho < T())
        {
            m_rho = -m_rho;
            m_phi += qtyFullPi;
        }

        // Normalize phi to [0,2π]
        if (isfinite(m_phi))
            m_phi -= qtyTwicePi * boost::units::floor(m_phi / qtyTwicePi);

        // If rho is zero, doNormalize angle to 0
        if (m_rho == T())
        {
            m_phi = A();
        }

        assert(isNormalized());
    }



    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr std::optional<DL(T)>
    VectorY3D<T, A>::scale(const VectorY3D<U, B> &other, const TOL& tol) const
    {
        const A qtyFullPi = static_cast<A>(boost::units::quantity<boost::units::si::plane_angle>(M_PI   * boost::units::si::radians));

        if (!tol.isZeroQ(m_rho) && !tol.isZeroQ(m_z) && !tol.isZeroQ(other.m_r) && !tol.isZeroQ(other.m_z))
        {
            auto funIsZeroOrPi = [&](const A& v) { return tol.isZeroA(v) || tol.isNearA(v,qtyFullPi); };

            std::optional<DIV(T,U)> ratio;

            DL(T) inversion;
            
            if (tol.isNearA(m_phi, other.m_phi))
                inversion = 1.;
            else if (tol.isNearA(abs(m_phi - other.m_phi), qtyFullPi))
                inversion = -1;
            
            if (inversion == 0.)
                return std::optional<DIV(T,U)>();
                
            if (!tol.isZeroQ(m_rho) && !tol.isZeroQ(other.m_rho))
            {
                if (!ratio.has_value())
                    ratio = std::make_optional(other.m_rho / m_rho);
                else if ( *ratio != other.m_rho / m_rho )
                    return std::make_optional<DIV(T,U)>();
            }
            else if (tol.isZeroQ(m_rho) && tol.isZeroQ(other.m_v))
            {
                // Do nothing
            }
            else
            {
                return std::optional<DIV(T,U)>();
            }
            
            if (!tol.isZeroQ(m_z) && !tol.isZeroQ(other.m_z))
            {
                if (!ratio.has_value())
                    ratio = std::make_optional(other.m_z / m_z);
                else if ( *ratio != other.m_z / m_z )
                    return std::make_optional<DIV(T,U)>();
            }
            else if (tol.isZeroQ(m_z) && tol.isZeroQ(other.m_v))
            {
                // Do nothing
            }
            else
            {
                return std::optional<DIV(T,U)>();
            }
            

        }
        return std::optional<DL(T)>();
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorY3D<T, A>::isParallel(const VectorY3D<U, B> &other, const TOL& tol) const
    {
        auto ratio = scale(other, tol);
        return ratio.has_value();
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorY3D<T, A>::isSameDirection(const VectorY3D<U, B> &other, const TOL& tol) const
    {
        auto ratio = getScale(other, tol);
        return ratio.has_value() && *ratio > 0.;
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorY3D<T, A>::isOppositeDirection(const VectorY3D<U, B> &other, const TOL& tol) const
    {
        auto ratio = getScale(other, tol);
        return ratio.has_value() && *ratio < 0.;
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorY3D<T, A>::isPerpendicular(const VectorY3D<U, B> &other, const TOL& tol) const
    {
        return !isNull(tol) && !other.isNull(tol) && (tol * tol).isZeroQ(dot(other));
    }



    template<typename T, typename A>
    constexpr inline bool VectorY3D<T, A>::isNormalized() const
    {
        if (!isfinite(m_rho) || !isfinite(m_phi) || !isfinite(m_z))
            return true;

        constexpr A qtyTwicePi = A(2. * M_PI * boost::units::si::radians);

        if (m_rho == T())
            return m_phi == A();

        return m_rho >= T() && m_phi >= A() && m_phi < qtyTwicePi;
    }


    /* Comparative operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<typename T, typename A>
    template<typename U, typename B>
    constexpr std::weak_ordering VectorY3D<T, A>::operator<=>(const VectorY3D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        assert(isNormalized());
        assert(right.isNormalized());

        auto norm2 = normSquared();
        auto otherNorm2 = right.normSquared();

        if ( norm2 < otherNorm2 )
            return std::weak_ordering::less;
        if (norm2 > otherNorm2)
            return std::weak_ordering::greater;

        if (m_z < right.m_z)
            return std::weak_ordering::less;
        if (m_z > right.m_z)
            return std::weak_ordering::greater;

        if (m_phi < right.m_phi)
            return std::weak_ordering::less;
        if (m_phi > right.m_phi)
            return std::weak_ordering::greater;

        return std::weak_ordering::equivalent;
    }

}