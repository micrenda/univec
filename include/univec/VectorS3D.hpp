#pragma once

#include <ostream>
#include <boost/units/quantity.hpp>
#include <boost/units/is_dimensionless.hpp>
#include <boost/units/io.hpp>
#include "VectorY3D.hpp"
#include "VectorC.hpp"

namespace dfpe
{
    template<typename T, typename A>
    class VectorY3D;

    /**
     * A 3D vector in a spherical coordinate system using physics convention (ISO 80000-2:2019)
     * @tparam T Type of the quantity
     * @tparam A Type of the angle
     */
    template<typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    class VectorS3D
    {
        template <int N, typename U, typename B>
        friend class VectorC;

        template <typename U, typename B>
        friend class VectorY3D;

    public:
        /// Create a null vector
        constexpr VectorS3D();

        /// Create a vector from another vector
        template<typename U, typename B>
        constexpr VectorS3D(const VectorS3D<U, B> &other);

        /// Create a vector from radius \f$r\f$, angle \f$theta\f$ and angle \f$phi\f$
        template<typename U, typename B, typename C>
        constexpr VectorS3D(const U &r, const B &theta, const C &phi);

        /**
         * \brief Create a vector from a 3D cartesian vector
         * It is an expensive operation
         */
        template<int N, typename U, typename B>
        explicit constexpr VectorS3D<T, A>(const VectorC<N, U, B>& other) requires (N == 3) : VectorS3D()
        {
            T other_norm = other.norm();

            m_r     = T(other_norm);
            m_theta = A(boost::units::acos(other[2] / other_norm));
            m_phi   = A(boost::units::atan2(other[1], other[0]));

            if (!isNormalized())
                doNormalize();
        }

        /**
         * \brief Create a vector from a 3D cylindrical vector
         * It is an expensive operation
         */
        template<typename U, typename B>
        explicit constexpr VectorS3D<T, A>(const VectorY3D<U,B>& other): VectorS3D()
        {
            m_r     = T(other.norm());
            m_theta = A(boost::units::atan2(other.m_rho, other.m_z));
            m_phi   = A(other.m_phi);

            if (!isNormalized())
                doNormalize();
        }

    protected:
        T m_r; ///< radius r value
        A m_theta; ///< angle theta value
        A m_phi; ///< angle phi value

    public:


        inline constexpr const T&     getR() const     { return m_r; } ///< @private
        inline constexpr void         setR(const T& value) ///< @private
        {
            m_r = value;
            if (!isNormalized())
            doNormalize();
        }


        inline constexpr const A&     getTheta() const     { return m_theta; } ///< @private
        inline constexpr void  setTheta(const A& value)     ///< @private
        {
            m_theta = value;
            if (!isNormalized())
                doNormalize();
        }

        inline constexpr const A&     getPhi() const     { return m_phi; } ///< @private
        inline constexpr void  setPhi(const A& value)     ///< @private
        {
            m_phi = value;
            if (!isNormalized())
                doNormalize();
        }


#ifndef GCC
        __declspec(property(get=getR, put=setR))         T r;     ///< @private
        __declspec(property(get=getTheta, put=setTheta)) A theta; ///< @private
        __declspec(property(get=getPhi, put=setPhi))     A phi;   ///< @private
#else
    #if ENABLE_GCC_PROPERTY
        Property<T,VectorS3D,&VectorS3D::getR,&VectorS3D::setR> r;             ///< @private
        Property<A,VectorS3D,&VectorS3D::getTheta,&VectorS3D::setTheta> theta; ///< @private
        Property<A,VectorS3D,&VectorS3D::getPhi,&VectorS3D::setPhi> phi;       ///< @private
    #endif
#endif


    public:
        /**
	     * \brief Calculate the euclidean norm of the vector
	     */
        constexpr inline T norm() const;

        /**
         * \brief Calculate the euclidean squared norm of the vector
         */
        constexpr POW(T,2) normSquared() const;

        /**
         * \brief Create a unit vector with the same direction of the vector
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr VectorS3D<T, A> versor() const;

        /**
         * \brief Scale a vector to an unit vector
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr void doVersor();

        /**
         * \brief Create a unit vector with the same direction of the vector
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr VectorS3D<DL(T), A> versorDl() const;

        /**
         * \brief Dot product between two vectors
         */
        template<typename U, typename B>
        constexpr MUL(T,U) dot(const VectorS3D<U, B> &other) const;

        /**
         * \brief Cross product between two vectors
         */
        template<typename U, typename B>
        constexpr VectorS3D<MUL(T,U), A> cross(const VectorS3D<U, B> &other) const;

        /**
		 * \brief Smallest angle between two vectors
		 * @param other The other vector
		 * @return The angle between two vectors
		 */
        template<typename U, typename B>
        constexpr A angle(const VectorS3D<U, B> &other) const;

        /**
        * Check if the vector has zero length
        * @return true is null, false otherwise
        */
        template<typename TOL = Tolerance<T,A>>
        constexpr bool isNull(const TOL& tol = TOL()) const;


        /**
        * Check if the vector are near with each other
        * @return true is null, false otherwise
        */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isNear(const VectorS3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
     * Check if all vector element are finite
     * @return true is all elements are finite, false otherwise
     */
        constexpr bool isFinite() const;

        /**
         * Check if all vector element are normal
         * @return true is all elements are normal, false otherwise
         */
        constexpr bool isNormal() const;



        /**
         * Check if any vector element is nan
         * @return true is any element is nan, false otherwise
         */
        constexpr bool isNan() const;

        /**
         * Check if any vector element is inf
         * @return true is any element is inf, false otherwise
         */
        constexpr bool isInfinite() const;


        /**
         * \brief Check if two vector are parallel
         * Two vector are parallel if they have the same direction or opposite direciton
         * If one vector is zero, then they are not parallel
         * @return true is are parallel, false otherwise
         */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isParallel(const VectorS3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
        * \brief Check if two have same direction
        * If one vector is zero, then they do not have same direction
        * @return true is they have same direction, false otherwise
        */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isSameDirection(const VectorS3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
        * \brief Check if two have opposite direction
        * If one vector is zero, then they do not have opposite direction
        * @return true is they have opposite direction, false otherwise
        */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isOppositeDirection(const VectorS3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
         * \brief Check if two have perpendicular direction
         * If one vector is zero, then they do not have perpendicular direction
         * @return true is they have perpendicular direction, false otherwise
         */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isPerpendicular(const VectorS3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
         *
         * \brief Get the ratio between the module of two parallel vectors.
         * If the vectors are not parallel or any of them is null, an empty optional value is returned.
         *
         * @tparam U
         * @tparam B
         * @tparam TOL
         * @param other
         * @param tol  vector to compare
         * @return
         */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr std::optional<DL(T)>
        scale(const VectorS3D<U, B> &other, const TOL& tol = TOL()) const;

    public: // Arithmetic operators

        constexpr VectorS3D<UADD(T), A> operator+() const;

        constexpr VectorS3D<USUB(T), A> operator-() const;

        template<typename U, typename B>
        constexpr VectorS3D<ADD(T,U), A> operator+(const VectorS3D<U, B> &right) const;

        template<typename U, typename B>
        constexpr VectorS3D<SUB(T,U), A> operator-(const VectorS3D<U, B> &right) const;

        template<typename U>
        constexpr VectorS3D<MUL(T,U), A> operator*(const U &right) const;

        template<typename U>
        constexpr VectorS3D<DIV(T,U), A> operator/(const U &right) const;

        template<typename U, typename B>
        constexpr MUL(T,U) operator|(const VectorS3D<U, B> &right) const;

        template<typename U, typename B>
        constexpr VectorS3D<MUL(T,U), A> operator^(const VectorS3D<U, B> &right) const;


        /**
	     * Sum another vector
	     * @param right
	     */
        template<typename U, typename B>
        constexpr VectorS3D<T, A> &operator+=(const VectorS3D<U, B> &right);

        /**
		 * Subtract another vector
		 * @param right
		 */
        template<typename U, typename B>
        constexpr VectorS3D<T, A> &operator-=(const VectorS3D<U, B> &right);

        /**
         * Scalar moltiplication with another quantity
         * @param right
         */
        template<typename U>
        constexpr VectorS3D<T, A> &operator*=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);

        /**
		 * Scalar division with another quantity
		 * @param right
		 */
        template<typename U>
        constexpr VectorS3D<T, A> &operator/=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);

    public: // Relational operators


        template<typename U, typename B>
        constexpr std::weak_ordering operator<=>(const VectorS3D<U, B> &right) const;


        /**
		 * Equality between two vectors
		 */
        template<typename U, typename B>
        constexpr bool operator==(const VectorS3D<U, B> &right) const;

        /**
		 * Disequality between two vectors
		 */
        template<typename U, typename B>
        constexpr bool operator!=(const VectorS3D<U, B> &right) const;

        friend std::ostream &operator<<(std::ostream &os, const VectorS3D<T, A> &right)
        {
            os << "(" << right.m_r << ", " << right.m_theta << ", " << right.m_phi << ")";
            return os;
        }

    protected:
        constexpr inline bool isNormalized() const;
        constexpr void doNormalize();
    };
}


#if BOOST_UNITS_HAS_BOOST_TYPEOF

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TEMPLATE(dfpe::VectorS3D, 2)
#endif

namespace dfpe
{

    template<typename T, typename U, typename B>
    constexpr VectorS3D<MUL(T,U), B> operator*(const T &left, const VectorS3D<U, B> &right);
}

#include "VectorS3D.tcc"
