#pragma once

#include <ostream>
#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/is_dimensionless.hpp>
#include <boost/units/io.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include "univec/utils.hpp"
#include "univec/VectorC.hpp"
#include "univec/Tolerance.hpp"

namespace dfpe
{
    template<int N, typename T, typename A>
    class VectorC;

    /**
     * A 2D vector in a polar coordinate system
     * @tparam T Type of the quantity
     * @tparam A Type of the angle
     */
    template<typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    class VectorP2D
    {

        template <int N, typename U, typename B>
        friend class VectorC;

    public:
        /// Create a null vector
        constexpr VectorP2D();

        /// Create a vector from another vector
        template<typename U, typename B>
        constexpr VectorP2D(const VectorP2D<U, B> &other);

        /// Create a vector from radius \f$r\f$ and angle \f$phi\f$
        template<typename U, typename B>
        constexpr VectorP2D(const U &r, const B &phi);

        /**
         * \brief Create a vector from a 2D cartesian vector
         * It is an expensive operation
         */
        template<int N, typename U, typename B>
        explicit constexpr VectorP2D<T, A>(const VectorC<N, U, B>& other) requires (N == 2) : VectorP2D()
        {
            m_r   = T(other.norm());
            m_phi = A(boost::units::atan2(other[1], other[0]));
        }

    protected:
        T m_r; ///< radius \f$r\f$ value
        A m_phi; ///< angle \f$phi\f$ value

    public:



        inline constexpr const T&     getR() const     { return m_r; } ///< @private
        inline constexpr void         setR(const T& value) ///< @private
        {
            m_r = value;
            if (!isNormalized())
                doNormalize();
        }

        inline constexpr const A& getPhi() const     { return m_phi; } ///< @private
        inline constexpr void     setPhi(const A& value)     ///< @private
        {
            m_phi = value;
            if (!isNormalized())
                doNormalize();
        }

#ifndef GCC
        __declspec(property(get=getR, put=setR)) T r; ///< @private
        __declspec(property(get=getPhi, put=setPhi)) A phi; ///< @private
#else
    #ifdef ENABLE_GCC_PROPERTY
        Property<T,VectorP2D,&VectorP2D::getR,&VectorP2D::setR>     r;         ///< @private
        Property<A,VectorP2D,&VectorP2D::getPhi,&VectorP2D::setPhi> phi;       ///< @private
    #endif
#endif
    public:
        /**
	     * \brief Calculate the euclidean norm of the vector
	     */
        constexpr T norm() const;

        /**
         * \brief Calculate the euclidean squared norm of the vector
         */
        constexpr POW(T,2) normSquared() const;

        /**
         * \brief Create a unit vector with the same direction of the vector
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr VectorP2D<T, A> versor() const;

        /**
         * \brief Scale to an unit vector
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr void doVersor();

        /**
         * \brief Create a unit vector with the same direction of the vector
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr VectorP2D<DL(T), A> versorDl() const;

        /**
         * \brief Dot product between two vectors
         */
        template<typename U, typename B>
        constexpr MUL(T,U) dot(const VectorP2D<U, B> &other) const;

        /**
         * \brief Cross product between two vectors
         */
        template<typename U, typename B>
        constexpr VectorP2D<MUL(T,U), A> cross(const VectorP2D<U, B> &other) const;

        /**
		 * \brief Smallest angle between two vectors
		 * @param other The other vector
		 * @return The angle between two vectors
		 */
        template<typename U, typename B>
        constexpr A angle(const VectorP2D<U, B> &other) const;

        /**
		 * Check if the vector has zero length
		 * @return true is null, false otherwise
		 */
        template<typename TOL=Tolerance<T,A>>
        constexpr bool isNull(const TOL& tol = TOL()) const;

        /**
         * Check if the vector are near with each other
		 * @return true is null, false otherwise
		 */
        template<typename U, typename B, typename TOL=Tolerance<T,A>>
        constexpr bool isNear(const VectorP2D<U, B> &other, const TOL& tol = TOL()) const;

        /**
     * Check if all vector element are finite
     * @return true is all elements are finite, false otherwise
     */
        constexpr bool isFinite() const;

        /**
         * Check if all vector element are normal
         * @return true is all elements are normal, false otherwise
         */
        constexpr bool isNormal() const;

        /**
         * Check if any vector element is nan
         * @return true is any element is nan, false otherwise
         */
        constexpr bool isNan() const;

        /**
         * Check if any vector element is inf
         * @return true is any element is inf, false otherwise
         */
        constexpr bool isInfinite() const;

        /**
         * \brief Check if two vector are parallel
         * Two vector are parallel if they have the same direction or opposite direciton
         * If one vector is zero, then they are not parallel
         * @return true is are parallel, false otherwise
         */
        template<typename U, typename B, typename TOL=Tolerance<T,A>>
        constexpr bool isParallel(const VectorP2D<U, B> &other, const TOL& tol = TOL()) const;

        /**
        * \brief Check if two have same direction
        * If one vector is zero, then they do not have same direction
        * @return true is they have same direction, false otherwise
        */
        template<typename U, typename B, typename TOL=Tolerance<T,A>>
        constexpr bool isSameDirection(const VectorP2D<U, B> &other, const TOL& tol = TOL()) const;

        /**
        * \brief Check if two have opposite direction
        * If one vector is zero, then they do not have opposite direction
        * @return true is they have opposite direction, false otherwise
        */
        template<typename U, typename B, typename TOL=Tolerance<T,A>>
        constexpr bool isOppositeDirection(const VectorP2D<U, B> &other, const TOL& tol = TOL()) const;

        /**
         * \brief Check if two have perpendicular direction
         * If one vector is zero, then they do not have perpendicular direction
         * @return true is they have perpendicular direction, false otherwise
         */
        template<typename U, typename B, typename TOL=Tolerance<T,A>>
        constexpr bool isPerpendicular(const VectorP2D<U, B> &other, const TOL& tol = TOL()) const;

         /**
          *
          * \brief Get the ratio between the module of two parallel vectors.
          * If the vectors are not parallel or any of them is null, an empty optional value is returned.
          *
          * @tparam U
          * @tparam B
          * @tparam TOL
          * @param other
          * @param tol  vector to compare
          * @return
          */
        template<typename U, typename B, typename TOL=Tolerance<T,A>>
        constexpr std::optional<DIV(U,T)>
        scale(const VectorP2D<U, B> &other, const TOL& tol = TOL()) const;
    public: // Arithmetic operators

        template<typename U, typename B>
        constexpr VectorP2D<ADD(T,U), A> operator+(const VectorP2D<U, B> &right) const;
        /**
	     * Sum another vector
	     * @param right
	     */
        template<typename U, typename B>
        constexpr VectorP2D<T, A> &operator+=(const VectorP2D<U, B> &right);

        /**
		 * Subtract another vector
		 * @param right
		 */
        template<typename U, typename B>
        constexpr VectorP2D<T, A> &operator-=(const VectorP2D<U, B> &right);

        constexpr VectorP2D<UADD(T), A> operator+() const;
        constexpr VectorP2D<USUB(T), A> operator-() const;

        template<typename U, typename B>
        constexpr VectorP2D<SUB(T,U), A> operator-(const VectorP2D<U, B> &right) const;

        template<typename U>
        constexpr VectorP2D<MUL(T,U), A> operator*(const U &right) const requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);

        template<typename U>
        constexpr VectorP2D<DIV(T,U), A> operator/(const U &right) const requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);

        /**
         * Scalar moltiplication with another quantity
         * @param right
         */
        template<typename U>
        constexpr VectorP2D<T, A> &operator*=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);

        /**
		 * Scalar division with another quantity
		 * @param right
		 */
        template<typename U>
        constexpr VectorP2D<T, A> &operator/=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);


        template<typename U, typename B>
        constexpr MUL(T,U) operator|(const VectorP2D<U, B> &right) const;

        template<typename U, typename B>
        constexpr VectorP2D<MUL(T,U), A> operator^(const VectorP2D<U, B> &right) const;

        template<typename U, typename B>
        constexpr std::weak_ordering operator<=>(const VectorP2D<U, B> &right) const;


        /**
		 * Equality between two vectors
		 */
        template<typename U, typename B>
        constexpr bool operator==(const VectorP2D<U, B> &right) const;

        /**
		 * Disequality between two vectors
		 */
        template<typename U, typename B>
        constexpr bool operator!=(const VectorP2D<U, B> &right) const;


    protected:
        constexpr inline bool isNormalized() const;
        constexpr void doNormalize();

    public:
        friend std::ostream &operator<<(std::ostream &os, const VectorP2D<T, A> &right)
        {
            os << "(" << right.m_r << ", " << right.m_phi << ")";
            return os;
        }
    };
}


#if BOOST_UNITS_HAS_BOOST_TYPEOF

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TEMPLATE(dfpe::VectorP2D, 2)
#endif

namespace dfpe
{
    /* Comparative operators */
    // ----------------------------------------------------------------------------------------------------------------

    template<typename T, typename U, typename B>
    constexpr VectorP2D<MUL(T,U), B> operator*(const T &left, const VectorP2D<U, B> &right)  requires (boost::units::is_dimensionless<T>::value || std::is_arithmetic_v<T>);

}

#include "VectorP2D.tcc"
