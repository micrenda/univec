#pragma once
#include <iostream>
#include <complex>
#include <boost/type_traits/is_same.hpp>
#include <boost/units/is_quantity.hpp>
#include <boost/units/reduce_unit.hpp>

#define UADD(W)      typename boost::units::unary_plus_typeof_helper<W>::type
#define ADD(W1,W2)   typename boost::units::add_typeof_helper<W1, W2>::type
#define USUB(W)      typename boost::units::unary_minus_typeof_helper<W>::type
#define SUB(W1,W2)   typename boost::units::subtract_typeof_helper<W1, W2>::type
#define MUL(W1,W2)   typename boost::units::multiply_typeof_helper<W1, W2>::type
#define DIV(W1,W2)   typename boost::units::divide_typeof_helper<W1, W2>::type
#define DL(W)        typename boost::units::divide_typeof_helper<W, W>::type
#define POW(W,X)     typename boost::units::power_typeof_helper<W,boost::units::static_rational<X>>::type
#define POWR(W,X,Y)  typename boost::units::power_typeof_helper<W, boost::units::static_rational<X,Y>>::type

#define M_UADD(WW)      VectorC<WW::dimensionality,UADD(typename WW::quantity_type), typename WW::angle_type>
#define M_ADD(WW1,WW2)  VectorC<WW1::dimensionality,ADD(typename WW1::quantity_type, typename WW2::quantity_type),typename WW1::angle_type>
#define M_USUB(WW)      VectorC<WW::dimensionality,USUB(typename WW::quantity_type),typename WW::angle_type>
#define M_SUB(WW1,WW2)  VectorC<WW1::dimensionality,SUB(typename WW1::quantity_type, typename WW2::quantity_type),typename WW1::angle_type>
#define M_MUL(WW1,WW2)  VectorC<WW1::dimensionality,MUL(typename WW1::quantity_type, typename WW2::quantity_type),typename WW1::angle_type>
#define M_DIV(WW1,WW2)  VectorC<WW1::dimensionality,DIV(typename WW1::quantity_type, typename WW2::quantity_type),typename WW1::angle_type>
#define M_DL(WW)        VectorC<WW::dimensionality,DL(typename WW::quantity_type),typename WW::angle_type>
#define M_POW(WW,X)     VectorC<WW::dimensionality,POW(typename WW::quantity_type,X),typename WW::angle_type>
#define M_POWR(WW,X,Y)  VectorC<WW::dimensionality,POWR(typename WW::quantity_type,X,Y),typename WW::angle_type>

#define L_ADD(WW1,W2)   VectorC<WW1::dimensionality,ADD(typename WW1::quantity_type, W2),typename WW1::angle_type>
#define L_SUB(WW1,W2)   VectorC<WW1::dimensionality,SUB(typename WW1::quantity_type, W2),typename WW1::angle_type>
#define L_MUL(WW1,W2)   VectorC<WW1::dimensionality,MUL(typename WW1::quantity_type, W2),typename WW1::angle_type>
#define L_DIV(WW1,W2)   VectorC<WW1::dimensionality,DIV(typename WW1::quantity_type, W2),typename WW1::angle_type>
                        
#define R_ADD(W1,WW2)   VectorC<WW2::dimensionality,ADD(W1, typename WW2::quantity_type),typename WW2::angle_type>
#define R_SUB(W1,WW2)   VectorC<WW2::dimensionality,SUB(W1, typename WW2::quantity_type),typename WW2::angle_type>
#define R_MUL(W1,WW2)   VectorC<WW2::dimensionality,MUL(W1, typename WW2::quantity_type),typename WW2::angle_type>
#define R_DIV(W1,WW2)   VectorC<WW2::dimensionality,DIV(W1, typename WW2::quantity_type),typename WW2::angle_type>

// This is a workaround for Clang 14: it seems that Clang 14 consider a consteval not to be constexpr.
// So when trying to use the value of a consteval within the definition of a constexpr, it fails with the error:
// error: call to consteval function 'dfpe::VectorC::symbol' is not a constant expression
//
// With Clang 16 this works well, because it adheres better to the C++ 20 standard. The cleanest solution would be
// to have as minimum Clang version >= 15, but at this moment, the last version of MacOS does provide only Clang 14,
// so we are stuck with it.
//
// So, for now, use this workaround, and then, when Clang 15 will be more widespread, we will replace CONSTEVAL_WA with consteval and require the
// minimum Clang version to be >= 15.

#if defined(__clang__) and __clang_major__ <= 14
#define CONSTEVAL_WA   constexpr
#else
#define CONSTEVAL_WA   consteval
#endif

// Make sure that any hpp include utils.hpp, because this macro disable some non compatible
#if defined(__GNUC__) && !defined(__llvm__) && !defined(__INTEL_COMPILER)
#define GCC
#endif

namespace dfpe
{

    template<int N, typename T, typename A>
    class VectorC;

    constexpr bool is_power_of_2(int v)
    {
        return v && ((v & (v - 1)) == 0);
    }


    template<bool X, typename...>
    constexpr bool value = X;

    template<typename T, typename U>
    constexpr bool check_type_compatibility()
    {
        if constexpr (!(boost::units::is_dimensionless<T>::value && std::is_arithmetic_v<U>))
        {
            if constexpr (!boost::is_same<typename boost::units::reduce_unit<T>::type::unit_type, typename boost::units::reduce_unit<U>::type::unit_type>::value)
            {
#ifdef WE_HAVE_CONSTEXPR_ASSERT
                // If future, when we will have constexpr_assert we may do something like this:
                // [P0596] std::constexpr_trace and std::constexpr_assert

                typename T::unit_type unit1;
                typename U::unit_type unit2;

                std::string unit1_str = symbol_string(unit1);
                std::string unit2_str = symbol_string(unit2);

                std::string msg = "[" + unit1_str + "] is not compatible with [" + unit2_str + "]";
                std::constexpr_assert(false, msg);
#else
                static_assert(value<false, T, U>, "Unit of left side is not implicitly convertible to the unit of right side");
#endif
            }
        }
        return true;
    }

    template<typename T, typename U>
    constexpr bool mat_check_type_compatibility()
    {
        return check_type_compatibility<typename T::quantity_type,typename U::quantity_type>();
    }

    template<typename T, typename U>
    constexpr bool left_check_type_compatibility()
    {
        return check_type_compatibility<typename T::quantity_type,U>();
    }

    template<typename T, typename U>
    constexpr bool right_check_type_compatibility()
    {
        return check_type_compatibility<T,typename U::quantity_type>();
    }


#if defined(GCC) && defined(ENABLE_GCC_PROPERTY)
    // This is a class that is used to simulate __declspec(private) with GCC
    // To be removed (and its associated code) after GCC will finally implement __declspec(private) or any
    // similar system

    // It is not working yet (compiler errors)


    template<typename C, typename P, const C& (P::*GETTER)() const, void (P::*SETTER)(const C &)>
    struct Property /// @private
    {
        P* instance;

        constexpr Property(P* instance): instance(instance)
        {

        }

        constexpr Property(const Property& other)
        {
            instance = other.instance;
            *this = (C) other;
        }

        constexpr operator C () const
        {
            return (instance->*GETTER)();
        }

        constexpr Property& operator=(const Property& other)
        {
            instance = other.instance;
            *this = (C) other;
            return *this;
        }

        constexpr Property& operator=(C value)
        {
            (instance->*SETTER)(value);
            return *this;
        }

        template<typename C2, typename P2, C2 (P2::*GETTER2)() const, void (P2::*SETTER2)(C2 &)>
        constexpr Property& operator=(const Property<C2, P2, GETTER2, SETTER2>& other)
        {
            return *this = (C) other;
        }

        constexpr bool operator==(const C& other) const
        {
            return (C)*this == other;
        }

        constexpr bool operator!=(const C& other) const
        {
            return (C)*this != other;
        }

        constexpr Property& operator+=(const C& other)
        {
            *this = (C)*this + other;
            return *this;
        }

        constexpr Property& operator-=(const C& other)
        {
            *this = (C)*this - other;
            return *this;
        }

        constexpr Property& operator*=(const C& other)
        {
            *this = (C)*this - other;
            return *this;
        }

        constexpr Property& operator/=(const C& other)
        {
            *this = (C)*this - other;
            return *this;
        }
    };



#endif
}