#pragma once

#include <ostream>
#include <cmath>
#include <random>
#include <array>
#include <optional>
#include <boost/units/quantity.hpp>
#include <boost/units/is_dimensionless.hpp>
#include <boost/units/io.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/systems/si.hpp>

#include "univec/utils.hpp"
#include "univec/VectorP2D.hpp"
#include "univec/VectorS3D.hpp"
#include "univec/VectorY3D.hpp"
#include "univec/Tolerance.hpp"
#include "univec/Real.hpp"

#define NN (N<0?-N:N)

namespace dfpe
{
    template<int N, int M, typename TT>
    class Matrix;
    
    template<typename T, typename A>
    class VectorP2D;
    template<typename T, typename A>
    class VectorS3D;
    template<typename T, typename A>
    class VectorY3D;
    /**
    * A n-D vector in a Cartesian coordinate system
    * @tparam N Dimensionality (1-> 1D, 2->2D, 3->3D, 4->Quaternion, etc.)
    * @tparam T Type of the quantity
    * @tparam A Type of the angle
    */
    template<int N, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    class VectorC
    {
    public:
        /**
         * \brief Dimensionality of the vector
         *  1: VectorC1D, 2: VectorC2D, 3: VectorC2D, etc.
         * -1 for Real, -2: Complex, -4: Quaternion, -8: Octonion, etc.
         */
        static constexpr int   dimensionality = N;
         /**
          * \brief Number of elements of the vector
          * 1 for VectorC1D, 2 for VectorC2D and Complex, 3 for VectorC3D, 4 for Quaternions, and so on.
          */
        static constexpr int    size = NN;

        /**
         * \brief Check if the class represents a real quantity
         * A VectorC is scalar only if N is equal to -1.
         * A VectorC1D (N = 1) entity is not considered a scalar value.
         */
        static constexpr bool real = N == -1;

        /**
         * \brief Check if the class represents a complex quantity
         * A VectorC is complex only if N is equal to -2.
         * A VectorC2D (N = 2) entity is not considered a complex value.
         */
        static constexpr bool complex = N == -2;

        /**
         * \brief Check if the class represents a quaternion quantity
         * A VectorC is quaternion only if N is equal to -4.
         * A VectorC4D (N = 4) entity is not considered a quaternion value.
         */
        static constexpr bool quaternion = N == -4;

        /**
         * \brief Check if the class represents an octonion quantity
         * A VectorC is octonion only if N is equal to -8. A VectorC with N = 8 entity is not considered an octonion value.
         */
        static constexpr bool octonion = N == -8;

        /**
         * \brief Check if the class represents a sedenion quantity
         * A VectorC is octonion only if N is equal to -16. A VectorC1 with N = 16 entity is not considered a sedenion value.
         */
        static constexpr bool sedenion = N == -8;


        typedef T              quantity_type;
        typedef A              angle_type;

    public:
        /// Create a null vector
        constexpr VectorC();

        /**
         * Create a vector from another vector
         * @tparam U Quantity unit
         * @tparam B Angle unit
         * @param other
         */
        template<typename U, typename B>
        constexpr VectorC(const VectorC<N, U, B> &other);


         /**
          * Create a vector from \f$x_1, x_2, \cdots, x_n\f$ coordinates
          *
          * @tparam Us Quantity units
          * @param args components
          */
        template<typename... Us>
        constexpr VectorC(Us... args) requires (sizeof...(Us) == NN) : VectorC()
        {
            m_v = std::array<T,NN>({args...});
        }

        /**
         * Create a vector from \f$x_1, x_2, \cdots, x_n\f$ coordinates
         * @tparam U Quantity units
         * @param list components
         */
        template<typename U>
        constexpr VectorC(std::initializer_list<U> list) requires
        (boost::units::is_dimensionless<T>::value && std::is_floating_point_v<U>);

        // Constructors that allows conversions from other types
        template<typename U, typename B>
        constexpr explicit VectorC(const VectorP2D<U,B>& other) requires (N == 2);

        template<typename U, typename B>
        constexpr explicit VectorC(const VectorS3D<U,B>& other) requires (N == 3);

        template<typename U, typename B>
        constexpr explicit VectorC(const VectorY3D<U,B>& other) requires (N == 3);

        // Construction for quaternions, octonions, etc.
        template<typename U>
        constexpr explicit VectorC(const U &a) requires(N < 0);

        template<typename U, typename V, typename B>
        constexpr VectorC(const U &a, const VectorC<-N-1, V, B> &vector) requires (N < 0);

        template<typename U, typename B, typename C>
        constexpr VectorC(const VectorC<-N-1, U, B> &axis, const C &angle) requires(N < 0);

    protected:
        std::array<T,NN> m_v;

    public:
        // Allow conversion from Real to a boost::quantity
        constexpr operator T&() requires(N == -1 || N == 1) { return m_v[0]; };
        constexpr operator T() const requires(N == -1 || N == 1) { return m_v[0]; };

#ifndef GCC
        inline constexpr const T&     getX() const     requires (N >= 1, N <= 3) { return m_v[0]; } ///< @private
        inline constexpr T&           getX()           requires (N >= 1, N <= 3) { return m_v[0]; } ///< @private
        inline constexpr void         setX(const T& v) requires (N >= 1, N <= 3) { m_v[0] = v;    } ///< @private

        inline constexpr const T&     getY() const     requires (N >= 2, N <= 3) { return m_v[1]; } ///< @private
        inline constexpr T&           getY()           requires (N >= 2, N <= 3) { return m_v[1]; } ///< @private
        inline constexpr void         setY(const T& v) requires (N >= 2, N <= 3) { m_v[1] = v;    } ///< @private

        inline constexpr const T&     getZ() const     requires (N >= 3, N <= 3) { return m_v[2]; } ///< @private
        inline constexpr T&           getZ()           requires (N >= 3, N <= 3) { return m_v[2]; } ///< @private
        inline constexpr void         setZ(const T& v) requires (N >= 3, N <= 3) { m_v[2] = v;    } ///< @private

        inline constexpr const T &    getA() const     requires (N == -2 || N == -4 || N == -8)   { return m_v[0]; } ///< @private
        inline constexpr T &          getA()           requires (N == -2 || N == -4 || N == -8)   { return m_v[0]; } ///< @private
        inline constexpr void         setA(const T &v) requires (N == -2 || N == -4 || N == -8)   { m_v[0] = v;    } ///< @private

        inline constexpr const T &    getB() const     requires (N == -2 || N == -4 || N == -8)   { return m_v[1]; } ///< @private
        inline constexpr T &          getB()           requires (N == -2 || N == -4 || N == -8)   { return m_v[1]; } ///< @private
        inline constexpr void         setB(const T &v) requires (N == -2 || N == -4 || N == -8)   { m_v[1] = v;    } ///< @private

        inline constexpr const T &    getC() const     requires (N == -4 || N == -8)        { return m_v[2]; } ///< @private
        inline constexpr T &          getC()           requires (N == -4 || N == -8)        { return m_v[2]; } ///< @private
        inline constexpr void         setC(const T &v) requires (N == -4 || N == -8)        { m_v[2] = v; } ///< @private

        inline constexpr const T &    getD() const     requires (N == -4 || N == -8)        { return m_v[3]; } ///< @private
        inline constexpr T &          getD()           requires (N == -4 || N == -8)        { return m_v[3]; } ///< @private
        inline constexpr void         setD(const T &v) requires (N == -4 || N == -8)        { m_v[3] = v; } ///< @private

        inline constexpr const T &    getE() const     requires (N == -8)                   { return m_v[4]; } ///< @private
        inline constexpr T &          getE()           requires (N == -8)                   { return m_v[4]; } ///< @private
        inline constexpr void         setE(const T &v) requires (N == -8)                   { m_v[4] = v; } ///< @private

        inline constexpr const T &    getF() const     requires (N == -8)                   { return m_v[5]; } ///< @private
        inline constexpr T &          getF()           requires (N == -8)                   { return m_v[5]; } ///< @private
        inline constexpr void         setF(const T &v) requires (N == -8)                   { m_v[5] = v; } ///< @private

        inline constexpr const T &    getG() const     requires (N == -8)                   { return m_v[6]; } ///< @private
        inline constexpr T &          getG()           requires (N == -8)                   { return m_v[6]; } ///< @private
        inline constexpr void         setG(const T &v) requires (N == -8)                   { m_v[6] = v; } ///< @private

        inline constexpr const T &    getH() const     requires (N == -8)                   { return m_v[7]; } ///< @private
        inline constexpr T &          getH()           requires (N == -8)                   { return m_v[7]; } ///< @private
        inline constexpr void         setH(const T &v) requires (N == -8)                   { m_v[7] = v; } ///< @private
#else
        constexpr static T zero = T();
        
        inline constexpr const T&     getX() const     { if constexpr (N >= 1 && N <= 3) return m_v[0]; else return zero; } ///< @private
        inline constexpr T&           getX()           { if constexpr (N >= 1 && N <= 3) return m_v[0]; else     return zero; } ///< @private
        inline constexpr void         setX(const T& v) { if constexpr (N >= 1 && N <= 3)        m_v[0] = v;    } ///< @private

        inline constexpr const T&     getY() const     { if constexpr (N >= 2 && N <= 3) return m_v[1]; else     return zero; } ///< @private
        inline constexpr T&           getY()           { if constexpr (N >= 2 && N <= 3) return m_v[1]; else     return zero; } ///< @private
        inline constexpr void         setY(const T& v) { if constexpr (N >= 2 && N <= 3)        m_v[1] = v;    } ///< @private

        inline constexpr const T&     getZ() const     { if constexpr (N >= 3 && N <= 3) return m_v[2]; else     return zero; } ///< @private
        inline constexpr T&           getZ()           { if constexpr (N >= 3 && N <= 3) return m_v[2]; else     return zero; } ///< @private
        inline constexpr void         setZ(const T& v) { if constexpr (N >= 3 && N <= 3)        m_v[2] = v;    } ///< @private

        inline constexpr const T&     getA() const     { if constexpr (N == -2 || N == -4 || N == -8) return m_v[0];     else return zero; } ///< @private
        inline constexpr T&           getA()           { if constexpr (N == -2 || N == -4 || N == -8) return m_v[0];     else return zero; } ///< @private
        inline constexpr void         setA(const T& v) { if constexpr (N == -2 || N == -4 || N == -8)        m_v[0] = v;    } ///< @private

        inline constexpr const T&     getB() const     { if constexpr (N == -2 || N == -4 || N == -8) return m_v[1];     else return zero; } ///< @private
        inline constexpr T&           getB()           { if constexpr (N == -2 || N == -4 || N == -8) return m_v[1];     else return zero; } ///< @private
        inline constexpr void         setB(const T& v) { if constexpr (N == -2 || N == -4 || N == -8)        m_v[1] = v;    } ///< @private

        inline constexpr const T&     getC() const     { if constexpr (N == -4 || N == -8) return m_v[2];     else return zero; } ///< @private
        inline constexpr T&           getC()           { if constexpr (N == -4 || N == -8) return m_v[2];     else return zero; } ///< @private
        inline constexpr void         setC(const T& v) { if constexpr (N == -4 || N == -8)        m_v[2] = v;    } ///< @private

        inline constexpr const T&     getD() const     { if constexpr (N == -4 || N == -8) return m_v[3];     else return zero; } ///< @private
        inline constexpr T&           getD()           { if constexpr (N == -4 || N == -8) return m_v[3];     else return zero; } ///< @private
        inline constexpr void         setD(const T& v) { if constexpr (N == -4 || N == -8)        m_v[3] = v;    } ///< @private

        inline constexpr const T&     getE() const     { if constexpr (N == -8) return m_v[4];     else return zero; } ///< @private
        inline constexpr T&           getE()           { if constexpr (N == -8) return m_v[4];     else return zero; } ///< @private
        inline constexpr void         setE(const T& v) { if constexpr (N == -8)        m_v[4] = v;    } ///< @private

        inline constexpr const T&     getF() const     { if constexpr (N == -8) return m_v[5];     else return zero; } ///< @private
        inline constexpr T&           getF()           { if constexpr (N == -8) return m_v[5];     else return zero; } ///< @private
        inline constexpr void         setF(const T& v) { if constexpr (N == -8)        m_v[5] = v;    } ///< @private

        inline constexpr const T&     getG() const     { if constexpr (N == -8) return m_v[6];     else return zero; } ///< @private
        inline constexpr T&           getG()           { if constexpr (N == -8) return m_v[6];     else return zero; } ///< @private
        inline constexpr void         setG(const T& v) { if constexpr (N == -8)        m_v[6] = v;    } ///< @private

        inline constexpr const T&     getH() const     { if constexpr (N == -8) return m_v[7];     else return zero; } ///< @private
        inline constexpr T&           getH()           { if constexpr (N == -8) return m_v[7];     else return zero; } ///< @private
        inline constexpr void         setH(const T& v) { if constexpr (N == -8)        m_v[7] = v;    } ///< @private

#endif

#ifndef GCC
        __declspec(property(get=getX, put=setX)) T x; ///< @private
        __declspec(property(get=getY, put=setY)) T y; ///< @private
        __declspec(property(get=getZ, put=setZ)) T z; ///< @private
        __declspec(property(get=getA, put=setA)) T a; ///< @private
        __declspec(property(get=getB, put=setB)) T b; ///< @private
        __declspec(property(get=getC, put=setC)) T c; ///< @private
        __declspec(property(get=getD, put=setD)) T d; ///< @private
        __declspec(property(get=getE, put=setE)) T e; ///< @private
        __declspec(property(get=getF, put=setF)) T f; ///< @private
        __declspec(property(get=getG, put=setG)) T g; ///< @private
        __declspec(property(get=getH, put=setH)) T h; ///< @private
#else

        #if ENABLE_GCC_PROPERTY
        Property<T,VectorC,&VectorC::getX,&VectorC::setX> x;         ///< @private
        Property<T,VectorC,&VectorC::getY,&VectorC::setY> y;         ///< @private
        Property<T,VectorC,&VectorC::getZ,&VectorC::setZ> z;         ///< @private
        Property<T,VectorC,&VectorC::getA,&VectorC::setA> a;         ///< @private
        Property<T,VectorC,&VectorC::getB,&VectorC::setB> b;         ///< @private
        Property<T,VectorC,&VectorC::getC,&VectorC::setC> c;         ///< @private
        Property<T,VectorC,&VectorC::getD,&VectorC::setD> d;         ///< @private
        Property<T,VectorC,&VectorC::getE,&VectorC::setE> e;         ///< @private
        Property<T,VectorC,&VectorC::getF,&VectorC::setF> f;         ///< @private
        Property<T,VectorC,&VectorC::getG,&VectorC::setG> g;         ///< @private
        Property<T,VectorC,&VectorC::getH,&VectorC::setH> h;         ///< @private
        #endif
#endif

    public:
        /**
         * \brief Return a \f$N \times N\f$ matrix representation.
         * Return a matrix representation of the complex, quaternion, etc.
         *  D specify the kind of matrix to return: 1 returns a scalar matrix, 2 a complex matrix, 4 a quaternion matrix.
         *  D should be a power of 2 and \f$D \le -N\f$. If \f$D=-N\f$, then a \f$1 \times 1\f$ matrix is returned containing the value.
         * \tparam D \f$D <= -N \f$
         */
        template<int D=1>
        constexpr Matrix<NN/D,NN/D,VectorC<-D,T,A>> matrix() const requires (N < 0 && D > 0 && D <= -N && is_power_of_2(D));


        /**
         * \brief Calculate the euclidean norm (L2-norm) of the vector
         *
         * \f$ v = |\mathbf{v}| = \|\mathbf{v}\|_2 = \sqrt{v_1^2 + v_2^2 + \cdots + v_n^2} = \sum_{i=1}^{n} v^2_i\f$
         */
        constexpr T norm() const;

        /**
         * \brief Calculate the supremum (\f$ L_\infty \f$) norm of the vector
         * defined as \f$ \left|v\right|_{\text{sup}} = \max(|v_1|,|v_2|,|v_2|, \dots, |v_n|)\f$
         */
        constexpr T normLInf() const;

        /**
         * \brief Calculate the L1 norm (Manhattan norm) of the vector
         * defined as \f$ |v|_{1} = \sum_{i=1}^{n} |v_i|\f$
         */
        constexpr T normL1() const;
   /**
         * \brief Calculate the generic \f$L_p\f$ norm of the vector
         * defined as \f$\left\|v\right\|_p=\left(\sum_{i=1}^n\left|v_i\right|^p\right)^{1 / p}\f$
         */
        constexpr T normL(typename T::value_type p) const;


        /**
         * \brief Calculate the euclidean squared norm of the vector
         *
         * \f$ v^2 = |\mathbf{v}|^2 = v_x^2 + v_y^2 + v_z^2\f$
         */
        constexpr POW(T,2) normSquared() const;

        /**
         * \brief Create a unit vector with the same direction of the vector
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr VectorC<N, T, A> versor() const;

        /**
         * \brief Scale the vector to unit length
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr void doVersor();

        /**
             * \brief Create a unit dimensionless vector with the same direction of the vector
             *
             * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
             */
        constexpr VectorC<N, DL(T), A> versorDl() const;

        /**
         * \brief Dot product between two vectors
         *
         * \f$ \mathbf{a} \cdot \mathbf{b} = a_x b_x + a_y b_y + a_z b_z\f$
         */
        template<typename U, typename B>
        constexpr MUL(T,U) dot(const VectorC<N, U, B> &other) const;

        /**
         * \brief Cross product between two vectors
         *
         * \f$ \mathbf{a} \times \mathbf{b} = (a_y b_z - a_z b_y) \mathbf{\hat{x}} -(a_x b_z - a_z b_x) \mathbf{\hat{y}} +(a_x b_y - a_y b_x)\mathbf{\hat{z}}\f$
         */
        template<typename U, typename B>
        constexpr VectorC<N, MUL(T,U), A> cross(const VectorC<N, U, B> &other) const requires ( N == 3 || N == 7 );

        /**
         * \brief Direct sum between two vectors
         *
         * \f$ \mathbf{a} \oplus \mathbf{b} = (a_1, a_2, \cdots, a_n, b_1, b_2, \cdots, b_p)\mathbf{\hat{z}}\f$
         */
        template<int P, typename U, typename B>
        constexpr VectorC<N + P, T, A> directSum(const VectorC<P, U, B> &other) const requires ( N > 0 || P > 0 );

        /**
         * \brief Entry-wise product (also know as Hadamard product or Schur product) of two vectors
         * @tparam U
         * @param other
         * @return
         */
        template<typename U, typename B>
        constexpr VectorC<N, MUL(T,U), A>
        elementwiseProduct(const VectorC<N, U, B> &other) const requires (N >= 0);

        /**
         * \brief Entry-wise division (also know as Hadamard division or Schur division) of two vectors
         * @tparam U
         * @param other
         * @return
         */
        template<typename U, typename B>
        constexpr VectorC<N, DIV(T,U), A>
        elementwiseDivision(const VectorC<N, U, B> &other) const requires (N >= 0);



        /**
         * \brief Smallest angle between two vectors
         *
         * \f$ \angle( \mathbf{a}, \mathbf{b}) = \arccos{\left(\frac{ \mathbf{a} \cdot \mathbf{b} }{a b}\right)}\f$
         *
         * @param other The other vector
         * @return The angle between two vectors
         */
        template<typename U, typename B>
        constexpr A angle(const VectorC<N, U, B> &other) const;

        /**
         * Check if the vector has zero length
         * @return true is null, false otherwise
         */
        template<typename TOL=Tolerance<T,A>>
        constexpr bool isNull(const TOL& tol = TOL()) const;

        /**
         * Check if the vector are near with each other
         * @return true is null, false otherwise
         */
        template<typename U, typename B, typename TOL=Tolerance<T,A>>
        constexpr bool isNear(const VectorC<N, U, B> &other, const TOL& tol = TOL()) const;

        /**
     * Check if all vector element are finite
     * @return true is all elements are finite, false otherwise
     */
        constexpr bool isFinite() const;

        /**
         * Check if all vector element are normal
         * @return true is all elements are normal, false otherwise
         */
        constexpr bool isNormal() const;

        /**
         * Check if any vector element is nan
         * @return true is any element is nan, false otherwise
         */
        constexpr bool isNan() const;

        /**
         * Check if any vector element is inf
         * @return true is any element is inf, false otherwise
         */
        constexpr bool isInfinite() const;


        /**
         * \brief Check if two vector are parallel
         * Two vector are parallel if they have the same direction or opposite direciton
         * If one vector is zero, then they are not parallel
         * @return true is are parallel, false otherwise
         */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isParallel(const VectorC<N, U, B> &other, const TOL& tol = TOL()) const;

         /**
         * \brief Check if two have same direction
         * If one vector is zero, then they do not have same direction
         * @return true is they have same direction, false otherwise
         */
         template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isSameDirection(const VectorC<N, U, B> &other, const TOL& tol = TOL()) const;

         /**
         * \brief Check if two have opposite direction
         * If one vector is zero, then they do not have opposite direction
         * @return true is they have opposite direction, false otherwise
         */
         template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isOppositeDirection(const VectorC<N, U, B> &other, const TOL& tol = TOL()) const;

        /**
         * \brief Check if two have perpendicular direction
         * If one vector is zero, then they do not have perpendicular direction
         * @return true is they have perpendicular direction, false otherwise
         */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isPerpendicular(const VectorC<N, U, B> &other, const TOL& tol = TOL()) const;

    public: // Arithmetic operators

        constexpr VectorC<N, UADD(T), A> operator+() const;
        constexpr VectorC<N, USUB(T), A> operator-() const;

        template<typename U, typename B>
        constexpr VectorC<N, ADD(T, U), A> operator+(const VectorC<N, U, B> &right) const;

        template<typename U, typename B>
        constexpr VectorC<N, SUB(T, U), A> operator-(const VectorC<N, U, B> &right) const;

        template<typename U, typename B>
        constexpr VectorC<N, MUL(T,U), A> operator*(const VectorC<N, U, B> &right) const requires ( N < 0 );

        template<typename U, typename B>
        constexpr VectorC<N, DIV(T,U), A> operator/(const VectorC<N, U, B> &right) const requires ( N < 0 );

        template<typename U>
        constexpr VectorC<N, MUL(T,U), A> operator*(const U &right) const requires (boost::units::is_quantity<U>::value);

        template<typename U>
        constexpr VectorC<N, T, A> operator*(const U &right) const requires (std::is_arithmetic_v<U>);

        template<typename U>
        constexpr VectorC<N, DIV(T,U), A> operator/(const U &right) const;

        template<typename U>
        constexpr VectorC<N, T, A> operator/(const U &right) const requires (std::is_arithmetic_v<U>);

        template<typename U, typename B>
        constexpr MUL(T,U) operator|(const VectorC<N, U, B> &right) const;

        template<typename U, typename B>
        constexpr VectorC<N, MUL(T,U), A> operator^(const VectorC<N, U, B> &right) const;



        /**
         * Sum another vector
         * @param right
         */
        template<typename U, typename B>
        constexpr VectorC<N, T, A> &operator+=(const VectorC<N, U, B> &right);

        /**
         * Subtract another vector
         * @param right
         */
        template<typename U, typename B>
        constexpr VectorC<N, T, A> &operator-=(const VectorC<N, U, B> &right);


        /**
         * Component-wise product with another vector
         * @param right
         */

        template<typename U, typename B>
        constexpr VectorC<N, T, A> &operator*=(const VectorC<N, U, B> &right) requires (N < 0 && boost::units::is_dimensionless<U>::value);

        /**
         * Component-wise division with another vector
         * @param right
         */
        template<typename U, typename B>
        constexpr VectorC<N, T, A> &operator/=(const VectorC<N, U, B> &right) requires (N < 0 && boost::units::is_dimensionless<U>::value);

        /**
         * Scalar multiplication with another quantity
         * @param right
         */
        template<typename U>
        constexpr VectorC<N, T, A> &operator*=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);

        /**
         * Scalar division with another quantity
         * @param right
         */
        template<typename U>
        constexpr VectorC<N, T, A> &operator/=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);

    public: // Relational operators
        template<typename U, typename B>
        constexpr std::strong_ordering operator<=>(const VectorC<N, U, B> &right) const;
        template<typename U, typename B>
        constexpr bool operator==(const VectorC<N, U, B> &right) const;
        template<typename U, typename B>
        constexpr bool operator!=(const VectorC<N, U, B> &right) const;

        template<typename U>
        constexpr std::strong_ordering operator<=>(const U &right) const requires (boost::units::is_quantity<U>::value && size == 1)  { return *this <=> Real<U>(right); }
        template<typename U>
        constexpr bool operator==(const U &right) const requires (boost::units::is_quantity<U>::value && size == 1) { return *this == Real<U>(right); }
        template<typename U>
        constexpr bool operator!=(const U &right) const requires (boost::units::is_quantity<U>::value && size == 1) { return *this != Real<U>(right); }
    public: // Access operators
        /**
         * Access element of a vector (1-based)
         * @param n
         */
        constexpr T &operator()(int n);

        /**
         * Access element of a vector (1-based)
         * @param n
         */
        constexpr const T &operator()(int n) const;

        /**
         * Access element of a vector (0-based)
         * @param n
         */
        constexpr T &operator[](int n);

        /**
         * Access element of a vector (0-based)
         * @param n
         */
        constexpr const T &operator[](int n) const;

    public:

        /**
         * Return the conjugate of the vector
         * @return
         */
        constexpr VectorC<N, T, A> conj() const requires (N < 0);

        /**
         * Replace the value of the vector by its conjugate
         */
        constexpr void doConj() requires (N < 0);

        /**
         * Return the vector associated, having the form \f$\left(b, c, d \right)\f$
         * @return
         */
        constexpr VectorC<-N-1, T, A> vector() const requires (N < 0);

        /**
         * Return the pure complex, quaternion, octonions, etc. associated, having the form \f$ 0 + b \mathbf{1} + c \mathbf{j} + d \mathbf{k} + \ldots \f$
         * @return
         */
        constexpr VectorC<N, T, A> pure() const requires (N < 0);

        /**
         * Return the vector complex, quaternion, octonions, etc. associated, having the form \f$ a + 0 \mathbf{1} + 0 \mathbf{j} + 0 \mathbf{k} + \ldots \f$
         * @return
         */
        constexpr VectorC<N, T, A> scalar() const requires (N < 0);

        /**
          * \brief Symbol associated with the field
          * \f$\mathbb{R}\f$ for Real, \f$\mathbb{C}\f$ for Complex, \f$\mathbb{H}\f$ for Quaternion, \f$\mathbb{O}\f$ for Octionion, \f$\mathbb{S}\f$ for Quaternions,
          * \f$\mathbb{R}^1\f$ for VectorC1, \f$\mathbb{R}^2\f$ for VectorC2, \f$\mathbb{R}^3\f$ for VectorC3 and so on.
          */
        static CONSTEVAL_WA std::string_view symbol();

        /**
         * Get a formal representation of the VectorC value
         * @param field prefix the value with the field prefix such as \f$\mathbb{R}^3\f$, \f$\mathbb{C}\f$, etc.
         * @return
         */
        std::string repr(bool field=true) const;

        constexpr A angle() const requires (N < 0 && N == -4);

        constexpr VectorC<-N-1, T, A> axis() const requires (N < 0 && N == -4 );

        /**
         * \brief Inverse according the hamilton multiplication
         * defined as \f$\mathbf{v}^{-1}=\frac{1}{a^2+b^2+c^2+d^2}(a-b \mathbf{i}-c \mathbf{j}-d \mathbf{k})\f$
         * @return the inverse
         */
        constexpr VectorC<N, POW(T,-1), A> inv() const requires ( N < 0 );

        /**
         * A pure quaternion (also a vector quaternion) is a quaternion with scalar part equal to 0.
         * @tparam TOL
         * @param tol
         * @return
         */
        template<typename TOL = Tolerance<T,A>>
        constexpr bool isPure(const TOL& tol = TOL()) const requires ( N < 0 );

        /**
         * A scalar (also a scalar quaternion or a real quaternion) is a quaternion with vector part equal to 0.
         * @tparam TOL
         * @param tol
         * @return
         */
        template<typename TOL = Tolerance<T,A>>
        constexpr bool isScalar(const TOL& tol = TOL()) const requires ( N < 0 );

    public:



        // Rotations
        /**
         * Rotate the vector by @param angle on the OX axis
         */
        template<typename B>
        constexpr VectorC<N, T, A> rotateX(B angle) const requires (N == 3);

        /**
         * Rotate the vector by @param angle on the OY axis
         */
        template<typename B>
        constexpr VectorC<N, T, A> rotateY(B angle) const requires (N == 3);

        /**
         * Rotate the vector by @param angle on the OZ axis
         */
        template<typename B>
        constexpr VectorC<N, T, A> rotateZ(B angle) const requires (N == 3);

        /**
         * Rotate the vector by @param angle
         */
        template<typename C>
        constexpr VectorC<N, T, A> rotate(C angle) const  requires (N == 2 || N == -2);

        /**
         * Rotate the vector using the \f$\mathbf{q} \mathbf{v} \mathbf{q}^{-1}\f$ formula
         * @tparam U
         * @tparam B
         * @param vector
         * @return
         */
        template<typename U, typename B>
        constexpr VectorC<-N-1, U, B> rotate(const VectorC<-N-1, U, B> &vector) const requires (N < 0);

        /**
         * Rotate the vector by angle on around the axis
         * @param angle angle of rotation
         * @param axis rotation axis
         */
        template<typename U, typename B, typename C>
        constexpr VectorC<N, T, A> rotate(C angle, const VectorC<N, U, B> &axis) const  requires (N == 3); // TODO: implement for any N

        // Rotations
        //Todo Michele Renda
        template<typename U, typename B>
        constexpr VectorC<N, T, A> rotateUz(const VectorC<N, U, B> &uz) const requires ( N == 3); // TODO: implement for any N, if possible


        // Rotations in-place
        /**
         * Rotate the vector by @param angle on the OX axis
         */
        template<typename B>
        constexpr void doRotateX(B angle) requires (N == 3);

        /**
         * Rotate the vector by @param angle on the OY axis
         */
        template<typename B>
        constexpr void doRotateY(B angle) requires (N == 3);

        /**
         * Rotate the vector by @param angle on the OZ axis
         */
        template<typename B>
        constexpr void doRotateZ(B angle) requires (N == 3);

        /**
         * Rotate the vector by @param angle
         */
        template<typename C>
        constexpr void doRotate(C angle)  requires (N == 2 || N == -2);

        /**
         * Rotate the vector using the \f$\mathbf{q} \mathbf{v} \mathbf{q}^{-1}\f$ formula
         * @tparam U
         * @tparam B
         * @param vector
         */
        template<typename U, typename B>
        constexpr void doRotate(const VectorC<-N-1, U, B> &vector) requires (N < 0);

        /**
         * Rotate the vector by angle on around the axis
         * @param angle angle of rotation
         * @param axis rotation axis
         */
        template<typename U, typename B, typename C>
        constexpr void doRotate(C angle, const VectorC<N, U, B> &axis)  requires (N == 3); // TODO: implement for any N

        // Rotations
        //Todo Michele Renda
        template<typename U, typename B>
        constexpr void doRotateUz(const VectorC<N, U, B> &uz) requires ( N == 3); // TODO: implement for any N, if possible

        /**
         * \brief Get the ratio between the module of two parallel vectors.
         * If the vectors are not parallel or any of them is null, an empty optional value is returned.
         *
         * @tparam U
         * @tparam B
         * @tparam TOL
         * @param other
         * @param tol
         * @return
         */
         template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr std::optional<DIV(T,U)>
        scale(const VectorC<N, U, B> &other, const TOL& tol = TOL()) const;



    public:
        /**
         * Static method to create a vector aligned along \f$\vec{x}\f$
         * @param x
         * @return
         */
        constexpr static VectorC<N, T, A> kX(const T &x = T::from_value(1.)) requires (N >= 1 && N <= 3) { VectorC<N, T, A> v; v[0] = x; return v; }; // TODO implement for quaternion fromA,fromB,fromC,fromD

        /**
         * Static method to create a vector aligned along \f$\vec{y}\f$
         * @param y
         * @return
         */
        constexpr static VectorC<N, T, A> kY(const T &y = T::from_value(1.)) requires (N >= 2 && N <= 3) { VectorC<N, T, A> v; v[1] = y; return v; };  // TODO implement for quaternion fromA,fromB,fromC,fromD

        /**
         * Static method to create a vector aligned along \f$\vec{z}\f$
         * @param z
         * @return
         */
        constexpr static VectorC<N, T, A> kZ(const T &z = T::from_value(1.)) requires (N >= 3 && N <= 3) { VectorC<N, T, A> v; v[2] = z; return v; };  // TODO implement for quaternion fromA,fromB,fromC,fromD

         /**
          * Static method to create an uniform random unit vector
          * @tparam URNG
          * @param g
          * @return
          */
        template<typename URNG>
        constexpr static VectorC<N, T, A> unitaryRandom(URNG &g) requires (N == 3)  // TODO: implement for 2D tool or for arbitrary N
        {
            A phi = A(2. * M_PI * distribution(g) * boost::units::si::radians);

            boost::units::quantity<boost::units::si::dimensionless> cosTheta = 2 * distribution(g) - 1;
            boost::units::quantity<boost::units::si::dimensionless> sinTheta = sin(acos(cosTheta));
            boost::units::quantity<boost::units::si::dimensionless> cosPhi   = cos(phi);
            boost::units::quantity<boost::units::si::dimensionless> sinPhi   = sin(phi);

            T rho = T::from_value(1.);

            return VectorC<N, T, A>(
                    rho * sinTheta * cosPhi,
                    rho * sinTheta * sinPhi,
                    rho * cosTheta);
        }

        template<typename URNG>
        constexpr static VectorC<N, T, A> random(URNG &g, const T &radius)  // TODO: implement for 2D tool or for arbitrary N
        {
            T rho = radius * cbrt(distribution(g));
            return rho * unitaryRandom(g);
        }

    protected:
        thread_local static std::uniform_real_distribution<typename T::value_type> m_distribution;

        thread_local constexpr static const std::array<std::array<std::pair<int, bool>, 16>, 16> m_mulTable
        {{
                {std::make_pair(0,  false), std::make_pair(1,  false), std::make_pair(2,  false), std::make_pair(3,  false), std::make_pair(4,  false), std::make_pair(5,  false), std::make_pair(6,  false), std::make_pair(7,  false), std::make_pair(8,  false), std::make_pair(9,  false), std::make_pair(10, false), std::make_pair(11, false), std::make_pair(12, false), std::make_pair(13, false), std::make_pair(14, false), std::make_pair(15, false)},
                {std::make_pair(1,  false), std::make_pair(0,  true),  std::make_pair(3,  false), std::make_pair(2,  true),  std::make_pair(5,  false), std::make_pair(4,  true),  std::make_pair(7,  true),  std::make_pair(6,  false), std::make_pair(9,  false), std::make_pair(8,  true),  std::make_pair(11, true),  std::make_pair(10, false), std::make_pair(13, true),  std::make_pair(12, false), std::make_pair(15, false), std::make_pair(14, true)},
                {std::make_pair(2,  false), std::make_pair(3,  true),  std::make_pair(0,  true),  std::make_pair(1,  false), std::make_pair(6,  false), std::make_pair(7,  false), std::make_pair(4,  true),  std::make_pair(5,  true),  std::make_pair(10, false), std::make_pair(11, false), std::make_pair(8,  true),  std::make_pair(9,  true),  std::make_pair(14, true),  std::make_pair(15, true),  std::make_pair(12, false), std::make_pair(13, false)},
                {std::make_pair(3,  false), std::make_pair(2,  false), std::make_pair(1,  true),  std::make_pair(0,  true),  std::make_pair(7,  false), std::make_pair(6,  true),  std::make_pair(5,  false), std::make_pair(4,  true),  std::make_pair(11, false), std::make_pair(10, true),  std::make_pair(9,  false), std::make_pair(8,  true),  std::make_pair(15, true),  std::make_pair(14, false), std::make_pair(13, true),  std::make_pair(12, false)},
                {std::make_pair(4,  false), std::make_pair(5,  true),  std::make_pair(6,  true),  std::make_pair(7,  true),  std::make_pair(0,  true),  std::make_pair(1,  false), std::make_pair(2,  false), std::make_pair(3,  false), std::make_pair(12, false), std::make_pair(13, false), std::make_pair(14, false), std::make_pair(15, false), std::make_pair(8,  true),  std::make_pair(9,  true),  std::make_pair(10, true),  std::make_pair(11, true)},
                {std::make_pair(5,  false), std::make_pair(4,  false), std::make_pair(7,  true),  std::make_pair(6,  false), std::make_pair(1,  true),  std::make_pair(0,  true),  std::make_pair(3,  true),  std::make_pair(2,  false), std::make_pair(13, false), std::make_pair(12, true),  std::make_pair(15, false), std::make_pair(14, true),  std::make_pair(9,  false), std::make_pair(8,  true),  std::make_pair(11, false), std::make_pair(10, true)},
                {std::make_pair(6,  false), std::make_pair(7,  false), std::make_pair(4,  false), std::make_pair(5,  true),  std::make_pair(2,  true),  std::make_pair(3,  false), std::make_pair(0,  true),  std::make_pair(1,  true),  std::make_pair(14, false), std::make_pair(15, true),  std::make_pair(12, true),  std::make_pair(13, false), std::make_pair(10, false), std::make_pair(11, true),  std::make_pair(8,  true),  std::make_pair(9,  false)},
                {std::make_pair(7,  false), std::make_pair(6,  true),  std::make_pair(5,  false), std::make_pair(4,  false), std::make_pair(3,  true),  std::make_pair(2,  true),  std::make_pair(1,  false), std::make_pair(0,  true),  std::make_pair(15, false), std::make_pair(14, false), std::make_pair(13, true),  std::make_pair(12, true),  std::make_pair(11, false), std::make_pair(10, false), std::make_pair(9,  true),  std::make_pair(8,  true)},
                {std::make_pair(8,  false), std::make_pair(9,  true),  std::make_pair(10, true),  std::make_pair(11, true),  std::make_pair(12, true),  std::make_pair(13, true),  std::make_pair(14, true),  std::make_pair(15, true),  std::make_pair(0,  true),  std::make_pair(1,  false), std::make_pair(2,  false), std::make_pair(3,  false), std::make_pair(4,  false), std::make_pair(5,  false), std::make_pair(6,  false), std::make_pair(7,  false)},
                {std::make_pair(9,  false), std::make_pair(8,  false), std::make_pair(11, true),  std::make_pair(10, false), std::make_pair(13, true),  std::make_pair(12, false), std::make_pair(15, false), std::make_pair(14, true),  std::make_pair(1,  true),  std::make_pair(0,  true),  std::make_pair(3,  true),  std::make_pair(2,  false), std::make_pair(5,  true),  std::make_pair(4,  false), std::make_pair(7,  false), std::make_pair(6,  true)},
                {std::make_pair(10, false), std::make_pair(11, false), std::make_pair(8,  false), std::make_pair(9,  true),  std::make_pair(14, true),  std::make_pair(15, true),  std::make_pair(12, false), std::make_pair(13, false), std::make_pair(2,  true),  std::make_pair(3,  false), std::make_pair(0,  true),  std::make_pair(1,  true),  std::make_pair(6,  true),  std::make_pair(7,  true),  std::make_pair(4,  false), std::make_pair(5,  false)},
                {std::make_pair(11, false), std::make_pair(10, true),  std::make_pair(9,  false), std::make_pair(8,  false), std::make_pair(15, true),  std::make_pair(14, false), std::make_pair(13, true),  std::make_pair(12, false), std::make_pair(3,  true),  std::make_pair(2,  true),  std::make_pair(1,  false), std::make_pair(0,  true),  std::make_pair(7,  true),  std::make_pair(6,  false), std::make_pair(5,  true),  std::make_pair(4,  false)},
                {std::make_pair(12, false), std::make_pair(13, false), std::make_pair(14, false), std::make_pair(15, false), std::make_pair(8,  false), std::make_pair(9,  true),  std::make_pair(10, true),  std::make_pair(11, true),  std::make_pair(4,  true),  std::make_pair(5,  false), std::make_pair(6,  false), std::make_pair(7,  false), std::make_pair(0,  true),  std::make_pair(1,  true),  std::make_pair(2,  true),  std::make_pair(3,  true)},
                {std::make_pair(13, false), std::make_pair(12, true),  std::make_pair(15, false), std::make_pair(14, true),  std::make_pair(9,  false), std::make_pair(8,  false), std::make_pair(11, false), std::make_pair(10, true),  std::make_pair(5,  true),  std::make_pair(4,  true),  std::make_pair(7,  false), std::make_pair(6,  true),  std::make_pair(1,  false), std::make_pair(0,  true),  std::make_pair(3,  false), std::make_pair(2,  true)},
                {std::make_pair(14, false), std::make_pair(15, true),  std::make_pair(12, true),  std::make_pair(13, false), std::make_pair(10, false), std::make_pair(11, true),  std::make_pair(8,  false), std::make_pair(9,  false), std::make_pair(6,  true),  std::make_pair(7,  true),  std::make_pair(4,  true),  std::make_pair(5,  false), std::make_pair(2,  false), std::make_pair(3,  true),  std::make_pair(0,  true),  std::make_pair(1,  false)},
                {std::make_pair(15, false), std::make_pair(14, false), std::make_pair(13, true),  std::make_pair(12, true),  std::make_pair(11, false), std::make_pair(10, false), std::make_pair(9,  true),  std::make_pair(8,  false), std::make_pair(7,  true),  std::make_pair(6,  false), std::make_pair(5,  true),  std::make_pair(4,  true),  std::make_pair(3,  false), std::make_pair(2,  false), std::make_pair(1,  true),  std::make_pair(0,  true)},
        }};
    };



}


#if BOOST_UNITS_HAS_BOOST_TYPEOF

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TEMPLATE(dfpe::VectorC, 2)
#endif

namespace dfpe
{

    template<int N, typename T, typename U, typename B>
    constexpr VectorC<N, MUL(T,U), B> operator*(const T &left, const VectorC<N, U, B> &right) requires (boost::units::is_quantity<T>::value);

    template<int N, typename T, typename U, typename B>
    constexpr VectorC<N, U, B> operator*(const T &left, const VectorC<N, U, B> &right) requires (std::is_arithmetic_v<T>);

    template<int N, typename T, typename U, typename B>
    constexpr VectorC<N, DIV(T,U), B> operator/(const T &left, const VectorC<N, U, B> &right) requires (boost::units::is_quantity<T>::value);

    template<int N, typename T, typename U, typename B>
    constexpr VectorC<N, U, B> operator/(const T &left, const VectorC<N, U, B> &right) requires (std::is_arithmetic_v<T>);

    //template<int N, typename T, typename A>
    //constexpr std::ostream &operator<<(std::ostream &os, const VectorC<N, T, A> &right);

    template<int N, typename T, typename A>
    constexpr std::ostream &operator<<(std::ostream &os, const VectorC<N, T, A> &right);

    // Trigonometric and exponential functions for complex, quaternions, etc.

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> exp(const VectorC<N, T, A> &arg) requires (N < 0, boost::units::is_dimensionless<T>::value);

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> log(const VectorC<N, T, A> &arg) requires (N < 0, boost::units::is_dimensionless<T>::value);

    template<int P, int Q = 1, int N, typename T, typename A>
    constexpr VectorC<N, POWR(T,P,Q), A>
    pow(const VectorC<N, T, A> &arg) requires (N < 0);

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> cos(const VectorC<N, T, A> &arg) requires (N < 0, boost::units::is_dimensionless<T>::value);

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> sin(const VectorC<N, T, A> &arg) requires (N < 0, boost::units::is_dimensionless<T>::value);

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> tan(const VectorC<N, T, A> &arg) requires (N < 0, boost::units::is_dimensionless<T>::value);

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> cosh(const VectorC<N, T, A> &arg) requires (N < 0, boost::units::is_dimensionless<T>::value);

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> sinh(const VectorC<N, T, A> &arg) requires (N < 0, boost::units::is_dimensionless<T>::value);

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> tanh(const VectorC<N, T, A> &arg) requires (N < 0, boost::units::is_dimensionless<T>::value);
}
#include "VectorC.tcc"