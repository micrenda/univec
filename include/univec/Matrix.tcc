#include "Complex.hpp"
#include "Matrix.hpp"
#include "constants.hpp"
#include "enum.hpp"
#include <boost/units/cmath.hpp>
#include <boost/units/is_dimensionless.hpp>

namespace dfpe
{

    template<int M, int N, typename TT>
    constexpr TT &Matrix<M, N, TT>::operator()(unsigned int m, unsigned int n)
    {
        assert(m >= 1 && m <= M);
        assert(n >= 1 && n <= N);

        if (!(m >= 1 && m <= M)) [[unlikely]]
            throw std::runtime_error("Matrix: (m,) index must be between 1 and " + std::to_string(M));
        if (!(n >= 1 && n <= N)) [[unlikely]]
            throw std::runtime_error("Matrix: (,n) index must be between 1 and " + std::to_string(N));

        return m_a[m - 1][n - 1];
    }

    template<int M, int N, typename TT>
    constexpr const TT &Matrix<M, N, TT>::operator()(unsigned int m, unsigned int n) const
    {
        return this(m, n);
    }


    template<int M, int N, typename TT>
    constexpr TT &Matrix<M, N, TT>::rowMajor(unsigned int i)
    {
        return m_a[i/N][i%N];
    }

    template<int M, int N, typename TT>
    constexpr const TT &Matrix<M, N, TT>::rowMajor(unsigned int i) const
    {
        return m_a[i/N][i%N];
    }

    template<int M, int N, typename TT>
    constexpr TT &Matrix<M, N, TT>::colMajor(unsigned int i)
    {
        return m_a[i%M][i/M];
    }


    template<int M, int N, typename TT>
    constexpr const TT &Matrix<M, N, TT>::colMajor(unsigned int i) const
    {
        return m_a[i%M][i/M];
    }



#if __cpp_multidimensional_subscript
    template<int M, int N, typename TT>
    constexpr TT &Matrix<M, N, TT>::operator[](unsigned int i, unsigned int j)
    {
        assert(i >= 0 && i <= M-1);
        assert(j >= 0 && j <= N-1);

        if (!(i >= 0 && i <= M-1)) [[unlikely]]
            throw std::runtime_error("Matrix: [i,] index must be between 0 and " + std::to_string(M-1));
        if (!(j >= 0 && j <= N-1)) [[unlikely]]
            throw std::runtime_error("Matrix: [,j] index must be between 0 and " + std::to_string(N-1));

       return m_a[i][j];
    }

    template<int M, int N, typename TT>
    constexpr const TT &Matrix<M, N, TT>::operator[](unsigned int i, unsigned int j) const
    {
        assert(i >= 0 && i <= M-1);
        assert(j >= 0 && j <= N-1);

        if (!(i >= 0 && i <= M-1)) [[unlikely]]
            throw std::runtime_error("Matrix: [i,] index must be between 0 and " + std::to_string(M-1));
        if (!(j >= 0 && j <= N-1)) [[unlikely]]
            throw std::runtime_error("Matrix: [,j] index must be between 0 and " + std::to_string(N-1));

       return m_a[i][j];
    }
#endif


    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, TT>::Matrix(const Matrix<M, N, UU> &other)
    {
        mat_check_type_compatibility<TT,UU>();
        m_a = other.m_a;
    }


    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, TT>::Matrix(const Matrix<M, N, UU> &other) requires ( TT::dimensionality < 0 && TT::dimensionality < UU::dimensionality )
    {
        mat_check_type_compatibility<TT, UU>();

        for (unsigned int i = 0; i < M; i++)
        {
            for (unsigned int j = 0; j < N; j++)
            {
                for (int d = 0; d < UU::size; d++)
                {
                    m_a[i][j][d] = other.m_a[i][j][d];
                }
            }
        }
    }

    /// Create a matrix from its components
    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, TT>::Matrix(const std::array<UU, M * N> &values)
    {
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                m_a[i][j] = values[i * N + j];
    }


    /* Addition and addition related tests */
    // ----------------------------------------------------------------------------------------------------------------
    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, TT> &Matrix<M, N, TT>::operator+=(const Matrix<M, N, UU> &right)
    {
        (*this) = (*this) + right;
        return (*this);
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, UADD(TT)> operator+(const Matrix<M, N, TT> &right)
    {
        return right;
    }

    template<int M, int N, typename TT, typename UU>
    constexpr Matrix<M, N, M_ADD(TT,UU)> operator+(const Matrix<M, N, TT> &left, const Matrix<M, N, UU> &right)
    {
        Matrix<M, N, TT> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = left.m_a[i][j] + right.m_a[i][j];

        return result;
    }


    /* Subtraction and subtraction related tests */
    // ----------------------------------------------------------------------------------------------------------------
    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, TT> &Matrix<M, N, TT>::operator-=(const Matrix<M, N, UU> &right)
    {
        (*this) = (*this) - right;
        return (*this);
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, M_USUB(TT)> operator-(const Matrix<M, N, TT> &right)
    {
        Matrix<M, N, TT> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = -right.m_a[i][j];

        return result;
    }

    template<int M, int N, typename TT, typename UU>
    constexpr Matrix<M, N, M_SUB(TT,UU)> operator-(const Matrix<M, N, TT> &left, const Matrix<M, N, UU> &right)
    {
        Matrix<M, N, TT> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = left.m_a[i][j] - right.m_a[i][j];
        return result;
    }

    /* Multiplication and multiplication related tests */
    // ----------------------------------------------------------------------------------------------------------------
    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, TT> &Matrix<M, N, TT>::operator*=(const UU &right)
    {
        (*this) = (*this) * right;
        return (*this);
    }

    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, TT> &Matrix<M, N, TT>::operator*=(const Matrix<M, N, UU> &right) requires (M == N)
    {
        (*this) = (*this) * right;
        return (*this);
    }


    template<int M, int N, int P, typename TT, typename UU>
    constexpr Matrix<M, P, M_MUL(TT,UU)> operator*(const Matrix<M, N, TT> &left, const Matrix<N, P, UU> &right)
    {
        Matrix<M, P, MUL(TT,UU)> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
        {
            for (unsigned int j = 0; j < P; j++)
            {
                MUL(TT,UU) s;
                for (unsigned int k = 0; k < N; k++)
                    s += left.m_a[i][k] * right.m_a[k][j];
                result.m_a[i][j] = s;
            }
        }
        return result;
    }

    template<int M, int N, typename TT, typename U>
    constexpr Matrix<M, N, L_MUL(TT,U)> operator*(const Matrix<M, N, TT> &left, const U &right) requires (boost::units::is_quantity<U>::value || std::is_arithmetic_v<U>)
    {
        Matrix<M, N, L_MUL(TT,U)> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = left.m_a[i][j] * right;
        return result;
    }

    template<int M, int N, typename TT, typename U, typename B>
    constexpr VectorC<M, MUL(typename TT::quantity_type,U)> operator*(const Matrix<M, N, TT> &left, const VectorC<N,U,B> &right) requires (TT::real)
    {
        VectorC<M,MUL(typename TT::quantity_type,U)> result;
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
            {
                result[i] += left.m_a[i][j][0] * right[j];
            }

        return result;
    }

    template<int M, int N, typename TT, typename UU>
    constexpr Matrix<M, N, M_MUL(TT,UU)> operator*(const TT &left, const Matrix<M, N, UU> &right)
    {
        Matrix<M, N, M_MUL(TT,UU)> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = left * right.m_a[i][j];
        return result;
    }

    template<int M, int N, typename T, typename UU>
    constexpr Matrix<M, N, R_MUL(T,UU)> operator*(const T &left, const Matrix<M, N, UU> &right) requires (boost::units::is_quantity<T>::value || std::is_arithmetic_v<T>)
    {
        Matrix<M, N, R_MUL(T,UU)> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = left * right.m_a[i][j];
        return result;
    }


    /* Division and division related tests */
    // ----------------------------------------------------------------------------------------------------------------
    template<int M, int N, typename TT>
    template<typename UU>
    constexpr const Matrix<M, N, TT> &Matrix<M, N, TT>::operator/=(const UU &right)
    {
        (*this) = (*this) / right;
        return (*this);
    }

    template<int M, int N, typename TT, typename UU>
    constexpr Matrix<M, N, M_DIV(TT,UU)> operator/(const Matrix<M, N, TT> &left, const UU &right)
    {
        Matrix<M, N, M_DIV(TT,UU)> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = left.m_a[i][j] / right;
        return result;
    }

    template<int M, int N, typename TT, typename U>
    constexpr Matrix<M, N, L_DIV(TT,U)> operator/(const Matrix<M, N, TT> &left, const U &right) requires (boost::units::is_quantity<U>::value || std::is_arithmetic_v<U>)
    {
        Matrix<M, N, L_DIV(TT,U)> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = left.m_a[i][j] / right;
        return result;
    }

    /* Comparative operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<int M, int N, typename TT>
    template<typename UU>
    constexpr bool Matrix<M, N, TT>::operator==(const Matrix<M, N, UU> &right) const
    {
        mat_check_type_compatibility<TT,UU>();
        return Matrix<M, N, TT>::m_a == right.m_a;
    }

    template<int M, int N, typename TT>
    template<typename UU>
    constexpr bool Matrix<M, N, TT>::operator!=(const Matrix<M, N, UU> &right) const
    {
        mat_check_type_compatibility<TT,UU>();
        return Matrix<M, N, TT>::m_a != right.m_a;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<N, M, TT>& Matrix<M, N, TT>::doT() requires (M == N)
    {
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = i + 1; j < N; j++)
                std::swap(m_a[i][j], m_a[j][i]);
        return *this;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<N, M, TT>& Matrix<M, N, TT>::doConjT() requires (M == N)
    {
        doT();
        if constexpr (!real)
            doConj();
        return *this;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<N, M, TT>& Matrix<M, N, TT>::doConj()
    {
        if constexpr (!real)
        {
            // Cycling from zero based array to optimize performances
            for (unsigned int i = 0; i < M; i++)
                for (unsigned int j = 0; j < N; j++)
                    m_a[i][j] = m_a[i][j].conj();
        }

        return *this;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<N, M, TT> Matrix<M, N, TT>::t() const
    {
        Matrix<N, M, TT> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[j][i] = m_a[i][j];
        return result;
    }

    template<int M, int N, typename TT>
    CONSTEVAL_WA unsigned int Matrix<M, N, TT>::order() const requires(M==N)
    {
        return N;
    }

    template<int M, int N, typename TT>
    CONSTEVAL_WA std::pair<unsigned int,unsigned int> Matrix<M, N, TT>::orders() const
    {
        return std::make_pair(M,N);
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isNull(const TOL& tol ) const
    {
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (!tol.isZero(m_a[i][j]))
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    template<typename UU, typename TOL>
    constexpr bool Matrix<M, N, TT>::isNear(const Matrix<M, N, UU> &other, const TOL& tol ) const requires (TT::dimensionality == UU::dimensionality)
    {
        mat_check_type_compatibility<TT,UU>();
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (!m_a[i][j].isNear(other.m_a[i][j], tol))
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isIdentity(const TOL& tol) const requires (M == N)
    {
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (!(i == j ? tol.isOne(m_a[i][j]) : tol.isZero(m_a[i][j])))
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    constexpr TT Matrix<M, N, TT>::tr() const
        requires(M == N)
    {
        // Cycling from zero based array to optimize performances
        TT result;
        for (unsigned int i = 0; i < M; i++)
            result += m_a[i][i];
        return result;
    }

    template<int M, int N, typename TT>
    constexpr M_POW(TT, N) Matrix<M, N, TT>::diagonalProduct() const
        requires(M == N)
    {
        M_DL(TT)        result =  M_DL(TT)(M_DL(TT)::quantity_type::from_value(1));

        for (unsigned int i = 0; i < M; i++)
            result *= m_a[i][i] / TT(TT::quantity_type::from_value(1));
        return result * M_POW(TT, N)(M_POW(TT, N)::quantity_type::from_value(1));
    }

    template<int M, int N, typename TT>
    constexpr TT Matrix<M, N, TT>::grandSum() const
    {
        TT result;
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result += m_a[i][j];
        return result;
    }

    template<int M, int N, typename TT>
    constexpr typename TT::quantity_type Matrix<M, N, TT>::normL1() const
    {
        typename TT::quantity_type result;
        for (unsigned int j = 0; j < N; j++)
        {
            typename TT::quantity_type buffer;
            for (unsigned int i = 0; i < M; i++)
                buffer += m_a[i][j].norm();
            if (buffer > result)
                result = buffer;
        }
        return result;

    }

    template<int M, int N, typename TT>
    constexpr typename TT::quantity_type Matrix<M, N, TT>::normLInf() const
    {
        // Cycling from zero based array to optimize performances
        typename TT::quantity_type result;
        for (unsigned int i = 0; i < M; i++)
        {
            typename TT::quantity_type buffer;
            for (unsigned int j = 0; j < N; j++)
                buffer += m_a[i][j].norm();

            if (buffer > result)
                result = buffer;
        }
        return result;
    }

    template<int M, int N, typename TT>
    constexpr typename TT::quantity_type Matrix<M, N, TT>::normLMax() const
    {
        typename TT::quantity_type result;
        for (unsigned int i = 0; i < M; i++)
        {
            for (unsigned int j = 0; j < N; j++)
            {
                typename TT::quantity_type buffer = m_a[i][j].norm();
                if (buffer > result)
                    result = buffer;
            }
        }
        return result;
    }

    template<int M, int N, typename TT>
    template<int P, int Q>
    constexpr typename TT::quantity_type Matrix<M, N, TT>::normL() const
    {
        POWR(typename TT::quantity_type,1,Q) sumN;
        // Cycling from zero based array to optimize performances
        for (unsigned int j = 0; j < N; j++)
        {
            POW(typename TT::quantity_type,P) sumM;
            for (unsigned int i = 0; i < M; i++)
                sumM += pow(m_a[i][j].norm(), P);
            sumN += boost::units::pow<boost::units::static_rational<Q,P>>(sumM.norm());
        }
        return pow<1,Q>(sumN);
    }

    template<int M, int N, typename TT>
    constexpr typename TT::quantity_type Matrix<M, N, TT>::normF() const
    {
        POW(typename TT::quantity_type,2) result;

        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result += m_a[i][j].normSquared();

        return sqrt(result);
    }

    template<int M, int N, typename TT>
    constexpr typename TT::quantity_type Matrix<M, N, TT>::norm() const requires (M == 1 || N == 1)
    {
        return sqrt(normSquared());
    }

    template<int M, int N, typename TT>
    constexpr POW(typename TT::quantity_type,2) Matrix<M, N, TT>::normSquared() const requires (M == 1 || N == 1)
    {
        POW(typename TT::quantity_type,2) result;

        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result += m_a[i][j].normSquared();

        return result;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isDiagonal(const TOL& tol) const requires (M == N)
    {
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (i != j && !tol.isZero(m_a[i][j]))
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isUnitary(const TOL &tol) const
        requires(M == N)
    {
        return conjT().isNear(inv(), tol);
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isTriangular(const TOL &tol) const
        requires(M == N)
    {
        return isUpperTriangular(tol) || isLowerTriangular();
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isUpperTriangular(const TOL &tol) const
        requires(M == N)
    {
        // Cycling from zero based array to optimize performances
        for (unsigned int j = 0; j < N; j++)
            for (unsigned int i = j + 1; i < M; i++)
                if (!tol.isZero(m_a[i][j]))
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isLowerTriangular(const TOL &tol) const
        requires(M == N)
    {
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = i + 1; j < N; j++)
                if (!tol.isZero(m_a[i][j]))
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isStrictTriangular(const TOL &tol) const
        requires(M == N)
    {
        if (!isTriangular(tol))
            return false;

        for (unsigned int i = 0; i < M; i++)
        {
            if (!tol.isZero(m_a[i][i])) [[unlikely]]
                return false;
        }

        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isUniTriangular(const TOL &tol) const
        requires(M == N)
    {
        if (!isTriangular(tol))
            return false;

        for (unsigned int i = 0; i < M; i++)
        {
            if (!tol.isOne(m_a[i][i])) [[unlikely]]
                return false;
        }

        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isSymmetric(const TOL &tol) const
        requires(M == N)
    {
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (!tol.isNear(m_a[i][j], m_a[j][i]))
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isSkewSymmetric(const TOL& tol) const requires (M == N)
    {
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (!tol.isNear(m_a[i][j], -m_a[j][i]))
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isHermitian(const TOL& tol) const requires (M == N)
    {
        if constexpr (!real)
        {
            // Cycling from zero based array to optimize performances
            for (unsigned int i = 0; i < M; i++)
                for (unsigned int j = 0; j < N; j++)
                    if (!tol.isNear(m_a[i][j], m_a[j][i].conj()))
                        return false;
            return true;
        }
        else
            return isSymmetric(tol);
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isSkewHermitian(const TOL& tol) const requires (M == N)
    {
        if constexpr (!real)
        {
            // Cycling from zero based array to optimize performances
            for (unsigned int i = 0; i < M; i++)
                for (unsigned int j = 0; j < N; j++)
                    if (!tol.isNear(m_a[i][j], -m_a[j][i].conj()))
                        return false;
            return true;
        }
        else
            return isSkewSymmetric(tol);
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M-1, N-1, TT> Matrix<M, N, TT>::sub(unsigned int m, unsigned int n) const
    {
        Matrix<M-1, N-1, TT> result;

        unsigned int offsetM = 0;

        // Cycling from zero based array to optimize performances
        for (unsigned i = 0; i < M; i++)
        {
            if (i == m - 1)
            {
                offsetM = 1;
                continue;
            }

            unsigned int offsetN = 0;

            for (unsigned int j = 0; j < N; j++)
            {
                if (j == n - 1)
                {
                    offsetN = 1;
                    continue;
                }

                result.m_a[i - offsetM][j - offsetN] = m_a[i][j];
            }
        }

        return result;
    }

    template<int M, int N, typename TT>
    constexpr M_POW(TT, N) Matrix<M, N, TT>::det(DeterminantMethod method) const
        requires(M == N)
    {
        if constexpr (M == 1 && N == 1)
        {
            return m_a[0][0];
        } else
        {
            DeterminantMethod actualMethod = method;

            if (method == DeterminantMethod::AUTO)
            {
                if constexpr (M <= 4 && N <= 4)
                    actualMethod = DeterminantMethod::LEIBNIZ_FORMULA;
                else
                    actualMethod = DeterminantMethod::GAUSSIAN_ELIMINATION;
            }
            assert(actualMethod != DeterminantMethod::AUTO);

            if (actualMethod == DeterminantMethod::LEIBNIZ_FORMULA)
            {
                if constexpr(N == 2)
                {
                    return m_a[0][0] * m_a[1][1] - m_a[0][1] * m_a[1][0];
                }
                else if constexpr (N == 3)
                {
                    return m_a[0][0] * m_a[1][1] * m_a[2][2] + m_a[0][1] * m_a[1][2] * m_a[2][0] + m_a[0][2] * m_a[1][0] * m_a[2][1] -
                           m_a[0][2] * m_a[1][1] * m_a[2][0] - m_a[0][1] * m_a[1][0] * m_a[2][2] - m_a[0][0] * m_a[1][2] * m_a[2][1];
                }
                else if constexpr (N == 4)
                {
                    return m_a[0][0] * m_a[1][1] * m_a[2][2] * m_a[3][3] + m_a[0][0] * m_a[1][2] * m_a[2][3] * m_a[3][1] +
                           m_a[0][0] * m_a[1][3] * m_a[2][1] * m_a[3][2] + m_a[0][1] * m_a[1][0] * m_a[2][3] * m_a[3][2] +
                           m_a[0][1] * m_a[1][2] * m_a[2][0] * m_a[3][3] + m_a[0][1] * m_a[1][3] * m_a[2][2] * m_a[3][0] +
                           m_a[0][2] * m_a[1][0] * m_a[2][1] * m_a[3][3] + m_a[0][2] * m_a[1][1] * m_a[2][3] * m_a[3][0] +
                           m_a[0][2] * m_a[1][3] * m_a[2][0] * m_a[3][1] + m_a[0][3] * m_a[1][0] * m_a[2][2] * m_a[3][1] +
                           m_a[0][3] * m_a[1][1] * m_a[2][0] * m_a[3][2] + m_a[0][3] * m_a[1][2] * m_a[2][1] * m_a[3][0] -
                           m_a[0][0] * m_a[1][1] * m_a[2][3] * m_a[3][2] - m_a[0][0] * m_a[1][2] * m_a[2][1] * m_a[3][3] -
                           m_a[0][0] * m_a[1][3] * m_a[2][2] * m_a[3][1] - m_a[0][1] * m_a[1][0] * m_a[2][2] * m_a[3][3] -
                           m_a[0][1] * m_a[1][2] * m_a[2][3] * m_a[3][0] - m_a[0][1] * m_a[1][3] * m_a[2][0] * m_a[3][2] -
                           m_a[0][2] * m_a[1][0] * m_a[2][3] * m_a[3][1] - m_a[0][2] * m_a[1][1] * m_a[2][0] * m_a[3][3] -
                           m_a[0][2] * m_a[1][3] * m_a[2][1] * m_a[3][0] - m_a[0][3] * m_a[1][0] * m_a[2][1] * m_a[3][2] -
                           m_a[0][3] * m_a[1][1] * m_a[2][2] * m_a[3][0] - m_a[0][3] * m_a[1][2] * m_a[2][0] * m_a[3][1];
                }
                else
                {
                    throw std::runtime_error("Leibniz formula is not yet implemented for matrices with size greater than 4");
                }
            }
            if (actualMethod == DeterminantMethod::LAPLACE_FORMULA)
            {
                M_POW(TT, N)
                result;

                // Cycling from zero based array to optimize performances
                unsigned int i = 0;
                {
                    for (unsigned int j = 0; j < N; j++)
                    {
                        result += ((i + j) % 2 == 0 ? 1. : -1.) * m_a[i][j] * sub(i + 1, j + 1).det();
                    }
                }
                return result;
            }
             else if (actualMethod == DeterminantMethod::GAUSSIAN_ELIMINATION)
            {
                Matrix<M, N, TT> buffer = *this;
                DL(TT) scaling;
                buffer.doReduction(ReductionMethod::GAUSS_ELIMINATION, &scaling);
                return scaling * buffer.diagonalProduct();
            }
            else if (actualMethod == DeterminantMethod::GAUSSIAN_JORDAN_ELIMINATION)
            {
                Matrix<M, N, TT> buffer = *this;
                DL(TT) scaling;
                buffer.doReduction(ReductionMethod::GAUSS_JORDAN_ELIMINATION, &scaling);
                return scaling * M_POW(TT, N)::quantity_type::from_value(1.);
            } else
            {
                throw std::runtime_error("Determinant method not yet supported: " + std::to_string((int) method));
            }
        }

    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isOrthogonal(const TOL& tol) const requires (M == N)
    {
        return (*this * this->t()).isIdentity(MatTolerance<POW(TT,2)>(tol * tol));
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isSingular(const TOL& tol) const requires (M == N)
    {
        return det().isNull(MatTolerance<M_POWR(TT,N,1)>(ipow<N,1>(tol)));
    }

    template<int M, int N, typename TT>
    constexpr M_POW(TT,N-1) Matrix<M, N, TT>::minor(unsigned int m, unsigned int n) const requires (M == N)
    {
        return sub(m, n).det();
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, M_POW(TT,N-1)> Matrix<M, N, TT>::minors() requires (M == N)
    {
        Matrix<M, N, M_POW(TT,N-1)> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = minor(i + 1, j + 1);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr M_POW(TT,N-1) Matrix<M,N, TT>::cofactor(unsigned int m, unsigned int n) const requires (M == N)
    {
        return ((m + n) % 2 == 0 ? 1. : -1.) * minor(m, n);
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, M_POW(TT,N-1)> Matrix<M, N, TT>::cofactors() const requires (M == N)
    {
        Matrix<M, N, M_POW(TT,N-1)> result;
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result.m_a[i][j] = cofactor(i + 1, j + 1);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, M_POW(TT,N-1)>
    Matrix<M, N, TT>::adj() const requires (M == N)
    {
        return cofactors().t();
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT>
    Matrix<M, N, TT>::conj() const
    {
        if constexpr (!real)
        {
            Matrix<M, N, TT> result;
            for (unsigned int i = 0; i < M; i++)
                for (unsigned int j = 0; j < N; j++)
                    result.m_a[i][j] = m_a[i][j].conj();
            return result;
        }
        else
            return *this;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<N, M, TT>
    Matrix<M, N, TT>::conjT() const
    {
        if constexpr (!real )
            return t().conj();
        else
            return t();
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M,N,M_POW(TT,-1)> Matrix<M, N, TT>::inv() const requires (M == N)
    {
        return adj() / det();
    }


    template<int M, int N, typename TT>
    constexpr Matrix<1, N, TT> Matrix<M, N, TT>::row(int m)
    {
        Matrix<1, N, TT> result;
        result.m_a[0] = m_a[m-1];
        return result;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, 1, TT> Matrix<M, N, TT>::col(int n)
    {
        Matrix<M, 1, TT> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < M; i++)
            result.m_a[i][0] = m_a[i][n-1];
        return result;
    }

    /* Validity check operations */
    // ----------------------------------------------------------------------------------------------------------------
    template<int M, int N, typename TT>
    constexpr bool dfpe::Matrix<M, N, TT>::isFinite() const
    {
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (!m_a[i][j].isFinite())
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    constexpr bool dfpe::Matrix<M, N, TT>::isNormal() const
    {
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (!m_a[i][j].isNormal())
                    return false;
        return true;
    }

    template<int M, int N, typename TT>
    constexpr bool dfpe::Matrix<M, N, TT>::isNan() const
    {
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (m_a[i][j].isNan())
                    return true;
        return false;
    }

    template<int M, int N, typename TT>
    constexpr bool dfpe::Matrix<M, N, TT>::isInfinite() const
    {
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (m_a[i][j].isInfinite())
                    return true;
        return false;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, 1, TT> Matrix<M,N,TT>::diagonal() const requires (M == N)
    {
        Matrix<M, 1, TT> result;
        for (unsigned int i = 0; i < M; i++)
            result.m_a[i][0] = m_a[i][i];

        return result;
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M,N,TT>::zap(TT threshold) const
    {
        Matrix<M, N, TT> result = *this;
        result.doZap(threshold);
        return result;
    }


    template<int M, int N, typename TT>
    constexpr void Matrix<M,N,TT>::doZap(TT threshold)
    {
        auto thresholdNormSquared = threshold.normSquared();
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                if (m_a[i][j].normSquared() < thresholdNormSquared)
                    m_a[i][j] = TT();
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M,N,TT>::doSortRows(bool descending )
    {
        auto fnSortingAscending = [](const std::array<TT, N>& a, std::array<TT, N> b)
        {
            for ( int j = 0; j < N; j++)
            {
                if (a[j] != b[j])
                    return a[j].normSquared() < b[j].normSquared();
            }
            return false;
        };

        auto fnSortingDescending = [](const std::array<TT, N>& a, std::array<TT, N> b)
        {
            for ( int j = 0; j < N; j++)
            {
                if (a[j] != b[j])
                    return a[j].normSquared() > b[j].normSquared();
            }
            return false;
        };

        if (!descending)
            std::sort(m_a.begin(), m_a.end(), fnSortingAscending);
        else
            std::sort(m_a.begin(), m_a.end(), fnSortingDescending);

    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M,N,TT>::sortRows(bool descending) const
    {
        Matrix<M,N,TT> result = *this;
        result.doSortRows(descending);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M,N,TT>::doSortCols(bool descending )
    {
        Matrix<N,M,TT> buffer = t();
        buffer.doSortRows(descending);
        *this = buffer.t();
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M,N,TT>::sortCols(bool descending ) const
    {
        Matrix<N,M,TT> buffer = t();
        buffer.doSortRows(descending);
        return buffer.t();
    }


    template<int M, int N, typename TT>
    template<int P, int Q, typename UU>
    constexpr Matrix<M*P, N*Q, M_MUL(TT,UU)> Matrix<M,N,TT>::kroneckerProduct(const Matrix<P, Q, UU>& other) const
    {
        Matrix<M * P, N * Q, M_MUL(TT, UU) > result;

        for (int i1 = 0; i1 < M; i1++)
            for (int j1 = 0; j1 < N; j1++)
                for (int i2 = 0; i2 < P; i2++)
                    for (int j2 = 0; j2 < Q; j2++)
                        result.m_a[i1 * P + i2][j1 * Q + j2] = m_a[i1][j1] * other.m_a[i2][j2];

        return result;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isRowEchelon(const TOL &tol)
    {
        int leading = -1;
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (!tol.isZero(m_a[i][j]))
                {
                    // The leading entry of every nonzero row is to the right of the leading entry of every row above.
                    if (j <= leading)
                        return false;

                    leading = j;
                    break;
                }
            }
        }
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isColEchelon(const TOL &tol)
    {
        int leading = -1;
        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < M; i++)
            {
                if (!tol.isZero(m_a[i][j]))
                {
                    // The leading entry of every nonzero column is below the leading entry of every column on the left.
                    if (i <= leading)
                        return false;

                    leading = i;
                    break;
                }
            }
        }
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isRedRowEchelon(const TOL &tol)
    {
        int leading = -1;
        for (int i = 0; i < M; i++)
        {
            for (int j = 0; j < N; j++)
            {
                if (!tol.isZero(m_a[i][j]))
                {
                    // The leading entry of every nonzero row is to the right of the leading entry of every row above.
                    if (j <= leading)
                        return false;

                    // The leading entry in any nonzero row is 1.
                    if (!tol.isOne(m_a[i][j]))
                        return false;

                    // The leading entry in each nonzero row is 1 and is the only nonzero entry in its column
                    for (int r = 0; r < M; r++)
                    {
                        if (r != j && !tol.isZero(m_a[r][j]))
                            return false;
                    }

                    leading = j;
                    break;
                }
            }
        }
        return true;
    }

    template<int M, int N, typename TT>
    template<typename TOL>
    constexpr bool Matrix<M, N, TT>::isRedColEchelon(const TOL &tol)
    {
        int leading = -1;
        for (int j = 0; j < N; j++)
        {
            for (int i = 0; i < M; i++)
            {
                if (!tol.isZero(m_a[i][j]))
                {
                    // The leading entry of every nonzero column is below the leading entry of every column on the left.
                    if (i <= leading)
                        return false;

                    // The leading entry in any nonzero column is 1.
                    if (!tol.isOne(m_a[i][j]))
                        return false;

                    // The leading entry in each nonzero column is 1 and is the only nonzero entry in its row
                    for (int c = 0; c < N; c++)
                    {
                        if (c != j && !tol.isZero(m_a[i][c]))
                            return false;
                    }

                    leading = i;
                    break;
                }
            }
        }
        return true;
    }

    template<int M, int N, typename TT>
    template<int P, int Q, typename UU>
    constexpr Matrix<M + P, N + Q, TT>
    Matrix<M, N, TT>::directSum(const Matrix<P, Q, UU> &other) const
    {
        Matrix<M + P, N + Q, TT> result;

        for (int i1 = 0; i1 < M; i1++)
            for (int j1 = 0; j1 < N; j1++)
                result.m_a[i1][j1] = m_a[i1][j1];

        for (int i2 = 0; i2 < P; i2++)
            for (int j2 = 0; j2 < Q; j2++)
                result.m_a[M + i2][N + j2] = other.m_a[i2][j2];
        ;

        return result;
    }


    template<int M, int N, typename TT>
    template<typename U>
    constexpr void Matrix<M, N, TT>::doRowMul(int i, const U &other)
    {
        for (int j = 0; j < N; ++j)
            m_a[i][j] *= other;
    }

    template<int M, int N, typename TT>
    template<typename U>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::rowMul(int i, const U &other) const
    {
        Matrix<M, N, TT> result(*this);
        result.doRowMul(i, other);
        return result;
    }


    template<int M, int N, typename TT>
    template<typename U>
    constexpr void Matrix<M, N, TT>::doColMul(int j, const U &other)
    {
        for (int i = 0; i < M; ++i)
            m_a[i][j] *= other;
    }

    template<int M, int N, typename TT>
    template<typename U>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::colMul(int j, const U &other) const
    {
        Matrix<M, N, TT> result(*this);
        result.doColMul(j, other);
        return result;
    }

    template<int M, int N, typename TT>
    template<typename U>
    constexpr void Matrix<M, N, TT>::doRowDiv(int i, const U &other)
    {
        for (int j = 0; j < N; ++j)
            m_a[i][j] /= other;
    }

    template<int M, int N, typename TT>
    template<typename U>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::rowDiv(int i, const U &other) const
    {
        Matrix<M, N, TT> result(*this);
        result.doRowDiv(i, other);
        return result;
    }

    template<int M, int N, typename TT>
    template<typename U>
    constexpr void Matrix<M, N, TT>::doColDiv(int j, const U &other)
    {
        for (int i = 0; i < M; ++i)
            m_a[i][j] /= other;
    }

    template<int M, int N, typename TT>
    template<typename U>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::colDiv(int j, const U &other) const
    {
        Matrix<M, N, TT> result(*this);
        result.doColDiv(j, other);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doRowAdd(int i1, int i2, std::optional<M_DL(TT)> s1, std::optional<M_DL(TT)> s2)
    {
        if (!s1.has_value() && !s2.has_value())
            for (int j = 0; j < N; ++j)
                m_a[i2][j] = m_a[i2][j] + m_a[i1][j];
        else if (s1.has_value() && !s2.has_value())
            for (int j = 0; j < N; ++j)
                m_a[i2][j] = m_a[i2][j] + *s1 * m_a[i1][j];
        else if (!s1.has_value() && s2.has_value())
            for (int j = 0; j < N; ++j)
                m_a[i2][j] = *s2 * m_a[i2][j] + m_a[i1][j];
        else
            for (int j = 0; j < N; ++j)
                m_a[i2][j] = *s2 * m_a[i2][j] + s1.value() * m_a[i1][j];

    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::rowAdd(int i1, int i2, std::optional<M_DL(TT)> s1, std::optional<M_DL(TT)> s2) const
    {
        Matrix<M, N, TT> result(*this);
        result.doRowAdd(i1, i2, s1, s2);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doColAdd(int j1, int j2, std::optional<M_DL(TT)> s1, std::optional<M_DL(TT)> s2)
    {
        if (!s1.has_value() && !s2.has_value())
            for (int i = 0; i < M; ++i)
                m_a[i][j2] = m_a[i][j2] + m_a[i][j1];
        else if (s1.has_value() && !s2.has_value())
            for (int i = 0; i < M; ++i)
                m_a[i][j2] = m_a[i][j2] + *s1 * m_a[i][j1];
        else if (!s1.has_value() && s2.has_value())
            for (int i = 0; i < M; ++i)
                m_a[i][j2] = *s2 * m_a[i][j2] + m_a[i][j1];
        else
            for (int i = 0; i < M; ++i)
                m_a[i][j2] = *s2 * m_a[i][j2] + *s1 * m_a[i][j1];
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::colAdd(int j1, int j2, std::optional<M_DL(TT)> s1, std::optional<M_DL(TT)> s2) const
    {
        Matrix<M, N, TT> result(*this);
        result.doColAdd(j1, j2, s1, s2);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doRowSub(int i1, int i2, std::optional<M_DL(TT)> s1, std::optional<M_DL(TT)> s2)
    {
        if (!s1.has_value() && !s2.has_value())
            for (int j = 0; j < N; ++j)
                m_a[i2][j] = m_a[i2][j] - m_a[i1][j];
        else if (s1.has_value() && !s2.has_value())
            for (int j = 0; j < N; ++j)
                m_a[i2][j] = m_a[i2][j] - *s1 * m_a[i1][j];
        else if (!s1.has_value() && s2.has_value())
            for (int j = 0; j < N; ++j)
                m_a[i2][j] = *s2 * m_a[i2][j] - m_a[i1][j];
        else
            for (int j = 0; j < N; ++j)
                m_a[i2][j] = *s2 * m_a[i2][j] - *s1 * m_a[i1][j];
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::rowSub(int i1, int i2, std::optional<M_DL(TT)> s1, std::optional<M_DL(TT)> s2) const
    {
        Matrix<M, N, TT> result(*this);
        result.doRowSub(i1, i2, s1, s2);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doColSub(int j1, int j2, std::optional<M_DL(TT)> s1, std::optional<M_DL(TT)> s2)
    {
        if (!s1.has_value() && !s2.has_value())
            for (int i = 0; i < M; ++i)
                m_a[i][j2] = m_a[i][j2] - m_a[i][j1];
        else if (s1.has_value() && !s2.has_value())
            for (int i = 0; i < M; ++i)
                m_a[i][j2] = m_a[i][j2] - *s1 * m_a[i][j1];
        else if (!s1.has_value() && s2.has_value())
            for (int i = 0; i < M; ++i)
                m_a[i][j2] = *s2 * m_a[i][j2] - m_a[i][j1];
        else
            for (int i = 0; i < M; ++i)
                m_a[i][j2] = *s2 * m_a[i][j2] - *s1 * m_a[i][j1];
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::colSub(int j1, int j2, std::optional<M_DL(TT)> s1, std::optional<M_DL(TT)> s2) const
    {
        Matrix<M, N, TT> result(*this);
        result.doColSub(j1, j2, s1, s2);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doRowSwap(int i1, int i2)
    {
        std::swap(m_a[i1], m_a[i2]);
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::rowSwap(int i1, int i2) const
    {
        Matrix<M, N, TT> result(*this);
        result.doRowSwap(i1, i2);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doColSwap(int j1, int j2)
    {
        for (int i = 0; i < M; i++)
            std::swap(m_a[i][j1], m_a[i][j2]);
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::colSwap(int j1, int j2) const
    {
        Matrix<M, N, TT> result(*this);
        result.doColSwap(j1, j2);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doReverseRows()
    {
        std::reverse(m_a.begin(), m_a.end());
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::reverseRows() const
    {
        Matrix<M, N, TT> result(*this);
        result.doReverseRows();
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doReverseCols()
    {
        for (int i = 0; i < M; i++)
            std::reverse(m_a[i].begin(), m_a[i].end());
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::reverseCols() const
    {
        Matrix<M, N, TT> result(*this);
        result.doReverseCols();
        return result;
    }

    template<int M, int N, typename TT>
    template <class URNG>
    constexpr void Matrix<M, N, TT>::doShuffleRows(URNG&& g)
    {
        std::shuffle(m_a.begin(), m_a.end(), g);
    }

    template<int M, int N, typename TT>
    template <class URNG>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::shuffleRows(URNG&& g) const
    {
        Matrix<M, N, TT> result(*this);
        result.doShuffleRows(g);
        return result;
    }

    template<int M, int N, typename TT>
    template <class URNG>
    constexpr void Matrix<M, N, TT>::doShuffleCols(URNG&& g)
    {
        std::array<size_t, N> indices;
        std::iota(indices.begin(), indices.end(), 0);
        std::shuffle(indices.begin(), indices.end(), g);

        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                std::swap(m_a[i][j], m_a[i][indices[j]]);
    }

    template<int M, int N, typename TT>
    template <class URNG>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::shuffleCols(URNG&& g) const
    {
        Matrix<M, N, TT> result(*this);
        result.doShuffleCols(g);
        return result;
    }

    template<int M, int N, typename TT>
    constexpr void Matrix<M, N, TT>::doReduction(ReductionMethod type, M_DL(TT)* scaling)
    {
        if (type == ReductionMethod::GAUSS_ELIMINATION)
        {
            M_DL(TT)  result =   M_DL(TT)( M_DL(TT)::quantity_type::from_value(1.));

            // Rules used to calculate result:
            //   Swapping two rows multiplies the determinant by −1
            //   Multiplying a row by a nonzero scalar multiplies the determinant by the same scalar
            //   Adding to one row a scalar multiple of another does not change the determinant.


            for (int i = 0; i < M; ++i)
            {
                // Find the pivot row
                int pivot = i;
                for (int r = i + 1; r < M; ++r)
                {
                    if (m_a[r][i].normSquared() > m_a[pivot][i].normSquared())
                        pivot = r;
                }

                // Swap the pivot row with row i
                if (pivot != i)
                {
                    doRowSwap(i, pivot);
                    result *= -1.;
                }

                // Column to eliminate
                int j = i;
                if (j >= N)
                    break;// It is a rectangular matrix, so we are done

                // Eliminate the j-th column
                for (int r = i + 1; r < M; ++r)
                {
                    const M_DL(TT) scale = m_a[r][j] / m_a[i][j];
                    doRowSub(i, r, scale);
                }
            }

            if (scaling != nullptr)
                *scaling = result;
        } else
        {
            // TODO: Implement Gauss-Jordan elimination
            // check results with https://matrix.reshish.com/gaussSolution.php

            throw std::runtime_error("Elimination type not yet supported: " + std::to_string((int) type));
        }
    }

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, TT> Matrix<M, N, TT>::reduction(ReductionMethod type, M_DL(TT)* scaling) const
    {
        Matrix<M, N, TT> result = *this;
        result.doReduction(type, scaling);
        return result;
    }

    // Forward declaration  to avoid the error "there are no arguments to ‘getIdentity’ that depend on a template parameter, so a declaration of ‘getIdentity’ must be available" on GCC
    template<int N, typename T, typename A>
    CONSTEVAL_WA static Matrix<N, N, Real<T, A>> getIdentity();

    template<int M, int N, typename TT>
    template<int P, int Q, typename UU>
    constexpr Matrix<M + P, N + Q, TT>
    Matrix<M, N, TT>::kroneckerSum(const Matrix<P, Q, UU> &other) const
        requires(M == N, P == Q)
    {
        return kroneckerProduct(getIdentity<P>()) + getIdentity<M>().kroneckerProduct(other);
    }

    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, M_MUL(TT,UU)>
    Matrix<M,N,TT>::elementwiseProduct(const Matrix<M, N, UU>& other) const
    {
        Matrix<M, N, M_MUL(TT, UU) > result;

        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                result.m_a[i][j] = m_a[i][j] * other.m_a[i][j];

        return result;
    }

    template<int M, int N, typename TT>
    template<typename UU>
    constexpr Matrix<M, N, M_DIV(TT,UU)>
    Matrix<M,N,TT>::elementwiseDivision(const Matrix<M, N, UU>& other) const
    {
        Matrix<M, N, M_DIV(TT, UU) > result;

        for (int i = 0; i < M; i++)
            for (int j = 0; j < N; j++)
                result.m_a[i][j] = m_a[i][j] / other.m_a[i][j];

        return result;
    }

    template<int M, int N, typename TT>
    template<int P, int Q, typename UU>
    constexpr M_MUL(TT,UU)
    Matrix<M,N,TT>::frobenius(const Matrix<P, Q, UU>& other) const
    {
        M_MUL(TT, UU)     result;
        for (unsigned int i = 0; i < M; i++)
            for (unsigned int j = 0; j < N; j++)
                result += m_a[i][j].conj() * other.m_a[i][j];
        return result;
    }
}

// /* Other operators */
// ----------------------------------------------------------------------------------------------------------------
template<int M, int N, typename TT>
constexpr std::ostream &dfpe::operator<<(std::ostream &os, const Matrix <M, N, TT> &right)
{
    // Cycling from zero based array to optimize performances
    for (unsigned int i = 0; i < M; i++)
    {
        for (unsigned int j = 0; j < N; j++)
        {
            os << right.m_a[i][j];
            if (j < N - 1)
                os << ", ";
        }
        os << std::endl;
    }

    return os;
}
