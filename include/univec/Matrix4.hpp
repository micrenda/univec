#pragma once

#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/quantity.hpp>

#include "univec/Matrix.hpp"

#include "univec/Real.hpp"
#include "univec/Complex.hpp"
#include "univec/Quaternion.hpp"
#include "univec/Octonion.hpp"
#include "univec/Sedenion.hpp"

namespace dfpe
{

    template<typename TT=Real<boost::units::quantity<boost::units::si::dimensionless>,boost::units::quantity<boost::units::si::plane_angle>>>
    using Matrix4 = Matrix<4, 4, TT>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using RMatrix4 = Matrix<4, 4, Real<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using CMatrix4 = Matrix<4, 4, Complex<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using HMatrix4 = Matrix<4, 4, Quaternion<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using OMatrix4 = Matrix<4, 4, Octonion<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using SMatrix4 = Matrix<4, 4, Sedenion<T,A>>;
}

