#pragma once

#include "VectorC.hpp"
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/quantity.hpp>

namespace dfpe {

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using Octonion = VectorC<-8, T, A>;
}

