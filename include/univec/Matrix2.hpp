#pragma once

#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/quantity.hpp>

#include "univec/Matrix.hpp"

#include "univec/Real.hpp"
#include "univec/Complex.hpp"
#include "univec/Quaternion.hpp"
#include "univec/Octonion.hpp"
#include "univec/Sedenion.hpp"

namespace dfpe
{

    template<typename TT=Real<boost::units::quantity<boost::units::si::dimensionless>,boost::units::quantity<boost::units::si::plane_angle>>>
    using Matrix2 = Matrix<2, 2, TT>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using RMatrix2 = Matrix<2, 2, Real<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using CMatrix2 = Matrix<2, 2, Complex<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using HMatrix2 = Matrix<2, 2, Quaternion<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using OMatrix2 = Matrix<2, 2, Octonion<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using SMatrix2 = Matrix<2, 2, Sedenion<T,A>>;
}

