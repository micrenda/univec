#pragma once

#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <iostream>

namespace dfpe
{

    template<typename T, typename A>
    template<typename B>
    constexpr EulerRotation3D<T, A>::EulerRotation3D(
            const B &alpha,
            const B &beta,
            const B &gamma,
            RotationMode rotationMode,
            RotationAxis axisAlpha,
            RotationAxis axisBeta,
            RotationAxis axisGamma) : alpha(A(alpha)), beta(A(beta)), gamma(A(gamma)), rotationMode(rotationMode), axisAlpha(axisAlpha), axisBeta(axisBeta), axisGamma(axisGamma)
    {

    }


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC3D<U, B> EulerRotation3D<T, A>::rotate(const VectorC3D<U, B> &v) const
    {
        VectorC3D<U, B> buffer = v;
        auto            rotateAxesExtrinsic = [&](const A &angle, RotationAxis axis)
        {
            switch (axis)
            {
                case RotationAxis::X:
                    if (angle != A()) buffer = buffer.rotateX(angle);
                    break;

                case RotationAxis::Y:
                    if (angle != A()) buffer = buffer.rotateY(angle);
                    break;

                case RotationAxis::Z:
                    if (angle != A()) buffer = buffer.rotateZ(angle);
                    break;

                case RotationAxis::SKIP:
                    break;

                default:
                    assert(false);
            }
        };

        if (rotationMode == RotationMode::EXTRINSIC)
        {
            rotateAxesExtrinsic(A(alpha), axisAlpha);
            rotateAxesExtrinsic(A(beta), axisBeta);
            rotateAxesExtrinsic(A(gamma), axisGamma);
        }
        else if (rotationMode == RotationMode::INTRINSIC)
        {
            // Any intrinsic rotation is equivalent to an extrinsic rotation by the same angles but with inverted order of elemental rotations, and vice versa.
            rotateAxesExtrinsic(A(gamma), axisGamma);
            rotateAxesExtrinsic(A(beta), axisBeta);
            rotateAxesExtrinsic(A(alpha), axisAlpha);
        }
        else
            assert(false);

        return buffer;
    }

    template<typename T, typename A>
    constexpr bool EulerRotation3D<T, A>::isTaitBryan() const
    {
        if (axisAlpha == RotationAxis::SKIP || axisBeta == RotationAxis::SKIP || axisGamma == RotationAxis::SKIP)
            return false;

        return axisAlpha != axisGamma && axisAlpha != axisBeta && axisBeta != axisGamma;
    }

    template<typename T, typename A>
    constexpr bool EulerRotation3D<T, A>::isProperEuler() const
    {
        if (axisAlpha == RotationAxis::SKIP || axisBeta == RotationAxis::SKIP || axisGamma == RotationAxis::SKIP)
            return false;

        return axisAlpha == axisGamma && axisAlpha != axisBeta && axisBeta != axisGamma;
    }

    template<typename T, typename A>
    constexpr bool EulerRotation3D<T, A>::isValid() const
    {
        if (axisAlpha == RotationAxis::SKIP || axisBeta == RotationAxis::SKIP || axisGamma == RotationAxis::SKIP)
            return true;

        return !(axisAlpha == axisGamma && axisAlpha == axisBeta && axisBeta == axisGamma);
    }
}
