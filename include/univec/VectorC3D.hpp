#pragma once

#include "VectorC.hpp"
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/quantity.hpp>

namespace dfpe {

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using VectorC3D = VectorC<3, T, A>;
}

