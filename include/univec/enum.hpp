#pragma once

namespace dfpe
{
    enum class RotationMode
    {
        INTRINSIC, EXTRINSIC
    };
    enum class RotationAxis
    {
        X, Y, Z, SKIP
    };

    enum class ReductionMethod
    {
        GAUSS_ELIMINATION,
        GAUSS_JORDAN_ELIMINATION,
    };

    enum class DeterminantMethod
    {
        AUTO, /// Automatically choose the best method (uses DeterminantMethod::LEIBNIZ_FORMULA for matrix up to \f$4\times4\f$ and DeterminantMethod::GAUSSIAN_ELIMINATION for larger matrices, it may change in the future)
        LAPLACE_FORMULA, /// Laplace recursive formula
        LEIBNIZ_FORMULA, /// Leibniz formula -- not implemented
        GAUSSIAN_ELIMINATION, /// Gaussian elimination
        GAUSSIAN_JORDAN_ELIMINATION, /// Gaussian-Jordan elimination
        LU_DECOMPOSITION,  /// LU decomposition with full pivoting - not implemented
        LUP_DECOMPOSITION, /// LU decomposition with partial pivoting - not implemented
        QR_DECOMPOSITION, /// QR decomposition - not implemented
        EIGEN_DECOMPOSITION, /// Eigen decomposition - not implemented
        CHOLESKY_DECOMPOSITION /// Cholesky decomposition - not implemented
    };


}