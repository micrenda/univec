#pragma once

#include <ostream>
#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <boost/units/is_dimensionless.hpp>
#include <boost/units/systems/si/plane_angle.hpp>
#include <boost/units/io.hpp>
#include "VectorS3D.hpp"
#include "VectorC.hpp"

namespace dfpe
{
    template<int N, typename T, typename A>
    class VectorC;

    template<typename T, typename A>
    class VectorS3D;

    /**
     * A 3D vector in a Cylindrical coordinate system
     * @tparam T Type of the quantity
     * @tparam A Type of the angle
     */
    template<typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    class VectorY3D
    {
        template <int N, typename U, typename B>
        friend class VectorC;

        template <typename U, typename B>
        friend class VectorS3D;

    public:
        /// Create a null vector
        constexpr VectorY3D();

        /// Create a vector from another vector
        template<typename U, typename B>
        constexpr VectorY3D(const VectorY3D<U, B> &other);

        /// Create a vector from radius \f$rho\f$, angle \f$phi\f$ and coordinate \f$z\f$
        template<typename U, typename B>
        constexpr VectorY3D(const U &rho, const B &phi, const T &z);

        /**
         * \brief Create a vector from a 3D cartesian vector
         * It is an expensive operation
         */
        template<int N, typename U, typename B>
        explicit constexpr VectorY3D<T, A>(const VectorC<N, U, B>& other) requires (N == 3)  : VectorY3D()
        {
            //check_type_compatibility<T,U>();

            m_rho = T(boost::units::hypot(other[0], other[1]));
            m_phi = A(boost::units::atan2(other[1], other[0]));
            m_z   = T(other[2]);

            if (!isNormalized())
                doNormalize();
        }

        /**
         * \brief Create a vector from a 3D spherical vector
         * It is an expensive operation
         */
        template<typename U, typename B>
        explicit constexpr VectorY3D<T, A>(const VectorS3D<U,B>& other) : VectorY3D()
        {
            //check_type_compatibility<T,U>();

            m_rho = T(other.m_r) * sin(other.m_theta);
            m_phi = A(other.m_phi);
            m_z   = T(other.m_r) * cos(other.m_theta);

            if (!isNormalized())
                doNormalize();
        }

    protected:
        T m_rho; ///< rho value
        A m_phi; ///< phi value
        T m_z; ///< z value

    public:

        inline constexpr const T&     getRho() const     { return m_rho; } ///< @private
        inline constexpr void         setRho(const T& value) ///< @private
        {
            m_rho = value;
            if (!isNormalized())
                doNormalize();
        }

        inline constexpr const A&     getPhi() const     { return m_phi; } ///< @private
        inline constexpr void         setPhi(const A& value)     ///< @private
        {
            m_phi = value;
            if (!isNormalized())
                doNormalize();
        }

        inline constexpr const T&     getZ() const     { return m_z; } ///< @private
        inline constexpr void         setZ(const T& value) ///< @private
        {
            m_z = value;
            if (!isNormalized())
                doNormalize();
        }


#ifndef GCC
        __declspec(property(get=getRho, put=setRho)) T rho; ///< @private
        __declspec(property(get=getPhi, put=setPhi)) A phi; ///< @private
        __declspec(property(get=getZ, put=setZ)) T z; ///< @private
#else
    #if ENABLE_GCC_PROPERTY
        Property<T,VectorY3D,&VectorY3D::getRho,&VectorY3D::setRho> rho;     ///< @private
        Property<A,VectorY3D,&VectorY3D::getPhi,&VectorY3D::setPhi> phi;     ///< @private
        Property<T,VectorY3D,&VectorY3D::getZ,&VectorY3D::setZ>     z;       ///< @private
    #endif
#endif
    public:
        /**
         * \brief Calculate the euclidean norm of the vector
         */
        constexpr T norm() const;

        /**
         * \brief Calculate the euclidean squared norm of the vector
         */
        constexpr POW(T,2) normSquared() const;

        /**
         * \brief Create a unit vector with the same direction of the vector
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr VectorY3D<T, A> versor() const;

        /**
         * \brief Scale the vector to have unit length
         *
         * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
         */
        constexpr void doVersor() ;

        /**
             * \brief Create a unit vector with the same direction of the vector
             *
             * \f$ \mathbf{\hat{v}} = \mathbf{v} / v\f$
             */
        constexpr VectorY3D<boost::units::quantity<boost::units::si::dimensionless>, A> versorDl() const;

        /**
         * \brief Dot product between two vectors
         */
        template<typename U, typename B>
        constexpr MUL(T,U) dot(const VectorY3D<U, B> &other) const;

        /**
         * \brief Cross product between two vectors
         */
        template<typename U, typename B>
        constexpr VectorY3D<MUL(T,U), A> cross(const VectorY3D<U, B> &other) const;

        /**
		 * \brief Smallest angle between two vectors
		 * @param other The other vector
		 * @return The angle between two vectors
		 */
        template<typename U, typename B>
        constexpr A angle(const VectorY3D<U, B> &other) const;

        /**
        * Check if the vector has zero length
        * @return true is null, false otherwise
        */
        template<typename TOL=Tolerance<T,A>>
        constexpr bool isNull(const TOL& tol = TOL()) const;

        /**
         * Check if the vector are near with each other
        * @return true is null, false otherwise
        */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isNear(const VectorY3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
     * Check if all vector element are finite
     * @return true is all elements are finite, false otherwise
     */
        constexpr bool isFinite() const;

        /**
         * Check if all vector element are normal
         * @return true is all elements are normal, false otherwise
         */
        constexpr bool isNormal() const;

        /**
         * Check if any vector element is nan
         * @return true is any element is nan, false otherwise
         */
        constexpr bool isNan() const;

        /**
         * Check if any vector element is inf
         * @return true is any element is inf, false otherwise
         */
        constexpr bool isInfinite() const;

        /**
         * \brief Check if two vector are parallel
         * Two vector are parallel if they have the same direction or opposite direciton
         * If one vector is zero, then they are not parallel
         * @return true is are parallel, false otherwise
         */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isParallel(const VectorY3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
        * \brief Check if two have same direction
        * If one vector is zero, then they do not have same direction
        * @return true is they have same direction, false otherwise
        */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isSameDirection(const VectorY3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
        * \brief Check if two have opposite direction
        * If one vector is zero, then they do not have opposite direction
        * @return true is they have opposite direction, false otherwise
        */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isOppositeDirection(const VectorY3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
         * \brief Check if two have perpendicular direction
         * If one vector is zero, then they do not have perpendicular direction
         * @return true is they have perpendicular direction, false otherwise
         */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr bool isPerpendicular(const VectorY3D<U, B> &other, const TOL& tol = TOL()) const;

        /**
         *
         * \brief Get the ratio between the module of two parallel vectors.
         * If the vectors are not parallel or any of them is null, an empty optional value is returned.
         *
         * @tparam U
         * @tparam B
         * @tparam TOL
         * @param other
         * @param tol  vector to compare
         * @return
         */
        template<typename U, typename B, typename TOL = Tolerance<T,A>>
        constexpr std::optional<DL(T)>
        scale(const VectorY3D<U, B> &other, const TOL& tol = TOL()) const;

    public: // Arithmetic operators


        constexpr VectorY3D<UADD(T), A> operator+() const;


        constexpr VectorY3D<USUB(T), A> operator-() const;

        template<typename U, typename B>
        constexpr VectorY3D<ADD(T,U), A> operator+(const VectorY3D<U, B> &right) const;

        template<typename U, typename B>
        constexpr VectorY3D<SUB(T,U), A> operator-(const VectorY3D<U, B> &right) const;

        template<typename U>
        constexpr VectorY3D<MUL(T,U), A> operator*(const U &right) const;

        template<typename U>
        constexpr VectorY3D<DIV(T,U), A> operator/(const U &right) const;

        template<typename U, typename B>
        constexpr MUL(T,U) operator|(const VectorY3D<U, B> &right) const;

        template<typename U, typename B>
        constexpr VectorY3D<MUL(T,U), A> operator^(const VectorY3D<U, B> &right) const;


        /**
	     * Sum another vector
	     * @param right
	     */
        template<typename U, typename B>
        constexpr VectorY3D<T, A> &operator+=(const VectorY3D<U, B> &right);

        /**
		 * Subtract another vector
		 * @param right
		 */
        template<typename U, typename B>
        constexpr VectorY3D<T, A> &operator-=(const VectorY3D<U, B> &right);

        /**
         * Scalar moltiplication with another quantity
         * @param right
         */
        template<typename U>
        constexpr VectorY3D<T, A> &operator*=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);
        /**
		 * Scalar division with another quantity
		 * @param right
		 */
        template<typename U>
        constexpr VectorY3D<T, A> &operator/=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>);

        //TODO: scale(), isParallel, isSameDirection, isOppositeDirection
    public: // Relational operators

        template<typename U, typename B>
        constexpr std::weak_ordering operator<=>(const VectorY3D<U, B> &right) const;

        /**
		 * Equality between two vectors
		 */
        template<typename U, typename B>
        constexpr bool operator==(const VectorY3D<U, B> &right) const;

        /**
		 * Disequality between two vectors
		 */
        template<typename U, typename B>
        constexpr bool operator!=(const VectorY3D<U, B> &right) const;

        friend std::ostream &operator<<(std::ostream &os, const VectorY3D<T, A> &right)
        {
            os << "(" << right.m_rho << ", " << right.m_phi << ", " << right.m_z << ")";
            return os;
        }

    protected:
        constexpr inline bool isNormalized() const;
        constexpr void doNormalize();
    };
}


#if BOOST_UNITS_HAS_BOOST_TYPEOF

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TEMPLATE(dfpe::VectorY3D, 2)
#endif

namespace dfpe
{

    template<typename T, typename U, typename B>
    constexpr VectorY3D<MUL(T,U), B> operator*(const T &left, const VectorY3D<U, B> &right);
}

#include "VectorY3D.tcc"
