#pragma once
#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <iostream>

namespace dfpe
{

    template<typename T>
    RotateFrame3D<T>::RotateFrame3D()
    {
        // Saving axes
        T zero;
        T one = T::from_value(1.);

        sourceAxisX = VectorC<3,T>(one,  zero, zero);
        sourceAxisY = VectorC<3,T>(zero,  one, zero);
        sourceAxisZ = VectorC<3,T>(zero, zero,  one);

        destAxisX   = VectorC<3,T>(one,  zero, zero);
        destAxisY   = VectorC<3,T>(zero,  one, zero);
        destAxisZ   = VectorC<3,T>(zero, zero,  one);

        initialize();
    }

    template<typename T>
    template<typename A>
    RotateFrame3D<T>::RotateFrame3D(const EulerRotation3D<T,A>& rotation) : RotateFrame3D()
    {
        destAxisX = rotation.rotate(sourceAxisX);
        destAxisY = rotation.rotate(sourceAxisY);
        destAxisZ = rotation.rotate(sourceAxisZ);

        initialize();
    }

    template<typename T>
    template<typename A>
    RotateFrame3D<T>::RotateFrame3D(const Quaternion<T,A>& rotation) : RotateFrame3D()
    {
        destAxisX = rotation.rotate(sourceAxisX);
        destAxisY = rotation.rotate(sourceAxisY);
        destAxisZ = rotation.rotate(sourceAxisZ);

        initialize();
    }

    template<typename T>
    void RotateFrame3D<T>::initialize()
    {
    // Checking we don't have a null axis
    if (sourceAxisX.isNull())
    throw std::runtime_error("Rotate3D: sourceAxisX is a null vector");

    if (sourceAxisY.isNull())
    throw std::runtime_error("Rotate3D: sourceAxisY is a null vector");

    if (sourceAxisZ.isNull())
    throw std::runtime_error("Rotate3D: sourceAxisZ is a null vector");

    if (destAxisX.isNull())
    throw std::runtime_error("Rotate3D: destAxisX is a null vector");

    if (destAxisY.isNull())
    throw std::runtime_error("Rotate3D: destAxisY is a null vector");

    if (destAxisZ.isNull())
    throw std::runtime_error("Rotate3D: destAxisZ is a null vector");


    //const boost::units::quantity<boost::units::si::plane_angle, double> &pi2 = boost::units::quantity<boost::units::si::plane_angle>(M_PI_2 * boost::units::si::radians);
    //const boost::units::quantity<boost::units::si::plane_angle, double> &epsilon = pi2 / 10000.;
    const T &epsilon     = T::from_value(1e-6);
    const auto &epsilon2 = epsilon * epsilon;

    // Checking if the source vectors are orthogonal
    if (abs(sourceAxisX.dot(sourceAxisY)) > epsilon2)
    throw std::runtime_error("Rotate3D: sourceAxisX and sourceAxisY are not orthogonal");

    if (abs(sourceAxisX.dot(sourceAxisZ)) > epsilon2)
    throw std::runtime_error("Rotate3D: sourceAxisX and sourceAxisZ are not orthogonal");

    if (abs(sourceAxisY.dot(sourceAxisZ)) > epsilon2)
    throw std::runtime_error("Rotate3D: sourceAxisY and sourceAxisZ are not orthogonal");


    // Checking if the dest vectors are orthogonals
    if (abs(destAxisX.dot(destAxisY)) > epsilon2)
    throw std::runtime_error("Rotate3D: destAxisX and destAxisY are not orthogonal");

    if (abs(destAxisX.dot(destAxisZ)) > epsilon2)
    throw std::runtime_error("Rotate3D: destAxisX and destAxisZ are not orthogonal");

    if (abs(destAxisY.dot(destAxisZ)) > epsilon2)
    throw std::runtime_error("Rotate3D: destAxisY and destAxisZ are not orthogonal");

    // Checking if the vector are unitary
    const T &one = T::from_value(1.);
    const auto& one2 = one * one;

    if (abs(sourceAxisX.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate3D: sourceAxisX is not unitary");
    if (abs(sourceAxisY.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate3D: sourceAxisY is not unitary");
    if (abs(sourceAxisZ.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate3D: sourceAxisZ is not unitary");

    if (abs(destAxisX.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate3D: destAxisX is not unitary");
    if (abs(destAxisY.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate3D: destAxisY is not unitary");
    if (abs(destAxisZ.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate3D: destAxisZ is not unitary");

    // Creating rotation matrix (this will be the backward matrix)
    rotationMatrixFw(1,1) = destAxisX.dot(sourceAxisX) / one2;
    rotationMatrixFw(1,2) = destAxisX.dot(sourceAxisY) / one2;
    rotationMatrixFw(1,3) = destAxisX.dot(sourceAxisZ) / one2;
    rotationMatrixFw(2,1) = destAxisY.dot(sourceAxisX) / one2;
    rotationMatrixFw(2,2) = destAxisY.dot(sourceAxisY) / one2;
    rotationMatrixFw(2,3) = destAxisY.dot(sourceAxisZ) / one2;
    rotationMatrixFw(3,1) = destAxisZ.dot(sourceAxisX) / one2;
    rotationMatrixFw(3,2) = destAxisZ.dot(sourceAxisY) / one2;
    rotationMatrixFw(3,3) = destAxisZ.dot(sourceAxisZ) / one2;

    rotationMatrixBw = rotationMatrixFw.inv();
    }

    template<typename T>
    RotateFrame3D<T>::RotateFrame3D(
            const VectorC<3,T>& sourceAxisX,
            const VectorC<3,T>& sourceAxisY,
            const VectorC<3,T>& sourceAxisZ,

            const VectorC<3,T>& destAxisX,
            const VectorC<3,T>& destAxisY,
            const VectorC<3,T>& destAxisZ) : sourceAxisX(sourceAxisX), sourceAxisY(sourceAxisY), sourceAxisZ(sourceAxisZ),
                                             destAxisX(destAxisX), destAxisY(destAxisY), destAxisZ(destAxisZ)
    {
        initialize();
    }

    template<typename T>
    VectorC<3,T> RotateFrame3D<T>::forward(const VectorC<3,T>  &source) const
    {
        return rotationMatrixFw * source;
    }

    template<typename T>
    VectorC<3,T> RotateFrame3D<T>::backward(const VectorC<3,T> &destination) const
    {
        return rotationMatrixBw * destination;
    }

    template<typename T>
    VectorC3D <boost::units::quantity<boost::units::si::dimensionless>> RotateFrame3D<T>::forwardRotOnlyDL(const VectorC3D <boost::units::quantity<boost::units::si::dimensionless>> &source) const
    {
        return forward(source);
    }

    template<typename T>
    VectorC3D <boost::units::quantity<boost::units::si::dimensionless>> RotateFrame3D<T>::backwardRotOnlyDL(const VectorC3D <boost::units::quantity<boost::units::si::dimensionless>> &destination) const
    {
        return backward(destination);
    }

    template<typename T>
    template<typename J>
    VectorC<3,J> RotateFrame3D<T>::forward(const VectorC<3,J>  &source) const
    {
        return rotationMatrixFw * source;
    }

    template<typename T>
    template<typename J>
    VectorC<3,J> RotateFrame3D<T>::backward(const VectorC<3,J> &destination) const
    {
        return rotationMatrixBw * destination;
    }

}
