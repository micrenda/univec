#pragma once

#include "univec/frame/TranslateFrame.hpp"

namespace dfpe {

    template <typename T=boost::units::quantity<boost::units::si::dimensionless>>
    using TranslateFrame3D = TranslateFrame<3,T>;
}

