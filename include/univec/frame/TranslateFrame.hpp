#pragma once

#include "univec/VectorC3D.hpp"
#include "BaseFrame.hpp"

namespace dfpe
{

    template <int N=3, typename T=boost::units::quantity<boost::units::si::dimensionless>>
    class TranslateFrame: public BaseFrame<N,N,T,T>
    {
    public:
        explicit TranslateFrame() {};
        explicit TranslateFrame(VectorC<N,T> translation): translation(std::move(translation)) {};

        VectorC<N,T> forward (const VectorC<N,T> &source) const override;
        VectorC<N,T> backward(const VectorC<N,T> &destination) const override;

        const VectorC<N,T> &getTranslation() const { return translation; }

    public:
        VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> forwardRotOnlyDL(const VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> &source) const override;
        VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> backwardRotOnlyDL(const VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> &destination) const override;

    protected:
        VectorC<N,T> translation;
    };
}

#include "TranslateFrame.tcc"
