#pragma once

#include "univec/VectorC3D.hpp"
namespace dfpe
{

    template <int N, int M, typename T, typename U=T>
    class BaseFrame
    {
    public:
        typedef T leftType;
        typedef U rightType;

        typedef void leftFrameType;
        typedef void rightFrameType;

        static const int leftN = N;
        static const int rightM = M;
    public:
        virtual VectorC<M,U> forward (const VectorC<N,T> &source)      const = 0;
        virtual VectorC<N,T> backward(const VectorC<M,U> &destination) const = 0;

    public:
        // Dimensions-less virtual functions
        virtual VectorC<M,boost::units::quantity<boost::units::si::dimensionless>> forwardRotOnlyDL (const VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> &source)      const = 0;
        virtual VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> backwardRotOnlyDL(const VectorC<M,boost::units::quantity<boost::units::si::dimensionless>> &destination) const = 0;

    public:
        template<typename J>        VectorC<M,J> forwardRotOnly(const VectorC<N,J>  &source) const;
        template<typename J>        VectorC<N,J> backwardRotOnly(const VectorC<M,J> &destination) const;
    };
}


#include "BaseFrame.tcc"
