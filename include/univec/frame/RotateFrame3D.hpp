#pragma once

#include "univec/VectorC.hpp"
#include "BaseFrame.hpp"
#include "univec/EulerRotation3D.hpp"
#include "univec/Quaternion.hpp"
#include "univec/Matrix.hpp"
#include "univec/frame/BaseFrame.hpp"
namespace dfpe
{
    template <typename T=boost::units::quantity<boost::units::si::dimensionless>>
    class RotateFrame3D: public BaseFrame<3,3,T,T>
    {
    public:

        explicit RotateFrame3D();
        /*explicit RotateAxesC3D(const VectorC3D<boost::units::quantity<boost::units::si::plane_angle>>& rotation);

        RotateAxesC3D(
                const boost::units::quantity<boost::units::si::plane_angle>& rotationX,
                const boost::units::quantity<boost::units::si::plane_angle>& rotationY,
                const boost::units::quantity<boost::units::si::plane_angle>& rotationZ) : RotateAxesC3D(VectorC3D<boost::units::quantity<boost::units::si::plane_angle>>(rotationX, rotationY, rotationZ)) {};
        */
        RotateFrame3D(
            const VectorC<3,T>& destAxisX,
            const VectorC<3,T>& destAxisY,
            const VectorC<3,T>& destAxisZ):
                RotateFrame3D(VectorC<3,T>(T::from_value(1.), T(), T()),
                              VectorC<3,T>(T(), T::from_value(1.), T()),
                              VectorC<3,T>(T(), T(), T::from_value(1.)),
                              destAxisX,
                              destAxisY,
                              destAxisZ) {}

        RotateFrame3D(
            const VectorC<3,T>& sourceAxisX,
            const VectorC<3,T>& sourceAxisY,
            const VectorC<3,T>& sourceAxisZ,

            const VectorC<3,T>& destAxisX,
            const VectorC<3,T>& destAxisY,
            const VectorC<3,T>& destAxisZ);

        template<typename A>
        explicit RotateFrame3D(const EulerRotation3D<T,A>& rotation);

        template<typename A>
        explicit RotateFrame3D(const Quaternion<T,A>& rotation);

        const VectorC<3,T>& getSourceAxisX() const { return sourceAxisX; };
        const VectorC<3,T>& getSourceAxisY() const { return sourceAxisY; };
        const VectorC<3,T>& getSourceAxisZ() const { return sourceAxisZ; };

        const VectorC<3,T> &getDestAxisX() const { return destAxisX; }
        const VectorC<3,T> &getDestAxisY() const { return destAxisY; }
        const VectorC<3,T> &getDestAxisZ() const { return destAxisZ; }

        VectorC<3,T> forward(const VectorC<3,T>  &source) const override;
        VectorC<3,T> backward(const VectorC<3,T> &destination) const override;

        template<typename J>
        VectorC<3,J> forward(const VectorC<3,J>  &source) const;
        template<typename J>
        VectorC<3,J> backward(const VectorC<3,J> &destination) const;

    public:
        VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> forwardRotOnlyDL(const VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> &source)       const override;
        VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> backwardRotOnlyDL(const VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> &destination) const override;

    protected:

        VectorC<3,T> sourceAxisX;
        VectorC<3,T> sourceAxisY;
        VectorC<3,T> sourceAxisZ;

        VectorC<3,T> destAxisX;
        VectorC<3,T> destAxisY;
        VectorC<3,T> destAxisZ;

        Matrix<3,3,Real<boost::units::quantity<boost::units::si::dimensionless>>> rotationMatrixFw;
        Matrix<3,3,Real<boost::units::quantity<boost::units::si::dimensionless>>> rotationMatrixBw;


    protected:
        void initialize();
    };

}

#include "RotateFrame3D.tcc"
