#pragma once
#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <iostream>

namespace dfpe
{

    template<typename T>
    RotateFrame2D<T>::RotateFrame2D()
    {
        // Saving axes
        T zero;
        T one = T::from_value(1.);

        sourceAxisX = VectorC<2,T>(one,  zero);
        sourceAxisY = VectorC<2,T>(zero,  one);

        destAxisX   = VectorC<2,T>(one,  zero);
        destAxisY   = VectorC<2,T>(zero,  one);

        initialize();
    }

    template<typename T>
    RotateFrame2D<T>::RotateFrame2D(boost::units::quantity<boost::units::si::plane_angle> angle)
    {
        // Saving axes
        T zero;
        T one = T::from_value(1.);

        sourceAxisX = VectorC<2,T>(one,  zero);
        sourceAxisY = VectorC<2,T>(zero,  one);

        destAxisX   = VectorC<2,T>(cos(angle) * one, -sin(angle) * one);
        destAxisY   = VectorC<2,T>(sin(angle) * one,  cos(angle) * one);

        initialize();
    }

    template<typename T>
    template<typename A>
    RotateFrame2D<T>::RotateFrame2D(const Complex<T,A>& rotation) : RotateFrame2D()
    {
        auto norm = rotation.norm();
        destAxisX = rotation[0] / norm;
        destAxisY = rotation[1] / norm;

        initialize();
    }

    template<typename T>
    void RotateFrame2D<T>::initialize()
    {
    // Checking we don't have a null axis
    if (sourceAxisX.isNull())
    throw std::runtime_error("Rotate2D: sourceAxisX is a null vector");

    if (sourceAxisY.isNull())
    throw std::runtime_error("Rotate2D: sourceAxisY is a null vector");

    if (destAxisX.isNull())
    throw std::runtime_error("Rotate2D: destAxisX is a null vector");

    if (destAxisY.isNull())
    throw std::runtime_error("Rotate2D: destAxisY is a null vector");

    //const boost::units::quantity<boost::units::si::plane_angle, double> &pi2 = boost::units::quantity<boost::units::si::plane_angle>(M_PI_2 * boost::units::si::radians);
    //const boost::units::quantity<boost::units::si::plane_angle, double> &epsilon = pi2 / 10000.;
    const T &epsilon     = T::from_value(1e-6);
    const auto &epsilon2 = epsilon * epsilon;

    // Checking if the source vectors are orthogonal
    if (abs(sourceAxisX.dot(sourceAxisY)) > epsilon2)
    throw std::runtime_error("Rotate2D: sourceAxisX and sourceAxisY are not orthogonal");

    // Checking if the dest vectors are orthogonals
    if (abs(destAxisX.dot(destAxisY)) > epsilon2)
    throw std::runtime_error("Rotate2D: destAxisX and destAxisY are not orthogonal");

    // Checking if the vector are unitary
    const T &one = T::from_value(1.);
    const auto& one2 = one * one;

    if (abs(sourceAxisX.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate2D: sourceAxisX is not unitary");
    if (abs(sourceAxisY.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate2D: sourceAxisY is not unitary");

    if (abs(destAxisX.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate2D: destAxisX is not unitary");
    if (abs(destAxisY.normSquared() - one2) > epsilon2)
    throw std::runtime_error("Rotate2D: destAxisY is not unitary");

    //TODO: move to Matrix2
    // Creating rotation matrix (this will be the backward matrix)
    rotationMatrixBw1 = VectorC<2,boost::units::quantity<boost::units::si::dimensionless>>(destAxisX.dot(sourceAxisX) / one2, destAxisX.dot(sourceAxisY) / one2);
    rotationMatrixBw2 = VectorC<2,boost::units::quantity<boost::units::si::dimensionless>>(destAxisY.dot(sourceAxisX) / one2, destAxisY.dot(sourceAxisY) / one2);

    // Creating the frame rotation matrix, the transpose of the previous matrix (this will be the forward matrix)
    rotationMatrixFw1 = VectorC<2,boost::units::quantity<boost::units::si::dimensionless>>(rotationMatrixBw1[0], rotationMatrixBw2[0]);
    rotationMatrixFw2 = VectorC<2,boost::units::quantity<boost::units::si::dimensionless>>(rotationMatrixBw1[1], rotationMatrixBw2[1]);
    }

    template<typename T>
    RotateFrame2D<T>::RotateFrame2D(
            const VectorC<2,T>& sourceAxisX,
            const VectorC<2,T>& sourceAxisY,

            const VectorC<2,T>& destAxisX,
            const VectorC<2,T>& destAxisY) : sourceAxisX(sourceAxisX), sourceAxisY(sourceAxisY),
                                             destAxisX(destAxisX), destAxisY(destAxisY)
    {
        initialize();
    }

    template<typename T>
    VectorC<2,T> RotateFrame2D<T>::forward(const VectorC<2,T>  &source) const
    {
        return VectorC<2,T>(source | rotationMatrixFw1, source | rotationMatrixFw2);
    }

    template<typename T>
    VectorC<2,T> RotateFrame2D<T>::backward(const VectorC<2,T> &destination) const
    {
        return VectorC<2,T>(destination | rotationMatrixBw1, destination | rotationMatrixBw2);
    }

    template<typename T>
    VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> RotateFrame2D<T>::forwardRotOnlyDL(const VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> &source) const
    {
        return forward(source);
    }

    template<typename T>
    VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> RotateFrame2D<T>::backwardRotOnlyDL(const VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> &destination) const
    {
        return backward(destination);
    }

    template<typename T>
    template<typename J>
    VectorC<2,J> RotateFrame2D<T>::forward(const VectorC<2,J>  &source) const
    {
        return VectorC<2,J>(source | rotationMatrixFw1, source | rotationMatrixFw2);
    }

    template<typename T>
    template<typename J>
    VectorC<2,J> RotateFrame2D<T>::backward(const VectorC<2,J> &destination) const
    {
        return VectorC<2,J>(destination | rotationMatrixBw1, destination | rotationMatrixBw2);
    }

}
