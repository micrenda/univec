#pragma once

#include "univec/frame/ScaleFrame.hpp"

namespace dfpe {
    template <typename T=boost::units::quantity<boost::units::si::dimensionless>, typename U=T>
    using ScaleFrame3D = ScaleFrame<3,T,U>;
}

