#pragma once

#include "univec/frame/ScaleFrame.hpp"

namespace dfpe {
    template <typename T=boost::units::quantity<boost::units::si::dimensionless>, typename U=T>
    using ScaleFrame2D = ScaleFrame<2,T,U>;
}

