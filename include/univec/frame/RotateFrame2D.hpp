#pragma once

#include <boost/units/systems/angle/degrees.hpp>

#include "univec/VectorC.hpp"
#include "univec/Complex.hpp"
#include "univec/frame/BaseFrame.hpp"

namespace dfpe
{
    template <typename T=boost::units::quantity<boost::units::si::dimensionless>>
    class RotateFrame2D: public BaseFrame<2,2,T,T>
    {
    public:

        explicit RotateFrame2D();
        explicit RotateFrame2D(boost::units::quantity<boost::units::si::plane_angle> angle);

        RotateFrame2D(
            const VectorC<2,T>& destAxisX,
            const VectorC<2,T>& destAxisY):
                RotateFrame2D(VectorC<2,T>(T::from_value(1.), T()),
                              VectorC<2,T>(T(), T::from_value(1.)),
                              destAxisX,
                              destAxisY) {}

        RotateFrame2D(
            const VectorC<2,T>& sourceAxisX,
            const VectorC<2,T>& sourceAxisY,

            const VectorC<2,T>& destAxisX,
            const VectorC<2,T>& destAxisY);

        template<typename A>
        explicit RotateFrame2D(const Complex<T,A>& rotation);

        const VectorC<2,T>& getSourceAxisX() const { return sourceAxisX; };
        const VectorC<2,T>& getSourceAxisY() const { return sourceAxisY; };

        const VectorC<2,T> &getDestAxisX() const { return destAxisX; }
        const VectorC<2,T> &getDestAxisY() const { return destAxisY; }

        VectorC<2,T> forward(const VectorC<2,T>  &source) const override;
        VectorC<2,T> backward(const VectorC<2,T> &destination) const override;

        template<typename J>
        VectorC<2,J> forward(const VectorC<2,J>  &source) const;
        template<typename J>
        VectorC<2,J> backward(const VectorC<2,J> &destination) const;

    public:
        VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> forwardRotOnlyDL(const VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> &source)       const override;
        VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> backwardRotOnlyDL(const VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> &destination) const override;

    protected:

        VectorC<2,T> sourceAxisX;
        VectorC<2,T> sourceAxisY;

        VectorC<2,T> destAxisX;
        VectorC<2,T> destAxisY;

        VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> rotationMatrixFw1;
        VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> rotationMatrixFw2;

        VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> rotationMatrixBw1;
        VectorC<2,boost::units::quantity<boost::units::si::dimensionless>> rotationMatrixBw2;

    protected:
        void initialize();
    };

}

#include "RotateFrame2D.tcc"
