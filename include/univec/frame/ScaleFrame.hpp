#pragma once

#include "univec/frame/BaseFrame.hpp"
#include "univec/VectorC.hpp"

namespace dfpe
{

    template <int N=3,typename T=boost::units::quantity<boost::units::si::dimensionless>, typename U=T>
    class ScaleFrame: public BaseFrame<N,N,T,U>
    {
    public:
        explicit ScaleFrame() requires (boost::is_same<T,U>::value) {
            typedef DIV(T,U) Type;
            for (int i = 0; i < N; i++)
                scale[i] = Type::from_value(1.);
        };

        explicit ScaleFrame(const DIV(U,T)&  scale): scale(scale, scale, scale) {};
        explicit ScaleFrame(const VectorC<N,DIV(U,T)>&  scale): scale(scale) {};

        VectorC<N,U> forward (const VectorC<N,T> &source)      const override;
        VectorC<N,T> backward(const VectorC<N,U> &destination) const override;

    public:
        VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> forwardRotOnlyDL (const VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> &source) const override;
        VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> backwardRotOnlyDL(const VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> &destination) const override;

    protected:
        VectorC<N,DIV(T,U)>  scale;
    };
}

#include "ScaleFrame.tcc"
