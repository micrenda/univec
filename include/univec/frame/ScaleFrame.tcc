#pragma once
#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <iostream>

#include "univec/VectorC.hpp"


namespace dfpe
{
    template<int N, typename T, typename U>
    VectorC<N,U> ScaleFrame<N,T,U>::forward(const VectorC<N,T> &source) const
    {
        VectorC<N,U> result;
        for (int i = 0; i < N; i++)
            result[i] = source[i] / scale[i];
        return result;
    }

    template<int N, typename T, typename U>
    VectorC<N,T> ScaleFrame<N,T,U>::backward(const VectorC<N,U> &destination) const
    {
        VectorC<N,T> result;
        for (int i = 0; i < N; i++)
            result[i] = destination[i] * scale[i];
        return result;
    }

    template<int N, typename T, typename U>
    VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> ScaleFrame<N,T,U>::forwardRotOnlyDL(const VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> &source) const
    {
        return source;
    }

    template<int N, typename T, typename U>
    VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> ScaleFrame<N,T,U>::backwardRotOnlyDL(const VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> &destination) const
    {
        return destination;
    }
}
