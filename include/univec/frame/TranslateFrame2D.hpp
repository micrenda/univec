#pragma once

#include "univec/frame/TranslateFrame.hpp"

namespace dfpe {

    template <typename T=boost::units::quantity<boost::units::si::dimensionless>>
    using TranslateFrame2D = TranslateFrame<2,T>;
}

