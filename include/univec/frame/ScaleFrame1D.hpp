#pragma once

#include "univec/frame/ScaleFrame.hpp"

namespace dfpe {
    template <typename T=boost::units::quantity<boost::units::si::dimensionless>, typename U=T>
    using ScaleFrame1D = ScaleFrame<1,T,U>;
}

