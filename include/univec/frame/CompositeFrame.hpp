#pragma once

#include "univec/VectorC3D.hpp"
#include "BaseFrame.hpp"
#include <typeinfo>

namespace dfpe
{
    template <typename TT, typename UU>
class CompositeFrame: public BaseFrame<TT::leftN, TT::rightM, typename TT::leftType,typename UU::rightType>
    {
    public:
        typedef TT leftFrameType;
        typedef UU rightFrameType;

    public:
        CompositeFrame(const TT left, const UU right) : left(std::move(left)), right(std::move(right)) {}

        virtual ~CompositeFrame() = default;

        VectorC3D<typename UU::rightType> forward(const VectorC3D<typename TT::leftType> &source)         const override;
        VectorC3D<typename TT::leftType> backward(const VectorC3D<typename UU::rightType> &destination)   const override;

    public:
        const TT left;
        const UU right;

    public:
        VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> forwardRotOnlyDL (const VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> &source)       const override;
        VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> backwardRotOnlyDL(const VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> &destination) const override;

    };

    template <typename TT, typename UU>
    CompositeFrame<TT,UU> operator>>(const TT lhs, const UU rhs)
    {
        return CompositeFrame<TT,UU>(lhs, rhs);
    }
}

#include "CompositeFrame.tcc"
