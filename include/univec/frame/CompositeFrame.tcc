#pragma once
#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <iostream>
#include "RotateFrame3D.hpp"
#include <type_traits>

namespace dfpe
{
    template<typename TT, typename UU>
    VectorC3D<typename UU::rightType> CompositeFrame<TT,UU>::forward(const VectorC3D<typename TT::leftType> &source) const
    {
        return right.forward(left.forward(source));
    }

    template<typename TT, typename UU>
    VectorC3D<typename TT::leftType> CompositeFrame<TT,UU>::backward(const VectorC3D<typename UU::rightType> &destination) const
    {
        return left.backward(right.backward(destination));
    }

    template<typename TT, typename UU>
    VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> CompositeFrame<TT, UU>::forwardRotOnlyDL(const VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> &source) const
    {
        return right.forwardRotOnlyDL(left.forwardRotOnlyDL(source));
    }

    template<typename TT, typename UU>
    VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> CompositeFrame<TT, UU>::backwardRotOnlyDL(const VectorC3D<boost::units::quantity<boost::units::si::dimensionless>> &destination) const
    {
        return left.backwardRotOnlyDL(right.backwardRotOnlyDL(destination));
    }

}
