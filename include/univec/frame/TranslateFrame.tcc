#pragma once
#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <iostream>

namespace dfpe
{
    template<int N, typename T>
    VectorC<N,T> TranslateFrame<N,T>::forward(const VectorC<N,T> &source) const
    {
        return source - translation;
    }

    template<int N, typename T>
    VectorC<N,T> TranslateFrame<N,T>::backward(const VectorC<N,T> &destination) const
    {
        return destination + translation;
    }

    template<int N, typename T>
    VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> TranslateFrame<N,T>::forwardRotOnlyDL(const VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> &source) const
    {
        return source;
    }

    template<int N, typename T>
    VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> TranslateFrame<N,T>::backwardRotOnlyDL(const VectorC <N,boost::units::quantity<boost::units::si::dimensionless>> &destination) const
    {
        return destination;
    }
}
