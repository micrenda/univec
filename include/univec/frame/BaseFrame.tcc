#pragma once
#include <boost/units/cmath.hpp>
#include <boost/units/quantity.hpp>
#include <iostream>
#include <typeinfo>
#include "BaseFrame.hpp"


namespace dfpe
{
    template<int N, int M, typename T, typename U>
    template<typename J>
    VectorC<M,J> BaseFrame<N,M,T,U>::forwardRotOnly(const VectorC<N,J> &source) const
    {
        VectorC<N,boost::units::quantity<boost::units::si::dimensionless>> buffer;
        for (int i = 0; i < M; i++)
            buffer[i] = source[i].value();

        buffer = forwardRotOnlyDL(buffer);

        VectorC<M,J> result;
        for (int j = 0; j < M; j++)
            result[j] = J::from_value(buffer[j].value());

        return result;
    }

    template<int N, int M, typename T, typename U>
    template<typename J>
    VectorC<N,J> BaseFrame<N,M,T,U>::backwardRotOnly(const VectorC<M,J> &destination) const
    {
        VectorC<M, boost::units::quantity<boost::units::si::dimensionless>> buffer;
        for (int j = 0; j < M; j++)
            buffer[j] = destination[j].value();

        buffer = backwardRotOnlyDL(buffer);

        VectorC<M,J> result;
        for (int i = 0; i < N; i++)
            result[i] = J::from_value(buffer[i].value());

        return result;
    }
}
