#pragma once

#include "univec/VectorC3D.hpp"
#include "univec/enum.hpp"
namespace dfpe
{

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    class EulerRotation3D
    {
    public:

        constexpr EulerRotation3D() = default;

        template<typename B>
        constexpr EulerRotation3D(
                const B &alpha,
                const B &beta,
                const B &gamma,
                RotationMode rotationMode,
                RotationAxis axisAlpha,
                RotationAxis axisBeta,
                RotationAxis axisGamma);

        template<typename B>
        constexpr EulerRotation3D(
                const B &alpha,
                const B &beta,
                RotationMode rotationMode,
                RotationAxis axisAlpha,
                RotationAxis axisBeta) : EulerRotation3D(alpha, beta, B(), rotationMode, axisAlpha, axisBeta, RotationAxis::SKIP) {}

        template<typename B>
        constexpr EulerRotation3D(const B &alpha, RotationAxis axisAlpha) : EulerRotation3D(alpha, B(), B(), RotationMode::INTRINSIC, axisAlpha, RotationAxis::SKIP, RotationAxis::SKIP) {}

    protected:

        A alpha;
        A beta;
        A gamma;

        RotationMode rotationMode;

        RotationAxis axisAlpha;
        RotationAxis axisBeta;
        RotationAxis axisGamma;

    public:

        template<typename U, typename B>
        constexpr VectorC3D<U, B> rotate(const VectorC3D<U, B> &vector) const;

    public:
        constexpr bool isTaitBryan() const;

        constexpr bool isProperEuler() const;

        constexpr bool isValid() const;
    };
}

#include "EulerRotation3D.tcc"
