#include <boost/units/cmath.hpp>
#include <boost/units/is_dimensionless.hpp>
#include "VectorP2D.hpp"


namespace dfpe
{
#ifndef GCC
    template<typename T, typename A>
    constexpr VectorP2D<T, A>::VectorP2D() { }
#else
    #ifdef ENABLE_GCC_PROPERTY
    template<typename T, typename A>
    constexpr VectorP2D<T, A>::VectorP2D() : r(this), phi(this) { }
    #else
    template<typename T, typename A>
    constexpr VectorP2D<T, A>::VectorP2D() { }
    #endif
#endif

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorP2D<T, A>::VectorP2D(const VectorP2D<U, B> &other) : VectorP2D()
    {
        m_r   = T(other.m_r);
        m_phi = A(other.m_phi);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorP2D<T, A>::VectorP2D(const U &r, const B &phi) : VectorP2D()
    {
        //check_type_compatibility<T,U>();
        //check_type_compatibility<A,B>();

        m_r   = T(r);
        m_phi = A(phi);

        if (!isNormalized())
            doNormalize();
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorP2D<T, A> &VectorP2D<T, A>::operator+=(const VectorP2D<U, B> &right)
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        *this = *this + right;
        return *this;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorP2D<T, A> &VectorP2D<T, A>::operator-=(const VectorP2D<U, B> &right)
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        *this = *this - right;
        return *this;
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorP2D<T, A> &VectorP2D<T, A>::operator*=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        m_r *= right;
        return *this;
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorP2D<T, A> &VectorP2D<T, A>::operator/=(const U &right) requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        m_r /= right;
        return *this;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorP2D<ADD(T,U), A> VectorP2D<T,A>::operator+(const VectorP2D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        typedef ADD(T,U) type;
        
        boost::units::quantity<boost::units::si::dimensionless> cosPhi = boost::units::cos(right.m_phi - m_phi);
        boost::units::quantity<boost::units::si::dimensionless> sinPhi = boost::units::sin(right.m_phi - m_phi);

        return VectorP2D<type, A>(
                boost::units::sqrt(m_r * m_r + right.m_r * right.m_r + 2. * m_r * right.m_r * cosPhi),
                m_phi + (A)(boost::units::atan2(right.m_r * sinPhi, m_r + right.m_r * cosPhi))
        );
    }


    template<typename T, typename A>
    template<typename U, typename B>
    constexpr VectorP2D<SUB(T,U), A> VectorP2D<T,A>::operator-(const VectorP2D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return *this + (-right);
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorP2D<MUL(T,U), A> VectorP2D<T,A>::operator*(const U &right) const requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        typedef MUL(T,U) type;
        return VectorP2D<type, A>(m_r * right, m_phi);

    }

    template<typename T, typename U, typename B>
    constexpr VectorP2D<MUL(T,U), B> operator*(const T &left, const VectorP2D<U, B> &right)  requires (boost::units::is_dimensionless<T>::value || std::is_arithmetic_v<T>)
    {
        typedef MUL(T,U) type;
        return VectorP2D<type, B>(left * right.m_r, right.m_phi);
    }

    template<typename T, typename A>
    template<typename U>
    constexpr VectorP2D<DIV(T,U), A> VectorP2D<T,A>::operator/(const U &right) const requires (boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        typedef DIV(T,U) type;
        return VectorP2D<type, A>(m_r / right, m_phi);
    }

    template<typename T, typename A>
    constexpr VectorP2D<UADD(T), A> VectorP2D<T,A>::operator+() const
    {
        typedef UADD(T) type;
        return VectorP2D<type, A>(*this);
    }

    template<typename T, typename A>
    constexpr VectorP2D<USUB(T), A> VectorP2D<T,A>::operator-() const
    {
        typedef USUB(T) type;
        return VectorP2D<type, A>(-m_r, m_phi);
    }


    template<typename T, typename A>
    constexpr T VectorP2D<T, A>::norm() const
    {
        return m_r;
    }

    template<typename T, typename A>
    constexpr POW(T,2) VectorP2D<T, A>::normSquared() const
    {
        return m_r * m_r;
    }

    template<typename T, typename A>
    constexpr VectorP2D<T, A> VectorP2D<T, A>::versor() const
    {
        return VectorP2D<T, A>(T::from_value(1.), m_phi);
    }

    template<typename T, typename A>
    constexpr void VectorP2D<T, A>::doVersor()
    {
        m_r = T::from_value(1.);
    }

    template<typename T, typename A>
    constexpr VectorP2D<DL(T), A> VectorP2D<T, A>::versorDl() const
    {
        return VectorP2D<DL(T), A>(1., m_phi);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr MUL(T,U) VectorP2D<T, A>::dot(const VectorP2D<U, B> &other) const
    {
        check_type_compatibility<A,B>();

        assert(isNormalized());
        assert(other.isNormalized());

        return m_r * other.m_r * boost::units::cos(angle(other));
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr MUL(T,U) VectorP2D<T,A>::operator|(const VectorP2D<U, B> &right) const
    {
        check_type_compatibility<A,B>();
        return dot(right);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr A VectorP2D<T, A>::angle(const VectorP2D<U, B> &right) const
    {
        check_type_compatibility<A,B>();

        if (*this == right) return A();
        A value = right.m_phi - m_phi;
        return value < A() ? -value : value;
    }

    template<typename T, typename A>
    template<typename TOL>
    constexpr bool VectorP2D<T, A>::isNull(const TOL& tol) const
    {
        return tol.isZeroQ(m_r);
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorP2D<T, A>::isNear(const VectorP2D<U, B> &other, const TOL& tol) const
    {
        check_type_compatibility<T,U>();

        return tol.isNearQ(m_r, other.m_r) && tol.isNearA(m_phi, other.m_phi);
    }


    template<typename T, typename A>
    constexpr bool VectorP2D<T, A>::isFinite() const
    {
        return isfinite(m_r) && isfinite(m_phi);
    }

    template<typename T, typename A>
    constexpr bool VectorP2D<T, A>::isNormal() const
    {
        return isnormal(m_r) && isnormal(m_phi);
    }

    template<typename T, typename A>
    constexpr bool VectorP2D<T, A>::isNan() const
    {
        return isnan(m_r) || isnan(m_phi);
    }

    template<typename T, typename A>
    constexpr bool VectorP2D<T, A>::isInfinite() const
    {
        return isinf(m_r) || isinf(m_phi);
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr bool VectorP2D<T, A>::operator==(const VectorP2D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        assert(isNormalized());
        assert(right.isNormalized());
        return m_r == right.m_r && m_phi == right.m_phi;
    }

    template<typename T, typename A>
    template<typename U, typename B>
    constexpr bool VectorP2D<T, A>::operator!=(const VectorP2D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        return !(*this == right);
    }



    template<typename T, typename A>
    constexpr inline void VectorP2D<T, A>::doNormalize()
    {
        if (!isfinite(m_r) || !isfinite(m_phi))
            return; // If we have inf, nan or other values, we can not doNormalize

        constexpr A qtyFullPi (boost::units::quantity<boost::units::si::plane_angle>(M_PI * boost::units::si::radians));
        constexpr A qtyTwicePi (boost::units::quantity<boost::units::si::plane_angle>(2 * M_PI * boost::units::si::radians));

        if (m_r < T())
        {
            m_r = -m_r;
            m_phi += qtyFullPi;
        }

        m_phi -= qtyTwicePi * boost::units::floor(m_phi / qtyTwicePi);

        if (m_r == T())
            m_phi = A();

        assert(isNormalized());

    }

    template<typename T, typename A>
    constexpr inline bool VectorP2D<T, A>::isNormalized() const
    {
        if (m_r == T())
            return m_phi == A();
        else
            return m_r >= T() && m_phi >= A() && m_phi < A(2. * M_PI * boost::units::si::radians);
    }


    /* Comparative operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<typename T, typename A>
    template<typename U, typename B>
    constexpr std::weak_ordering VectorP2D<T, A>::operator<=>(const VectorP2D<U, B> &right) const
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        assert(isNormalized());
        assert(right.isNormalized());

        if (m_r < right.m_r)
            return std::weak_ordering::less;
        if (m_r > right.m_r)
            return std::weak_ordering::greater;
        if (m_phi < right.m_phi)
            return std::weak_ordering::less;
        if (m_phi > right.m_phi)
            return std::weak_ordering::greater;

        return std::weak_ordering::equivalent;
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr std::optional<DIV(U,T)>
    VectorP2D<T, A>::scale(const VectorP2D<U, B> &other, const TOL& tol) const
    {
      if (!tol.isZeroQ(m_r) && !tol.isZeroQ(other.m_r))
      {
          if (tol.isNearA(m_phi, other.m_phi))
              return other.m_r / m_r;
          else if (tol.isNearA(abs(m_phi - other.m_phi), A(M_PI * boost::units::si::radians)))
              return -other.m_r / m_r;
      }
      return std::optional<DIV(U,T)>();
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorP2D<T, A>::isParallel(const VectorP2D<U, B> &other, const TOL& tol) const
    {
        auto ratio = scale(other, tol);
        return ratio.has_value();
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorP2D<T, A>::isSameDirection(const VectorP2D<U, B> &other, const TOL& tol) const
    {
        auto ratio = scale(other, tol);
        return ratio.has_value() && *ratio > 0.;
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorP2D<T, A>::isOppositeDirection(const VectorP2D<U, B> &other, const TOL& tol) const
    {
        auto ratio = scale(other, tol);
        return ratio.has_value() && *ratio < 0.;
    }

    template<typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorP2D<T, A>::isPerpendicular(const VectorP2D<U, B> &other, const TOL& tol) const
    {
        return !isNull(tol) && !other.isNull(tol) && (tol*tol).isZeroQ(dot(other));
    }

}