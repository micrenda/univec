#pragma once
#include "Matrix.hpp"
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/quantity.hpp>

#include "univec/Matrix.hpp"

#include "univec/Real.hpp"
#include "univec/Complex.hpp"
#include "univec/Quaternion.hpp"
#include "univec/Octonion.hpp"
#include "univec/Sedenion.hpp"

namespace dfpe
{

    template<typename TT=Real<boost::units::quantity<boost::units::si::dimensionless>,boost::units::quantity<boost::units::si::plane_angle>>>
    using Matrix3 = Matrix<3, 3, TT>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using RMatrix3 = Matrix<3, 3, Real<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using CMatrix3 = Matrix<3, 3, Complex<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using HMatrix3 = Matrix<3, 3, Quaternion<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using OMatrix3 = Matrix<3, 3, Octonion<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using SMatrix3 = Matrix<3, 3, Sedenion<T,A>>;
}

