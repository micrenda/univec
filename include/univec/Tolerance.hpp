#pragma once
#include "univec/utils.hpp"
#include <boost/units/is_quantity.hpp>

namespace dfpe
{
    /**
     * Represents a tolerance used to determine if two quantities or angles are equals
     *
     * @tparam T Type of the quantity
     * @tparam A Type of the angle
     */
    template<typename T, typename A>
    class Tolerance
    {
    public:
        inline constexpr Tolerance(T quantity = T::from_value(1e-10), A angle = A::from_value(1e-10)):
            quantity(std::move(quantity)), angle(std::move(angle)) {};

        inline constexpr Tolerance( const Tolerance<T,A>& other) = default;

        inline constexpr bool isNearQ(const T& v1, const T& v2) const {
                return abs(v1 - v2) < quantity;
        };

        inline constexpr bool isNearA(const A& v1, const A& v2) const
        {
            return abs(v1 - v2) < angle;
        };

        inline constexpr bool isZeroQ(const T& v) const
        {
                return abs(v) < quantity;
        };

        inline constexpr bool isZeroA(const A& v) const
        {
            return abs(v) < angle;
        };

        inline constexpr bool isOneQ(const T& v) const
        {
                return abs(v - T::from_value(1.)) < quantity;
        };

    public:
        T quantity;
        A angle;

        template<typename T1, typename A1, typename T2, typename A2>
        friend Tolerance<MUL(T1,T2), ADD(A1,A2)>
        inline constexpr operator*(const Tolerance<T1,A1>& t1, const Tolerance<T2,A2>& t2);

        template<typename T1, typename A1, int N, int P>
        friend Tolerance<POWR(T1,N,P), A>
        inline constexpr ipow(const Tolerance<T1,A1>& t);
    };

    template<typename T1, typename A1, typename T2, typename A2>
    Tolerance<MUL(T1,T2), ADD(A1,A2)>
    inline constexpr operator*(const Tolerance<T1,A1>& t1, const Tolerance<T2,A2>& t2)
    {
        return Tolerance<MUL(T1,T2), ADD(A1,A2)> (
            (abs(t1.quantity / T1::from_value(1.)) + abs(t2.quantity / T2::from_value(1.))) * boost::units::multiply_typeof_helper<T1,T2>::type::from_value(1.),
            abs(t1.angle) + abs(t2.angle)
        );
    }

    template< int N, int P, typename T1, typename A1>
    Tolerance<POWR(T1,N,P), A1>
    inline constexpr ipow(const Tolerance<T1,A1>& t)
    {
        auto ratio =  typename T1::value_type (N) / typename T1::value_type (P);

        return Tolerance<POWR(T1,N,P), A1> (
            abs(t.quantity / T1::from_value(1.) * ratio) * boost::units::power_typeof_helper<T1, boost::units::static_rational<N,P>>::type::from_value(1.),
            abs(t.angle) * ratio
        );
    }


    /**
     * Represents a tolerance used to determine if two quantities or angles are equals
     *
     * @tparam TT Type of the quantity
     */
    template<typename TT>
    class MatTolerance: public Tolerance<typename TT::quantity_type,typename TT::angle_type>
    {
    public:
        inline constexpr MatTolerance(const Tolerance<typename TT::quantity_type, typename TT::angle_type>& tol):
                Tolerance<typename TT::quantity_type,typename TT::angle_type>(tol) {};

        inline constexpr MatTolerance(typename TT::quantity_type quantity = TT::quantity_type::from_value(1e-10), typename TT::angle_type angle = TT::angle_type::from_value(1e-10)):
                Tolerance<typename TT::quantity_type,typename TT::angle_type>(quantity, angle) {};

        inline constexpr bool isNear(const TT& v1, const TT& v2) const
        {
            return v1.isNear(v2, *this);
        };

        inline constexpr bool isZero(const TT& v) const
        {
            return v.isNull(*this);
        };

        inline constexpr bool isOne(const TT& v) const
        {
            return  v.isNear(TT(TT::quantity_type::from_value(1.)), *this);
        };

    };


}