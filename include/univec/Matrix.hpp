#pragma once

#include <ostream>
#include <boost/units/quantity.hpp>
#include <boost/units/is_dimensionless.hpp>
#include <boost/units/io.hpp>

#include "univec/utils.hpp"
#include "univec/enum.hpp"

#include "univec/VectorC.hpp"
#include "univec/Real.hpp"
#include "univec/Complex.hpp"
#include "univec/Quaternion.hpp"
#include "univec/Octonion.hpp"
#include "univec/Sedenion.hpp"

namespace dfpe
{
     /**
      * A generic \f$ M \times N \f$ matrix.
      *
      * @tparam M number of rows
      * @tparam N number of cols
      * @tparam T Quantity unit
      * @tparam A Angle unit
      */
    template<int M, int N, typename TT=Real<boost::units::quantity<boost::units::si::dimensionless>,boost::units::quantity<boost::units::si::plane_angle>>>
    class Matrix
    {
    public:

        typedef typename TT::quantity_type quantity_type;
        typedef typename TT::angle_type    angle_type;

        /**
         * \brief Number of rows of the matrix
         */
        static constexpr unsigned int rows = M;

        /**
         * \brief Number of columns of the matrix
         */
        static constexpr unsigned int cols = N;

        /**
        * \brief Check if the matrix is square
        */
        static constexpr bool square = M == N;


        /// \brief Check if the matrix contains real values
        static constexpr bool real = TT::real;

        /// \brief Check if the matrix contains complex values
        static constexpr bool complex = TT::complex;

        /// \brief Check if the matrix contains quaternion values
        static constexpr bool quaternion = TT::quaternion;

        /// \brief Check if the matrix contains octonion values
        static constexpr bool octonion = TT::octonion;

        /// \brief Check if the matrix contains sedenion values
        static constexpr bool sedenion = TT::sedenion;


    public:
        /// Create an empty matrix
        constexpr Matrix() = default;

        /// Create a matrix from another matrix
        template<typename UU>
        constexpr Matrix(const Matrix<M, N, UU> &other);

        /// Create a complex matrix from another real matrix
        template<typename UU>
        constexpr Matrix(const Matrix<M, N, UU> &other) requires ( TT::dimensionality < 0 && TT::dimensionality < UU::dimensionality );

        /// Create a matrix from its components
        template<typename UU>
        constexpr Matrix(const std::array<UU, M * N> &values);

        /// Create a matrix from \f$a_{1,1}, a_{1,2}, \cdots, a_{m,n}\f$ components
        template<typename... UUs>
        constexpr Matrix(UUs... args) requires (sizeof...(UUs) == M * N ) : Matrix(std::initializer_list<std::tuple_element_t<0, std::tuple<UUs ...>>>({args...}))
        {
        }

        /// Create a matrix from \f$a_{1,1}, a_{1,2}, \cdots, a_{m,n}\f$ components
        template<typename UU>
        constexpr Matrix(std::initializer_list<UU> list) requires (!boost::units::is_quantity<UU>::value && !std::is_arithmetic_v<UU>)
        {
            mat_check_type_compatibility<TT,UU>();

            unsigned int k = 0;
            for (auto it = list.begin(); it != list.end(); it++)
            {
                m_a[k / N][k % N] = *it;
                k++;
            }
        }

        /// Create a matrix from \f$a_{1,1}, a_{1,2}, \cdots, a_{m,n}\f$ components
        template<typename U>
        constexpr Matrix(std::initializer_list<U> list) requires (boost::units::is_quantity<U>::value || std::is_arithmetic_v<U>)
        {
            left_check_type_compatibility<TT,U>();

            unsigned int k = 0;
            for (auto it = list.begin(); it != list.end(); it++)
            {
                m_a[k / N][k % N] = TT(*it);
                k++;
            }
        }

    public:
        std::array<std::array<TT, N>, M> m_a;

    public:

        constexpr operator TT&() requires ( M == 1 && N == 1) { return m_a[0][0]; };
        constexpr operator TT() const requires ( M == 1 && N == 1) { return m_a[0][0]; };

        // TODO: Implement these normes
        // https://mast.queensu.ca/~andrew/notes/pdf/2010a.pdf



        /**
         * \brief Calculates the \f$L_1\f$ norm of the matrix
         *
         * Calculates the \f$L_1\f$ norm of the matrix defined as \f$ L_{1}=\max _{1 \leq j \leq m}\left(\sum_{i=1}^{n}\left|a_{i j}\right|\right) \f$
         * If is also know as "maximum absolute col sum norm"
         */
        constexpr typename TT::quantity_type normL1() const;

        /**
          * \brief Calculates the  \f$L_infty\f$ norm of the matrix
          *
          * \f$ L_{\infty}=\max _{1 \leq i \leq n}\left(\sum_{j=1}^{m}\left|a_{i j}\right|\right) \f$
          *
          * If is also know as "maximum absolute row sum norm"
          */
        constexpr typename TT::quantity_type normLInf() const;

        /**
          * \brief Calculates the  \f$L_max\f$ norm of the matrix
          *
          * \f$ L_{\max}=\max_{i,j}\left|a_{i j}\right| \f$
          *
          * If is also know as "maximum absolute value element"
          */
        constexpr typename TT::quantity_type normLMax() const;

        /**
          * \brief Calculates the \f$L_{pq}\f$ norm of the matrix
          *
          * \f$ \|A\|_{p, q}=\left(\sum_{j=1}^{n}\left(\sum_{i=1}^{m}\left|a_{i j}\right|^{p}\right)^{\frac{q}{p}}\right)^{\frac{1}{q}} \f$
          *
          * Whenever possible, it is better to use the normL1(), normL2(), normLF() and normLInf() functions due to better performances
          */
        template<int P, int Q>
        constexpr typename TT::quantity_type normL() const;

        /**
          * \brief Calculates Frobenius norm of a matrix
          *
          * \f$ L_{2}=\sqrt{\sum_{i,j} \left|a_{i j}\right|^{2}} \f$
          *
          * This norm is also known as Euclidean, Hilbert–Schmidt, or Schur norm, and can also be calculated as normL<2,2>() (but with worse performances)
          */
        constexpr typename TT::quantity_type normF() const;

        /**
          * \brief Euclidean norm of a col or row matrix
          *
          * It is implements as Frobenius norm, but it is limited only to row or col matrices, to avoid confusion.
          *
          * \see normF()
          */
        constexpr typename TT::quantity_type norm() const requires (M == 1 || N == 1);

        /**
          * \brief Squared Euclidean norm of a col or row matrix
          *
          * It is implements as Frobenius norm, but it is limited only to row or col matrices, to avoid confusion.
          *
          * \see normF()
          */
        constexpr POW(typename TT::quantity_type,2) normSquared() const requires (M == 1 || N == 1);



        /**
         * \brief Return the order of a squared matrix
         * @return order of the square matrix
         */
        CONSTEVAL_WA unsigned int order() const requires(M==N);

        /**
           * \brief Return the order of a matrix
           * @return a std::pair containing the matrix order
           */
        CONSTEVAL_WA std::pair<unsigned int,unsigned int> orders() const;

        /**
         * \brief Check if the matrix has only zeros
         * @return true is null, false otherwise
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isNull(const TOL& tol = TOL()) const;

        /**
         * \brief Check if the matrix have near values
         * @return true is null, false otherwise
         */
        template<typename UU, typename TOL = MatTolerance<TT>>
        constexpr bool isNear(const Matrix<M,N,UU>& other, const TOL& tol = TOL()) const requires (TT::dimensionality == UU::dimensionality);

        /**
     * Check if all matrix element are finite
     * @return true is all elements are finite, false otherwise
     */
        constexpr bool isFinite() const;

        /**
         * Check if all matrix element are normal
         * @return true is all elements are normal, false otherwise
         */
        constexpr bool isNormal() const;

        /**
         * Check if any matrix element is nan
         * @return true is any element is nan, false otherwise
         */
        constexpr bool isNan() const;

        /**
         * Check if any matrix element is inf
         * @return true is any element is inf, false otherwise
         */
        constexpr bool isInfinite() const;

        /**
         * \brief Check if the matrix is identity
         * @return true is identity, false otherwise
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isIdentity(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if the matrix is singular (det() == 0)
         * @return true is identity, false otherwise
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isSingular(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if the matrix is diagonal
         * @return true is identity, false otherwise
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isDiagonal(const TOL& tol = TOL()) const  requires (M == N);

        /**
         * \brief Check if the matrix is symmetric
         * @return true is symmetric, false otherwise
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isSymmetric(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if the matrix is hermitian
         *  An matrix is hermitian if it is equal to its own conjugate transpose
         * @return true is hermitian, false hermitian
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isHermitian(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if the matrix is skew-symmetric
         * @return true is skew-symmetric, false otherwise
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isSkewSymmetric(const TOL& tol = TOL()) const requires (M == N);
        /**
         * \brief Check if the matrix is skew-hermitian
         * Check if the matrix is skew-hermitian, \f$M = -M^H \f$
         * @return true is skew-hermitian, false otherwise
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isSkewHermitian(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if the matrix is orthogonal
         * @return true is orthogonal, false otherwise
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isOrthogonal(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if the matrix is unitary.
         * Check if the matrix is unitary, \f$M^H = M^{-1}\f$, where \f$M^H\f$ is the conjugate transpose
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isUnitary(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if the matrix #isUpperTriangular or #isLowerTriangular.
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isTriangular(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if all the elements that are below the main diagonal are 0.
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isUpperTriangular(const TOL& tol = TOL()) const requires (M == N);

        /**
         * \brief Check if the matrix is in row echelon form.
         *
         * All rows consisting of only zeroes are at the bottom.
         * The leading entry of every nonzero row is to the right of the leading entry of every row above.
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isRowEchelon(const TOL& tol = TOL());

        /**
         * \brief Check if the matrix is in column echelon form.
         *
         * All columns consisting of only zeroes are at the right.
         * The leading entry of every nonzero column is below the leading entry of every column on the left.
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isColEchelon(const TOL& tol = TOL());

        /**
         * \brief Check if the matrix is in reduced row echelon form.
         *
         * Like #isRowEchelon, but the leading entry in each nonzero row is 1 and is the only nonzero entry in its column.
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isRedRowEchelon(const TOL& tol = TOL());

        /**
         * \brief Check if the matrix is in reduced column echelon form.
         *
         * Like #isColEchelon, but the leading entry in each nonzero column is 1 and is the only nonzero entry in its row.
         */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isRedColEchelon(const TOL& tol = TOL());

        /**
        * \brief Check if all the elements that are above the main diagonal are 0.
        */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isLowerTriangular(const TOL& tol = TOL()) const requires (M == N);

        /**
        * \brief Check if the matrix #isTriangular and all the elements of the diagonal are 1.
        */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isUniTriangular(const TOL& tol = TOL()) const requires (M == N);

        /**
        * \brief Check if the matrix #isTriangular and all the elements of the diagonal are 0.
        */
        template<typename TOL = MatTolerance<TT>>
        constexpr bool isStrictTriangular(const TOL& tol = TOL()) const requires (M == N);

        /**
        * \brief Trace of a matrix
        * @return Sum of all the element of the diagonal
        */
        constexpr TT tr() const requires (M == N);

        /**
        * \brief Product of all the elements of the diagonal
        * @return
        */
        constexpr M_POW(TT,N) diagonalProduct() const requires (M == N);

        /**
        * \brief Sum of all the elements of the matrix
        * @return
        */
        constexpr TT grandSum() const;

        /**
        * Transpose of a matrix
         * @return transpose of a matrix
        */
        constexpr Matrix<N, M, TT> t() const;

        /**
         * Replace a matrix with its transposed
         */
        constexpr Matrix<N, M, TT>& doT() requires (M == N);

        /**
         * Replace a matrix with its transposed conjugated
         */
        constexpr Matrix<N, M, TT>& doConjT() requires (M == N);

        /**
         * Replace a matrix with its conjugated
         */
        constexpr Matrix<N, M, TT>& doConj();

        /**
        * \brief Inverse of a matrix
        * @return inverse of a matrix
        */
        constexpr Matrix<M, N, M_POW(TT,-1)>
                inv() const requires (M == N);

        /**
        * \brief Pseudo-inverse (Moore–Penrose inverse) of a matrix
        * @return inverse of a matrix
        */
        constexpr Matrix<M, N, M_POW(TT,-1)>
                pseudoInv() const;

        /**
         * \brief Kronecker product (also know as tensor  product) of two matrices
         * Return a \f$MP \times NQ \f$ matrix containing the \f$A \otimes B\f$ product of two matrices.
         *
         * @tparam P
         * @tparam Q
         * @tparam UU
         * @param other
         * @return
         */
        template<int P, int Q, typename UU>
        constexpr Matrix<M*P, N*Q, M_MUL(TT,UU)>
        kroneckerProduct(const Matrix<P, Q, UU>& other) const;

        /**
         * \brief Direct sum  of two matrices
         * Return a \f$ (M+P) \times (N+Q) \f$ matrix containing the \f$A \oplus B\f$ direct sum of two matrices.
         *
         * @tparam P
         * @tparam Q
         * @tparam UU
         * @param other
         * @return
         */
        template<int P, int Q, typename UU>
        constexpr Matrix<M+P, N+Q, TT>
        directSum(const Matrix<P, Q, UU>& other) const;


        /**
         * \brief Kronecker sum of two matrices
         * Return a \f$(M+P) \times (N+Q) \f$ matrix containing the Kronecker sum of two matrices defined as \f$\mathrm{A} \oplus \mathrm{B} = \mathrm{A} \otimes \mathrm{I}_b+\mathrm{I}_a \otimes \mathrm{B}\f$.
         *
         * @tparam P
         * @tparam Q
         * @tparam UU
         * @param other
         * @return
         */
        template<int P, int Q, typename UU>
        constexpr Matrix<M+P, N+Q, TT>
        kroneckerSum(const Matrix<P, Q, UU>& other) const requires (M==N, P==Q);

        /**
         * \brief Entry-wise product (also know as Hadamard product or Schur product) of two matrices
         * Return a \f$M \times N \f$ matrix containing the element-wise product of two matrices.
         *
         * @tparam UU
         * @param other
         * @return
         */
        template<typename UU>
        constexpr Matrix<M, N, M_MUL(TT,UU)>
        elementwiseProduct(const Matrix<M, N, UU>& other) const;

        /**
         * \brief Entry-wise division (also know as Hadamard division or Schur division) of two matrices
         * Return a \f$M \times N \f$ matrix containing the element-wise division of two matrices.
         *
         * @tparam UU
         * @param other
         * @return
         */
        template<typename UU>
        constexpr Matrix<M, N, M_DIV(TT,UU)>
        elementwiseDivision(const Matrix<M, N, UU>& other) const;

        /**
         * \brief Frobenius product of two matrices
         * \f$ \langle A, B\rangle_{F} = \operatorname{Tr}\left(\mathbf{A}^{\dagger} \mathbf{B}\right) \f$
         *
         * @tparam P 
         * @tparam Q 
         * @tparam UU
         * @return
         */
        template<int P, int Q, typename UU>
        constexpr M_MUL(TT,UU) frobenius(const Matrix<P,Q,UU>& other) const;


        /**
         * Sub-matrix of a given matrix, where the m-row and n-col where removed
         * @return sub-matrix of a matrix
        */
        constexpr Matrix<M-1, N-1, TT> sub(unsigned int m, unsigned int n) const;

        /**
          * \brief Returns the diagonal elements
          * @return \f$M \times 1 \f$ matrix containing the diagonal elements
          */
        constexpr Matrix<M, 1, TT> diagonal() const requires (M == N);

        /**
         * \brief Zap to zero all the elements with norm less than a given threshold
         *
         * To decide if an element is bigger or smaller than the given threshold, the norm squared of the element is used.
         *
         * @param threshold
         * @return
         */
        constexpr Matrix<M, N, TT> zap(TT threshold) const;

        /**
         * \brief Zap to zero all the elements with norm less than a given threshold.
         *
         * To decide if an element is bigger or smaller than the given threshold, the norm squared of the element is used.
         *
         * @param threshold
         */
        constexpr void doZap(TT threshold);

        /**
          * \brief Returns a specified row
          * @return \f$1 \times N \f$ matrix containing the \f$m\f$-row elements
          */
        constexpr Matrix<1, N, TT> row(int m);

        /**
          * \brief Returns a specified col
          * @return \f$M \times 1 \f$ matrix containing the \f$n\f$-col elements
          */
        constexpr Matrix<M, 1, TT> col(int n);

        /**
          * \brief Determinant of a matrix
          * @return determinant of a matrix
          */
        constexpr M_POW(TT,N) det(DeterminantMethod method = DeterminantMethod::AUTO) const requires (M == N);

        /**
        * Adjugate of a matrix
        * @return adjugate of a matrix
        */
        constexpr Matrix<M, N, M_POW(TT,N-1)> adj() const requires (M == N);

        /**
        *  \brief Conjugate of a matrix
        *  Return a matrix, where any element is replaced by its complex conjugate, \f$ M^H_{i,j} = \bar{M}_{i,j} \f$
        * @return  conjugate of a matrix
        */
        constexpr Matrix<M, N, TT> conj() const;

        /**
        *  \brief Conjugate transpose of a matrix, also known as Hermitian adjoint
        *  Return a matrix, which is transposed and then any element is replaced by its complex conjugate, \f$M^H\f$
        * @return  transpose conjugate of a matrix
        */
        constexpr Matrix<N, M, TT> conjT() const;

        /**
        * Minor(< em>m, n< /em>) of a matrix
        * @return minor(< em>m, n< /em>) of a matrix
        */
        constexpr M_POW(TT,N-1) minor(unsigned int m, unsigned int n) const requires (M == N);

        /**
        * Minors of a matrix
        * @return minors of a matrix
        */
        constexpr Matrix<M, N, M_POW(TT,N-1)> minors() requires (M == N);

        /**
        * Cofactor(< em>m, n< /em>) of a matrix
        * @return cofactor(< em>m, n< /em>) of a matrix
        */
        constexpr M_POW(TT,N-1) cofactor(unsigned int m, unsigned int n) const requires (M == N);

        /**
        * Cofactors of a matrix
        * @return Cofactors of a matrix
        */
        constexpr Matrix<M, N, M_POW(TT,N-1)> cofactors() const requires (M == N);

      public:
        // Matrix in-place operations

        /**
         * Multiply the i-row by a scalar
         *
         * @tparam U
         * @param i index of the row
         * @param other scalar to multiply the row by
         */
        template<typename U>
        constexpr void doRowMul(int i, const U& other );

        /**
         * Multiply the i-row by a scalar
         *
         * @tparam U
         * @param i index of the row
         * @param other scalar to multiply the row by
         * @return
         */
        template<typename U>
        constexpr Matrix<M, N, TT> rowMul(int i, const U& other ) const;

        /**
         * Multiply the j-col by a scalar
         *
         * @tparam U
         * @param j index of the column
         * @param other scalar to multiply the column by
         */
        template<typename U>
        constexpr void doColMul(int j, const U& other );

        /**
         * Multiply the j-col by a scalar
         *
         * @tparam U
         * @param j index of the column
         * @param other scalar to multiply the column by
         * @return
         */
        template<typename U>
        constexpr Matrix<M, N, TT> colMul(int j, const U& other ) const;

        /**
         * Divide the i-row by a scalar
         *
         * @tparam U
         * @param i index of the row
         * @param other scalar to divide the row by
         */
        template<typename U>
        constexpr void doRowDiv(int i, const U& other );

        /**
         * Divide the i-row by a scalar
         *
         * @tparam U
         * @param i index of the row
         * @param other scalar to divide the row by
         * @return
         */
        template<typename U>
        constexpr Matrix<M, N, TT> rowDiv(int i, const U& other ) const;

        /**
         * Divide the j-col by a scalar
         *
         * @tparam U
         * @param j index of the columnn
         * @param other scalar to divide the column by
         */
        template<typename U>
        constexpr void doColDiv(int j, const U& other );

        /**
         * Divide the j-col by a scalar
         *
         * @tparam U
         * @param j index of the columnn
         * @param other scalar to divide the column by
         * @return
         */
        template<typename U>
        constexpr Matrix<M, N, TT> colDiv(int j, const U& other ) const;

        /**
         * Take row i1, multiply with s1 and sum it with row i2 multiplied with s2, saving the result in row i2
         *
          * @param i1 index of the first row
          * @param i2 index of the second row
          * @param s1 scaling factor for the first row
          * @param s2 scaling factor for the second row
          */
        constexpr void doRowAdd(int i1, int i2, std::optional<M_DL(TT)> s1 = std::optional<M_DL(TT)>(), std::optional<M_DL(TT)> s2 = std::optional<M_DL(TT)>());
        
        /**
         * Take row i1, multiply with s1 and sum it with row i2 multiplied with s2, saving the result in row i2
         *
          * @param i1 index of the first row
          * @param i2 index of the second row
          * @param s1 scaling factor for the first row
          * @param s2 scaling factor for the second row
          * @return
          */
        constexpr Matrix<M, N, TT> rowAdd(int i1, int i2, std::optional<M_DL(TT)> s1= std::optional<M_DL(TT)>(), std::optional<M_DL(TT)> s2= std::optional<M_DL(TT)>()) const;

        /**
         * Take col j1, multiply it with s1 and sum it with col j2 multiplied with s2, saving the result in col j2
         *
          * @param j1 index of the first column
          * @param j2 index of the second column
          * @param s1 scaling factor for the first column
          * @param s2 scaling factor for the second column
          */
        constexpr void doColAdd(int j1, int j2, std::optional<M_DL(TT)> s1= std::optional<M_DL(TT)>(), std::optional<M_DL(TT)> s2 = std::optional<M_DL(TT)>());

        /**
         * Take col j1, multiply it with s1 and sum it with col j2 multiplied with s2, saving the result in col j2
         * @param j1 index of the first column
         * @param j2 index of the second column
         * @param s1 scaling factor for the first column
         * @param s2 scaling factor for the second column
         * @return
         */
        constexpr Matrix<M, N, TT> colAdd(int j1, int j2, std::optional<M_DL(TT)> s1= std::optional<M_DL(TT)>(), std::optional<M_DL(TT)> s2 = std::optional<M_DL(TT)>()) const;


        /**
         * Take row i1, multiply it with s1 and subtract it from row i2 multiplied with s2, saving the result in row i2
         * @param i1 index of the first row
         * @param i2 index of the second row
         * @param s1 scaling factor for the first row
         * @param s2 scaling factor for the second row
         */
        constexpr void doRowSub(int i1, int i2, std::optional<M_DL(TT)> s1 = std::optional<M_DL(TT)>(), std::optional<M_DL(TT)> s2 = std::optional<M_DL(TT)>());

        /**
         * Take row i1, multiply it with s1 and subtract it from row i2 multiplied with s2, saving the result in row i2
        * @param i1 index of the first row
        * @param i2 index of the second row
        * @param s1 scaling factor for the first row
        * @param s2 scaling factor for the second row
        * @return
        */
        constexpr Matrix<M, N, TT> rowSub(int i1, int i2, std::optional<M_DL(TT)> s1= std::optional<M_DL(TT)>(), std::optional<M_DL(TT)> s2 = std::optional<M_DL(TT)>()) const;

        /**
        * Take col j1, multiply it with s1 and subtract it from col j2 multiplied with s2, saving the result in col j2
        * @param j1 index of the first column
        * @param j2 index of the second column
        * @param s1 scaling factor for the first column
        * @param s2 scaling factor for the second column
        */
        constexpr void doColSub(int j1, int j2, std::optional<M_DL(TT)> s1= std::optional<M_DL(TT)>(), std::optional<M_DL(TT)> s2 = std::optional<M_DL(TT)>());

        /**
         * Take col j1, multiply it with a s1 and subtract it from col j2 multiplied with s2, saving the result in col j2
          * @param j1 index of the first column
          * @param j2 index of the second column
          * @param s1 is a scaling factor for the first column
          * @param s2 is a scaling factor for the second column
          * @return
          */
        constexpr Matrix<M, N, TT> colSub(int j1, int j2, std::optional<M_DL(TT)> s1= std::optional<M_DL(TT)>(), std::optional<M_DL(TT)> s2 = std::optional<M_DL(TT)>()) const;

        /**
         * Swap row i1 with row i2
         *
         * @param i1 index of the first row
         * @param i2 index of the second row
         */
        constexpr void doRowSwap(int i1, int i2 );

        /**
         * Swap row i1 with row i2
         *
         * @param i1 index of the first row
         * @param i2 index of the second row
         * @return
         */
        constexpr Matrix<M, N, TT> rowSwap(int i1, int i2 ) const;

        /**
         * Swap col j1 with col j2
         *
         * @param j1 index of the first column
         * @param j2 index of the second column
         */
        constexpr void doColSwap(int j1, int j2 );

        /**
         * Swap col j1 with col j2
         *
         * @param j1 index of the first column
         * @param j2 index of the second column
         * @return
         */
        constexpr Matrix<M, N, TT> colSwap(int j1, int j2 ) const;

        /**
         * \brief Perform the reduction of the matrix using the given dfpe::ReductionMethod
         *
          * @param type which method to use for the reduction
          * @param scaling return the scaling factors used for the reduction, which can be used to calculate the determinant
          */
        constexpr void doReduction(ReductionMethod type, M_DL(TT)* scaling = nullptr);

        /**
        * \brief Perform the reduction of the matrix using the given dfpe::ReductionMethod
        *
        * @param type which method to use for the reduction
        * @param scaling return the scaling factors used for the reduction, which can be used to calculate the determinant
        * @return
         */
        constexpr Matrix<M, N, TT> reduction(ReductionMethod type, M_DL(TT)* scaling = nullptr) const;

        /**
         * Reverse the rows of the matrix
         */
        constexpr void doReverseRows();

        /**
         * Reverse the rows of the matrix
         * @return
         */
        constexpr Matrix<M, N, TT> reverseRows() const;

        /**
         * Reverse the columns of the matrix
         */
        constexpr void doReverseCols();

        /**
         * Reverse the columns of the matrix
         * @return
         */
        constexpr Matrix<M, N, TT> reverseCols() const;

        /**
         * \brief Sort the rows of the matrix according the norm squared of the leftmost element of each row
         *
         * In two rows have the same leftmost element, the next element is used, and so on.
         */
        constexpr void doSortRows(bool descending = false);

        /**
         * \brief Sort the rows of the matrix according the norm squared of the leftmost element of each row
         *
         * In two rows have the same leftmost element, the next element is used, and so on.
         *
         * @return
         */
        constexpr Matrix<M, N, TT> sortRows(bool descending = false) const;

        /**
         * \brief Sort the columns of the matrix according the norm squared of the topmost element of each column
         *
         * In two columns have the same topmost element, the next element is used, and so on.
         */
        constexpr void doSortCols(bool descending = false);

        /**
         * \brief Sort the columns of the matrix according the norm squared of the topmost element of each column
         *
         * In two columns have the same topmost element, the next element is used, and so on.
         *
         * @return
         */

        constexpr Matrix<M, N, TT> sortCols(bool descending = false) const;

        /**
         * Shuffle the rows of the matrix
         */
        template <class URNG>
        constexpr void doShuffleRows(URNG&& g);

        /**
         * Shuffle the rows of the matrix
         * @return
         */
        template <class URNG>
        constexpr Matrix<M, N, TT> shuffleRows(URNG&& g) const;

        /**
         * Shuffle the columns of the matrix
         */
        template <class URNG>
        constexpr void doShuffleCols(URNG&& g);

        /**
         * Shuffle the columns of the matrix
         * @return
         */
        template <class URNG>
        constexpr Matrix<M, N, TT> shuffleCols(URNG&& g) const;


    public:

        /**
         * Sum another matrix
         * @param right
         */
        template<typename UU>
        constexpr Matrix<M, N, TT> &operator+=(const Matrix<M, N, UU> &right);

        /**
         * Subtract another matrix
         * @param right
         */
        template<typename UU>
        constexpr Matrix<M, N, TT> &operator-=(const Matrix<M, N, UU> &right);

        /**
         * product with another matrix
         * @param right
         */

        template<typename UU>
        constexpr Matrix<M, N, TT> &operator*=(const Matrix<M, N, UU> &right) requires (M == N);

        /**
         * Scalar multiplication with another quantity
         * @param right
         */
        template<typename U>
        constexpr Matrix<M, N, TT> &operator*=(const U &right);

        /**
         * Scalar division with another quantity
         * @param right
         */
        template<typename U>
        constexpr const Matrix<M, N, TT> &operator/=(const U &right);

    public: // Relational operators

        /**
         * Equality between two matrix
         */
        template<typename UU>
        constexpr bool operator==(const Matrix<M, N, UU> &right) const;

        /**
         * Dis-equality between two matrix
         */
        template<typename UU>
        constexpr bool operator!=(const Matrix<M, N, UU> &right) const;

    public:
        constexpr TT &operator()(unsigned int m, unsigned int n);
        constexpr const TT &operator()(unsigned int m, unsigned int n) const;
    public:

        constexpr TT       &rowMajor(unsigned int i);
        constexpr const TT &rowMajor(unsigned int i) const;

        constexpr TT       &colMajor(unsigned int i);
        constexpr const TT &colMajor(unsigned int i) const;

#if __cpp_multidimensional_subscript
        constexpr TT &operator[](unsigned int i, unsigned int j);
        constexpr const TT &operator[](unsigned int i, unsigned int j) const;
#endif
    };
}

#if BOOST_UNITS_HAS_BOOST_TYPEOF

#include BOOST_TYPEOF_INCREMENT_REGISTRATION_GROUP()

BOOST_TYPEOF_REGISTER_TEMPLATE(dfpe::Matrix, 3)
#endif

namespace dfpe
{
    template<int M, int N, typename TT>
    constexpr Matrix<M, N, M_UADD(TT)> operator+(const Matrix<M, N, TT> &right);

    template<int M, int N, typename TT>
    constexpr Matrix<M, N, M_USUB(TT)> operator-(const Matrix<M, N, TT> &right);

    template<int M, int N, typename TT, typename UU>
    constexpr Matrix<M, N, M_ADD(TT,UU)> operator+(const Matrix<M, N, TT> &left, const Matrix<M, N, UU> &right);

    template<int M, int N, typename TT, typename UU>
    constexpr Matrix<M, N, M_SUB(TT,UU)> operator-(const Matrix<M, N, TT> &left, const Matrix<M, N, UU> &right);

    template<int M, int N, int P, typename TT, typename UU>
    constexpr Matrix<M, P, M_MUL(TT,UU)> operator*(const Matrix<M, N, TT> &left, const Matrix<N, P, UU> &right);

    template<int M, int N, typename TT, typename U>
    constexpr Matrix<M, N, L_MUL(TT,U)> operator*(const Matrix<M, N, TT> &left, const U &right) requires (boost::units::is_quantity<U>::value || std::is_arithmetic_v<U>);

    template<int M, int N, typename TT, typename U, typename B>
    constexpr VectorC<M, MUL(typename TT::quantity_type,U)> operator*(const Matrix<M, N, TT> &left, const VectorC<N,U,B> &right) requires (TT::real);

    template<int M, int N, typename TT, typename UU>
    constexpr Matrix<M, N, M_DIV(TT,UU)> operator/(const Matrix<M, N, TT> &left, const UU &right);

    template<int M, int N, typename TT, typename U>
    constexpr Matrix<M, N, L_DIV(TT,U)> operator/(const Matrix<M, N, TT> &left, const U &right) requires (boost::units::is_quantity<U>::value || std::is_arithmetic_v<U>);

    template<int M, int N, typename TT, typename UU>
    constexpr Matrix<M, N, M_MUL(TT,UU)> operator*(const TT &left, const Matrix<M, N, UU> &right);

    template<int M, int N, typename T, typename UU>
    constexpr Matrix<M, N, R_MUL(T,UU)> operator*(const T &left, const Matrix<M, N, UU> &right) requires (boost::units::is_quantity<T>::value || std::is_arithmetic_v<T>);

    template<int M, int N, typename TT>
    constexpr std::ostream &operator<<(std::ostream &os, const Matrix<M, N, TT> &right);


    template<int M, int N, typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using RMatrix = Matrix<M, N, Real<T,A>>;

    template<int M, int N, typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using CMatrix = Matrix<M, N, Complex<T,A>>;

    template<int M, int N, typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using HMatrix = Matrix<M, N, Quaternion<T,A>>;

    template<int M, int N, typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using OMatrix = Matrix<M, N, Octonion<T,A>>;

    template<int M, int N, typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using SMatrix = Matrix<M, N, Sedenion<T,A>>;


}

#include "Matrix.tcc"
