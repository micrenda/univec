#include <boost/units/cmath.hpp>
#include <boost/units/is_dimensionless.hpp>
#include <compare>
#include <cmath>
#include "VectorC.hpp"
#include "univec/utils.hpp"

namespace dfpe
{
#ifndef GCC
    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A>::VectorC() { }
#else
    #if ENABLE_GCC_PROPERTY
        template<int N, typename T, typename A>
        constexpr VectorC<N, T, A>::VectorC() : x(this), y(this), z(this), a(this), b(this), c(this), d(this), e(this), f(this), g(this), h(this) { }
    #else
        template<int N, typename T, typename A>
        constexpr VectorC<N, T, A>::VectorC() { }
    #endif
#endif

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A>::VectorC(const VectorC<N, U, B> &other) : VectorC()
    {
        //check_type_compatibility<T,U>();
        for (int i = 0; i < NN; i++)
            m_v[i] = T(other[i]);
    }

    template<int N, typename T, typename A>
    template<typename U>
    constexpr VectorC<N, T, A>::VectorC(std::initializer_list<U> l) requires
    (boost::units::is_dimensionless<T>::value && std::is_floating_point_v<U>) : VectorC()
    {
        //check_type_compatibility<T,U>();
        std::copy(l.begin(), l.end(), m_v.begin());
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A>::VectorC(const VectorP2D<U,B>& other) requires (N == 2) : VectorC()
    {
        DL(U) sinPhi = boost::units::sin(other.m_phi);
        DL(U) cosPhi = boost::units::cos(other.m_phi);

        m_v = {other.m_r * cosPhi, other.m_r * sinPhi};
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A>::VectorC(const VectorS3D<U,B>& other) requires (N == 3) : VectorC()
    {
        DL(U) sinTheta = boost::units::sin(other.m_theta);
        DL(U) cosTheta = boost::units::cos(other.m_theta);
        DL(U) sinPhi   = boost::units::sin(other.m_phi);
        DL(U) cosPhi   = boost::units::cos(other.m_phi);
        m_v = {
                other.m_r * sinTheta * cosPhi,
                other.m_r * sinTheta * sinPhi,
                other.m_r * cosTheta
        };
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A>::VectorC(const VectorY3D<U,B>& other) requires (N == 3):
            VectorC<3, T, A>(
                    other.m_rho * boost::units::cos(other.m_phi),
                    other.m_rho * boost::units::sin(other.m_phi),
                    other.m_z)
    {

    }

    template<int N, typename T, typename A>
    template<typename U>
    constexpr VectorC<N, T, A>::VectorC(const U &a) requires(N < 0): VectorC()
    {
        m_v[0] = a;
    }

    template<int N, typename T, typename A>
    template<typename U, typename V, typename B>
    constexpr VectorC<N, T, A>::VectorC(const U &a, const VectorC<-N-1, V, B> &vec) requires (N < 0) : VectorC()
    {
        //check_type_compatibility<T,U>();
        //check_type_compatibility<T,V>();
        //check_type_compatibility<A,B>();

        m_v[0]     = a;
        for (int i = 1; i < NN; i++)
            m_v[i] = vec[i - 1];
    }

    template<int N, typename T, typename A>
    template<typename U, typename B, typename C>
    constexpr VectorC<N, T, A>::VectorC(const VectorC<-N-1, U, B> &axis, const C &angle) requires (N < 0) : VectorC()
    {
        //check_type_compatibility<T,U>();
        //check_type_compatibility<A,B>();
        //check_type_compatibility<A,C>();

        m_v[0] = axis.norm() * cos(angle / 2.);

        boost::units::quantity<boost::units::si::dimensionless> sinAngle = sin(angle / 2.);
        for (int i = 1; i < NN; i++)
            m_v[i] = axis[i - 1] * sinAngle;
    }


    /* Addition and addition related operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<int N, typename T, typename A>
    constexpr VectorC<N, UADD(T), A>
    VectorC<N, T, A>::operator+() const
    {
        typedef UADD(T) type;
        return VectorC<N, type, A>(*this);
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, ADD(T,U), A>
    VectorC<N, T, A>::operator+(const VectorC<N, U, B> &right) const
    {
        check_type_compatibility<T,U>();

        typedef ADD(T,U) type;
        VectorC<N, type, A> result;
        for (int i = 0; i < NN; i++)
            result[i] = m_v[i] + right.m_v[i];
        return result;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A> &VectorC<N, T, A>::operator+=(const VectorC<N, U, B> &right)
    {
        check_type_compatibility<T,U>();
        check_type_compatibility<A,B>();

        for (int i = 0; i < NN; i++)
            m_v[i] += right.m_v[i];
        return *this;
    }


    /* Subtraction and subtraction related operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<int N, typename T, typename A>
    constexpr VectorC<N, USUB(T), A>
    VectorC<N, T, A>::operator-() const
    {
        typedef USUB(T) type;
        VectorC<N, type, A> result;
        for (int i = 0; i < NN; i++)
            result.m_v[i] = -m_v[i];
        return result;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A> &VectorC<N, T, A>::operator-=(const VectorC<N, U, B> &right)
    {
        check_type_compatibility<T,U>();

        for (int i = 0; i < NN; i++)
            m_v[i] -= right[i];
        return *this;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, SUB(T,U), A>
    VectorC<N, T, A>::operator-(const VectorC<N, U, B> &right) const
    {
        check_type_compatibility<T,U>();

        typedef SUB(T,U) type;
        VectorC<N, type, A> result;
        for (int i = 0; i < NN; i++)
            result.m_v[i] = m_v[i] - right.m_v[i];
        return result;
    }

    /* Multiplication and multiplication related operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A> &
    VectorC<N, T, A>::operator*=(const VectorC<N, U, B> &right) requires(N < 0 && boost::units::is_dimensionless<U>::value)
    {
        *this = *this * right;
        return *this;
    }

    template<int N, typename T, typename A>
    template<typename U>
    constexpr VectorC<N, T, A> &
    VectorC<N, T, A>::operator*=(const U &right) requires(boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        for (int i = 0; i < NN; i++)
            m_v[i] *= right;
        return *this;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, MUL(T,U), A>
    VectorC<N, T, A>::operator*(const VectorC<N, U, B> &right) const requires ( N < 0 )
    {
        VectorC<N,MUL(T,U), A> result;
        if constexpr (N == -2) // Optimized for complex
        {
            result[0] = m_v[0] * right[0] - m_v[1] * right[1];
            result[1] = m_v[0] * right[1] + m_v[1] * right[0];
        }
        else if constexpr (N == -4) // Optimized for quaternions
        {
            result[0] = m_v[0] * right[0] - m_v[1] * right[1] - m_v[2] * right[2] - m_v[3] * right[3];
            result[1] = m_v[0] * right[1] + m_v[1] * right[0] + m_v[2] * right[3] - m_v[3] * right[2];
            result[2] = m_v[0] * right[2] - m_v[1] * right[3] + m_v[2] * right[0] + m_v[3] * right[1];
            result[3] = m_v[0] * right[3] + m_v[1] * right[2] - m_v[2] * right[1] + m_v[3] * right[0];
        }
        else // General formula for octonion and sedenion
        {
            for (int i = 0; i < NN; i++)
            {
                for (int j = 0; j < NN; j++)
                {
                    const auto &p = m_mulTable[i][j];
                    result[p.first] += m_v[i] * right[j] * (p.second ? -1. : 1.);
                }
            }
        }
        return result;
    }

    template<int N, typename T, typename A>
    template<int D>
    constexpr Matrix<NN/D,NN/D,VectorC<-D,T,A>> VectorC<N,T,A>::matrix() const requires (N < 0 && D > 0 && D <= -N && is_power_of_2(D))
    {
        /*
        Matrix<NN/D,NN/D,VectorC<-D,T,A>> result;
        for (int i = 0; i < NN/D; i++)
        {
            for (int j = 0; j < NN/D; j++)
            {
                for (int d = 0; d < D; d++)
                {
                    const auto &p = m_mulTable[i][j];
                    result[i][j][d] = m_v[p.first] * (p.second ? -1. : 1.) * (j > 0 ? -1. : 1.);
                }
            }
        }

        return result;
         */
        throw std::logic_error("Not yet implemented");
        return Matrix<NN/D,NN/D,VectorC<-D,T,A>>();
    }

    template<int N, typename T, typename A>
    template<typename U>
    constexpr VectorC<N, MUL(T,U), A>
    VectorC<N, T, A>::operator*(const U &right) const requires (boost::units::is_quantity<U>::value)
    {
        VectorC<N, MUL(T,U), A> result;
        for (int i = 0; i < NN; i++)
            result[i] = m_v[i] * right;
        return result;
    }

    template<int N, typename T, typename A>
    template<typename U>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::operator*(const U &right) const requires (std::is_arithmetic_v<U>)
    {
        VectorC<N, T, A> result;
        for (int i = 0; i < NN; i++)
            result.m_v[i] = m_v[i] * (typename T::value_type) right;
        return result;
    }

    template<int N, typename T, typename U, typename B>
    constexpr VectorC<N, MUL(T,U), B>
    operator*(const T &left, const VectorC<N, U, B> &right) requires (boost::units::is_quantity<T>::value)
    {
        typedef MUL(T,U) type;
        VectorC<N, type, B> result;
        for (int i = 0; i < NN; i++)
            result[i] = left * right[i];
        return result;
    }

    template<int N, typename T, typename U, typename B>
    constexpr VectorC<N, U, B> operator*(const T &left, const VectorC<N, U, B> &right) requires (std::is_arithmetic_v<T>)
    {
        VectorC<N, U, B> result;
        for (int i = 0; i < NN; i++)
            result[i] = (typename U::value_type) left * right[i];
        return result;
    }


    /* Division and division related operator */
    // ----------------------------------------------------------------------------------------------------------------
    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A> &
    VectorC<N, T, A>::operator/=(const VectorC<N, U, B> &right) requires(N < 0 && boost::units::is_dimensionless<U>::value)
    {
        *this = *this / right;
        return *this;
    }

    template<int N, typename T, typename A>
    template<typename U>
    constexpr VectorC<N, T, A> &
    VectorC<N, T, A>::operator/=(const U &right) requires(boost::units::is_dimensionless<U>::value || std::is_arithmetic_v<U>)
    {
        *this = *this / right;
        return *this;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, DIV(T,U), A>
    VectorC<N, T, A>::operator/(const VectorC<N, U, B> &right) const requires ( N < 0 )
    {
        if (right.isNull()) [[unlikely]]
        {
            using ResultType = DIV(T,U);

            if ( !isNull() )
                return VectorC<N,DIV(T,U),A>(ResultType::from_value(std::numeric_limits<typename T::value_type>::infinity()));
            else
                return VectorC<N,DIV(T,U),A>(ResultType::from_value(std::numeric_limits<typename T::value_type>::quiet_NaN()));
        }

        return *this * right.inv();
    }

    template<int N, typename T, typename A>
    template<typename U>
    constexpr VectorC<N, DIV(T,U), A>
    VectorC<N, T, A>::operator/(const U &right) const
    {
        typedef DIV(T,U) type;
        VectorC<N, type, A> result;
        for (int i = 0; i < NN; i++)
            result[i] = m_v[i] / right;
        return result;
    }

    template<int N, typename T, typename A>
    template<typename U>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::operator/(const U &right) const requires (std::is_arithmetic_v<U>)
    {
        VectorC<N, T, A> result;
        for (int i = 0; i < NN; i++)
            result.m_v[i] = m_v[i] / (typename T::value_type) right;
        return result;
    }

    template<int N, typename T, typename U, typename B>
    constexpr VectorC<N, DIV(T,U), B>
    operator/(const T &left, const VectorC<N, U, B> &right) requires (boost::units::is_quantity<T>::value)
    {
        typedef DIV(T,U) type;
        VectorC<N, type, B> result;
        for (int i = 0; i < NN; i++)
            result[i] = left / right[i];
        return result;
    }

    template<int N, typename T, typename U, typename B>
    constexpr VectorC<N, U, B> operator/(const T &left, const VectorC<N, U, B> &right) requires (std::is_arithmetic_v<T>)
    {
        VectorC<N, U, B> result;
        for (int i = 0; i < NN; i++)
            result[i] = (typename U::value_type) left / right[i];
        return result;
    }

    /* Common vector operations */
    // ----------------------------------------------------------------------------------------------------------------
    template<int N, typename T, typename A>
    constexpr T VectorC<N, T, A>::norm() const
    {
        if constexpr ( NN == 1)
        {
            return abs(m_v[0]);
        }
        else
        {
            T result = m_v[0];
            for (int i = 1; i < NN; i++)
                result = boost::units::hypot(result, m_v[i]);
            return result;
        }
    }

    template<int N, typename T, typename A>
    constexpr T VectorC<N, T, A>::normLInf() const {
        T result = abs(m_v[0]);
        for (int i = 1; i < NN; i++)
            result = std::max(result, abs(m_v[i]));
        return result;
    }

    template<int N, typename T, typename A>
    constexpr T VectorC<N, T, A>::normL1() const {
        T result;
        for (int i = 0; i < NN; i++)
            result += abs(m_v[i]);
        return result;
    }

    template<int N, typename T, typename A>
    constexpr T VectorC<N, T, A>::normL(typename T::value_type p) const {
        typename T::value_type result = 0;
        for (int i = 0; i < NN; i++)
            result += std::pow(abs(m_v[i].value()), p);
        return T::from_value(std::pow( result, 1./p ));
    }

    template<int N, typename T, typename A>
    constexpr POW(T,2) VectorC<N, T, A>::normSquared() const {
        POW(T,2) result;
        for (int i = 0; i < NN; i++)
            result += m_v[i] * m_v[i];
        return result;
    }

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::versor() const
    {
        VectorC<N, T, A> result(*this);
        result.doVersor();
        return result;
    }

    template<int N, typename T, typename A>
    constexpr void VectorC<N, T, A>::doVersor()
    {
        T norm = VectorC<N, T, A>::norm();
        *this /= norm / T::from_value(1.);
    }

    template<int N, typename T, typename A>
    constexpr VectorC<N, DL(T), A>
    VectorC<N, T, A>::versorDl() const {
        T scale(VectorC<N, T, A>::norm());
        return *this / scale;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr MUL(T,U)
    VectorC<N, T, A>::dot(const VectorC<N, U, B> &other) const {
        MUL(T,U) result;
        for (int i = 0; i < NN; i++)
            result += m_v[i] * other[i];
        return result;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr MUL(T,U)
    VectorC<N, T, A>::operator|(const VectorC<N, U, B> &right) const
    {
        return dot(right);
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, MUL(T,U), A>
    VectorC<N, T, A>::cross(const VectorC<N, U, B> &other) const requires (N == 3 || N == 7 )
    {
        if constexpr ( N == 3 ) // Optimized solution for vector 3D
        {
            typedef MUL(T,U) type;
            return VectorC<N, type, A> (
                    m_v[1] * other[2] - m_v[2] * other[1],
                    m_v[2] * other[0] - m_v[0] * other[2],
                    m_v[0] * other[1] - m_v[1] * other[0]
            );
        }
        else // Generic solution (e.g. vector 7D)
        {
            return (VectorC<-N-1,T,A>(T(), *this) * VectorC<-N-1,T,A>(U(), other)).vector();
        }
    }

    template<int N, typename T, typename A>
    template<int P, typename U, typename B>
    constexpr VectorC<N + P, T, A> VectorC<N, T, A>::directSum(const VectorC<P, U, B> &other) const requires ( N > 0 || P > 0 )
    {
        VectorC<N + P, T, A> result;

        for (int i = 0; i < N; ++i)
            result[i] = m_v[i];

        for (int i = 0; i < P; ++i)
            result[N+i] = other[i];

        return result;
    }


    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, MUL(T,U), A>
    VectorC<N, T, A>::elementwiseProduct(const VectorC<N, U, B> &other) const requires (N >= 0)
    {
        VectorC<N, MUL(T,U), A> result;

        for (int i = 0; i < NN; i++)
            result[i] = m_v[i] * other[i];

        return result;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, DIV(T,U), A>
    VectorC<N, T, A>::elementwiseDivision(const VectorC<N, U, B> &other) const requires (N >= 0)
    {
        VectorC<N, DIV(T,U), A> result;

        for (int i = 0; i < NN; i++)
            result[i] = m_v[i] / other[i];

        return result;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, MUL(T,U), A>
    VectorC<N, T, A>::operator^(const VectorC<N, U, B> &right) const
    {
        return cross(right);
    }



    /* Angle and angle related operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr A VectorC<N, T, A>::angle(const VectorC<N, U, B> &other) const
    {
        return *this == other ? A() : A(boost::units::acos((*this | other) / (norm() * other.norm())));
    }

    template<int N, typename T, typename A>
    template<typename B>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::rotateX(B angle) const requires (N == 3)
    {
        double sinAngle = sin(angle);
        double cosAngle = cos(angle);

        VectorC<N, T, A> result;
        result[0] = 1. * m_v[0] + 0. * m_v[1] + 0. * m_v[2];
        result[1] = 0. * m_v[0] + cosAngle * m_v[1] - sinAngle * m_v[2];
        result[2] = 0. * m_v[0] + sinAngle * m_v[1] + cosAngle * m_v[2];
        return result;
    }

    template<int N, typename T, typename A>
    template<typename B>
    constexpr void VectorC<N, T, A>::doRotateX(B angle) requires (N == 3)
    {
        *this = rotateX(angle);
    }

    template<int N, typename T, typename A>
    template<typename B>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::rotateY(B angle) const requires (N == 3)
    {
        double sinAngle = sin(angle);
        double cosAngle = cos(angle);

        VectorC<N, T, A> result;
        result[0] = cosAngle * m_v[0] + 0. * m_v[1] + sinAngle * m_v[2];
        result[1] = 0. * m_v[0] + 1. * m_v[1] + 0. * m_v[2];
        result[2] = -sinAngle * m_v[0] + 0. * m_v[1] + cosAngle * m_v[2];
        return result;
    }


    template<int N, typename T, typename A>
    template<typename B>
    constexpr void VectorC<N, T, A>::doRotateY(B angle) requires (N == 3)
    {
        *this = rotateY(angle);
    }

    template<int N, typename T, typename A>
    template<typename B>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::rotateZ(B angle) const requires (N == 3)
    {
        double sinAngle = sin(angle);
        double cosAngle = cos(angle);

        VectorC<N, T, A> result;
        result[0] = cosAngle * m_v[0] - sinAngle * m_v[1] + 0. * m_v[2];
        result[1] = sinAngle * m_v[0] + cosAngle * m_v[1] + 0. * m_v[2];
        result[2] = 0. * m_v[0] + 0. * m_v[1] + 1. * m_v[2];
        return result;
    }


    template<int N, typename T, typename A>
    template<typename B>
    constexpr void VectorC<N, T, A>::doRotateZ(B angle) requires (N == 3)
    {
        *this = rotateZ(angle);
    }

    template<int N, typename T, typename A>
    template<typename U, typename B, typename C>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::rotate(C angle, const VectorC<N, U, B> &axis) const requires (N == 3)
    {
        // Rodrigues' rotation formula
        auto k = axis / axis.norm();

        auto cosA = cos(angle).value();
        auto sinA = sin(angle).value();

        VectorC<N, T, A> res;
        res += *this * cosA;
        res += k.cross(*this) * sinA;
        res += k * k.dot(*this) * (1. - cosA);
        return res;
    }


    template<int N, typename T, typename A>
    template<typename U, typename B, typename C>
    constexpr void VectorC<N, T, A>::doRotate(C angle, const VectorC<N, U, B> &axis) requires (N == 3)
    {
        *this = rotate(angle, axis);
    }

    template<int N, typename T, typename A>
    template<typename C>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::rotate(C angle) const requires (N == 2 || N == -2)
    {
        auto sinAngle = sin(angle);
        auto cosAngle = cos(angle);

        VectorC<N, T, A> result (
                cosAngle * m_v[0] - sinAngle * m_v[1],
                sinAngle * m_v[0] + cosAngle * m_v[1]
        );

        return result;
    }


    template<int N, typename T, typename A>
    template<typename C>
    constexpr void VectorC<N, T, A>::doRotate(C angle) requires (N == 2 || N == -2)
    {
        *this = rotate(angle);
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::rotateUz(const VectorC<N, U, B> &uz) const requires (N == 3)
    {
        const VectorC<N, U, B> uzVersor = uz.versor();

        double u1 = uzVersor[0].value();
        double u2 = uzVersor[1].value();
        double u3 = uzVersor[2].value();

        double up2 = u1 * u1 + u2 * u2;

        VectorC<N, T, A> result(*this);

        if (up2 > 0) {
            double up = sqrt(up2);

            T px = m_v[0], py = m_v[1], pz = m_v[2];

            result[0] = (u1 * u3 * px - u2 * py) / up + u1 * pz;
            result[1] = (u2 * u3 * px + u1 * py) / up + u2 * pz;
            result[2] = -up * px + u3 * pz;
        } else if (u3 < 0) {
            result[0] = -result[0];   // phi=0  theta=pi
            result[2] = -result[2];
        } else {

        }

        return result;
    }


    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr void VectorC<N, T, A>::doRotateUz(const VectorC<N, U, B> &uz) requires (N == 3)
    {
        *this = rotateUz(uz);
    }

    /* Comparative operators */
    // ----------------------------------------------------------------------------------------------------------------
    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr std::strong_ordering VectorC<N, T, A>::operator<=>(const VectorC<N, U, B> &right) const
    {
        check_type_compatibility<T,U>();

        auto v1 = normSquared();
        auto v2 = right.normSquared();
        if (v1 < v2)
            return std::strong_ordering::less;
        if (v1 > v2)
            return std::strong_ordering::greater;

        for (int i = 0; i < NN; i++) {
            if (m_v[i] < right[i])
                return std::strong_ordering::less;
            if (m_v[i] > right[i])
                return std::strong_ordering::greater;
        }
        return std::strong_ordering::equal;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr bool VectorC<N, T, A>::operator==(const VectorC<N, U, B> &right) const
    {
        check_type_compatibility<T,U>();

        return *this <=> right == std::strong_ordering::equal;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr bool VectorC<N, T, A>::operator!=(const VectorC<N, U, B> &right) const
    {
        check_type_compatibility<T,U>();

        return !(*this == right);
    }

    template<int N, typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorC<N, T, A>::isNear(const VectorC<N, U, B> &other, const TOL& tol) const
    {
        check_type_compatibility<T,U>();
        for (int i = 0; i < NN; i++)
            if (!tol.isNearQ(m_v[i], other.m_v[i]))
                return false;
        return true;
    }
    /* Other operators */
    // ----------------------------------------------------------------------------------------------------------------

    template<int N, typename T, typename A>
    constexpr std::ostream &operator<<(std::ostream &os, const VectorC<N, T, A> &right)
    {

        if constexpr (N < 0)
        {
            constexpr std::array<const std::string_view,4>  keys1 = { "", "i", "j", "k" };
            constexpr std::array<const std::string_view,16> keys2 = { "e₀", "e₁", "e₂", "e₃", "e₄", "e₅", "e₆", "e₇", "e₈", "e₉", "e₁₀", "e₁₁", "e₁₂", "e₁₃", "e₁₄", "e₁₅" };

            for (int i = 0; i < NN; i++)
            {
                if (i != 0)
                    os << (right[i] < T() ? " - " : " + ");
                else
                    os << (right[i] < T() ? "- " : "");

                os << abs(right[i]);

                if (N < -4 || i > 0)
                    os << " ";

                if constexpr ( N >= -4)
                    os << keys1[i];
                else
                    os << keys2[i];
            }
        }
        else
        {
            os << "(";
            for (int i = 0; i < NN; i++)
            {
                if (i != 0)
                    os << ", ";
                os << right[i];
            }
            os << ")";
        }

        return os;
    }

    template<int N, typename T, typename A>
    constexpr T &VectorC<N, T, A>::operator()(int n)
    {
        if (!(n >= 1 && n <= NN)) [[unlikely]]
            throw std::runtime_error("VectorC: (n) indexes must be between 1 and " + std::to_string(NN));

        return m_v[n - 1];
    }

    /**
     * Access element of a vector (1-based)
     * @param n
     */
    template<int N, typename T, typename A>
    constexpr const T &VectorC<N, T, A>::operator()(int n) const
    {
        if (!(n >= 1 && n <= NN)) [[unlikely]]
            throw std::runtime_error("VectorC: (n) indexes must be between 1 and " + std::to_string(NN));

        return m_v[n - 1];
    }

    /**
     * Access element of a vector (0-based)
     * @param n
     */
    template<int N, typename T, typename A>
    constexpr T &VectorC<N, T, A>::operator[](int i)
    {
        if (!(i >= 0 && i <= NN-1)) [[unlikely]]
            throw std::runtime_error("VectorC: [i] indexes must be between 0 and " + std::to_string(NN-1));

        return m_v[i];
    }

    /**
     * Access element of a vector (0-based)
     * @param n
     */
    template<int N, typename T, typename A>
    constexpr const T &VectorC<N, T, A>::operator[](int i) const
    {
        if (!(i >= 0 && i <= NN-1)) [[unlikely]]
            throw std::runtime_error("VectorC: [i] indexes must be between 0 and " + std::to_string(NN-1));

        return m_v[i];
    }

    template<int N, typename T, typename A>
    thread_local std::uniform_real_distribution<typename T::value_type> VectorC<N, T, A>::m_distribution;


    /* Validity check operations */
    // ----------------------------------------------------------------------------------------------------------------
    template<int N, typename T, typename A>
    template<typename TOL>
    constexpr bool VectorC<N, T, A>::isNull(const TOL& tol) const {
        for (int i = 0; i < NN; i++)
            if (!tol.isZeroQ(m_v[i]))
                return false;
        return true;
    }

    template<int N, typename T, typename A>
    constexpr bool VectorC<N, T, A>::isFinite() const {
        for (int i = 0; i < NN; i++)
            if (!isfinite(m_v[i]))
                return false;
        return true;
    }

    template<int N, typename T, typename A>
    constexpr bool VectorC<N, T, A>::isNormal() const {
        for (int i = 0; i < NN; i++)
            if (!isnormal(m_v[i]))
                return false;
        return true;
    }

    template<int N, typename T, typename A>
    constexpr bool VectorC<N, T, A>::isNan() const {
        for (int i = 0; i < NN; i++)
            if (isnan(m_v[i]))
                return true;
        return false;
    }

    template<int N, typename T, typename A>
    constexpr bool VectorC<N, T, A>::isInfinite() const {
        for (int i = 0; i < NN; i++)
            if (isinf(m_v[i]))
                return true;
        return false;
    }


    template<int N, typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr std::optional<DIV(T,U)>
    VectorC<N, T, A>::scale(const VectorC<N, U, B> &other, const TOL& tol) const
    {
        std::optional<DIV(T,U)> ratio;
        for (int i = 0; i < NN; i++)
        {
            if (!tol.isZeroQ(m_v[i]) && !tol.isZeroQ(other.m_v[i]))
            {
                if (!ratio.has_value())
                    ratio = std::make_optional(other.m_v[i] / m_v[i]);
                else if ( *ratio != other.m_v[i] / m_v[i] )
                    return std::make_optional<DIV(T,U)>();
            }
            else if (tol.isZeroQ(m_v[i]) && tol.isZeroQ(other.m_v[i]))
            {
                // Do nothing
            }
            else
            {
                return std::optional<DIV(T,U)>();
            }
        }
        return ratio;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorC<N, T, A>::isParallel(const VectorC<N, U, B> &other, const TOL& tol) const
    {
        auto ratio = scale(other, tol);
        return ratio.has_value();
    }

    template<int N, typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorC<N, T, A>::isSameDirection(const VectorC<N, U, B> &other, const TOL& tol) const
    {
        typedef DIV(T,U) type;
        auto ratio = scale(other, tol);
        return ratio.has_value() && *ratio > type();
    }

    template<int N, typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorC<N, T, A>::isOppositeDirection(const VectorC<N, U, B> &other, const TOL& tol) const
    {
        typedef DIV(T,U) type;
        auto ratio = scale(other, tol);
        return ratio.has_value() && *ratio < type();
    }

    template<int N, typename T, typename A>
    template<typename U, typename B, typename TOL>
    constexpr bool VectorC<N, T, A>::isPerpendicular(const VectorC<N, U, B> &other, const TOL& tol) const
    {
        return !isNull(tol) && !other.isNull(tol) && (tol*tol).isZeroQ(dot(other));
    }

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::conj() const requires (N < 0)
    {
        VectorC<N, T, A> value = *this;
        value.doConj();
        return value;
    }

    template<int N, typename T, typename A>
    constexpr void VectorC<N, T, A>::doConj() requires (N < 0)
    {
        for (int i = 1; i < NN; ++i)
            m_v[i] = - m_v[i];
    }

    template<int N, typename T, typename A>
    constexpr VectorC<-N-1, T, A> VectorC<N, T, A>::vector() const requires (N < 0)
    {
        VectorC<-N-1, T, A> result;
        for (int i = 1; i < NN; i++)
            result[i-1] = m_v[i];
        return result;
    }

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::scalar() const requires (N < 0)
    {
        VectorC<N, T, A> result;

        result.m_v[0]     = m_v[0];
        for (int i = 1; i < NN; i++)
            result.m_v[i] = T();
        return result;
    }

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> VectorC<N, T, A>::pure() const requires (N < 0)
    {
        VectorC<N, T, A> result;

        result.m_v[0]     = T();
        for (int i = 1; i < NN; i++)
            result.m_v[i] = m_v[i];
        return result;
    }

    template<int N, typename T, typename A>
    constexpr A VectorC<N, T, A>::angle() const requires (N < 0 && N == -4)
    {
        return A( 2. * atan2(vector().norm(), m_v[0]) );
    }

    template<int N, typename T, typename A>
    constexpr VectorC<-N-1, T, A> VectorC<N, T, A>::axis() const requires (N < 0 && N == -4 )
    {
        VectorC<-N-1, T, A> vec = vector();
        return !vec.isNull() ? (vec / sin( angle() / 2.).value()) : vec;
    }

    template<int N, typename T, typename A>
    template<typename U, typename B>
    constexpr VectorC<-N-1, U, B> VectorC<N, T, A>::rotate(const VectorC<-N-1, U, B> &vector) const requires (N < 0)
    {
        return ((*this * VectorC<N, T, A>(U(), vector)) * inv()).vector();
    }

    template<int N, typename T, typename A>
    constexpr VectorC<N, POW(T,-1), A> VectorC<N, T, A>::inv() const requires ( N < 0 )
    {
        return conj() / normSquared();
    }

    template<int N, typename T, typename A>
    template<typename TOL>
    constexpr bool VectorC<N, T, A>::isPure(const TOL& tol) const requires ( N < 0 )
    {
        return tol.isZeroQ(m_v[0]);
    }

    template<int N, typename T, typename A>
    template<typename TOL>
    constexpr bool VectorC<N, T, A>::isScalar(const TOL& tol) const requires ( N < 0 )
    {
        for (int i = 1; i < NN; ++i)
            if (!tol.isZeroQ(m_v[i]))
                return false;
        return true;
    }

    template<int N, typename T, typename A>
    constexpr  VectorC<N, T, A> exp(const VectorC<N, T, A> &arg) requires (N < 0 && boost::units::is_dimensionless<T>::value)
    {
        T norm = arg.vector().norm();
        A ang  = A( norm * boost::units::si::radians );

        VectorC<-N-1, T, A> v;

        if (!isnan(norm) && norm != T())
            v = arg.vector() / norm * sin(ang).value();

        return exp(arg.a) * VectorC<N, T, A>(T(cos(ang)), v);
    }

    template<int N, typename T, typename A>
    constexpr  VectorC<N, T, A> log(const VectorC<N, T, A> &arg) requires (N < 0 && boost::units::is_dimensionless<T>::value)
    {
        T norm = arg.norm();

        if (isnan(norm))
            norm = T();

        return VectorC<N, T, A>(log(norm), arg.vector() / norm * A(acos(arg.a / norm)) / A::from_value(1.));
    }



    template< int P, int Q, int N, typename T, typename A>
    constexpr VectorC<N, POWR(T,P,Q), A> pow(const VectorC<N, T, A> &arg) requires (N < 0)
    {
        // This will never work
        A                angle  = arg.angle();
        VectorC<-N-1, T, A> versor = arg.vector().versor();

        if (isnan(versor.normSquared()))
            versor = VectorC<3, T, A>();

        typename T::value_type e = (typename T::value_type) P /  (typename T::value_type) Q;
        return std::pow(arg.norm().value(), e) * VectorC<N, T, A>(cos(angle * e), versor * sin(angle * e)) ;
    }

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> cos(const VectorC<N, T, A> &arg)  requires (N < 0 && boost::units::is_dimensionless<T>::value)
    {
        A  z  = A(arg.vector().norm() * boost::units::si::radians);
        typename A::value_type  zz = (z / (1. * boost::units::si::radians)).value();
        T    w = T((-sin(arg.a * boost::units::si::radians) * std::sinh(zz) / z).value());
        return(VectorC<N, T, A>(cos(arg[0] * boost::units::si::radians) * std::cosh(zz), w * arg.vector()));
    }

    template<int N, typename T, typename A>
    constexpr VectorC<N, T, A> sin(const VectorC<N, T, A> &arg)  requires (N < 0 && boost::units::is_dimensionless<T>::value)
    {
        A  z  = A(arg.vector().norm() * boost::units::si::radians);
        typename A::value_type  zz = (z / (1. * boost::units::si::radians)).value();
        T    w = T((+cos(arg.a * boost::units::si::radians) * std::sinh(zz) / z).value());
        return(VectorC<N, T, A>(sin(arg[0] * boost::units::si::radians) * std::cosh(zz), w * arg.vector()));
    }

    template<int N, typename T, typename A>
    CONSTEVAL_WA std::string_view VectorC<N, T, A>::symbol()
    {
        switch (N)
        {
            case -1:
                return "ℝ";
            case -2:
                return "ℂ";
            case -4:
                return "ℍ";
            case -8:
                return "𝕆";
            case -16:
                return "𝕊";
            case 1:
                return "ℝ¹";
            case 2:
                return "ℝ²";
            case 3:
                return "ℝ³";
            case 4:
                return "ℝ⁴";
            case 5:
                return "ℝ⁵";
            case 6:
                return "ℝ⁶";
            case 7:
                return "ℝ⁷";
            case 8:
                return "ℝ⁸";
            case 9:
                return "ℝ⁹";
            case 10:
                return "ℝ¹⁰";
            default:
                return "";
        }
    }

    template<int N, typename T, typename A>
    std::string VectorC<N, T, A>::repr(bool field) const
    {
        std::stringstream ss;
        if (field) ss << symbol();
        if constexpr(N < 0) ss << "(";
        ss << *this;
        if constexpr(N < 0) ss << ")";
        return ss.str();
    }

}