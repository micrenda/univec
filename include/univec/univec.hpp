#pragma once

#include "univec/VectorC.hpp"
#include "univec/VectorC1D.hpp"
#include "univec/VectorC2D.hpp"
#include "univec/VectorC3D.hpp"
#include "univec/VectorC7D.hpp"
#include "univec/VectorP2D.hpp"
#include "univec/VectorS3D.hpp"
#include "univec/VectorY3D.hpp"

#include "univec/Real.hpp"
#include "univec/Complex.hpp"
#include "univec/Quaternion.hpp"
#include "univec/Octonion.hpp"
#include "univec/Sedenion.hpp"

#include "univec/Matrix.hpp"
#include "univec/Matrix2.hpp"
#include "univec/Matrix3.hpp"
#include "univec/Matrix4.hpp"

#include "univec/Tolerance.hpp"

#include "univec/EulerRotation3D.hpp"

#include "univec/frame/BaseFrame.hpp"
#include "univec/frame/CompositeFrame.hpp"
#include "univec/frame/RotateFrame2D.hpp"
#include "univec/frame/RotateFrame3D.hpp"
#include "univec/frame/ScaleFrame.hpp"
#include "univec/frame/ScaleFrame1D.hpp"
#include "univec/frame/ScaleFrame2D.hpp"
#include "univec/frame/ScaleFrame3D.hpp"
#include "univec/frame/TranslateFrame.hpp"
#include "univec/frame/TranslateFrame1D.hpp"
#include "univec/frame/TranslateFrame2D.hpp"
#include "univec/frame/TranslateFrame2D.hpp"

#include "univec/constants.hpp"


