#pragma once

#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/quantity.hpp>

#include "univec/Matrix.hpp"

#include "univec/Real.hpp"
#include "univec/Complex.hpp"
#include "univec/Quaternion.hpp"
#include "univec/Octonion.hpp"
#include "univec/Sedenion.hpp"

namespace dfpe
{

    template<typename TT=Real<boost::units::quantity<boost::units::si::dimensionless>,boost::units::quantity<boost::units::si::plane_angle>>>
    using Matrix1 = Matrix<1, 1, TT>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using RMatrix1 = Matrix<1, 1, Real<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using CMatrix1 = Matrix<1, 1, Complex<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using HMatrix1 = Matrix<1, 1, Quaternion<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using OMatrix1 = Matrix<1, 1, Octonion<T,A>>;

    template<typename T=boost::units::quantity<boost::units::si::dimensionless>,
            typename A=boost::units::quantity<boost::units::si::plane_angle>>
    using SMatrix1 = Matrix<1, 1, Sedenion<T,A>>;
}

