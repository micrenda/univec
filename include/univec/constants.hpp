#pragma once
#include "univec/Matrix.hpp"
#include "univec/Complex.hpp"
#include <boost/units/quantity.hpp>
#include <boost/units/systems/si.hpp>
namespace dfpe
{

    template<int N, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    CONSTEVAL_WA static Matrix<N, N, Real<T,A>> getIdentity()
    {
        Matrix<N, N, Real<T,A>> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < N; i++)
        {
            for (unsigned int j = 0; j < N; j++)
            {
                result.m_a[i][j] = i == j ? Real<T,A>(T::from_value(1.)) : Real<T,A>();
            }
        }
        return result;
    }

    template<int N, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    CONSTEVAL_WA static Matrix<N, N, Real<T,A>> getMetricTensorG()
    {
        Matrix<N, N, Real<T,A>> result;
        // Cycling from zero based array to optimize performances
        for (unsigned int i = 0; i < N; i++)
        {
            for (unsigned int j = 0; j < N; j++)
            {
                result.m_a[i][j] = i == j ? Real<T,A>(T::from_value(i == 0 ? 1. : -1)) : Real<T,A>();
            }
        }
        return result;
    }

    template<int K, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    CONSTEVAL_WA Matrix<2, 2, Complex<T,A>> getSigmaPauli()
    {
        Matrix<2, 2, Complex<T,A>> result;
        if constexpr (K == 0)
        {
            result(1,1) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(2,2) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
        }
        else if constexpr (K == 1)
        {
            result(1,2) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(2,1) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
        }
        else if constexpr (K == 2)
        {
            result(1,2) = Complex<T,A>(T::from_value( 0.), T::from_value(-1.));
            result(2,1) = Complex<T,A>(T::from_value( 0.), T::from_value( 1.));
        }
        else if constexpr (K == 3)
        {
            result(1,1) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(2,2) = Complex<T,A>(T::from_value(-1.), T::from_value( 0.));
        }
        else
            static_assert(value<false, T, A>, "Invalid index for Pauli sigma matrix");

        return result;
    }

    template<int K, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    CONSTEVAL_WA Matrix<4, 4, Complex<T,A>> getGammaDirac()
    {
        Matrix<4, 4, Complex<T,A>> result;
        if constexpr (K == 0)
        {
            result(1,1) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(2,2) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(3,3) = Complex<T,A>(T::from_value(-1.), T::from_value( 0.));
            result(4,4) = Complex<T,A>(T::from_value(-1.), T::from_value( 0.));
        }
        else if constexpr (K == 1)
        {
            result(1,4) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(2,3) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(3,2) = Complex<T,A>(T::from_value(-1.), T::from_value( 0.));
            result(4,1) = Complex<T,A>(T::from_value(-1.), T::from_value( 0.));
        }
        else if constexpr (K == 2)
        {
            result(1,4) = Complex<T,A>(T::from_value( 0.), T::from_value(-1.));
            result(2,3) = Complex<T,A>(T::from_value( 0.), T::from_value( 1.));
            result(3,2) = Complex<T,A>(T::from_value( 0.), T::from_value( 1.));
            result(4,1) = Complex<T,A>(T::from_value( 0.), T::from_value(-1.));
        }
        else if constexpr (K == 3)
        {
            result(1,3) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(2,4) = Complex<T,A>(T::from_value(-1.), T::from_value( 0.));
            result(3,1) = Complex<T,A>(T::from_value(-1.), T::from_value( 0.));
            result(4,2) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
        }
        else if constexpr (K == 5)
        {
            result(1,3) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(2,4) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(3,1) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
            result(4,2) = Complex<T,A>(T::from_value( 1.), T::from_value( 0.));
        }
        else
            static_assert(value<false, T, A>, "Invalid index for Dirac-Pauli gamma matrix");

        return result;
    }

    /**
     * Identity real matrix \f$\I_n\f$
     */
    template<int N, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Real<T, A>> RI = getIdentity<N, T, A>();

    /**
     * Identity complex matrix \f$\I_n\f$
     */
    template<int N, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Complex<T,A>> CI = getIdentity<N, T, A>();

    /**
     * Identity quaternion matrix \f$\I_n\f$
     */
    template<int N, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Quaternion<T, A>> HI = getIdentity<N, T, A>();

    /**
     * Identity octonion matrix \f$\I_n\f$
     */
    template<int N, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Octonion<T, A>> OI = getIdentity<N, T, A>();

    /**
     * Identity sedenion matrix \f$\I_n\f$
     */
    template<int N, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Sedenion<T, A>> SI = getIdentity<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor matrix \f$g_{\mu \nu}\f$ or \f$g^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Real<T,A>> MetricTensorG = getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor complex matrix \f$g_{\mu \nu}\f$ or \f$g^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Complex<T,A>> CMetricTensorG = getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor quaternion matrix \f$g_{\mu \nu}\f$ or \f$g^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Quaternion<T,A>> HMetricTensorG = getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor octonion matrix \f$g_{\mu \nu}\f$ or \f$g^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Octonion<T,A>> OMetricTensorG = getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor sedenion matrix \f$g_{\mu \nu}\f$ or \f$g^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Sedenion<T,A>> SMetricTensorG = getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor matrix \f$\eta_{\mu \nu}\f$ or \f$\eta^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Real<T,A>> MetricTensorEta = -getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor complex matrix \f$\eta_{\mu \nu}\f$ or \f$\eta^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Complex<T,A>> CMetricTensorEta = -getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor quaternion matrix \f$\eta_{\mu \nu}\f$ or \f$\eta^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Quaternion<T,A>> HMetricTensorEta = -getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor octonion matrix \f$\eta_{\mu \nu}\f$ or \f$\eta^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Octonion<T,A>> OMetricTensorEta = -getMetricTensorG<N, T, A>();

    /**
     * Metric \f$4 \times 4\f$ tensor sedenion matrix \f$\eta_{\mu \nu}\f$ or \f$\eta^{\mu \nu}\f$
     */
    template<int N=4, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<N, N, Sedenion<T,A>> SMetricTensorEta = -getMetricTensorG<N, T, A>();


    /**
     * Pauli spin matrix \f$\sigma_k\f$
     */
    template<int K, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<2, 2, Complex<T,A>> pauliSigma = getSigmaPauli<K, T, A>();

    /**
     * Dirac-Pauli \f$\gamma^k\f$ matrix
     */
    template<int K, typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<4, 4, Complex<T,A>> diracGamma = getGammaDirac<K, T, A>();

    /**
     * \brief Dirac-Pauli \f$\gamma^mu\f$ matrix
     */
    template<typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<4, 4, Complex<T,A>> diracGammaMu(int mu)
    {
        switch (mu)
        {
            case 0:
                return diracGamma<0,T,A>;
            case 1:
                return diracGamma<1,T,A>;
            case 2:
                return diracGamma<2,T,A>;
            case 3:
                return diracGamma<3,T,A>;
            case 5:
                return diracGamma<5,T,A>;
            default:
                throw std::invalid_argument("Invalid index " + std::to_string(mu) + " for Dirac-Pauli gamma matrix");
        }
    }

    /**
     * \brief Pauli \f$\sigma_k\f$ matrix
     */
    template<typename T=boost::units::quantity<boost::units::si::dimensionless>, typename A=boost::units::quantity<boost::units::si::plane_angle>>
    constexpr Matrix<2, 2, Complex<T,A>> pauliSigmaK(int k)
    {
        switch (k)
        {
            case 1:
                return pauliSigma<1,T,A>;
            case 2:
                return pauliSigma<2,T,A>;
            case 3:
                return pauliSigma<3,T,A>;
            default:
                throw std::invalid_argument("Invalid index " + std::to_string(k) + " for Pauli sigma matrix");
        }
    }
}