#include <iostream>

#include <boost/units/quantity.hpp>

#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/angle/degrees.hpp>

#include "univec/VectorS3D.hpp"

#include <gtest/gtest.h>

#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"


using namespace std;
using namespace boost::units;
using namespace boost::units::si;
using namespace boost::units::degree;
using namespace dfpe;
using namespace testing;

TEST(VectorS3D, isParallel)
{
    EXPECT_TRUE(VectorS3D<QtySiLength>  (1.*meter, 0.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 2.*meter, 0.*degrees, 15.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength>  (1.*meter, 0.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> (-2.*meter, 0.*degrees, 15.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength>  (1.*meter, 90.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 2.*meter, 90.*degrees, 15.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength>  (1.*meter, 90.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 2.*meter,-90.*degrees, 15.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength>  (1.*meter, 90.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 2.*meter, 90.*degrees, 195.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength>  (1.*meter, 90.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 2.*meter,-90.*degrees,-165.*degrees)));

    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 2.*meter,  45.*degrees, 15.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 2.*meter, 135.*degrees, 15.*degrees)));

    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 0.*meter, 0.*degrees, 15.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (0.*meter, 0.*degrees, 15.*degrees).isParallel(VectorS3D<QtySiLength> ( 1.*meter, 0.*degrees, 15.*degrees)));
}


TEST(VectorS3D, isSameDirection)
{
    EXPECT_TRUE (VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isSameDirection(VectorS3D<QtySiLength> ( 2.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isSameDirection(VectorS3D<QtySiLength> (-2.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_TRUE (VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 0.*degrees).isSameDirection(VectorS3D<QtySiLength> ( 2.*meter, 90.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 0.*degrees).isSameDirection(VectorS3D<QtySiLength> ( 2.*meter,-90.*degrees, 0.*degrees)));

    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isSameDirection(VectorS3D<QtySiLength> ( 2.*meter,  45.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isSameDirection(VectorS3D<QtySiLength> ( 2.*meter, 135.*degrees, 0.*degrees)));

    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isSameDirection(VectorS3D<QtySiLength> ( 0.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (0.*meter, 0.*degrees, 0.*degrees).isSameDirection(VectorS3D<QtySiLength> ( 1.*meter, 0.*degrees, 0.*degrees)));
}

TEST(VectorS3D, isOppositeDirection)
{
    EXPECT_FALSE (VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isOppositeDirection(VectorS3D<QtySiLength> ( 2.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isOppositeDirection(VectorS3D<QtySiLength> (-2.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_FALSE (VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 0.*degrees).isOppositeDirection(VectorS3D<QtySiLength> ( 2.*meter, 90.*degrees, 0.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 0.*degrees).isOppositeDirection(VectorS3D<QtySiLength> ( 2.*meter,-90.*degrees, 0.*degrees)));

    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isOppositeDirection(VectorS3D<QtySiLength> ( 2.*meter,  45.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isOppositeDirection(VectorS3D<QtySiLength> ( 2.*meter, 135.*degrees, 0.*degrees)));

    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isOppositeDirection(VectorS3D<QtySiLength> ( 0.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (0.*meter, 0.*degrees, 0.*degrees).isOppositeDirection(VectorS3D<QtySiLength> ( 1.*meter, 0.*degrees, 0.*degrees)));
}

TEST(VectorS3D, isPerpendicular)
{
    EXPECT_FALSE (VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> (-2.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_FALSE (VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter, 90.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter,-90.*degrees, 0.*degrees)));

    EXPECT_TRUE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter,  90.*degrees, 0.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter, -90.*degrees, 0.*degrees)));

    EXPECT_TRUE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter,  90.*degrees, 45.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter, -90.*degrees, 45.*degrees)));

    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 90.*degrees,  45.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter,  90.*degrees,  45.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 90.*degrees, -45.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter,  90.*degrees, -45.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength>  (1.*meter, 90.*degrees, -45.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter,  90.*degrees,  45.*degrees)));
    EXPECT_TRUE(VectorS3D<QtySiLength>  (1.*meter, 90.*degrees,  45.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 2.*meter, -90.*degrees, -45.*degrees)));

    EXPECT_FALSE(VectorS3D<QtySiLength> (1.*meter, 0.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 0.*meter, 0.*degrees, 0.*degrees)));
    EXPECT_FALSE(VectorS3D<QtySiLength> (0.*meter, 0.*degrees, 0.*degrees).isPerpendicular(VectorS3D<QtySiLength> ( 1.*meter, 0.*degrees, 0.*degrees)));
}


TEST(VectorS3D, norm)
{
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (-3.  * meter,  45. * degrees, 80. * degrees).norm(), 3. * si::meter);
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> ( 3.5 * meter,  4. * degrees, 3.5 * degrees).norm(), 3.5 * si::meter);
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> ( 0.  * meter,  4. * degrees, 0. * degrees).norm(), 0.  * si::meter);
}

TEST(VectorS3D, normSquared)
{
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (-3.  * meter,  45. * degrees, 80. * degrees).normSquared(), 9. * si::meter);
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> ( 2. * meter,  4. * degrees, 3.5 * degrees).normSquared(), 4. * si::meter);
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> ( 0.  * meter,  4. * degrees, 0. * degrees).normSquared(), 0.  * si::meter);
}

TEST(VectorS3D, isNull)
{
    EXPECT_FALSE(VectorS3D<QtySiLength> ( 3. * meters, 4. * degrees, 3.5 * degrees).isNull());
    EXPECT_TRUE(VectorS3D<QtySiLength> ( 0. * meters, 4. * degrees, 3.5 * degrees).isNull());
    EXPECT_TRUE(VectorS3D<QtySiLength>  ( 0. * meters, 0. * degrees, 0. * degrees).isNull());
}

TEST(VectorS3D, versor)
{
    VectorS3D<QtySiLength> v1( 3. * meters, 4. * degrees, 3.5 * degrees);
    VectorS3D<QtySiLength> result( 1. * meters, 4. * degrees, 3.5 * degrees);
    VEC_EXPECT_NEAR(v1.versor(), result);

    v1.doVersor();
    VEC_EXPECT_NEAR(v1, result);

    //VEC_EXPECT_NEAR(VectorS3D<QtySiLength> ( -3. * meters, 90. * degrees, 45. * degrees).versor(), VectorS3D<QtySiLength> ( 1. * meters, 270. * degrees, 135. * degrees));  // This does not work as it should
}

TEST(VectorS3D, versorDl)
{
    VEC_EXPECT_NEAR(VectorS3D<QtySiLength> ( 3. * meters, 4. * degrees, 3.5 * degrees).versorDl(), VectorS3D<QtySiDimensionless> ( 1., 4. * degrees, 3.5 * degrees));
//    VEC_EXPECT_NEAR(VectorS3D<QtySiLength> ( -3. * meters, 90. * degrees, 45. * degrees).versorDl(), VectorS3D<QtySiDimensionless> ( 1., 270. * degrees, 135. * degrees));  // This does not work as it should
}

TEST(VectorS3D, dot)
{
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (   1. * meters, 430. * degrees, 0.  * degrees).dot( VectorS3D<QtySiLength> (  1. * meter,   10. *degrees,  0.  * degrees)),   0.5 * square_meter);
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (   1. * meters,   0. * degrees, 0.  * degrees).dot( VectorS3D<QtySiLength> (  1. * meter,   90. *degrees,  0.  * degrees)),   0. * square_meter);
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (   1. * meters,   0. * degrees, 0.  * degrees).dot( VectorS3D<QtySiLength> (  1. * meter,    0. *degrees,  0.  * degrees)),   1. * square_meter);
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (   1. * meters,   0. * degrees, 0.  * degrees).dot( VectorS3D<QtySiLength> ( -1. * meter,    0. *degrees,  0.  * degrees)),  -1. * square_meter);
}

TEST(VectorS3D, cross)
{
    VEC_EXPECT_NEAR(VectorS3D<QtySiLength> (   1. * meters,  0. * degrees, 0.  * degrees).cross( VectorS3D<QtySiLength> (1. * meter,  90. * degrees, 0. * degrees)), VectorS3D<QtySiArea> ( 1. * square_meter, 90. * degrees,  90. * degrees));
    VEC_EXPECT_NEAR(VectorS3D<QtySiLength> (   1. * meters,  0. * degrees, 0.  * degrees).cross( VectorS3D<QtySiLength> (1. * meter, -90. * degrees, 0. * degrees)), VectorS3D<QtySiArea> ( 1. * square_meter, 90. * degrees, -90. * degrees));
    VEC_EXPECT_NEAR(VectorS3D<QtySiLength> (   1. * meters,  0. * degrees, 0.  * degrees).cross( VectorS3D<QtySiLength> (1. * meter,   0. * degrees, 0. * degrees)), VectorS3D<QtySiArea> ( 0. * square_meter, 0. * degrees,    0. * degrees));
    VEC_EXPECT_NEAR(VectorS3D<QtySiLength> (   1. * meters,  0. * degrees, 0.  * degrees).cross( VectorS3D<QtySiLength> (1. * meter, 180. * degrees, 0. * degrees)), VectorS3D<QtySiArea> ( 0. * square_meter, 0. * degrees,    0. * degrees));
}

TEST(VectorS3D, angle)
{
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (  1. * meters,   0. * degrees, 0. * degrees).angle(VectorS3D<QtySiLength> ( -1. * meters,  0. * degrees,  0. * degrees)), QtySiPlaneAngle(180. * degrees));
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (  1. * meters,   0. * degrees, 0. * degrees).angle(VectorS3D<QtySiLength> (  1. * meters, 90. * degrees,  0. * degrees)), QtySiPlaneAngle( 90. * degrees));
    QTY_EXPECT_NEAR(VectorS3D<QtySiLength> (  1. * meters,   0. * degrees, 0. * degrees).angle(VectorS3D<QtySiLength> (  1. * meters,  0. * degrees, 90. * degrees)), QtySiPlaneAngle(  0. * degrees));
}

TEST(VectorS3D, isFinite)
{
    EXPECT_TRUE( VectorS3D<QtySiLength> (     1. * meters,      0. * degrees, 3.5 * degrees).isFinite());
    EXPECT_TRUE( VectorS3D<QtySiLength> (     0. * meters,      0. * degrees, 3.5 * degrees).isFinite());
    EXPECT_TRUE( VectorS3D<QtySiLength> (   999. * meters,    430. * degrees, 3.5 * degrees).isFinite());
    EXPECT_FALSE( VectorS3D<QtySiLength> (nan_val * meters,    430. * degrees, 3.5 * degrees).isFinite());
    EXPECT_FALSE( VectorS3D<QtySiLength> (     2. * meters, nan_val * degrees, 3.5 * degrees).isFinite());
    EXPECT_FALSE( VectorS3D<QtySiLength> (nan_val * meters, nan_val * degrees, 3.5 * degrees).isFinite());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (     2. * meters,     inf_val * degrees, 3.5 * degrees).isFinite());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    inf_val * meters,     inf_val * degrees, 3.5 * degrees).isFinite());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    inf_val * meters,      2. * degrees, 3.5 * degrees).isFinite());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    1. * meters,      2. * degrees, inf_val * degrees).isFinite());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    inf_val * meters, nan_val * degrees, 3.5 * degrees).isFinite());
}

TEST(VectorS3D, isNormal)
{
    EXPECT_FALSE( VectorS3D<QtySiLength> (     1. * meters,      0. * degrees, 3.5 * degrees).isNormal()); // 0 is not normal
    EXPECT_FALSE( VectorS3D<QtySiLength> (     0. * meters,      0. * degrees, 3.5 * degrees).isNormal()); // 0 is not normal
    EXPECT_TRUE( VectorS3D<QtySiLength> (   999. * meters,    430. * degrees, 3.5 * degrees).isNormal());
    EXPECT_FALSE( VectorS3D<QtySiLength> (nan_val * meters,    430. * degrees, 3.5 * degrees).isNormal());
    EXPECT_FALSE( VectorS3D<QtySiLength> (     2. * meters, nan_val * degrees, 3.5 * degrees).isNormal());
    EXPECT_FALSE( VectorS3D<QtySiLength> (nan_val * meters, nan_val * degrees, 3.5 * degrees).isNormal());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (     2. * meters,     inf_val * degrees, 3.5 * degrees).isNormal());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    inf_val * meters,     inf_val * degrees, 3.5 * degrees).isNormal());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    inf_val * meters,      2. * degrees, 3.5 * degrees).isNormal());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    inf_val * meters, nan_val * degrees, 3.5 * degrees).isNormal());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    inf_val * meters, nan_val * degrees, inf_val * degrees).isNormal());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    inf_val * meters, nan_val * degrees, nan_val * degrees).isNormal());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    1. * meters, 0. * degrees, nan_val * degrees).isNormal());
    EXPECT_FALSE(  VectorS3D<QtySiLength> (    1. * meters, 0. * degrees, inf_val * degrees).isNormal());
}

TEST(VectorS3D, isNan)
{
    // does not work
//    EXPECT_FALSE( VectorS3D<QtySiLength> (     1. * meters,      0. * degrees, 3.5 * degrees).isNan());
//    EXPECT_FALSE( VectorS3D<QtySiLength> (     0. * meters,      0. * degrees, 3.5 * degrees).isNan());
//    EXPECT_FALSE( VectorS3D<QtySiLength> (   999. * meters,    430. * degrees, 3.5 * degrees).isNan());
//    EXPECT_TRUE(  VectorS3D<QtySiLength> (nan_val * meters,    430. * degrees, 3.5 * degrees).isNan());
//    EXPECT_TRUE(  VectorS3D<QtySiLength> (     2. * meters, nan_val * degrees, 3.5 * degrees).isNan());
//    EXPECT_TRUE(  VectorS3D<QtySiLength> (nan_val * meters, nan_val * degrees, 3.5 * degrees).isNan());
//    EXPECT_FALSE( VectorS3D<QtySiLength> (     2. * meters,     inf_val * degrees, 3.5 * degrees).isNan());
//    EXPECT_FALSE( VectorS3D<QtySiLength> (    inf_val * meters,     inf_val * degrees, 3.5 * degrees).isNan());
//    EXPECT_FALSE( VectorS3D<QtySiLength> (    inf_val * meters,      2. * degrees, 3.5 * degrees).isNan());
//    EXPECT_TRUE(  VectorS3D<QtySiLength> (    inf_val * meters, nan_val * degrees, 3.5 * degrees).isNan());
//    EXPECT_TRUE(  VectorS3D<QtySiLength> (    1. * meters, 0. * degrees, nan_val * degrees).isNan());
//    EXPECT_TRUE(  VectorS3D<QtySiLength> (    inf_val * meters, 0. * degrees, nan_val * degrees).isNan());
}

TEST(VectorS3D, isInf)
{
    EXPECT_FALSE(VectorS3D<QtySiLength>(1. * meters, 0. * degrees, 3.5 * degrees).isInfinite());
    EXPECT_FALSE(VectorS3D<QtySiLength>(0. * meters, 0. * degrees, 3.5 * degrees).isInfinite());
    EXPECT_FALSE(VectorS3D<QtySiLength>(999. * meters, 430. * degrees, 3.5 * degrees).isInfinite());
    EXPECT_FALSE(VectorS3D<QtySiLength>(nan_val * meters, 430. * degrees, 3.5 * degrees).isInfinite());
    EXPECT_FALSE(VectorS3D<QtySiLength>(2. * meters, nan_val * degrees, 3.5 * degrees).isInfinite());
    EXPECT_FALSE(VectorS3D<QtySiLength>(nan_val * meters, nan_val * degrees, 3.5 * degrees).isInfinite());
    EXPECT_TRUE(VectorS3D<QtySiLength>(2. * meters, inf_val * degrees, 3.5 * degrees).isInfinite());
    EXPECT_TRUE(VectorS3D<QtySiLength>(inf_val * meters, inf_val * degrees, 3.5 * degrees).isInfinite());
    EXPECT_TRUE(VectorS3D<QtySiLength>(inf_val * meters, 2. * degrees, 3.5 * degrees).isInfinite());
    EXPECT_TRUE(VectorS3D<QtySiLength>(inf_val * meters, nan_val * degrees, 3.5 * degrees).isInfinite());
    EXPECT_TRUE(VectorS3D<QtySiLength>(1. * meters, 0. * degrees, inf_val * degrees).isInfinite());
    EXPECT_TRUE(VectorS3D<QtySiLength>(1. * meters, nan_val * degrees, inf_val * degrees).isInfinite());
    EXPECT_TRUE(VectorS3D<QtySiLength>(inf_val * meters, nan_val * degrees, inf_val * degrees).isInfinite());
    EXPECT_TRUE(VectorS3D<QtySiLength>(1. * meters, inf_val * degrees, inf_val * degrees).isInfinite());
}


TEST(VectorS3D, addition)
{
    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  90. * degrees);

    VectorS3D<QtySiLength> vec_result_1 = vec_a + vec_b;
    VectorS3D<QtySiLength> vec_result_2 = vec_a + vec_c;
    VectorS3D<QtySiLength> vec_result_3 = vec_c + vec_d;
    VectorS3D<QtySiLength> vec_result_4 = vec_a + vec_d;

    VEC_EXPECT_NEAR(vec_result_1, VectorS3D<QtySiLength> (3. * meter, 90. * degrees, 0. * degrees));
    VEC_EXPECT_NEAR(vec_result_2, VectorS3D<QtySiLength> (1. * meter, 90. * degrees, 0. * degrees));
    VEC_EXPECT_NEAR(vec_result_3, VectorS3D<QtySiLength> (1. * meter, 0. * degrees, 90. * degrees));
    VEC_EXPECT_NEAR(vec_result_4, VectorS3D<QtySiLength> (sqrt(2.) * meter, 45. * degrees, 0. * degrees));
}

TEST(VectorS3D, additionEqual)
{

    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_c_1(0. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  90. * degrees);

    vec_b += vec_a;
    VEC_EXPECT_NEAR(vec_b, VectorS3D<QtySiLength> (3. * meter, 90. * degrees, 0. * degrees));

    vec_c += vec_a;
    VEC_EXPECT_NEAR(vec_c, VectorS3D<QtySiLength> (1. * meter, 90. * degrees, 0. * degrees));

    vec_c_1 += vec_d;
    VEC_EXPECT_NEAR(vec_c_1, VectorS3D<QtySiLength> (1. * meter, 0. * degrees, 90. * degrees));

    vec_a += vec_d;
    VEC_EXPECT_NEAR(vec_a, VectorS3D<QtySiLength> (sqrt(2.) * meter, 45. * degrees, 0. * degrees));
}

TEST(VectorS3D, subtraction)
{

    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  90. * degrees);

    VectorS3D<QtySiLength> vec_result_1 = vec_a - vec_b;
    VectorS3D<QtySiLength> vec_result_2 = vec_a - vec_c;
    VectorS3D<QtySiLength> vec_result_3 = vec_c - vec_d;
    VectorS3D<QtySiLength> vec_result_4 = vec_a - vec_d;

    VEC_EXPECT_NEAR(vec_result_1, VectorS3D<QtySiLength> (1 * meter,  270. * degrees,   0. * degrees));
    VEC_EXPECT_NEAR(vec_result_2, VectorS3D<QtySiLength> (1. * meter,  90. * degrees,   0. * degrees));
    VEC_EXPECT_NEAR(vec_result_3, VectorS3D<QtySiLength> (1. * meter, 180. * degrees,   270. * degrees));
    VEC_EXPECT_NEAR(vec_result_4, VectorS3D<QtySiLength> (sqrt(2.) * meter, 135. * degrees, 0. * degrees));
}

TEST(VectorS3D, subtractionEqual)
{
    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_c_1(0. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  90. * degrees);

    vec_b -= vec_a;
    VEC_EXPECT_NEAR(vec_b, VectorS3D<QtySiLength> (1. * meter, 90. * degrees, 0. * degrees));

    vec_c -= vec_a;
    VEC_EXPECT_NEAR(vec_c, VectorS3D<QtySiLength> (1. * meter, 270. * degrees, 0. * degrees));

    vec_c_1 -= vec_d;
    VEC_EXPECT_NEAR(vec_c_1, VectorS3D<QtySiLength> (1. * meter, 180. * degrees, 270. * degrees));  // not working

    vec_a -= vec_d;
    VEC_EXPECT_NEAR(vec_a, VectorS3D<QtySiLength> (sqrt(2.) * meter, 135. * degrees, 0. * degrees));  // not working
}

TEST(VectorS3D, multiplication)
{
    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 50. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  90. * degrees);

    VEC_EXPECT_NEAR(vec_a * 5.,   VectorS3D<QtySiLength> (5. * meters, 90. * degrees, 0. * degrees));

    VEC_EXPECT_NEAR(vec_b * 105., VectorS3D<QtySiLength> (210. * meters, 90. * degrees, 50. * degrees));

    // All zero radius polar points are normalized to zero angle
    VEC_EXPECT_NEAR(vec_c * 200., VectorS3D<QtySiLength> (0. * meters, 0. * degrees, 0.* degrees));
}

TEST(VectorS3D, multiplicationEqual)
{
    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 50. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  90. * degrees);

    //MultiplicationEqual with scalar
    vec_a *= 5;
    VEC_EXPECT_NEAR(vec_a,   VectorS3D<QtySiLength> (5. * meters, 90. * degrees, 0. * degrees));

    vec_b *= 105.;
    VEC_EXPECT_NEAR(vec_b, VectorS3D<QtySiLength> (210. * meters, 90. * degrees, 50. * degrees));

    // All zero radius polar points are normalized to zero angle
    vec_c *= 200.;
    VEC_EXPECT_NEAR(vec_c, VectorS3D<QtySiLength> (0. * meters, 0. * degrees, 0.* degrees));
}

TEST(VectorS3D, divisionEqual)
{
    VectorS3D<QtySiLength> vec_a(3. * meters, 90. * degrees, 3. * degrees);
    VectorS3D<QtySiLength> vec_b(0. * meters, 90. * degrees, 7. * degrees);
    
    vec_a /= 3;
    vec_b /= 7;
    VEC_EXPECT_NEAR(vec_a, VectorS3D<QtySiLength> (1. * meters, 90. * degrees, 3. * degrees));
    VEC_EXPECT_NEAR(vec_b, VectorS3D<QtySiLength> (0. * meters, 90. * degrees, 7. * degrees));
}



TEST(VectorS3D, greaterThan)
{
    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 45. * degrees, 70. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 100. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(-1. * meters, 0. * degrees,  90. * degrees);

    EXPECT_GT(vec_b, vec_a);
    EXPECT_GT(vec_b, vec_c);
    EXPECT_GT(vec_d, vec_c);
}

TEST(VectorS3D, greaterThanOrEqualTo)
{
    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 45. * degrees, 70. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 100. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(-1. * meters, 0. * degrees,  90. * degrees);

    EXPECT_GE(vec_b, vec_a);
    EXPECT_GE(vec_b, vec_c);
    EXPECT_GE(vec_d, vec_a);
}

TEST(VectorS3D, lessThan)
{
    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 45. * degrees, 70. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 100. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(-1. * meters, 0. * degrees,  90. * degrees);

    EXPECT_LT(vec_a, vec_b);
    EXPECT_LT(vec_c, vec_b);
    EXPECT_LT(vec_c, vec_d);
}

TEST(VectorS3D, lessThanOrEqualTo)
{
    VectorS3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_b(2. * meters, 45. * degrees, 70. * degrees);
    VectorS3D<QtySiLength> vec_c(0. * meters, 100. * degrees, 0. * degrees);
    VectorS3D<QtySiLength> vec_d(-1. * meters, 0. * degrees,  90. * degrees);

    EXPECT_LE(vec_a, vec_b);
    EXPECT_LE(vec_c, vec_b);
    EXPECT_LE(vec_a, vec_d);
}

TEST(VectorS3D, isEqualTo)
{
    VectorS3D<QtySiLength> vec_a(3. * meters,  0. * degrees, 3.5 * degrees);
    VectorS3D<QtySiLength> vec_b(3. * meters,  0. * degrees, 3.5 * degrees);

    VEC_EXPECT_NEAR(vec_a, vec_b);
}

TEST(VectorS3D, notEqualTo)
{
    VectorS3D<QtySiLength> vec_a(3. * meters,  0. * degrees, 3.5 * degrees);
    VectorS3D<QtySiLength> vec_b(3. * meters,  90. * degrees, 3.5 * degrees);
    VectorS3D<QtySiLength> vec_c(9. * meters, 55. * degrees, 3.5 * degrees);

    EXPECT_NE(vec_a, vec_b);
    EXPECT_NE(vec_b, vec_c);
}

TEST(VectorS3D, toCartesian)
{
    VectorS3D<QtySiLength> vec_a(5. * meters,  90. * degrees, 0. * degrees);
    VectorC<3,QtySiLength> vec_b(vec_a);
    VectorC<3,QtySiLength> result1(5. * meters, 0. * meters, 0. * meters);

    VEC_EXPECT_NEAR(vec_b, result1);

    VectorS3D<QtySiLength> vec_c(5. * meters,  0. * degrees, 0. * degrees);
    VectorC<3,QtySiLength> vec_d(vec_c);
    VectorC<3,QtySiLength> result2(0. * meters, 0. * meters, 5. * meters);

    VEC_EXPECT_NEAR(vec_d, result2);

    VectorS3D<QtySiLength> vec_e(sqrt(2) * meters,  45. * degrees, 90. * degrees);
    VectorC<3,QtySiLength> vec_f(vec_e);
    VectorC<3,QtySiLength> result3(0. * meters, 1. * meters, 1. * meters);

    VEC_EXPECT_NEAR(vec_f, result3);
}

TEST(VectorS3D, toCylindrical)
{
    VectorS3D<QtySiLength> vec_a(5. * meters,  90. * degrees, 0. * degrees);
    VectorY3D<QtySiLength>  vec_b(vec_a);
    VectorY3D<QtySiLength> result1(5. * meters,  0. * degrees, 0. * meter);

    VEC_EXPECT_NEAR(vec_b, result1);

    VectorS3D<QtySiLength> vec_c(5. * meters,  0. * degrees, 0. * degrees);
    VectorY3D<QtySiLength> vec_d(vec_c);
    VectorY3D<QtySiLength> result2(0. * meters,  0. * degrees, 5. * meter);

    VEC_EXPECT_NEAR(vec_d, result2);

    VectorS3D<QtySiLength> vec_e(sqrt(2) * meters,  45. * degrees, 90. * degrees);
    VectorY3D<QtySiLength> vec_f(vec_e);
    VectorY3D<QtySiLength> result3(1. * meters,  90. * degrees, 1. * meter);

    VEC_EXPECT_NEAR(vec_f, result3);
}

TEST(VectorS3D, isNear)
{
    VectorS3D<QtySiLength> vec_a(1.*meter, 0.*degrees, 15.*degrees);
    VectorS3D<QtySiLength> vec_b(4.*meter, 15.*degrees, 28.*degrees);
    VectorS3D<QtySiLength> vec_c(1.*meter, 0.00000000000001*degrees, 15.*degrees);
    VectorS3D<QtySiLength> vec_d(1.000000000001*meter, 0.*degrees, 15.*degrees);

    VectorS3D<QtySiLength> vec_e(0.*meter, 15.*degrees, 45.*degrees);
    VectorS3D<QtySiLength> vec_f(0.0000000000001*meter, 15.*degrees, 45.*degrees);

    EXPECT_FALSE(vec_a.isNear(vec_b));
    // Todo: test fails because vec_a is degenerated, theta = 0
    EXPECT_TRUE(vec_a.isNear(vec_c));
    EXPECT_TRUE(vec_a.isNear(vec_d));
    // Todo: test fails because vec_a is degenerated, r = 0
    EXPECT_TRUE(vec_e.isNear(vec_f));
}


TEST(VectorS3D, elementAccess)
{
#if !defined(GCC) || defined(ENABLE_GCC_PROPERTY)
    EXPECT_EQ(VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 120.*degrees).r,       1.*meter);   // name-based
    EXPECT_EQ(VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 120.*degrees).theta,  QtySiPlaneAngle(90.*degrees));  // name-based
    EXPECT_EQ(VectorS3D<QtySiLength> (1.*meter, 90.*degrees, 120.*degrees).phi,    QtySiPlaneAngle(120.*degrees)); // name-based
#endif
}