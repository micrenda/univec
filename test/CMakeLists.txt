option(ENABLE_TESTS "Enable unit testing" ON)
option(ENABLE_COVERAGE  "Enable code coverage (gcovr)" OFF)

if (${ENABLE_TESTS})
    option(DOWNLOAD_GTEST "Download GoogleTest" OFF)

    # This is used because without this, GoogleTest will fails
    # with the message: "Could NOT find Threads"
    # this problem only exists with GCC

    if ("${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU")
        set(CMAKE_THREAD_LIBS_INIT "-lpthread")
        set(CMAKE_HAVE_THREADS_LIBRARY 1)
        set(CMAKE_USE_WIN32_THREADS_INIT 0)
        set(CMAKE_USE_PTHREADS_INIT 1)
        set(THREADS_PREFER_PTHREAD_FLAG ON)
    endif ()

    if (${DOWNLOAD_GTEST})
        include(FetchContent)
        FetchContent_Declare(
                googletest
                GIT_REPOSITORY https://github.com/google/googletest.git
                GIT_TAG release-1.11.0
        )

        add_library(GTest::gtest INTERFACE IMPORTED ../include/univec/enum.hpp)
        FetchContent_MakeAvailable(googletest)
    else ()
        find_package(GTest REQUIRED)
    endif ()

    enable_testing()

    if (NOT EXISTS "${PROJECT_SOURCE_DIR}/test/external/qtydef/include")
        message(FATAL_ERROR "Unable to find the directory 'external/univec/include'.
							 Did you perform?
							   git submodule init
							   git submodule update")
    endif ()

    include_directories(external/qtydef/include)
    include_directories(include)

    # Search all the cpp files
    file(GLOB CPP_FILES RELATIVE_PATH "${CMAKE_SOURCE_DIR}/test"  "test_*.cpp")
    add_executable(tests ${CPP_FILES})

    target_compile_features(tests PUBLIC cxx_std_20)

    if (${ENABLE_COVERAGE})
        target_compile_options(tests PUBLIC --coverage)
        target_link_options(tests    PUBLIC -lgcov --coverage)
        
        message("PROJECT_SOURCE_DIR ${PROJECT_SOURCE_DIR}")
        message("CMAKE_CURRENT_SOURCE_DIR ${CMAKE_CURRENT_SOURCE_DIR}")
        
        message("PROJECT_BINARY_DIR ${PROJECT_BINARY_DIR}")
        message("CMAKE_CURRENT_BINARY_DIR ${CMAKE_CURRENT_BINARY_DIR}")
        
        add_custom_target( gcovr
			COMMAND gcovr -r ${PROJECT_SOURCE_DIR} . --html coverage.html --json coverage.json --xml coverage.xml --xml-pretty --output coverage.log
			WORKING_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}
			COMMENT "Execute gcovr to extract coverage data"
			VERBATIM )
			
        

    endif ()

    target_link_libraries(tests GTest::gtest GTest::gtest_main)

    add_test(gtests tests)

endif ()
