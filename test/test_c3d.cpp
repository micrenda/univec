#include <boost/units/quantity.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/systems/si/length.hpp>
#include <gtest/gtest.h>

#include "univec/VectorC3D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace boost::units::degree;
using namespace dfpe;
using namespace si;

/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, addition)
{
    VectorC3D<QtySiLength> v1(3.*meter, 0.*meter, 4.*meter);
    VectorC3D<QtySiLength> v2(2.*meter, 2.*meter, 3.*meter);
    VectorC3D<QtySiLength> v3(1.*meter, 4.*meter, 5.*meter);

    VEC_EXPECT_NEAR(v1 + v2, VectorC3D<QtySiLength> (5.*meter, 2.*meter, 7.*meter));
    VEC_EXPECT_NEAR(v1 + v2 + v3, VectorC3D<QtySiLength> (6.*meter, 6.*meter, 12.*meter));
}

TEST(VectorC3D, additionEqual)
{
    VectorC3D<QtySiLength> v1(3.*meter, 0.*meter, 3*meter);
    VectorC3D<QtySiLength> v2(2.*meter, 2.*meter, 4*meter);
    v1 += v2;

    VEC_EXPECT_NEAR(v1, VectorC3D<QtySiLength> (5.*meter, 2.*meter, 7*meter));
}

/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, subtraction)
{
    VectorC3D<QtySiLength> v0(0.*meter, 0.*meter, 0*meter);
    VectorC3D<QtySiLength> v1(3.*meter, 0.*meter, 6*meter);
    VectorC3D<QtySiLength> v2(2.*meter, -2.*meter, 4*meter);

    VEC_EXPECT_NEAR(v0-v1, VectorC3D<QtySiLength> (-3.*meter, 0.*meter, -6*meter));
    VEC_EXPECT_NEAR(v1-v2, VectorC3D<QtySiLength> (1.*meter, 2.*meter, 2*meter));
}

TEST(VectorC3D, subtractionEqual)
{
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter) -= VectorC3D<QtySiLength> (3.*meter, 0.*meter, 6*meter), VectorC3D<QtySiLength> (-3.*meter, 0.*meter, -6*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 0.*meter, 6*meter) -= VectorC3D<QtySiLength> (2.*meter, -2.*meter, 4*meter), VectorC3D<QtySiLength> (1.*meter, 2.*meter, 2*meter));
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, mutiplication)
{
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 7*meter) * 2, VectorC3D<QtySiLength> (6.*meter, 8.*meter, 14*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (-2.*meter, -3.*meter, -4*meter) * 0.5, VectorC3D<QtySiLength> (-1.*meter, -1.5*meter, -2*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (2.*meter, 0.*meter, -3*meter) * -2, VectorC3D<QtySiLength> (-4.*meter, 0.*meter, 6*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (0*meter, 0*meter, 0*meter) * 2, VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter));

    VEC_EXPECT_NEAR(2 * VectorC3D<QtySiLength> (3.*meter, 4.*meter, 7*meter), VectorC3D<QtySiLength> (6.*meter, 8.*meter, 14*meter));
    VEC_EXPECT_NEAR(0.5 * VectorC3D<QtySiLength> (-2.*meter, -3.*meter, -4*meter), VectorC3D<QtySiLength> (-1.*meter, -1.5*meter, -2*meter));
    VEC_EXPECT_NEAR(-2 * VectorC3D<QtySiLength> (2.*meter, 0.*meter, -3*meter), VectorC3D<QtySiLength> (-4.*meter, 0.*meter, 6*meter));
    VEC_EXPECT_NEAR(2 * VectorC3D<QtySiLength> (0*meter, 0*meter, 0*meter), VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter));
}

TEST(VectorC3D, multiplicationEqual)
{
    // Multiplication with a scalar
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 7*meter) *= 2, VectorC3D<QtySiLength> (6.*meter, 8.*meter, 14*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (-2.*meter, -3.*meter, -4*meter) *= 0.5, VectorC3D<QtySiLength> (-1.*meter, -1.5*meter, -2*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (2.*meter, 0.*meter, -3*meter) *= -2, VectorC3D<QtySiLength> (-4.*meter, 0.*meter, 6*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (0*meter, 0*meter, 0*meter) *= 2, VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter));
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, division)
{
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 7*meter) / 0.5, VectorC3D<QtySiLength> ( 6.*meter, 8.*meter, 14*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (-2.*meter, -3.*meter, -4*meter) / 2, VectorC3D<QtySiLength> (-1.*meter, -1.5*meter, -2*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter) / -4., VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter));
}

TEST(VectorC3D, divisionEqual)
{
    // Division with a scalar
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 7*meter) /= 0.5, VectorC3D<QtySiLength> ( 6.*meter, 8.*meter, 14*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (-2.*meter, -3.*meter, -4*meter) /= 2, VectorC3D<QtySiLength> (-1.*meter, -1.5*meter, -2*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter) /= -4., VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter));
}

/* Comparative tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, greaterThan)
{
    EXPECT_GT( VectorC3D<QtySiLength> (3.*meter, 0.*meter, 4*meter), VectorC3D<QtySiLength> (2.*meter, 2.*meter, 2*meter));
}

TEST(VectorC3D, greaterThanOrEqualTo)
{
    EXPECT_GE(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter), VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter));
    EXPECT_GE(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 5*meter), VectorC3D<QtySiLength> (3.*meter, 4.*meter, 3*meter));
}

TEST(VectorC3D, lessThan)
{
    EXPECT_LT(VectorC3D<QtySiLength> (3.*meter, 0.*meter, 0*meter), VectorC3D<QtySiLength> (4.*meter, 0.*meter, 0*meter));
}

TEST(VectorC3D, lessThanOrEqualTo)
{
    EXPECT_LE(VectorC3D<QtySiLength> (3.*meter, 0.*meter, 6*meter), VectorC3D<QtySiLength> (3.*meter, 0.*meter, 6*meter));
    EXPECT_LE(VectorC3D<QtySiLength> (3.*meter, 0.*meter, 6*meter), VectorC3D<QtySiLength> (3.*meter, 1.*meter, 6*meter));
    EXPECT_LE(VectorC3D<QtySiLength> (3.*meter, 0.*meter, 6*meter), VectorC3D<QtySiLength> (3.*meter, 0.*meter, 7*meter));
}

TEST(VectorC3D, isEqualTo)
{
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (2.*meter, 4.*meter, 3*meter), VectorC3D<QtySiLength> (2.*meter, 4.*meter, 3*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtyAngleDegree> (0.*degrees, 0.*degrees, 0.*degrees), VectorC3D<QtyAngleDegree> (0.*degrees, 0.*degrees, 0.*degrees));
    EXPECT_EQ(VectorC3D<QtyAngleDegree> (0.*degrees, 0.*degrees, 0*degrees), VectorC3D<QtyAngleDegree> (0.*degrees, 0.*degrees, 0.*degrees));
    EXPECT_EQ(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter), VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0.*meter));
}

TEST(VectorC3D, notEqualTo)
{
    EXPECT_NE(VectorC3D<QtyAngleDegree> (3.*degrees, 2.*degrees, 4*degrees), VectorC3D<QtyAngleDegree> (3.*degrees, 2.*degrees, 5*degrees));
    EXPECT_NE(VectorC3D<QtySiLength> (3.*meter, 2.*meter, 4*meter), VectorC3D<QtySiLength> (3.*meter, 3.*meter, 4*meter));
    EXPECT_NE(VectorC3D<QtySiLength> (3.*meter, 2.*meter, 4*meter), VectorC3D<QtySiLength> (3.*meter, 3.*meter, 4.*meter));
}

TEST(VectorC3D, isNear)
{
    VectorC3D<QtySiLength> vec_a(2*meter, 3.*meter, 1.*meter);
    VectorC3D<QtySiLength> vec_b(4.*meter, 3.*meter, 5*meter);
    VectorC3D<QtySiLength> vec_c(2*meter,3.00000000001*meter, 1.*meter);

    EXPECT_FALSE(vec_a.isNear(vec_b));
    EXPECT_TRUE(vec_a.isNear(vec_c));
}

/* Common vector operations tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, norm)
{
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (2*meter, 3.*meter, 4.*meter).norm(), sqrt(29)*meter);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (-2*meter, -3.*meter, -4.*meter).norm(), sqrt(29)*meter);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (4*meter, -3.*meter, 0.*meter).norm(), 5*meter);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (0*meter, 0.*meter, 0.*meter).norm(), 0*meter);
    QTY_EXPECT_NEAR(VectorC3D<QtyAngleDegree> (0*degrees, 0.*degrees, 0.*degrees).norm(), 0*degrees);
}

TEST(VectorC3D, normL1)
{
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (3*meter, 4.*meter, 1.*meter).normL1(), 8*meter);
}

TEST(VectorC3D, normL)
{
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (3*meter, 4.*meter, 1.*meter).normL(1), 8*meter);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (4*meter, -3.*meter, 0.*meter).normL(2.), 5*meter);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (4*meter, -3.*meter, 0.*meter).normL(2), 5*meter);
}

TEST(VectorC3D, normLInf)
{
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (3*meter, 4.*meter, 5.*meter).normLInf(), 5*meter);
}

TEST(VectorC3D, normSquared) {
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength>(2*meter, 3.*meter, 4.*meter).normSquared(), 29*square_meters);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength>(-2*meter, -3.*meter, -4.*meter).normSquared(), 29*square_meters);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength>(4*meter, -3.*meter, 0.*meter).normSquared(), 25*square_meters);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength>(0*meter, 0.*meter, 0.*meter).normSquared(), 0*square_meters);
}

TEST(VectorC3D, versor)
{
    VectorC3D<QtySiLength> v1(3.*meter, 4.*meter, 2*meter);
    VectorC3D<QtySiLength> result(3/sqrt(29)*meter, 4/sqrt(29)*meter, 2/sqrt(29)*meter);

    VEC_EXPECT_NEAR(v1.versor(), result);

    v1.doVersor();
    VEC_EXPECT_NEAR(v1, result);
}

TEST(VectorC3D, versorDl)
{
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter).versorDl(), VectorC3D<QtySiDimensionless> (3/sqrt(29), 4/sqrt(29), 2/sqrt(29)));
}

TEST(VectorC3D, dot)
{
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter).dot(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter)), 29.*square_meter);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter).dot(VectorC3D<QtySiLength> (3.*meter, -1.*meter, -2*meter)), 1.*square_meter);
}

TEST(VectorC3D, dotPipe)
{
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter).dot(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter)), 29.*square_meter);
    QTY_EXPECT_NEAR(VectorC3D<QtySiLength> (3.*meter, 4.*meter, 2*meter).dot(VectorC3D<QtySiLength> (3.*meter, -1.*meter, -2*meter)), 1.*square_meter);
}

TEST(VectorC3D, cross)
{
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength>(1.*meter, -7.*meter, 1*meter).cross(
                    VectorC3D<QtySiLength>(5.*meter, 2.*meter, 4*meter)),
                    VectorC3D<QtySiArea>(-30.*square_meter, 1.*square_meter, 37*square_meter));
}

TEST(VectorC3D, directSum)
{
    VectorC<5,QtySiLength> res = VectorC3D<QtySiLength>(1.*meter, 2.*meter, 3*meter).directSum(VectorC<2,QtySiLength>(4.*meter, 5.*meter));
    VectorC<5,QtySiLength> ref (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter);

    VEC_EXPECT_NEAR(res,ref);
}

TEST(VectorC3D, crossHat)
{
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength>(1.*meter, -7.*meter, 1*meter) ^
                    VectorC3D<QtySiLength>(5.*meter, 2.*meter, 4*meter),
                    VectorC3D<QtySiArea>(-30.*square_meter, 1.*square_meter, 37*square_meter));
}

TEST(VectorC3D, elementwiseProduct)
{
    VectorC3D<QtySiLength> v1 (2. * meter,  4. * meter,  6. * meter);
    VectorC3D<QtySiForce>  v2 (1. * newton, 2. * newton, 3. * newton);
    VectorC3D<QtySiEnergy> v3 (2. * meter * newton,   8. * meter * newton,  18. * meter * newton);

    MAT_EXPECT_NEAR(v1.elementwiseProduct(v2), v3);
}

TEST(VectorC3D, elementwiseDivision)
{
    VectorC3D<QtySiLength>   v1 (2. * meter,  4. * meter,  6. * meter);
    VectorC3D<QtySiTime>     v2 (1. * second, 2. * second, 3. * second);
    VectorC3D<QtySiVelocity> v3 (2. * meter / second, 2. * meter / second, 2. * meter / second);

    MAT_EXPECT_NEAR(v1.elementwiseDivision(v2), v3);
}

TEST(VectorC3D, isParallel)
{
    EXPECT_TRUE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isParallel(VectorC3D<QtySiLength> ( 2.*meter, 0.*meter, 0*meter)));
    EXPECT_TRUE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isParallel(VectorC3D<QtySiLength> (-2.*meter, 0.*meter, 0*meter)));
    EXPECT_TRUE(VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isParallel(VectorC3D<QtySiLength> ( 0.*meter, 2.*meter, 0*meter)));
    EXPECT_TRUE(VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isParallel(VectorC3D<QtySiLength> ( 0.*meter,-2.*meter, 0*meter)));
    EXPECT_TRUE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isParallel(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 2*meter)));
    EXPECT_TRUE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isParallel(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter,-2*meter)));

    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isParallel(VectorC3D<QtySiLength> ( 2.*meter, 2.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isParallel(VectorC3D<QtySiLength> (-2.*meter, 2.*meter, 0*meter)));

    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isParallel(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter).isParallel(VectorC3D<QtySiLength> ( 1.*meter, 0.*meter, 0*meter)));
}

TEST(VectorC3D, isSameDirection)
{
    EXPECT_TRUE (VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isSameDirection(VectorC3D<QtySiLength> ( 2.*meter, 0.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isSameDirection(VectorC3D<QtySiLength> (-2.*meter, 0.*meter, 0*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isSameDirection(VectorC3D<QtySiLength> ( 0.*meter, 2.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isSameDirection(VectorC3D<QtySiLength> ( 0.*meter,-2.*meter, 0*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isSameDirection(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 2*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isSameDirection(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter,-2*meter)));

    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isSameDirection(VectorC3D<QtySiLength> ( 2.*meter, 2.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isSameDirection(VectorC3D<QtySiLength> (-2.*meter, 2.*meter, 0*meter)));

    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isSameDirection(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter).isSameDirection(VectorC3D<QtySiLength> ( 1.*meter, 0.*meter, 0*meter)));
}

TEST(VectorC3D, isOppositeDirection)
{
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isOppositeDirection(VectorC3D<QtySiLength> ( 2.*meter, 0.*meter, 0*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isOppositeDirection(VectorC3D<QtySiLength> (-2.*meter, 0.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isOppositeDirection(VectorC3D<QtySiLength> ( 0.*meter, 2.*meter, 0*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isOppositeDirection(VectorC3D<QtySiLength> ( 0.*meter,-2.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isOppositeDirection(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 2*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isOppositeDirection(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter,-2*meter)));

    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isOppositeDirection(VectorC3D<QtySiLength> ( 2.*meter, 2.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isOppositeDirection(VectorC3D<QtySiLength> (-2.*meter, 2.*meter, 0*meter)));

    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isOppositeDirection(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter).isOppositeDirection(VectorC3D<QtySiLength> ( 1.*meter, 0.*meter, 0*meter)));
}

TEST(VectorC3D, isPerpendicular)
{
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 1.*meter, 0.*meter, 0*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 0.*meter, 1.*meter, 0*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 1*meter)));

    EXPECT_TRUE (VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 1.*meter, 0.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 0.*meter, 1.*meter, 0*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 1*meter)));

    EXPECT_TRUE (VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 1.*meter, 0.*meter, 0*meter)));
    EXPECT_TRUE (VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 0.*meter, 1.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1.*meter).isPerpendicular(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 1*meter)));

    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter).isPerpendicular(VectorC3D<QtySiLength> ( 2.*meter, 0.*meter, 0*meter)));
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isPerpendicular(VectorC3D<QtySiLength> ( 0.*meter, 0.*meter, 0*meter)));
}


/* Angle and angle related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, angle)
{
    QtyAngleDegree result (VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).angle(VectorC3D<QtySiLength> (-1.*meter, 0.*meter, 0*meter)));
    QTY_EXPECT_NEAR(result, 180.*degrees);
}

TEST(VectorC3D, rotateX)
{
    VectorC3D<QtySiLength> v1(1.*meter, 0.*meter, 0.*meter);
    VectorC3D<QtySiLength> v2(0.*meter, 1.*meter, 0.*meter);

    VectorC3D<QtySiLength> result1(1. * meter, 0. * meter, 0.*meter);
    VectorC3D<QtySiLength> result2(0. * meter, 0. * meter, 1.*meter);

    VEC_EXPECT_NEAR(v1.rotateX(180.*degrees), result1);
    VEC_EXPECT_NEAR(v2.rotateX( 90.*degrees), result2);

    v1.doRotateX(180.*degrees);
    v2.doRotateX( 90.*degrees);

    VEC_EXPECT_NEAR(v1, result1);
    VEC_EXPECT_NEAR(v2, result2);
}

TEST(VectorC3D, rotateY)
{
    VectorC3D<QtySiLength> v1(1.*meter, 0.*meter, 0.*meter);
    VectorC3D<QtySiLength> v2(1.*meter, 0.*meter, 0.*meter);

    VectorC3D<QtySiLength> result1(-1. * meter, 0. * meter, 0.*meter);
    VectorC3D<QtySiLength> result2(0. * meter, 0. * meter, -1.*meter);

    VEC_EXPECT_NEAR(v1.rotateY(180.*degrees), result1);
    VEC_EXPECT_NEAR(v2.rotateY(90.*degrees), result2);

    v1.doRotateY(180.*degrees);
    v2.doRotateY( 90.*degrees);

    VEC_EXPECT_NEAR(v1, result1);
    VEC_EXPECT_NEAR(v2, result2);
}

TEST(VectorC3D, rotateZ)
{
    VectorC3D<QtySiLength> v1(1.*meter, 0.*meter, 0.*meter);
    VectorC3D<QtySiLength> v2(1.*meter, 0.*meter, 0.*meter);

    VectorC3D<QtySiLength> result1(-1. * meter, 0. * meter, 0.*meter);
    VectorC3D<QtySiLength> result2(0. * meter, 1. * meter, 0.*meter);

    VEC_EXPECT_NEAR(v1.rotateZ(180.*degrees), result1);
    VEC_EXPECT_NEAR(v2.rotateZ( 90.*degrees), result2);

    v1.doRotateZ(180.*degrees);
    v2.doRotateZ( 90.*degrees);

    VEC_EXPECT_NEAR(v1, result1);
    VEC_EXPECT_NEAR(v2, result2);}

/* Validity check operations */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, isNull)
{
    EXPECT_TRUE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 0*meter).isNull());
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 0.*meter, 0*meter).isNull());
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 1.*meter, 0*meter).isNull());
    EXPECT_FALSE(VectorC3D<QtySiLength> (0.*meter, 0.*meter, 1*meter).isNull());
}

TEST(VectorC3D, isFinite)
{

    EXPECT_TRUE(VectorC3D<QtySiLength> (1.*meter, 1.*meter, 1*meter).isFinite());
    EXPECT_FALSE(VectorC3D<QtySiLength> (inf_val*meter, 1.*meter, 1*meter).isFinite());
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, inf_val*meter, 1*meter).isFinite());
    EXPECT_FALSE(VectorC3D<QtySiLength> (inf_val*meter, inf_val*meter, inf_val*meter).isFinite());
}

TEST(VectorC3D, isNormal)
{
    EXPECT_TRUE(VectorC3D<QtySiLength> (1.*meter, 1.*meter, 1*meter).isNormal());
    EXPECT_FALSE(VectorC3D<QtySiLength> (nan_val*meter, 1.*meter, 1*meter).isNormal());
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, nan_val*meter, 1*meter).isNormal());
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 1*meter, nan_val*meter).isNormal());
    EXPECT_FALSE(VectorC3D<QtySiLength> (nan_val*meter, nan_val*meter, nan_val*meter).isNormal());
}

TEST(VectorC3D, isNan)
{
    EXPECT_FALSE(VectorC3D<QtySiLength> (1.*meter, 1.*meter, 0*meter).isNan());
    EXPECT_TRUE(VectorC3D<QtySiLength> (nan_val*meter, 1.*meter, 1*meter).isNan());
    EXPECT_TRUE(VectorC3D<QtySiLength> (1.*meter, nan_val*meter, 1*meter).isNan());
    EXPECT_TRUE(VectorC3D<QtySiLength> (1.*meter, 1*meter, nan_val*meter).isNan());
    EXPECT_TRUE(VectorC3D<QtySiLength> (nan_val*meter, nan_val*meter, nan_val*meter).isNan());
}

TEST(VectorC3D, isInf)
{
    EXPECT_FALSE(VectorC3D<QtySiLength>(1. * meter, 1. * meter, 1 * meter).isInfinite());
    EXPECT_TRUE(VectorC3D<QtySiLength>(inf_val * meter, 1. * meter, 1 * meter).isInfinite());
    EXPECT_TRUE(VectorC3D<QtySiLength>(1. * meter, inf_val * meter, 1 * meter).isInfinite());
    EXPECT_TRUE(VectorC3D<QtySiLength>(1. * meter, 1 * meter, inf_val * meter).isInfinite());
    EXPECT_TRUE(VectorC3D<QtySiLength>(inf_val * meter, inf_val * meter, inf_val * meter).isInfinite());
}

/* Other operators */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC3D, elementAccess)
{
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter)[0], 1*meter); // 0-based
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter)[1], 2*meter); // 0-based
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter)[2], 3*meter); // 0-based

    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter)(1), 1*meter); // 1-based
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter)(2), 2*meter); // 1-based
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter)(3), 3*meter); // 1-based

#ifndef GCC
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter).x, 1*meter); // name-based
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter).y, 2*meter); // name-based
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter).z, 3*meter); // name-based
#endif
}

TEST(VectorC3D, symbol)
{
    EXPECT_EQ(VectorC3D<QtySiLength> (1.*meter, 2.*meter, 3.*meter).symbol(), "ℝ³");
}

TEST(VectorC3D, cout)
{
    std::stringstream ss1, ss2;
    ss1 << VectorC3D<QtySiLength> (  1.*meter, -2.*meter,  3.*meter);
    ss2 << VectorC3D<QtySiLength> ( -1.*meter,  2.*meter, -3.*meter);

    EXPECT_EQ(ss1.str(), "(1 m, -2 m, 3 m)");
    EXPECT_EQ(ss2.str(), "(-1 m, 2 m, -3 m)");
}

TEST(VectorC3D, repr)
{
    VectorC3D<QtySiLength> v1(  1.*meter, -2.*meter,  3.*meter);
    VectorC3D<QtySiLength> v2( -1.*meter,  2.*meter, -3.*meter);

    EXPECT_EQ(v1.repr(false), "(1 m, -2 m, 3 m)");
    EXPECT_EQ(v2.repr(false), "(-1 m, 2 m, -3 m)");

    EXPECT_EQ(v1.repr(true), "ℝ³(1 m, -2 m, 3 m)");
    EXPECT_EQ(v2.repr(true), "ℝ³(-1 m, 2 m, -3 m)");
}

TEST(VectorC3D, kUnit)
{
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength>::kX(), VectorC3D<QtySiLength>(1.*meter, 0.*meter, 0.*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength>::kY(), VectorC3D<QtySiLength>(0.*meter, 1.*meter, 0.*meter));
    VEC_EXPECT_NEAR(VectorC3D<QtySiLength>::kZ(), VectorC3D<QtySiLength>(0.*meter, 0.*meter, 1.*meter));
}
