#include "test_utils.hpp"
#include <limits>

double inf_val = std::numeric_limits<double>::infinity();
double nan_val = std::numeric_limits<double>::quiet_NaN();