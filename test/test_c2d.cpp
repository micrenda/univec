#include <iostream>

#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <limits>
#include <cmath>
#include <gtest/gtest.h>

#include "univec/VectorC2D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace boost::units::degree;
using namespace dfpe;
using namespace si;

/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC2D, addition)
{
    VectorC2D<QtySiLength> vec_a(3.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_b(2.*meter, 2.*meter);

    VEC_EXPECT_NEAR(vec_a + vec_b, VectorC2D<QtySiLength> (5.*meter, 2.*meter));
}

TEST(VectorC2D, additionEqual)
{
    VectorC2D<QtySiLength> vec_a(3.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_b(2.*meter, 2.*meter);
    vec_a += vec_b;

    VEC_EXPECT_NEAR(vec_a, VectorC2D<QtySiLength> (5.*meter, 2.*meter));
}

/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC2D, subtraction)
{
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (3.*meter, 0.*meter) - VectorC2D<QtySiLength> (2.*meter, 2.*meter), VectorC2D<QtySiLength> (1.*meter, -2.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (2.*meter, 2.*meter) - VectorC2D<QtySiLength> (3.*meter, 0.*meter), VectorC2D<QtySiLength> (-1.*meter, 2.*meter));
}

TEST(VectorC2D, subtractionEqual)
{
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (3.*meter, 0.*meter) -= VectorC2D<QtySiLength> (2.*meter, 2.*meter), VectorC2D<QtySiLength> (1.*meter, -2.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (2.*meter, 2.*meter) -= VectorC2D<QtySiLength> (3.*meter, 0.*meter), VectorC2D<QtySiLength> (-1.*meter, 2.*meter));
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC2D, multiplication)
{
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (3.*meter, 4.*meter) * 2, VectorC2D<QtySiLength> (6.*meter, 8.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (-2.*meter, -3.*meter) * 0.5, VectorC2D<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (2.*meter, 0.*meter) * 2, VectorC2D<QtySiLength> (4.*meter, 0.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (0.*meter, -4.*meter) * 2, VectorC2D<QtySiLength> (0.*meter, -8.*meter));

    VEC_EXPECT_NEAR(2 * VectorC2D<QtySiLength> (3.*meter, 4.*meter), VectorC2D<QtySiLength> (6.*meter, 8.*meter));
    VEC_EXPECT_NEAR(0.5 * VectorC2D<QtySiLength> (-2.*meter, -3.*meter), VectorC2D<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(2 * VectorC2D<QtySiLength> (2.*meter, 0.*meter), VectorC2D<QtySiLength> (4.*meter, 0.*meter));
    VEC_EXPECT_NEAR(2 * VectorC2D<QtySiLength> (0.*meter, -4.*meter), VectorC2D<QtySiLength> (0.*meter, -8.*meter));
}

TEST(VectorC2D, multiplicationEqual)
{
    // Multiplication with a scalar
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (3.*meter, 4.*meter) *= 2, VectorC2D<QtySiLength> (6.*meter, 8.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (-2.*meter, -3.*meter) *= 0.5, VectorC2D<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (2.*meter, 0.*meter) *= 2, VectorC2D<QtySiLength> (4.*meter, 0.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (0.*meter, -4.*meter) *= 2, VectorC2D<QtySiLength> (0.*meter, -8.*meter));
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC2D, division)
{
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (3.*meter, 4.*meter) / 0.5, VectorC2D<QtySiLength> ( 6.*meter, 8.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (-2.*meter, -3.*meter) / 2, VectorC2D<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (6.*meter, 0.*meter) / 3., VectorC2D<QtySiLength> (2.*meter, 0.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (0.*meter, -4.*meter) / -4., VectorC2D<QtySiLength> (0.*meter, 1.*meter));
}

TEST(VectorC2D, divisionEqual)
{
    // Division with a scalar
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (3.*meter, 4.*meter) /= 0.5, VectorC2D<QtySiLength> ( 6. * meter, 8. * meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (-2.*meter, -3.*meter) /= 2., VectorC2D<QtySiLength> (-1. * meter, -1.5 * meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (6.*meter, 0.*meter) /= 3., VectorC2D<QtySiLength> (2. * meter, 0. * meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (0.*meter, -4.*meter) /= -4., VectorC2D<QtySiLength> (0. * meter, 1. * meter));
}

/* Comparative tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC2D, greaterThan)
{
    VectorC2D<QtySiLength> vec_a(3.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_b(2.*meter, 2.*meter);

    EXPECT_GT(vec_a, vec_b);
}

TEST(VectorC2D, greaterThanOrEqualTo)
{
    VectorC2D<QtySiLength> vec_a(3.*meter, 4.*meter);
    VectorC2D<QtySiLength> vec_b(3.*meter, 2.*meter);
    VectorC2D<QtySiLength> vec_c(2.*meter, 3.*meter);

    EXPECT_GE(vec_a, vec_b);
    EXPECT_GE(vec_b, vec_c);
}

TEST(VectorC2D, lessThan)
{
    VectorC2D<QtySiLength> vec_a(3.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_b(2.*meter, 2.*meter);

    EXPECT_LT(vec_b, vec_a);
}

TEST(VectorC2D, lessThanOrEqualTo)
{
    VectorC2D<QtySiLength> vec_a(3.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_b(4.*meter, 3.*meter);
    VectorC2D<QtySiLength> vec_c(4.*meter, 4.*meter);

    EXPECT_LE(vec_a, vec_b);
    EXPECT_LE(vec_b, vec_c);
}

TEST(VectorC2D, isEqualTo)
{
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (2.*meter, 2.*meter), VectorC2D<QtySiLength> (2.*meter, 2.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> (0.*meter, 0.*meter), VectorC2D<QtySiLength> (0.*meter, 0.*meter));
}

TEST(VectorC2D, notEqualTo)
{
    VectorC2D<QtySiLength> vec_a(3.*meter, 2.*meter);
    VectorC2D<QtySiLength> vec_b(1.*meter, 4.*meter);
    VectorC2D<QtySiLength> vec_c(3.*meter, 3.*meter);
    VectorC2D<QtySiLength> vec_d(2.*meter, 2.*meter);

    EXPECT_NE(vec_a, vec_b);
    EXPECT_NE(vec_a, vec_c);
    EXPECT_NE(vec_a, vec_d);
}

TEST(VectorC2D, isNear)
{
    VectorC2D<QtySiLength> vec_a(3.*meter, 1.*meter);
    VectorC2D<QtySiLength> vec_b(4.*meter, 3.*meter);
    VectorC2D<QtySiLength> vec_c(3.00000000001*meter, 1.*meter);

    EXPECT_FALSE(vec_a.isNear(vec_b));
    EXPECT_TRUE(vec_a.isNear(vec_c));
}

/* Common vector operations tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC2D, norm)
{
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (3.*meter, 4.*meter).norm(), 5.*meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (-3.*meter, 4.*meter).norm(), 5.*meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (3.*meter, -4.*meter).norm(), 5.*meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (-3.*meter, -4.*meter).norm(), 5.*meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (0.*meter, 0.*meter).norm(), 0.*meter);
}

TEST(VectorC2D, normL1)
{
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (3*meter, 4.*meter).normL1(), 7*meter);
}

TEST(VectorC2D, normL)
{
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (3*meter, 4.*meter).normL(1), 7*meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (3*meter, 4.*meter).normL(2), 5*meter);
}

TEST(VectorC2D, normLInf)
{
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (2*meter, 4.*meter).normLInf(), 4*meter);
}

TEST(VectorC2D, normSquared)
{
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (2.*meter, 3.*meter).normSquared(), 13.*square_meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (-2.*meter, 3.*meter).normSquared(), 13.*square_meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (2.*meter, -3.*meter).normSquared(), 13.*square_meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (-2.*meter, -3.*meter).normSquared(), 13.*square_meter);
    QTY_EXPECT_NEAR(VectorC2D<QtySiLength> (0.*meter, 0.*meter).normSquared(), 0.*square_meter);
}

TEST(VectorC2D, versor)
{
    VectorC2D<QtySiLength> v1( 3. * meter, 4. * meter);
    VectorC2D<QtySiLength> result = v1 / 5.;

    VEC_EXPECT_NEAR(v1.versor(),result);

    v1.doVersor();
    VEC_EXPECT_NEAR(v1, result);
}

TEST(VectorC2D, versorDl)
{
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> ( 3. * meter, 4. * meter).versorDl(), VectorC2D<QtySiDimensionless> (0.6, 0.8));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength> ( 3. * meter, 4. * meter).versorDl(), VectorC2D<QtySiDimensionless> (3., 4.) / 5.);
}

TEST(VectorC2D, dot)
{
    VectorC2D<QtySiLength> vec_a(1.*meter, 3.*meter);
    VectorC2D<QtySiLength> vec_b(-5.*meter, 2.*meter);
    QTY_EXPECT_NEAR(vec_a.dot(vec_b), 1.*square_meter);

    VectorC2D<QtySiLength> vec_c1(1.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_c2(0.*meter, 1.*meter);
    VectorC2D<QtySiLength> vec_c3(-1.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_c4(0.*meter, -1.*meter);

    QTY_EXPECT_NEAR(vec_c1.dot(vec_c2), 0.*square_meter);
    QTY_EXPECT_NEAR(vec_c1.dot(vec_c3), -1.*square_meter);
    QTY_EXPECT_NEAR(vec_c1.dot(vec_c4), 0.*square_meter);
    QTY_EXPECT_NEAR(vec_c1.dot(vec_c1), 1.*square_meter);
}

TEST(VectorC2D, dotPipe)
{
    VectorC2D<QtySiLength> vec_a(1.*meter, 3.*meter);
    VectorC2D<QtySiLength> vec_b(-5.*meter, 2.*meter);
    QTY_EXPECT_NEAR(vec_a.dot(vec_b), 1.*square_meter);

    VectorC2D<QtySiLength> vec_c1(1.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_c2(0.*meter, 1.*meter);
    VectorC2D<QtySiLength> vec_c3(-1.*meter, 0.*meter);
    VectorC2D<QtySiLength> vec_c4(0.*meter, -1.*meter);

    QTY_EXPECT_NEAR(vec_c1|vec_c2, 0.*square_meter);
    QTY_EXPECT_NEAR(vec_c1|vec_c3, -1.*square_meter);
    QTY_EXPECT_NEAR(vec_c1|vec_c4, 0.*square_meter);
    QTY_EXPECT_NEAR(vec_c1|vec_c1, 1.*square_meter);
}

TEST(VectorC2D, directSum)
{
    VectorC<3,QtySiLength> res = VectorC2D<QtySiLength>(1.*meter, 2.*meter).directSum(VectorC<1,QtySiLength>(3.*meter));
    VectorC<3,QtySiLength> ref (1.*meter, 2.*meter, 3.*meter);

    VEC_EXPECT_NEAR(res,ref);
}

TEST(VectorC2D, elementwiseProduct)
{
    VectorC2D<QtySiLength> v1 (2. * meter,  4. * meter);
    VectorC2D<QtySiForce>  v2 (1. * newton, 2. * newton);
    VectorC2D<QtySiEnergy> v3 (2. * meter * newton,   8. * meter * newton);

    MAT_EXPECT_NEAR(v1.elementwiseProduct(v2), v3);
}

TEST(VectorC2D, elementwiseDivision)
{
    VectorC2D<QtySiLength>   v1 (2. * meter,  4. * meter);
    VectorC2D<QtySiTime>     v2 (1. * second, 2. * second);
    VectorC2D<QtySiVelocity> v3 (2. * meter / second, 2. * meter / second);

    MAT_EXPECT_NEAR(v1.elementwiseDivision(v2), v3);
}


TEST(VectorC2D, isParallel)
{
    EXPECT_TRUE(VectorC2D<QtySiLength>  (1.*meter, 0.*meter).isParallel(VectorC2D<QtySiLength> ( 2.*meter, 0.*meter)));
    EXPECT_TRUE(VectorC2D<QtySiLength>  (1.*meter, 0.*meter).isParallel(VectorC2D<QtySiLength> (-2.*meter, 0.*meter)));
    EXPECT_TRUE(VectorC2D<QtySiLength>  (0.*meter, 1.*meter).isParallel(VectorC2D<QtySiLength> ( 0.*meter, 2.*meter)));
    EXPECT_TRUE(VectorC2D<QtySiLength>  (0.*meter, 1.*meter).isParallel(VectorC2D<QtySiLength> ( 0.*meter,-2.*meter)));

    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isParallel(VectorC2D<QtySiLength> ( 2.*meter, 2.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isParallel(VectorC2D<QtySiLength> (-2.*meter, 2.*meter)));

    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isParallel(VectorC2D<QtySiLength> ( 0.*meter, 0.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (0.*meter, 0.*meter).isParallel(VectorC2D<QtySiLength> ( 1.*meter, 0.*meter)));
}

TEST(VectorC2D, isSameDirection)
{
    EXPECT_TRUE (VectorC2D<QtySiLength> (1.*meter, 0.*meter).isSameDirection(VectorC2D<QtySiLength> ( 2.*meter, 0.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isSameDirection(VectorC2D<QtySiLength> (-2.*meter, 0.*meter)));
    EXPECT_TRUE (VectorC2D<QtySiLength> (0.*meter, 1.*meter).isSameDirection(VectorC2D<QtySiLength> ( 0.*meter, 2.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (0.*meter, 1.*meter).isSameDirection(VectorC2D<QtySiLength> ( 0.*meter,-2.*meter)));

    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isSameDirection(VectorC2D<QtySiLength> ( 2.*meter, 2.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isSameDirection(VectorC2D<QtySiLength> (-2.*meter, 2.*meter)));

    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isSameDirection(VectorC2D<QtySiLength> ( 0.*meter, 0.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (0.*meter, 0.*meter).isSameDirection(VectorC2D<QtySiLength> ( 1.*meter, 0.*meter)));
}

TEST(VectorC2D, isOppositeDirection)
{
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isOppositeDirection(VectorC2D<QtySiLength> ( 2.*meter, 0.*meter)));
    EXPECT_TRUE (VectorC2D<QtySiLength> (1.*meter, 0.*meter).isOppositeDirection(VectorC2D<QtySiLength> (-2.*meter, 0.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (0.*meter, 1.*meter).isOppositeDirection(VectorC2D<QtySiLength> ( 0.*meter, 2.*meter)));
    EXPECT_TRUE (VectorC2D<QtySiLength> (0.*meter, 1.*meter).isOppositeDirection(VectorC2D<QtySiLength> ( 0.*meter,-2.*meter)));

    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isOppositeDirection(VectorC2D<QtySiLength> ( 2.*meter, 2.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isOppositeDirection(VectorC2D<QtySiLength> (-2.*meter, 2.*meter)));

    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isOppositeDirection(VectorC2D<QtySiLength> ( 0.*meter, 0.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (0.*meter, 0.*meter).isOppositeDirection(VectorC2D<QtySiLength> ( 1.*meter, 0.*meter)));
}

TEST(VectorC2D, isPerpendicular)
{
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isPerpendicular(VectorC2D<QtySiLength> ( 1.*meter, 0.*meter)));
    EXPECT_TRUE (VectorC2D<QtySiLength> (1.*meter, 0.*meter).isPerpendicular(VectorC2D<QtySiLength> ( 0.*meter, 1.*meter)));

    EXPECT_TRUE (VectorC2D<QtySiLength> (0.*meter, 1.*meter).isPerpendicular(VectorC2D<QtySiLength> ( 1.*meter, 0.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (0.*meter, 1.*meter).isPerpendicular(VectorC2D<QtySiLength> ( 0.*meter, 1.*meter)));

    EXPECT_FALSE(VectorC2D<QtySiLength> (0.*meter, 0.*meter).isPerpendicular(VectorC2D<QtySiLength> ( 2.*meter, 0.*meter)));
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isPerpendicular(VectorC2D<QtySiLength> ( 0.*meter, 0.*meter)));
}


/* Angle and angle related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC2D, angle)
{
    QtyAngleDegree result (VectorC2D<QtySiLength> (1.*meter, 0.*meter).angle(VectorC2D<QtySiLength> (-1.*meter, 0.*meter)));
    QTY_EXPECT_NEAR(result, 180. * degrees);
}

TEST(VectorC2D, rotate)
{
    VectorC2D<QtySiLength> v1(1. * meter, 0. * meter);
    VectorC2D<QtySiLength> result(0. * meter, 1. * meter);

    VEC_EXPECT_NEAR(v1.rotate(90. * degrees), result);

    v1.doRotate(90. * degrees);
    VEC_EXPECT_NEAR(v1, result);
}

/* Validity check operations */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC2D, isNull)
{
    EXPECT_TRUE(VectorC2D<QtySiLength> (0.*meter, 0.*meter).isNull());
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 0.*meter).isNull());
    EXPECT_FALSE(VectorC2D<QtySiLength> (0.*meter, 1.*meter).isNull());
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 1.*meter).isNull());
}

TEST(VectorC2D, isFinite)
{
    EXPECT_TRUE(VectorC2D<QtySiLength> (1.*meter, 1.*meter).isFinite());
    EXPECT_FALSE(VectorC2D<QtySiLength> (inf_val*meter, 1.*meter).isFinite());
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, inf_val*meter).isFinite());
    EXPECT_FALSE(VectorC2D<QtySiLength> (inf_val*meter, inf_val*meter).isFinite());
}

TEST(VectorC2D, isNormal)
{
    EXPECT_TRUE(VectorC2D<QtySiLength> (1.*meter, 1.*meter).isNormal());
    EXPECT_FALSE(VectorC2D<QtySiLength> (nan_val*meter, 1.*meter).isNormal());
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, nan_val*meter).isNormal());
    EXPECT_FALSE(VectorC2D<QtySiLength> (nan_val*meter, nan_val*meter).isNormal());
}

TEST(VectorC2D, isNan)
{
    EXPECT_FALSE(VectorC2D<QtySiLength> (1.*meter, 1.*meter).isNan());
    EXPECT_TRUE(VectorC2D<QtySiLength> (nan_val*meter, 1.*meter).isNan());
    EXPECT_TRUE(VectorC2D<QtySiLength> (1.*meter, nan_val*meter).isNan());
    EXPECT_TRUE(VectorC2D<QtySiLength> (nan_val*meter, nan_val*meter).isNan());
}

TEST(VectorC2D, isInf)
{
    EXPECT_FALSE(VectorC2D<QtySiLength>(1. * meter, 1. * meter).isInfinite());
    EXPECT_TRUE(VectorC2D<QtySiLength>(inf_val * meter, 1. * meter).isInfinite());
    EXPECT_TRUE(VectorC2D<QtySiLength>(1. * meter, inf_val * meter).isInfinite());
    EXPECT_TRUE(VectorC2D<QtySiLength>(inf_val * meter, inf_val * meter).isInfinite());
}

TEST(VectorC2D, elementAccess)
{
    EXPECT_EQ(VectorC2D<QtySiLength> (1.*meter, 2.*meter)[0], 1*meter); // 0-based
    EXPECT_EQ(VectorC2D<QtySiLength> (1.*meter, 2.*meter)[1], 2*meter); // 0-based

    EXPECT_EQ(VectorC2D<QtySiLength> (1.*meter, 2.*meter)(1), 1*meter); // 1-based
    EXPECT_EQ(VectorC2D<QtySiLength> (1.*meter, 2.*meter)(2), 2*meter); // 1-based

#ifndef GCC
    EXPECT_EQ(VectorC2D<QtySiLength> (1.*meter, 2.*meter).x, 1*meter); // name-based
    EXPECT_EQ(VectorC2D<QtySiLength> (1.*meter, 2.*meter).y, 2*meter); // name-based
#endif
}

TEST(VectorC2D, symbol)
{
    EXPECT_EQ(VectorC2D<QtySiLength> (1.*meter, 2.*meter).symbol(), "ℝ²");
}

TEST(VectorC2D, cout)
{
    std::stringstream ss1, ss2;
    ss1 << VectorC2D<QtySiLength> (  1.*meter, -2.*meter);
    ss2 << VectorC2D<QtySiLength> ( -1.*meter,  2.*meter);

    EXPECT_EQ(ss1.str(), "(1 m, -2 m)");
    EXPECT_EQ(ss2.str(), "(-1 m, 2 m)");
}

TEST(VectorC2D, kUnit)
{
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength>::kX(), VectorC2D<QtySiLength>(1.*meter, 0.*meter));
    VEC_EXPECT_NEAR(VectorC2D<QtySiLength>::kY(), VectorC2D<QtySiLength>(0.*meter, 1.*meter));
}


TEST(VectorC2D, repr)
{
    VectorC2D<QtySiLength> v1(  1.*meter, -2.*meter);
    VectorC2D<QtySiLength> v2( -1.*meter,  2.*meter);

    EXPECT_EQ(v1.repr(false), "(1 m, -2 m)");
    EXPECT_EQ(v2.repr(false), "(-1 m, 2 m)");

    EXPECT_EQ(v1.repr(true), "ℝ²(1 m, -2 m)");
    EXPECT_EQ(v2.repr(true), "ℝ²(-1 m, 2 m)");
}
