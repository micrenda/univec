#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <gtest/gtest.h>

#include "univec/Matrix3.hpp"
#include "univec/Matrix2.hpp"
#include "univec/VectorC3D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace dfpe;
using namespace si;

/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix3, addition)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> result (
            4. * meter, 4. * meter, 4. * meter,
            0 * meter, 4 * meter, 13 * meter,
            2. * meter, -3 * meter, 10. * meter
    );

    EXPECT_EQ(m1 + m2, result);
}

TEST(RMatrix3, additionEqual)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> result (
            4. * meter, 4. * meter, 4. * meter,
            0 * meter, 4 * meter, 13 * meter,
            2. * meter, -3 * meter, 10. * meter
    );

    EXPECT_EQ(m1 += m2, result);
}

/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix3, subtraction)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> result (
            -2. * meter, -6. * meter, -4. * meter,
            4 * meter, 10 * meter, -1 * meter,
            2. * meter, -5 * meter, 0. * meter
    );

    EXPECT_EQ(m1 - m2, result);
}

TEST(RMatrix3, subtractionEqual)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> result (
            -2. * meter, -6. * meter, -4. * meter,
            4 * meter, 10 * meter, -1 * meter,
            2. * meter, -5 * meter, 0. * meter
    );

    EXPECT_EQ(m1 -= m2, result);
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix3, multiplicationByScalar)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> result (
            2. * meter, -2. * meter, 0. * meter,
            4. * meter, 14 * meter, 12 * meter,
            4. * meter, -8 * meter, 10. * meter
    );

    EXPECT_EQ(m1 * 2., result);
}

TEST(RMatrix3, multiplicationByMatrix)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    RMatrix3<QtySiArea> result (
            5. * square_meter, 8 * square_meter, -3. * square_meter,
            -8. * square_meter, -5 * square_meter, 87 * square_meter,
            14. * square_meter, 27 * square_meter, 5. * square_meter
    );

    EXPECT_EQ(m1*m2, result);
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix3, scalarDivision)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> result (
            0.5 * meter, -0.5 * meter, 0. * meter,
            1. * meter, 3.5 * meter, 3 * meter,
            1. * meter, -2 * meter, 2.5 * meter
    );

    EXPECT_EQ(m1 / 2., result);
}


TEST(RMatrix3, directSum)
{
    RMatrix3<QtySiLength> m1(
            1. * meter, 2. * meter, 3. * meter,
            4. * meter, 5. * meter, 6. * meter,
            7. * meter, 8. * meter, 9. * meter
    );

    RMatrix3<QtySiLength> m2(
            11. * meter, 12. * meter, 13. * meter,
            14. * meter, 15. * meter, 16. * meter,
            17. * meter, 18. * meter, 19. * meter
    );

    RMatrix<6,6,QtySiLength> result(
            1. * meter, 2. * meter, 3. * meter,  0. * meter,  0. * meter,  0. * meter,
            4. * meter, 5. * meter, 6. * meter,  0. * meter,  0. * meter,  0. * meter,
            7. * meter, 8. * meter, 9. * meter,  0. * meter,  0. * meter,  0. * meter,
            0. * meter, 0. * meter, 0. * meter, 11. * meter, 12. * meter, 13. * meter,
            0. * meter, 0. * meter, 0. * meter, 14. * meter, 15. * meter, 16. * meter,
            0. * meter, 0. * meter, 0. * meter, 17. * meter, 18. * meter, 19. * meter
    );

    MAT_EXPECT_NEAR(m1.directSum(m2), result);
}


/* Comparative tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix3, isEqual)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter,  7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    EXPECT_EQ(m1, m2);
}

TEST(RMatrix3, isNotEqual)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            2. * meter, -1. * meter, 0. * meter,
            3. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    EXPECT_NE(m1, m2);
}

/* Common matrix operations tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix3, transpose)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> result (
            1. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_EQ(m1.t(), result);
}

TEST(RMatrix3, replaceWithTranspose)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> result (
            1. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    m1.doT();
    EXPECT_EQ(m1, result);
}

TEST(RMatrix3, square)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.square);
}

TEST(RMatrix3, orderOfSquareMatrix)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    EXPECT_EQ(m1.order(), 3);
}

TEST(RMatrix3, order)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    pair<unsigned int,unsigned int> result(3,3);
    EXPECT_EQ(m1.orders(), result);
}

TEST(RMatrix3, isIdentity)
{
    RMatrix3<QtySiLength> m1(
            1. * meter, 0. * meter, 0. * meter,
            0. * meter, 1. * meter, 0. * meter,
            0. * meter, 0. * meter, 1. * meter
    );
    RMatrix3<QtySiLength> m2 (
            1. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.isIdentity());
    EXPECT_FALSE(m2.isIdentity());
}

TEST(RMatrix3, trace)
{
    RMatrix3<QtySiLength> m1 (
            -3. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_EQ(m1.tr(), 9. * meter);
}

TEST(RMatrix3, isDiagonal)
{
    RMatrix3<QtySiLength> m1 (
            -3. * meter, 0. * meter, 0 * meter,
            0 * meter, 7. * meter, 0. * meter,
            0. * meter, 0 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            -3. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.isDiagonal());
    EXPECT_FALSE(m2.isDiagonal());
}

TEST(RMatrix3, isUpperTriangular)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, 2. * meter, 3 * meter,
            0. * meter, 5. * meter, 6. * meter,
            0. * meter, 0. * meter, 9. * meter
    );

    RMatrix3<QtySiLength> m2 (
            1. * meter, 2. * meter, 3. * meter,
            4. * meter, 5. * meter, 6. * meter,
            7. * meter, 8. * meter, 9. * meter
    );

    EXPECT_TRUE(m1.isUpperTriangular());
    EXPECT_FALSE(m2.isUpperTriangular());
}

TEST(RMatrix3, isLowerTriangular)
{
    RMatrix3<QtySiLength> m1 (
      1. * meter, 0. * meter, 0 * meter,
      4. * meter, 5. * meter, 0. * meter,
      7. * meter, 8 * meter, 9. * meter
    );

    RMatrix3<QtySiLength> m2 (
      1. * meter, 2. * meter, 3. * meter,
      4. * meter, 5. * meter, 6. * meter,
      7. * meter, 8. * meter, 9. * meter
    );

    EXPECT_TRUE(m1.isLowerTriangular());
    EXPECT_FALSE(m2.isLowerTriangular());
}


TEST(RMatrix3, isTriangular)
{
    RMatrix3<QtySiLength> m0 (
        1. * meter, 0. * meter, 0 * meter,
        0. * meter, 5. * meter, 0. * meter,
        0. * meter, 0. * meter, 9. * meter
    );

    RMatrix3<QtySiLength> m1 (
        1. * meter, 2. * meter, 3 * meter,
        0. * meter, 5. * meter, 6. * meter,
        0. * meter, 0. * meter, 9. * meter
    );

    RMatrix3<QtySiLength> m2 (
        1. * meter, 0. * meter, 0 * meter,
        4. * meter, 5. * meter, 0. * meter,
        7. * meter, 8. * meter, 9. * meter
    );

    RMatrix3<QtySiLength> m3 (
        1. * meter, 2. * meter, 3. * meter,
        4. * meter, 5. * meter, 6. * meter,
        7. * meter, 8. * meter, 9. * meter
    );

    EXPECT_TRUE(m0.isTriangular());
    EXPECT_TRUE(m1.isTriangular());
    EXPECT_TRUE(m2.isTriangular());
    EXPECT_FALSE(m3.isTriangular());
}

TEST(RMatrix3, isStrictTriangular)
{
    RMatrix3<QtySiLength> m1 (
        1. * meter, 0. * meter, 0 * meter,
        4. * meter, 1. * meter, 0. * meter,
        7. * meter, 8. * meter, 1. * meter
    );

    RMatrix3<QtySiLength> m2 (
        1. * meter, 2. * meter, 3 * meter,
        0. * meter, 5. * meter, 6. * meter,
        0. * meter, 0. * meter, 9. * meter
    );

    RMatrix3<QtySiLength> m3 (
        0. * meter, 0. * meter, 0 * meter,
        4. * meter, 0. * meter, 0. * meter,
        7. * meter, 8. * meter, 0. * meter
    );

    RMatrix3<QtySiLength> m4 (
        0. * meter, 2. * meter, 3 * meter,
        0. * meter, 0. * meter, 6. * meter,
        0. * meter, 0. * meter, 0. * meter
    );
    EXPECT_FALSE(m1.isStrictTriangular());
    EXPECT_FALSE(m2.isStrictTriangular());
    EXPECT_TRUE(m3.isStrictTriangular());
    EXPECT_TRUE(m4.isStrictTriangular());
}

TEST(RMatrix3, isUniTriangular)
{
      RMatrix3<QtySiLength> m1 (
          1. * meter, 0. * meter, 0 * meter,
          4. * meter, 5. * meter, 0. * meter,
          7. * meter, 8. * meter, 9. * meter
      );

      RMatrix3<QtySiLength> m2 (
          1. * meter, 2. * meter, 3 * meter,
          0. * meter, 5. * meter, 6. * meter,
          0. * meter, 0. * meter, 9. * meter
      );

      RMatrix3<QtySiLength> m3 (
          1. * meter, 0. * meter, 0 * meter,
          4. * meter, 1. * meter, 0. * meter,
          7. * meter, 8. * meter, 1. * meter
      );

      RMatrix3<QtySiLength> m4 (
          1. * meter, 2. * meter, 3 * meter,
          0. * meter, 1. * meter, 6. * meter,
          0. * meter, 0. * meter, 1. * meter
      );

    EXPECT_FALSE(m1.isUniTriangular());
    EXPECT_FALSE(m2.isUniTriangular());
    EXPECT_TRUE(m3.isUniTriangular());
    EXPECT_TRUE(m4.isUniTriangular());
}


TEST(RMatrix3, isSymmetric)
{
    RMatrix3<QtySiLength> m1 (
            -3. * meter, 2. * meter, 3. * meter,
            2. * meter, 7. * meter, 6. * meter,
            3. * meter, 6 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            -3. * meter, 2. * meter, 3. * meter,
            2. * meter, 7. * meter, -4. * meter,
            3. * meter, 6 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.isSymmetric());
    EXPECT_FALSE(m2.isSymmetric());
}

TEST(RMatrix3, isSkewSymmetric)
{
    RMatrix3<QtySiLength> m1 (
            -3. * meter, 2. * meter, 3. * meter,
            2. * meter, 7. * meter, 6. * meter,
            3. * meter, 6 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            0. * meter, 1. * meter, 3. * meter,
            -1. * meter, 0. * meter, 2. * meter,
            -3. * meter, -2 * meter, 0. * meter
    );

    EXPECT_FALSE(m1.isSkewSymmetric());
    EXPECT_TRUE(m2.isSkewSymmetric());
}

TEST(RMatrix3, submatrix)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix2<QtySiLength> result (
            7. * meter, 6. * meter,
            -4. * meter, 5. * meter
    );

    EXPECT_EQ(m1.sub(1,1), result);
}

TEST(RMatrix3, determinantLeibniz)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::LEIBNIZ_FORMULA), 57. * cubic_meter);
}

TEST(RMatrix3, determinantLaplace)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::LAPLACE_FORMULA), 57. * cubic_meter);
}

TEST(RMatrix3, determinantGauss)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::GAUSSIAN_ELIMINATION), 57. * cubic_meter);
}

TEST(RMatrix3, isOrthogonal)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    RMatrix3<QtySiLength> m2 (
            1. * meter, 0. * meter, 0. * meter,
            0. * meter, -1. * meter, 0. * meter,
            0. * meter, 0 * meter, 1. * meter
    );

    RMatrix3<QtySiLength> m3 (
            1./3. * meter, 2./3. * meter, -2./3. * meter,
            -2./3. * meter, 2./3. * meter,  1./3. * meter,
            2./3. * meter, 1./3. * meter,  2./3. * meter
    );

    EXPECT_FALSE(m1.isOrthogonal());
    EXPECT_TRUE(m2.isOrthogonal());
    EXPECT_TRUE(m3.isOrthogonal());
}

TEST(RMatrix3, isSingular)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiLength> m2(
            1. * meter, 2 * meter, 2. * meter,
            1. * meter, 2 * meter, 2 * meter,
            3. * meter, 2 * meter, -1. * meter
    );

    EXPECT_FALSE(m1.isSingular());
    EXPECT_TRUE(m2.isSingular());
}

TEST(RMatrix3, minor)
{
    RMatrix3<QtySiLength> m1(
            1. * meter, 0 * meter, 2. * meter,
            2. * meter, -2 * meter, 1 * meter,
            3. * meter, 2 * meter, 4. * meter
    );

    EXPECT_EQ(m1.minor(2,2), -2. * square_meter);
}

TEST(RMatrix3, minors)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiArea> matrixOfMinors(
            2. * square_meter, 2 * square_meter, 2. * square_meter,
            -2. * square_meter, 3 * square_meter, 3 * square_meter,
            0. * square_meter, -10 * square_meter, 0. * square_meter
    );

    EXPECT_EQ(m1.minors(), matrixOfMinors);
}

TEST(RMatrix3, cofactor)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    EXPECT_EQ(m1.cofactor(1,1),  2. * square_meter);
    EXPECT_EQ(m1.cofactor(1,2), -2. * square_meter);
    EXPECT_EQ(m1.cofactor(1,3),  2. * square_meter);
    EXPECT_EQ(m1.cofactor(2,1),  2. * square_meter);
    EXPECT_EQ(m1.cofactor(2,2),  3. * square_meter);
    EXPECT_EQ(m1.cofactor(2,3), -3. * square_meter);
    EXPECT_EQ(m1.cofactor(3,1),  0. * square_meter);
    EXPECT_EQ(m1.cofactor(3,2), 10. * square_meter);
    EXPECT_EQ(m1.cofactor(3,3),  0. * square_meter);
}

TEST(RMatrix3, cofactors)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiArea> result(
            2. * square_meter, -2. * square_meter, 2. * square_meter,
            2. * square_meter, 3. * square_meter, -3 * square_meter,
            0. * square_meter, 10. * square_meter, 0. * square_meter
    );

    EXPECT_EQ(m1.cofactors(), result);
}

TEST(RMatrix3, adjugate)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiArea> result(
            2. * square_meter, 2 * square_meter, 0. * square_meter,
            -2. * square_meter, 3 * square_meter, 10 * square_meter,
            2. * square_meter, -3 * square_meter, 0. * square_meter
    );

    EXPECT_EQ(m1.adj(), result);
}

TEST(RMatrix3, inv)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiWavenumber> result(
             0.2 * reciprocal_meter,  0.2 * reciprocal_meter, 0. * reciprocal_meter,
            -0.2 * reciprocal_meter,  0.3 * reciprocal_meter, 1 * reciprocal_meter,
             0.2 * reciprocal_meter, -0.3 * reciprocal_meter, 0 * reciprocal_meter
    );

    MAT_EXPECT_NEAR(m1.inv(), result);
}

TEST(RMatrix3, diagonal)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    RMatrix<3,1,QtySiLength> result(3. * meter, 0. * meter, 1. * meter);

    MAT_EXPECT_NEAR(m1.diagonal(), result);
}



TEST(RMatrix3, row)
{
    RMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    RMatrix<1,3,QtySiLength> result1(3. * meter, 0. * meter,  2. * meter);
    RMatrix<1,3,QtySiLength> result2(2. * meter, 0. * meter, -2. * meter);
    RMatrix<1,3,QtySiLength> result3(0. * meter, 1. * meter,  1. * meter);

    MAT_EXPECT_NEAR(m1.row(1), result1);
    MAT_EXPECT_NEAR(m1.row(2), result2);
    MAT_EXPECT_NEAR(m1.row(3), result3);
}

TEST(RMatrix3, col)
{
    RMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    RMatrix<3,1,QtySiLength> result1(3. * meter,  2. * meter,  0. * meter);
    RMatrix<3,1,QtySiLength> result2(0. * meter,  0. * meter,  1. * meter);
    RMatrix<3,1,QtySiLength> result3(2. * meter, -2. * meter,  1. * meter);

    MAT_EXPECT_NEAR(m1.col(1), result1);
    MAT_EXPECT_NEAR(m1.col(2), result2);
    MAT_EXPECT_NEAR(m1.col(3), result3);
}


TEST(RMatrix3, normL1)
{
    RMatrix3<QtySiLength> m1 (
             1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter,
             7. * meter, -8 * meter,  9 * meter
    );

    QTY_EXPECT_NEAR(m1.normL1(), 18. * meter);
}

TEST(RMatrix3, normLInf)
{
    RMatrix3<QtySiLength> m1 (
             1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter,
             7. * meter, -8 * meter,  9 * meter
    );

    QTY_EXPECT_NEAR(m1.normLInf(), 24. * meter);
}

TEST(RMatrix3, normLMax)
{
    RMatrix3<QtySiLength> m1 (
             1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter,
             7. * meter, -8 * meter,  9 * meter
    );

    QTY_EXPECT_NEAR(m1.normLMax(), 9. * meter);
}

TEST(RMatrix3, normL)
{
    // TODO
}

TEST(RMatrix3, normF)
{
    RMatrix3<QtySiLength> m1 (
             1. * meter, -1 * meter,  1 * meter,
            -1. * meter,  1 * meter, -1 * meter,
            1. * meter, -1 * meter,  1 * meter
    );

    QTY_EXPECT_NEAR(m1.normF(), 3. * meter);
}

/* Validity check operations */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix3, isNull)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    RMatrix3<QtySiLength> m2 (
            0. * meter, 0. * meter, 0. * meter,
            0. * meter, 0. * meter, 0. * meter,
            0. * meter, 0. * meter, 0. * meter
    );

    EXPECT_FALSE(m1.isNull());
    EXPECT_TRUE(m2.isNull());
}

TEST(RMatrix3, isFinite)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiLength> m2(
            inf_val * meter, inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter, inf_val * meter
    );

    EXPECT_TRUE(m1.isFinite());
    EXPECT_FALSE(m2.isFinite());
}

TEST(RMatrix3, isInfinite)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiLength> m2(
            inf_val * meter, inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter, inf_val * meter
    );

    EXPECT_FALSE(m1.isInfinite());
    EXPECT_TRUE(m2.isInfinite());
}

TEST(RMatrix3, isNormal)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiLength> m2(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, nan_val * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    EXPECT_FALSE(m1.isNormal());
    EXPECT_FALSE(m2.isNormal());
}

TEST(RMatrix3, isNan)
{
    RMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    RMatrix3<QtySiLength> m2(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, nan_val * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    EXPECT_FALSE(m1.isNan());
    EXPECT_TRUE(m2.isNan());
}


TEST(RMatrix3, elementwiseProduct)
{
    RMatrix3<QtySiLength> m1 (
             2. * meter,  4. * meter,  6. * meter,
             8. * meter, 10. * meter, 12. * meter,
            14. * meter, 16. * meter, 18. * meter
    );
    
    RMatrix3<QtySiForce> m2 (
            1. * newton, 2. * newton, 3. * newton,
            4. * newton, 5. * newton, 6. * newton,
            7. * newton, 8. * newton, 9. * newton
    );

    RMatrix3<QtySiEnergy> m3 (
             2. * meter * newton,   8. * meter * newton,  18. * meter * newton,
            32. * meter * newton,  50. * meter * newton,  72. * meter * newton,
            98. * meter * newton, 128. * meter * newton, 162. * meter * newton
    );

    MAT_EXPECT_NEAR(m1.elementwiseProduct(m2), m3);
}

TEST(RMatrix3, elementwiseDivision)
{
    RMatrix3<QtySiLength> m1 (
             2. * meter,  4. * meter,  6. * meter,
             8. * meter, 10. * meter, 12. * meter,
            14. * meter, 16. * meter, 18. * meter
    );
    
    RMatrix3<QtySiTime> m2 (
            1. * second, 2. * second, 3. * second,
            4. * second, 5. * second, 6. * second,
            7. * second, 8. * second, 9. * second
    );

    RMatrix3<QtySiVelocity> m3 (
            2. * meter / second, 2. * meter / second, 2. * meter / second,
            2. * meter / second, 2. * meter / second, 2. * meter / second,
            2. * meter / second, 2. * meter / second, 2. * meter / second
    );

    MAT_EXPECT_NEAR(m1.elementwiseDivision(m2), m3);
}

TEST(RMatrix3, rowMajor)
{
    RMatrix3<QtySiLength> m1 (
            0. * meter,  1. * meter,  2. * meter,
            3. * meter,  4. * meter,  5. * meter,
            6. * meter,  7. * meter,  8. * meter
    );

    EXPECT_EQ(m1.rowMajor(0), 0. * meter);
    EXPECT_EQ(m1.rowMajor(1), 1. * meter);
    EXPECT_EQ(m1.rowMajor(2), 2. * meter);
    EXPECT_EQ(m1.rowMajor(3), 3. * meter);
    EXPECT_EQ(m1.rowMajor(4), 4. * meter);
    EXPECT_EQ(m1.rowMajor(5), 5. * meter);
    EXPECT_EQ(m1.rowMajor(6), 6. * meter);
    EXPECT_EQ(m1.rowMajor(7), 7. * meter);
    EXPECT_EQ(m1.rowMajor(8), 8. * meter);
}

TEST(RMatrix3, colMajor)
{
    RMatrix3<QtySiLength> m1 (
            0. * meter,  3. * meter,  6. * meter,
            1. * meter,  4. * meter,  7. * meter,
            2. * meter,  5. * meter,  8. * meter
    );

    EXPECT_EQ(m1.colMajor(0), 0. * meter);
    EXPECT_EQ(m1.colMajor(1), 1. * meter);
    EXPECT_EQ(m1.colMajor(2), 2. * meter);
    EXPECT_EQ(m1.colMajor(3), 3. * meter);
    EXPECT_EQ(m1.colMajor(4), 4. * meter);
    EXPECT_EQ(m1.colMajor(5), 5. * meter);
    EXPECT_EQ(m1.colMajor(6), 6. * meter);
    EXPECT_EQ(m1.colMajor(7), 7. * meter);
    EXPECT_EQ(m1.colMajor(8), 8. * meter);
}

TEST(RMatrix3, diagonalProduct)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    EXPECT_EQ(m1.diagonalProduct(), 45. * cubic_meter);
}

TEST(RMatrix3, grandSum)
{
    RMatrix3<QtySiLength> m1(
            1. * meter, 2. * meter, 3. * meter,
            4. * meter, 5. * meter, 6. * meter,
            7. * meter, 8. * meter, 9. * meter);

    EXPECT_EQ(m1.grandSum(), 45. * meter);
}

TEST(RMatrix3, rowAdd)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> m2 = m1;
    RMatrix3<QtySiLength> m3 = m1;
    RMatrix3<QtySiLength> m4 = m1;

    RMatrix3<QtySiLength> result1 (
            1. * meter,  2. * meter,  3. * meter,
            5. * meter,  7. * meter,  9. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result2 (
            1. * meter,  2. * meter,  3. * meter,
            6. * meter,  9. * meter, 12. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result3 (
            1. * meter,  2. * meter,  3. * meter,
           13. * meter, 17. * meter, 21. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result4 (
            1. * meter,  2. * meter,  3. * meter,
           14. * meter, 19. * meter, 24. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    MAT_EXPECT_NEAR(m1.rowAdd(0, 1, std::nullopt, std::nullopt), result1);
    MAT_EXPECT_NEAR(m2.rowAdd(0, 1, 2., std::nullopt), result2);
    MAT_EXPECT_NEAR(m3.rowAdd(0, 1, std::nullopt, 3.), result3);
    MAT_EXPECT_NEAR(m4.rowAdd(0, 1, 2., 3.), result4);

    m1.doRowAdd(0, 1);
    MAT_EXPECT_NEAR(m1, result1);

    m2.doRowAdd(0, 1, 2.);
    MAT_EXPECT_NEAR(m2, result2);

    m3.doRowAdd(0, 1, std::nullopt, 3.);
    MAT_EXPECT_NEAR(m3, result3);

    m4.doRowAdd(0, 1, 2., 3.);
    MAT_EXPECT_NEAR(m4, result4);
}

TEST(RMatrix3, rowSub)
{
    RMatrix3<QtySiLength> m1(
            1. * meter, 2. * meter, 3. * meter,
            4. * meter, 5. * meter, 6. * meter,
            7. * meter, 8. * meter, 9. * meter);

    RMatrix3<QtySiLength> m2 = m1;
    RMatrix3<QtySiLength> m3 = m1;
    RMatrix3<QtySiLength> m4 = m1;

    RMatrix3<QtySiLength> result1(
            1. * meter, 2. * meter, 3. * meter,
            3. * meter, 3. * meter, 3. * meter,
            7. * meter, 8. * meter, 9. * meter);

    RMatrix3<QtySiLength> result2(
            1. * meter, 2. * meter, 3. * meter,
            2. * meter, 1. * meter, 0. * meter,
            7. * meter, 8. * meter, 9. * meter);

    RMatrix3<QtySiLength> result3(
             1. * meter,  2. * meter,  3. * meter,
            11. * meter, 13. * meter, 15. * meter,
             7. * meter,  8. * meter,  9. * meter);

    RMatrix3<QtySiLength> result4(
            1. * meter, 2. * meter, 3. * meter,
            10. * meter, 11. * meter, 12. * meter,
            7. * meter, 8. * meter, 9. * meter);


    MAT_EXPECT_NEAR(m1.rowSub(0, 1, std::nullopt, std::nullopt), result1);
    MAT_EXPECT_NEAR(m2.rowSub(0, 1, 2., std::nullopt), result2);
    MAT_EXPECT_NEAR(m3.rowSub(0, 1, std::nullopt, 3.), result3);
    MAT_EXPECT_NEAR(m4.rowSub(0, 1, 2., 3.), result4);


    m1.doRowSub(0, 1, std::nullopt, std::nullopt);
    MAT_EXPECT_NEAR(m1, result1);

    m2.doRowSub(0, 1, 2., std::nullopt);
    MAT_EXPECT_NEAR(m2, result2);

    m3.doRowSub(0, 1, std::nullopt, 3.);
    MAT_EXPECT_NEAR(m3, result3);

    m4.doRowSub(0, 1, 2., 3.);
    MAT_EXPECT_NEAR(m4, result4);
}

TEST(RMatrix3, rowMul)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result (
            1. * meter,  2. * meter,  3. * meter,
            8. * meter, 10. * meter, 12. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    MAT_EXPECT_NEAR(m1.rowMul(1, 2.), result);

    m1.doRowMul(1., 2.);
    MAT_EXPECT_NEAR(m1, result);
}

TEST(RMatrix3, rowDiv)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result (
            1. * meter, 2.  * meter,  3.  * meter,
            2. * meter, 2.5 * meter,  3 * meter,
            7. * meter, 8.  * meter,  9.  * meter
    );

    MAT_EXPECT_NEAR(m1.rowDiv(1, 2.), result);

    m1.doRowDiv(1., 2.);
    MAT_EXPECT_NEAR(m1, result);
}

TEST(RMatrix3, rowSwap)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result (
            7. * meter,  8. * meter,  9. * meter,
            4. * meter,  5. * meter,  6. * meter,
            1. * meter,  2. * meter,  3. * meter
    );

    MAT_EXPECT_NEAR(m1.rowSwap(0, 2), result);

    m1.doRowSwap(0, 2);
    MAT_EXPECT_NEAR(m1, result);
}

TEST(RMatrix3, reverseRows)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result (
            7. * meter,  8. * meter,  9. * meter,
            4. * meter,  5. * meter,  6. * meter,
            1. * meter,  2. * meter,  3. * meter
    );

    MAT_EXPECT_NEAR(m1.reverseRows(), result);

    m1.doReverseRows();
    MAT_EXPECT_NEAR(m1, result);
}

TEST(RMatrix3, shuffleRows)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    // We know that the generator will always produce the same numbers
    std::default_random_engine generator;

    EXPECT_EQ(m1.grandSum(), 45. * meter);

    m1.doShuffleRows(generator);
    EXPECT_EQ(m1.grandSum(), 45. * meter);
}

TEST(RMatrix3, sortRows)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  5. * meter,  6. * meter,
            1. * meter,  2. * meter,  3. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result1 (
            1. * meter,  2. * meter,  3. * meter,
            1. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result2 (
            7. * meter,  8. * meter,  9. * meter,
            1. * meter,  5. * meter,  6. * meter,
            1. * meter,  2. * meter,  3. * meter
    );

    MAT_EXPECT_NEAR(m1.sortRows(false), result1);
    MAT_EXPECT_NEAR(m1.sortRows(true),  result2);

    m1.doSortRows(false);
    MAT_EXPECT_NEAR(m1, result1);
    m1.doSortRows(true);
    MAT_EXPECT_NEAR(m1, result2);
}

TEST(RMatrix3, sortCols)
{
    RMatrix3<QtySiLength> m1 (
            2. * meter,  1. * meter,  3. * meter,
            5. * meter,  1. * meter,  6. * meter,
            8. * meter,  7. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result1 (
            1. * meter,  2. * meter,  3. * meter,
            1. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result2 (
            3. * meter,  2. * meter,  1. * meter,
            6. * meter,  5. * meter,  1. * meter,
            9. * meter,  8. * meter,  7. * meter
    );

    MAT_EXPECT_NEAR(m1.sortCols(false), result1);
    MAT_EXPECT_NEAR(m1.sortCols(true),  result2);

    m1.doSortCols(false);
    MAT_EXPECT_NEAR(m1, result1);

    m1.doSortCols(true);
    MAT_EXPECT_NEAR(m1,  result2);
}


TEST(RMatrix3, colAdd)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> m2 = m1;
    RMatrix3<QtySiLength> m3 = m1;
    RMatrix3<QtySiLength> m4 = m1;

    RMatrix3<QtySiLength> result1 (
            1. * meter,  3. * meter,  3. * meter,
            4. * meter,  9. * meter,  6. * meter,
            7. * meter,  15. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result2 (
            1. * meter,  4. * meter,  3. * meter,
            4. * meter, 13. * meter,  6. * meter,
            7. * meter, 22. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result3 (
            1. * meter,  7. * meter,  3. * meter,
            4. * meter,  19. * meter,  6. * meter,
            7. * meter,  31. * meter,  9. * meter
    );


RMatrix3<QtySiLength> result4 (
        1. * meter,  8. * meter,  3. * meter,
        4. * meter,  23. * meter,  6. * meter,
        7. * meter,  38. * meter,  9. * meter
);

    MAT_EXPECT_NEAR(m1.colAdd(0, 1, std::nullopt, std::nullopt), result1);
    MAT_EXPECT_NEAR(m2.colAdd(0, 1, 2., std::nullopt), result2);
    MAT_EXPECT_NEAR(m3.colAdd(0, 1, std::nullopt, 3.), result3);
    MAT_EXPECT_NEAR(m4.colAdd(0, 1, 2., 3.), result4);

    m1.doColAdd(0, 1,  std::nullopt, std::nullopt);
    MAT_EXPECT_NEAR(m1, result1);

    m2.doColAdd(0, 1, 2., std::nullopt);
    MAT_EXPECT_NEAR(m2, result2);

    m3.doColAdd(0, 1, std::nullopt, 3.);
    MAT_EXPECT_NEAR(m3, result3);

    m4.doColAdd(0, 1, 2., 3.);
    MAT_EXPECT_NEAR(m4, result4);
}

TEST(RMatrix3, colSub)
{
    RMatrix3<QtySiLength> m1(
            1. * meter, 2. * meter, 3. * meter,
            4. * meter, 5. * meter, 6. * meter,
            7. * meter, 8. * meter, 9. * meter);

    RMatrix3<QtySiLength> m2 = m1;
    RMatrix3<QtySiLength> m3 = m1;
    RMatrix3<QtySiLength> m4 = m1;

    RMatrix3<QtySiLength> result1(
            1. * meter, 1. * meter, 3. * meter,
            4. * meter, 1. * meter, 6. * meter,
            7. * meter, 1. * meter, 9. * meter);

    RMatrix3<QtySiLength> result2(
            1. * meter, 0. * meter, 3. * meter,
            4. * meter, -3. * meter, 6. * meter,
            7. * meter, -6. * meter, 9. * meter);

    RMatrix3<QtySiLength> result3(
            1. * meter, 5. * meter, 3. * meter,
            4. * meter, 11. * meter, 6. * meter,
            7. * meter, 17. * meter, 9. * meter);

    RMatrix3<QtySiLength> result4(
            1. * meter, 4. * meter, 3. * meter,
            4. * meter, 7. * meter, 6. * meter,
            7. * meter, 10. * meter, 9. * meter);

    MAT_EXPECT_NEAR(m1.colSub(0, 1, std::nullopt, std::nullopt), result1);
    MAT_EXPECT_NEAR(m2.colSub(0, 1, 2., std::nullopt), result2);
    MAT_EXPECT_NEAR(m3.colSub(0, 1, std::nullopt, 3.), result3);
    MAT_EXPECT_NEAR(m4.colSub(0, 1, 2., 3.), result4);

    m1.doColSub(0, 1, std::nullopt, std::nullopt);
    MAT_EXPECT_NEAR(m1, result1);

    m2.doColSub(0, 1, 2., std::nullopt);
    MAT_EXPECT_NEAR(m2, result2);

    m3.doColSub(0, 1, std::nullopt, 3.);
    MAT_EXPECT_NEAR(m3, result3);

    m4.doColSub(0, 1, 2., 3.);
    MAT_EXPECT_NEAR(m4, result4);
}

TEST(RMatrix3, colMul)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result (
            1. * meter,  4. * meter,  3. * meter,
            4. * meter, 10. * meter,  6. * meter,
            7. * meter, 16. * meter,  9. * meter
    );

    MAT_EXPECT_NEAR(m1.colMul(1, 2.), result);

    m1.doColMul(1, 2.);
    MAT_EXPECT_NEAR(m1, result);
}

TEST(RMatrix3, colDiv)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result (
            1. * meter,  1.  * meter,  3. * meter,
            4. * meter,  2.5 * meter,  6. * meter,
            7. * meter,  4.  * meter,  9. * meter
    );

    MAT_EXPECT_NEAR(m1.colDiv(1, 2.), result);

    m1.doColDiv(1, 2.);
    MAT_EXPECT_NEAR(m1, result);
}

TEST(RMatrix3, colSwap)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result (
            1. * meter,  3. * meter,  2. * meter,
            4. * meter,  6. * meter,  5. * meter,
            7. * meter,  9. * meter,  8. * meter
    );

    MAT_EXPECT_NEAR(m1.colSwap(1, 2), result)

    m1.doColSwap(1, 2);
    MAT_EXPECT_NEAR(m1, result);
}

TEST(RMatrix3, reverseCols)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    RMatrix3<QtySiLength> result (
            3. * meter,  2. * meter,  1. * meter,
            6. * meter,  5. * meter,  4. * meter,
            9. * meter,  8. * meter,  7. * meter
    );

    MAT_EXPECT_NEAR(m1.reverseCols(), result);

    m1.doReverseCols();
    MAT_EXPECT_NEAR(m1, result);
}

TEST(RMatrix3, shuffleCols)
{
    RMatrix3<QtySiLength> m1 (
            1. * meter,  2. * meter,  3. * meter,
            4. * meter,  5. * meter,  6. * meter,
            7. * meter,  8. * meter,  9. * meter
    );

    // We know that this generator will always produce the same numbers
    std::default_random_engine generator;

    EXPECT_EQ(m1.shuffleCols(generator).grandSum(), 45. * meter);

    m1.doShuffleCols(generator);
    EXPECT_EQ(m1.grandSum(), 45. * meter);
}


TEST(RMatrix3, zap)
{
    RMatrix3<QtySiLength> m1 (
            1.   * meter,  2. * meter,  3.   * meter,
            0.01 * meter,  0. * meter, -0.01 * meter,
            7.   * meter,  8. * meter,  9.   * meter
    );

    QtySiLength threshold = 0.1 * meter;

    RMatrix3<QtySiLength> result (
            1.   * meter,  2. * meter,  3.   * meter,
            0.   * meter,  0. * meter,  0.   * meter,
            7.   * meter,  8. * meter,  9.   * meter
    );

    ASSERT_EQ(m1.zap(threshold), result);

    m1.doZap(threshold);
    ASSERT_EQ(m1, result);
}

TEST(RMatrix3, reductionGauss)
{

    RMatrix3<QtySiLength> m1 (
            2. * meter,  1. * meter,  0. * meter,
            1. * meter, -1. * meter,  3. * meter,
           -1. * meter,  2. * meter, -2. * meter
    );

    RMatrix3<QtySiLength> result (
            2. * meter,  1.  * meter,  0.  * meter,
            0. * meter,  2.5 * meter, -2.  * meter,
            0. * meter,  0.  * meter,  1.8 * meter
    );
    // Be aware that Gauss elimination is not unique and it is algorithm dependent
    // a different alogirthm may produce a different valid result

    MAT_EXPECT_NEAR(m1.reduction(ReductionMethod::GAUSS_ELIMINATION), result);

    m1.doReduction(ReductionMethod::GAUSS_ELIMINATION);
    MAT_EXPECT_NEAR(m1, result);
}


//constexpr M_DL(TT) doReduction(ReductionMethod type = ReductionMethod::GAUSS_ELIMINATION );


