#include <iostream>

#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <limits>
#include <cmath>
#include <gtest/gtest.h>

#include "univec/Complex.hpp"
#include "univec/VectorC1D.hpp"
#include "univec/VectorC2D.hpp"
#include "univec/Matrix2.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace boost::units::degree;
using namespace dfpe;
using namespace si;

TEST(Complex, conj)
{
    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 2.*meter).conj(), Complex<QtySiLength> (1.*meter, -2.*meter));
    EXPECT_NE(Complex<QtySiLength> (-1.*meter, 2.*meter).conj(), Complex<QtySiLength> (1.*meter, -2.*meter));
    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 0.*meter).conj(), Complex<QtySiLength> (1.*meter, 0.*meter));
}

TEST(Complex, vector)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (1.*meter,   2.*meter).vector(),   VectorC1D<QtySiLength> (2.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter,   0.*meter).vector(),   VectorC1D<QtySiLength> (0.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-4.*meter, -2.*meter).vector(),   VectorC1D<QtySiLength> (-2.*meter));
}

TEST(Complex, pure)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (1.*meter,   2.*meter).pure(), Complex<QtySiLength> (0.*meter, 2.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter,   0.*meter).pure(), Complex<QtySiLength> (0.*meter, 0.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-4.*meter, -2.*meter).pure(), Complex<QtySiLength> (0.*meter, -2.*meter));
}

TEST(Complex, scalar)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (1.*meter,   2.*meter).scalar(), Complex<QtySiLength> (1.*meter,  0.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter,   0.*meter).scalar(), Complex<QtySiLength> (0.*meter,  0.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-4.*meter, -2.*meter).scalar(), Complex<QtySiLength> (-4.*meter, 0.*meter));
}

TEST(Complex, angle)
{
    // Angle is defined only for quaternions (see angle/axis quaternion representation
}

TEST(Complex, axis)
{
    // Axis is defined only for quaternions (see angle/axis quaternion representation
}

TEST(Complex, inv)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (1.*meter, 1.*meter).inv(), Complex<QtySiWavenumber> (0.5*reciprocal_meter, -0.5*reciprocal_meter));
}

TEST(Complex, isPure)
{
    EXPECT_FALSE(Complex<QtySiLength> (-4.*meter, 4.*meter).isPure());
    EXPECT_FALSE(Complex<QtySiLength> (-4.*meter, 0.*meter).isPure());
    EXPECT_TRUE (Complex<QtySiLength> (-0.*meter, 4.*meter).isPure());
    EXPECT_TRUE (Complex<QtySiLength> (-0.*meter, 0.*meter).isPure());
}

TEST(Complex, isScalar)
{
    EXPECT_FALSE(Complex<QtySiLength> (-4.*meter, 4.*meter).isScalar());
    EXPECT_TRUE (Complex<QtySiLength> (-4.*meter, 0.*meter).isScalar());
    EXPECT_FALSE(Complex<QtySiLength> (-0.*meter, 4.*meter).isScalar());
    EXPECT_TRUE (Complex<QtySiLength> (-0.*meter, 0.*meter).isScalar());
}

TEST(Complex, rotate)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (5.*meter, 0.*meter).rotate(  90.*degrees), Complex<QtySiLength> ( 0.*meter,  5.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (5.*meter, 0.*meter).rotate( -90.*degrees), Complex<QtySiLength> ( 0.*meter, -5.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (5.*meter, 0.*meter).rotate( 180.*degrees), Complex<QtySiLength> (-5.*meter,  0.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (5.*meter, 0.*meter).rotate(-180.*degrees), Complex<QtySiLength> (-5.*meter,  0.*meter));
}

TEST(Complex, matrix)
{
    // Disabling for now
    //VEC_EXPECT_NEAR(Complex<QtySiLength> (2.*meter, -3.*meter).matrix(), RMatrix2<QtySiLength> ( 2.*meter,  3.*meter, - 3.*meter, 2.*meter ));
}


TEST(Complex, elementAccess)
{
    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 2.*meter)[0], 1*meter); // 0-based
    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 2.*meter)[1], 2*meter); // 0-based

    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 2.*meter)(1), 1*meter); // 1-based
    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 2.*meter)(2), 2*meter); // 1-based

#ifndef GCC
    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 2.*meter).a, 1*meter); // name-based
    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 2.*meter).b, 2*meter); // name-based
#endif
}

TEST(Complex, symbol)
{
    EXPECT_EQ(Complex<QtySiLength> (1.*meter, 2.*meter).symbol(), "ℂ");
}

TEST(Complex, cout)
{
    std::stringstream ss1, ss2;
    ss1 << Complex<QtySiLength> (  1.*meter, -2.*meter);
    ss2 << Complex<QtySiLength> ( -1.*meter,  2.*meter);

    EXPECT_EQ(ss1.str(), "1 m - 2 m i");
    EXPECT_EQ(ss2.str(), "- 1 m + 2 m i");
}

TEST(Complex, repr)
{
    Complex<QtySiLength> v1(  1.*meter, -2.*meter);
    Complex<QtySiLength> v2( -1.*meter,  2.*meter);

    EXPECT_EQ(v1.repr(false), "(1 m - 2 m i)");
    EXPECT_EQ(v2.repr(false), "(- 1 m + 2 m i)");

    EXPECT_EQ(v1.repr(true), "ℂ(1 m - 2 m i)");
    EXPECT_EQ(v2.repr(true), "ℂ(- 1 m + 2 m i)");
}


/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Complex, addition)
{
    Complex<QtySiLength> vec_a(3.*meter, 0.*meter);
    Complex<QtySiLength> vec_b(2.*meter, 2.*meter);

    VEC_EXPECT_NEAR(vec_a + vec_b, Complex<QtySiLength> (5.*meter, 2.*meter));
}

TEST(Complex, additionEqual)
{
    Complex<QtySiLength> vec_a(3.*meter, 0.*meter);
    Complex<QtySiLength> vec_b(2.*meter, 2.*meter);
    vec_a += vec_b;

    VEC_EXPECT_NEAR(vec_a, Complex<QtySiLength> (5.*meter, 2.*meter));
}

/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Complex, subtraction)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (3.*meter, 0.*meter) - Complex<QtySiLength> (2.*meter, 2.*meter), Complex<QtySiLength> (1.*meter, -2.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (2.*meter, 2.*meter) - Complex<QtySiLength> (3.*meter, 0.*meter), Complex<QtySiLength> (-1.*meter, 2.*meter));
}

TEST(Complex, subtractionEqual)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (3.*meter, 0.*meter) -= Complex<QtySiLength> (2.*meter, 2.*meter), Complex<QtySiLength> (1.*meter, -2.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (2.*meter, 2.*meter) -= Complex<QtySiLength> (3.*meter, 0.*meter), Complex<QtySiLength> (-1.*meter, 2.*meter));
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Complex, scalarMutiplication)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (3.*meter, 4.*meter) * 2, Complex<QtySiLength> (6.*meter, 8.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-2.*meter, -3.*meter) * 0.5, Complex<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (2.*meter, 0.*meter) * 2, Complex<QtySiLength> (4.*meter, 0.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter, -4.*meter) * 2, Complex<QtySiLength> (0.*meter, -8.*meter));

    VEC_EXPECT_NEAR(2 * Complex<QtySiLength> (3.*meter, 4.*meter), Complex<QtySiLength> (6.*meter, 8.*meter));
    VEC_EXPECT_NEAR(0.5 * Complex<QtySiLength> (-2.*meter, -3.*meter), Complex<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(2 * Complex<QtySiLength> (2.*meter, 0.*meter), Complex<QtySiLength> (4.*meter, 0.*meter));
    VEC_EXPECT_NEAR(2 * Complex<QtySiLength> (0.*meter, -4.*meter), Complex<QtySiLength> (0.*meter, -8.*meter));
}

TEST(Complex, scalarMultiplicationEqual)
{
    // Multiplication with a scalar
    VEC_EXPECT_NEAR(Complex<QtySiLength> (3.*meter, 4.*meter) *= 2, Complex<QtySiLength> (6.*meter, 8.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-2.*meter, -3.*meter) *= 0.5, Complex<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (2.*meter, 0.*meter) *= 2, Complex<QtySiLength> (4.*meter, 0.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter, -4.*meter) *= 2, Complex<QtySiLength> (0.*meter, -8.*meter));
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Complex, scalarDivision)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (3.*meter, 4.*meter) / 0.5, Complex<QtySiLength> ( 6.*meter, 8.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-2.*meter, -3.*meter) / 2, Complex<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (6.*meter, 0.*meter) / 3., Complex<QtySiLength> (2.*meter, 0.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter, -4.*meter) / -4., Complex<QtySiLength> (0.*meter, 1.*meter));
}

TEST(Complex, scalarDivisionEqual)
{
    // Division with a scalar
    VEC_EXPECT_NEAR(Complex<QtySiLength> (3.*meter, 4.*meter) /= 0.5, Complex<QtySiLength> ( 6. * meter, 8. * meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-2.*meter, -3.*meter) /= 2., Complex<QtySiLength> (-1. * meter, -1.5 * meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (6.*meter, 0.*meter) /= 3., Complex<QtySiLength> (2. * meter, 0. * meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter, -4.*meter) /= -4., Complex<QtySiLength> (0. * meter, 1. * meter));
}

TEST(Complex, mutiplication)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (3.*meter, 4.*meter)   * Complex<>(2, 1), Complex<QtySiLength> (2.*meter, 11.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-2.*meter, -3.*meter) * Complex<>(0.5), Complex<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (2.*meter, 0.*meter)   * Complex<>(0, 2), Complex<QtySiLength> (0.*meter, 4.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter, -4.*meter)  * Complex<>(), Complex<QtySiLength> ());
}

TEST(Complex, multiplicationEqual)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength> (3.*meter, 4.*meter)   *= Complex<>(2, 1), Complex<QtySiLength> (2.*meter, 11.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (-2.*meter, -3.*meter) *= Complex<>(0.5), Complex<QtySiLength> (-1.*meter, -1.5*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (2.*meter, 0.*meter)   *= Complex<>(0, 2), Complex<QtySiLength> (0.*meter, 4.*meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength> (0.*meter, -4.*meter)  *= Complex<>(), Complex<QtySiLength> ());
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Complex, division)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength>(2. * meter, 11. * meter) / Complex<>(2, 1), Complex<QtySiLength>(3. * meter, 4. * meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength>(-1. * meter, -1.5 * meter) / Complex<>(0.5), Complex<QtySiLength>(-2. * meter, -3. * meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength>(0. * meter, 4. * meter) / Complex<>(0, 2), Complex<QtySiLength>(2. * meter, 0. * meter));
    EXPECT_TRUE((Complex<QtySiLength>(1. * meter, 4. * meter) / Complex<>()).isInfinite());
    EXPECT_TRUE((Complex<QtySiLength>() / Complex<>()).isNan());
}

TEST(Complex, divisionEqual)
{
    VEC_EXPECT_NEAR(Complex<QtySiLength>(2. * meter, 11. * meter) /= Complex<>(2, 1), Complex<QtySiLength>(3. * meter, 4. * meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength>(-1. * meter, -1.5 * meter) /= Complex<>(0.5), Complex<QtySiLength>(-2. * meter, -3. * meter));
    VEC_EXPECT_NEAR(Complex<QtySiLength>(0. * meter, 4. * meter) /= Complex<>(0, 2), Complex<QtySiLength>(2. * meter, 0. * meter));
    EXPECT_TRUE((Complex<QtySiLength>(1. * meter, 4. * meter) /= Complex<>()).isInfinite());
    EXPECT_TRUE((Complex<QtySiLength>() /= Complex<>()).isNan());
}

// exp, log, pow, cos, sin, tan, cosh, sinh, tanh