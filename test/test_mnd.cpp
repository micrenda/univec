#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <gtest/gtest.h>

#include "univec/Matrix.hpp"
#include "univec/VectorC2D.hpp"
#include "univec/VectorC3D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace dfpe;
using namespace si;

/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrixMN, addition)
{
    RMatrix<3,2, QtySiLength> m1 (
            1. * meter, -1. * meter,
            2. * meter, 7. * meter,
            2. * meter, -4 * meter
    );
    RMatrix<3,2, QtySiLength> m2 (
            3. * meter, 5. * meter,
            -2 * meter, -3 * meter,
            0. * meter, 1 * meter
    );
    RMatrix<3,2, QtySiLength> result (
            4. * meter, 4. * meter,
            0 * meter, 4 * meter,
            2. * meter, -3 * meter
    );

    EXPECT_EQ(m1 + m2, result);
}

TEST(RMatrixMN, additionEqual)
{
    RMatrix<3,2, QtySiLength> m1 (
            1. * meter, -1. * meter,
            2. * meter, 7. * meter,
            2. * meter, -4 * meter
    );
    RMatrix<3,2, QtySiLength> m2 (
            3. * meter, 5. * meter,
            -2 * meter, -3 * meter,
            0. * meter, 1 * meter
    );
    RMatrix<3,2, QtySiLength> result (
            4. * meter, 4. * meter,
            0 * meter, 4 * meter,
            2. * meter, -3 * meter
    );

    EXPECT_EQ(m1 += m2, result);
}


/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrixMN, subtraction)
{
    RMatrix<3,2, QtySiLength> m1 (
            1. * meter, -1. * meter,
            2. * meter, 7. * meter,
            2. * meter, -4 * meter
    );
    RMatrix<3,2, QtySiLength> m2 (
            3. * meter, 5. * meter,
            -2 * meter, -3 * meter,
            0. * meter, 1 * meter
    );
    RMatrix<3,2, QtySiLength> result (
            -2. * meter, -6. * meter,
            4 * meter, 10 * meter,
            2. * meter, -5 * meter
    );

    EXPECT_EQ(m1 - m2, result);
}

TEST(RMatrixMN, subtractionEqual)
{
    RMatrix<3,2, QtySiLength> m1 (
            1. * meter, -1. * meter,
            2. * meter, 7. * meter,
            2. * meter, -4 * meter
    );
    RMatrix<3,2, QtySiLength> m2 (
            3. * meter, 5. * meter,
            -2 * meter, -3 * meter,
            0. * meter, 1 * meter
    );
    RMatrix<3,2, QtySiLength> result (
            -2. * meter, -6. * meter,
            4 * meter, 10 * meter,
            2. * meter, -5 * meter
    );

    EXPECT_EQ(m1 -= m2, result);
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrixMN, multiplicationByScalar)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, -1. * meter,
            0.5 * meter, 2. * meter,
            2 * meter, -4. * meter
    );
    RMatrix<3,2, QtySiLength> result(
            2. * meter, -2. * meter,
            1. * meter, 4. * meter,
            4 * meter, -8 * meter
    );

    EXPECT_EQ(m1 * 2., result);
}

TEST(RMatrixMN, multiplicationByMatrix)
{

}

TEST(RMatrixMN, multiplicationByVector) {
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );
    VectorC2D<QtySiLength> v1(2. * meter, 4. * meter);
    VectorC3D<QtySiArea> result(10. * square_meter, 22. * square_meter, 34. * square_meter);

    EXPECT_EQ(m1 * v1, result);
}


TEST(RMatrixMN, elementwiseProduct)
{
    RMatrix<3,2, QtySiLength> m1 (
            2. * meter,  4. * meter,
            8. * meter, 10. * meter,
            14. * meter, 16. * meter
    );

    RMatrix<3,2, QtySiForce> m2 (
            1. * newton, 2. * newton,
            4. * newton, 5. * newton,
            7. * newton, 8. * newton
    );

    RMatrix<3,2, QtySiEnergy> m3 (
            2. * meter * newton,    8. * meter * newton,
            32. * meter * newton,  50. * meter * newton,
            98. * meter * newton, 128. * meter * newton
    );

    MAT_EXPECT_NEAR(m1.elementwiseProduct(m2), m3);
}

TEST(RMatrixMN, elementwiseDivision)
{
    RMatrix<3,2, QtySiLength> m1 (
            2. * meter,   4. * meter,
            8. * meter,  10. * meter,
            14. * meter, 16. * meter
    );

    RMatrix<3,2, QtySiTime> m2 (
            1. * second, 2. * second,
            4. * second, 5. * second,
            7. * second, 8. * second
    );

    RMatrix<3,2, QtySiVelocity> m3 (
            2. * meter / second, 2. * meter / second,
            2. * meter / second, 2. * meter / second,
            2. * meter / second, 2. * meter / second
    );

    MAT_EXPECT_NEAR(m1.elementwiseDivision(m2), m3);
}



/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrixMN, scalarDivision)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<3,2, QtySiLength> result (
            0.5 * meter, 1 * meter,
            1.5 * meter, 2 * meter,
            2.5 * meter, 3 * meter
    );

    EXPECT_EQ(m1 / 2., result);
}

/* Comparative tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrixMN, isEqual)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<3,2, QtySiLength> m2(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );

    EXPECT_EQ(m1, m2);
}

TEST(RMatrixMN, isNotEqual)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<3,2, QtySiLength> m2(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 3. * meter
    );

    EXPECT_NE(m1, m2);
}


/* Common matrix operations tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrixMN, transpose)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<2,3, QtySiLength> result(
            1. * meter, 3. * meter, 5 * meter,
            2. * meter, 4. * meter, 6 * meter
    );

    EXPECT_EQ(m1.t(), result);
}

TEST(RMatrixMN, isSquare)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );

    EXPECT_FALSE(m1.square);
}

TEST(RMatrixMN, order)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );

    pair<unsigned int,unsigned int> result(3,2);
    EXPECT_EQ(m1.orders(), result);
}

TEST(RMatrixMN, row)
{
    RMatrix<2,3, QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter
    );

    RMatrix<1,3,QtySiLength> result1(3. * meter, 0. * meter,  2. * meter);
    RMatrix<1,3,QtySiLength> result2(2. * meter, 0. * meter, -2. * meter);

    MAT_EXPECT_NEAR(m1.row(1), result1);
    MAT_EXPECT_NEAR(m1.row(2), result2);
}

TEST(RMatrixMN, col)
{
    RMatrix<2,3, QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter
    );

    RMatrix<2,1,QtySiLength> result1(3. * meter,  2. * meter);
    RMatrix<2,1,QtySiLength> result2(0. * meter,  0. * meter);
    RMatrix<2,1,QtySiLength> result3(2. * meter, -2. * meter);

    MAT_EXPECT_NEAR(m1.col(1), result1);
    MAT_EXPECT_NEAR(m1.col(2), result2);
    MAT_EXPECT_NEAR(m1.col(3), result3);
}


TEST(RMatrixMN, normL1)
{
    RMatrix<2,3,QtySiLength> m1 (
            1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter
    );

    QTY_EXPECT_NEAR(m1.normL1(), 9. * meter);
}

TEST(RMatrixMN, normLInf)
{
    RMatrix<2,3,QtySiLength> m1 (
             1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter
    );

    QTY_EXPECT_NEAR(m1.normLInf(), 15. * meter);
}

TEST(RMatrixMN, normLMax)
{
    RMatrix<2,3,QtySiLength> m1 (
             1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter
    );

    QTY_EXPECT_NEAR(m1.normLMax(), 6. * meter);
}

TEST(RMatrixMN, normL)
{
    // TODO
}

TEST(RMatrixMN, normF)
{
    RMatrix<2,3,QtySiLength> m1 (
             8. * meter, -8 * meter,  0* meter,
            -8. * meter,  8 * meter, -63 * meter
    );

    QTY_EXPECT_NEAR(m1.normF(), 65. * meter);
}

TEST(RMatrixMN, norm)
{
    RMatrix<3,1,QtySiLength> m1(
            3. * meter, 6. * meter,	22. * meter
    );
    RMatrix<1,3,QtySiLength> m2(
            3. * meter, 6. * meter,	22. * meter
    );

    QTY_EXPECT_NEAR(m1.norm(), 23. * meter);
    QTY_EXPECT_NEAR(m2.norm(), 23. * meter);
}

TEST(RMatrixMN, normSquared)
{
    RMatrix<3,1,QtySiLength> m1(
            3. * meter, 6. * meter,	22. * meter
    );
    RMatrix<1,3,QtySiLength> m2(
            3. * meter, 6. * meter,	22. * meter
    );

    QTY_EXPECT_NEAR(m1.normSquared(), 529. * square_meter);
    QTY_EXPECT_NEAR(m2.normSquared(), 529. * square_meter);
}

TEST(RMatrixMN, submatrix)
{
    RMatrix<4,2,QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter,
            7. * meter, 8 * meter
    );

    RMatrix<3,1,QtySiLength> result1(2. * meter, 4. * meter, 8 * meter);

    EXPECT_EQ(m1.sub(3, 1), result1);
}

/* Validity check operations */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrixMN, isNull)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<3,2, QtySiLength> m2(
            0. * meter, 0. * meter,
            0. * meter, 0. * meter,
            0. * meter, 0. * meter
    );

    EXPECT_FALSE(m1.isNull());
    EXPECT_TRUE(m2.isNull());
}

TEST(RMatrixMN, isFinite)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<3,2, QtySiLength> m2(
            inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter
    );

    EXPECT_TRUE(m1.isFinite());
    EXPECT_FALSE(m2.isFinite());
}

TEST(RMatrixMN, isInf)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<3,2, QtySiLength> m2(
            inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter
    );

    EXPECT_FALSE(m1.isInfinite());
    EXPECT_TRUE(m2.isInfinite());
}

TEST(RMatrixMN, isNormal)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 0. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<3,2, QtySiLength> m2(
            1. * meter, 2. * meter,
            3. * meter, -4. * meter,
            nan_val * meter, 6. * meter
    );

    EXPECT_FALSE(m1.isNormal());
    EXPECT_FALSE(m2.isNormal());
}

TEST(RMatrixMN, isNan)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 0. * meter,
            5. * meter, 6. * meter
    );
    RMatrix<3,2, QtySiLength> m2(
            1. * meter, 2. * meter,
            3. * meter, -4. * meter,
            nan_val * meter, 6. * meter
    );

    EXPECT_FALSE(m1.isNan());
    EXPECT_TRUE(m2.isNan());
}


TEST(RMatrixMN, elementAccess)
{
    RMatrix<3,2, QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter,
            5. * meter, 6. * meter
    );


#if __cpp_multidimensional_subscript

    auto a00 = m1[0,0];
    auto a01 = m1[0,1];
    auto a10 = m1[1,0];
    auto a11 = m1[1,1];
    auto a20 = m1[2,0];
    auto a21 = m1[2,1];

    EXPECT_EQ(a00, Real<QtySiLength>(1*meter)); // 0-based
    EXPECT_EQ(a01, Real<QtySiLength>(2*meter)); // 0-based
    EXPECT_EQ(a10, Real<QtySiLength>(3*meter)); // 0-based
    EXPECT_EQ(a11, Real<QtySiLength>(4*meter)); // 0-based
    EXPECT_EQ(a20, Real<QtySiLength>(5*meter)); // 0-based
    EXPECT_EQ(a21, Real<QtySiLength>(6*meter)); // 0-based
#endif

    auto b11 = m1(1,1);
    auto b12 = m1(1,2);
    auto b21 = m1(2,1);
    auto b22 = m1(2,2);
    auto b31 = m1(3,1);
    auto b32 = m1(3,2);

    EXPECT_EQ(b11, Real<QtySiLength>(1*meter)); // 1-based
    EXPECT_EQ(b12, Real<QtySiLength>(2*meter)); // 1-based
    EXPECT_EQ(b21, Real<QtySiLength>(3*meter)); // 1-based
    EXPECT_EQ(b22, Real<QtySiLength>(4*meter)); // 1-based
    EXPECT_EQ(b31, Real<QtySiLength>(5*meter)); // 1-based
    EXPECT_EQ(b32, Real<QtySiLength>(6*meter)); // 1-based

}


TEST(RMatrixMN, rowMajor)
{
    RMatrix<3,2, QtySiLength> m1 (
            0. * meter,  1. * meter,
            2. * meter,  3. * meter,
            4. * meter,  5. * meter
    );

    EXPECT_EQ(m1.rowMajor(0), 0. * meter);
    EXPECT_EQ(m1.rowMajor(1), 1. * meter);
    EXPECT_EQ(m1.rowMajor(2), 2. * meter);
    EXPECT_EQ(m1.rowMajor(3), 3. * meter);
    EXPECT_EQ(m1.rowMajor(4), 4. * meter);
    EXPECT_EQ(m1.rowMajor(5), 5. * meter);
}

TEST(RMatrixMN, colMajor)
{
    RMatrix<3,2, QtySiLength> m1 (
            0. * meter,  3. * meter,
            1. * meter,  4. * meter,
            2. * meter,  5. * meter
    );

    EXPECT_EQ(m1.colMajor(0), 0. * meter);
    EXPECT_EQ(m1.colMajor(1), 1. * meter);
    EXPECT_EQ(m1.colMajor(2), 2. * meter);
    EXPECT_EQ(m1.colMajor(3), 3. * meter);
    EXPECT_EQ(m1.colMajor(4), 4. * meter);
    EXPECT_EQ(m1.colMajor(5), 5. * meter);
}