#include <iostream>

#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <limits>
#include <cmath>
#include <gtest/gtest.h>

#include "univec/Complex.hpp"
#include "univec/VectorC1D.hpp"
#include "univec/VectorC3D.hpp"
#include "univec/Matrix.hpp"
#include "univec/Matrix4.hpp"
#include "univec/Quaternion.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace boost::units::degree;
using namespace dfpe;
using namespace si;

TEST(Quaternion, conjugate)
{
    Quaternion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<QtySiLength> result(1*meter, -2 * meter, -3 * meter, -4 * meter);

    EXPECT_EQ(q.conj(), result);
    EXPECT_EQ(q.conj().conj(), q);
}

TEST(Quaternion, pure)
{
    Quaternion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<QtySiLength> result(0*meter, 2 * meter, 3 * meter, 4 * meter);

    EXPECT_EQ(q.pure(), result);
}

TEST(Quaternion, scalar)
{
    Quaternion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<QtySiLength> result(1*meter, 0 * meter, 0 * meter, 0 * meter);

    EXPECT_EQ(q.scalar(), result);
}

TEST(Quaternion, vector)
{
    Quaternion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter);
    VectorC3D<QtySiLength> result(2 * meter, 3 * meter, 4 * meter);

    VEC_EXPECT_NEAR(q.vector(), result);
}

TEST(Quaternion, angle)
{
    Quaternion<QtySiLength> q(0. *meter,0. *meter, 0. *meter,-1. * meter);
    QTY_EXPECT_NEAR(q.angle(), QtySiPlaneAngle(180. * degrees));
}

TEST(Quaternion, rotate)
{
//    TODO: Michele
//    Axis 1,1,1 angle 90 degree
    Quaternion<QtySiLength> q(1/sqrt(2) * meter, 1/sqrt(6) * meter, 1/sqrt(6) * meter, 1/sqrt(6) * meter);
    VectorC3D<QtySiLength> v0(1 * meter, 1 * meter, 1 * meter);
//    VectorC3D<QtySiLength> v1(1 * meter, 0 * meter, 0 * meter);
//    VectorC3D<QtySiLength> v2(0 * meter, 1 * meter, 0 * meter);
//    VectorC3D<QtySiLength> v3(0 * meter, 0 * meter, 1 * meter);

    VEC_EXPECT_NEAR(q.rotate(v0), v0);
//    VEC_EXPECT_NEAR(q.rotate(v1), v0);
//    EXPECT_EQ(q.rotate(v1), v0);
}

TEST(Quaternion, axis)
{
    Quaternion<QtySiLength> q(0. *meter,0. *meter, 0. *meter,-1. * meter);
    VectorC3D<QtySiLength> result(0. * meter, 0. * meter, -1. * meter);
    VEC_EXPECT_NEAR(q.axis(), result);
}

TEST(Quaternion, inverse)
{
    Quaternion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<QtySiWavenumber> result(1./30. * reciprocal_meter, -2./30. * reciprocal_meter, -3./30. * reciprocal_meter, -4./30. * reciprocal_meter);

    EXPECT_EQ(q.inv(), result);
}

TEST(Quaternion, isPure)
{
    Quaternion<QtySiLength> q1(0*meter, 0 * meter, 0 * meter, 0 * meter);
    Quaternion<QtySiLength> q2(1*meter, 0 * meter, 0 * meter, 0 * meter);
    Quaternion<QtySiLength> q3(0*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<QtySiLength> q4(1*meter, 2 * meter, 3 * meter, 4 * meter);

    EXPECT_TRUE(q1.isPure());
    EXPECT_FALSE(q2.isPure());
    EXPECT_TRUE(q3.isPure());
    EXPECT_FALSE(q4.isPure());
}

TEST(Quaternion, isScalar)
{
    Quaternion<QtySiLength> q1(0*meter, 0 * meter, 0 * meter, 0 * meter);
    Quaternion<QtySiLength> q2(1*meter, 0 * meter, 0 * meter, 0 * meter);
    Quaternion<QtySiLength> q3(0*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<QtySiLength> q4(1*meter, 2 * meter, 3 * meter, 4 * meter);

    EXPECT_TRUE(q1.isScalar());
    EXPECT_TRUE(q2.isScalar());
    EXPECT_FALSE(q3.isScalar());
    EXPECT_FALSE(q4.isScalar());
}

TEST(Quaternion, matrix)
{
    //
    // Disabling for now
    /*
    Quaternion<QtySiLength> q1(1.*meter, 2.*meter, 3.*meter, 4.*meter);
    RMatrix4<QtySiLength> result(
        1.*meter, -2.*meter, -3.*meter, -4.*meter,
        2.*meter,  1.*meter, -4.*meter,  3.*meter,
        3.*meter,  4.*meter,  1.*meter, -2.*meter,
        4.*meter, -3.*meter,  2.*meter,  1.*meter
    );

    VEC_EXPECT_NEAR(q1.matrix(), result);
     */
}

TEST(Quaternion, multiplication)
{
    Quaternion<QtySiLength> q1(1*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<QtySiLength> q2(5*meter, 6 * meter, 7 * meter, 8 * meter);

    Quaternion<QtySiArea> result1(-60*square_meter, 12 * square_meter, 30 * square_meter, 24 * square_meter);
    Quaternion<QtySiArea> result2(-60*square_meter, 20 * square_meter, 14 * square_meter, 32 * square_meter);

    VEC_EXPECT_NEAR(q1 * q2, result1);
    VEC_EXPECT_NEAR(q2 * q1, result2);
}

TEST(Quaternion, division)
{
    Quaternion<QtySiLength> q1(1*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<QtySiLength> q2(2*meter, 4 * meter, 6 * meter, 8 * meter);

    Quaternion<QtySiDimensionless> result1(0.5);
    Quaternion<QtySiDimensionless> result2(2.0);

    VEC_EXPECT_NEAR(q1 / q2, result1);
    VEC_EXPECT_NEAR(q2 / q1, result2);
}

TEST(Quaternion, multiplicationAssignment)
{
    Quaternion<QtySiLength> q1(1*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<> q2(5, 6, 7, 8);

    Quaternion<> p1(1, 2, 3, 4);
    Quaternion<QtySiLength> p2(5*meter, 6 * meter, 7 * meter, 8 * meter);

    Quaternion<QtySiLength> result1(-60*meter, 12*meter, 30*meter, 24*meter);
    Quaternion<QtySiLength> result2(-60*meter, 20*meter, 14*meter, 32*meter);

    VEC_EXPECT_NEAR(q1 *= q2, result1);
    VEC_EXPECT_NEAR(p2 *= p1, result2);
}

TEST(Quaternion, divisionAssignment)
{
    Quaternion<QtySiLength> q1(1*meter, 2 * meter, 3 * meter, 4 * meter);
    Quaternion<> q2(2, 4, 6, 8);

    Quaternion<> p1(1, 2, 3, 4);
    Quaternion<QtySiLength> p2(2*meter, 4 * meter, 6 * meter, 8 * meter);

    Quaternion<QtySiLength> result1(0.5*meter);
    Quaternion<QtySiLength> result2(2.0*meter);

    VEC_EXPECT_NEAR(q1 /= q2, result1);
    VEC_EXPECT_NEAR(p2 /= p1, result2);
}


TEST(Quaternion, elementAccess)
{
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter)[0], 1*meter); // 0-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter)[1], 2*meter); // 0-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter)[2], 3*meter); // 0-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter)[3], 4*meter); // 0-based

    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter)(1), 1*meter); // 1-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter)(2), 2*meter); // 1-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter)(3), 3*meter); // 1-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter)(4), 4*meter); // 1-based

#ifndef GCC
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter).a, 1*meter); // name-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter).b, 2*meter); // name-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter).c, 3*meter); // name-based
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter).d, 4*meter); // name-based
#endif
}

TEST(Quaternion, symbol)
{
    EXPECT_EQ(Quaternion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter).symbol(), "ℍ");
}


TEST(Quaternion, cout)
{
    std::stringstream ss1, ss2;
    ss1 << Quaternion<QtySiLength> ( 1.*meter,  2.*meter, -3.*meter, -4.*meter);
    ss2 << Quaternion<QtySiLength> (-1.*meter, -2.*meter,  3.*meter,  4.*meter);

    EXPECT_EQ(ss1.str(), "1 m + 2 m i - 3 m j - 4 m k");
    EXPECT_EQ(ss2.str(), "- 1 m - 2 m i + 3 m j + 4 m k");
}


TEST(Quaternion, repr)
{
    Quaternion<QtySiLength> v1( 1.*meter,  2.*meter, -3.*meter, -4.*meter);
    Quaternion<QtySiLength> v2(-1.*meter, -2.*meter,  3.*meter,  4.*meter);

    EXPECT_EQ(v1.repr(false), "(1 m + 2 m i - 3 m j - 4 m k)");
    EXPECT_EQ(v2.repr(false), "(- 1 m - 2 m i + 3 m j + 4 m k)");

    EXPECT_EQ(v1.repr(true), "ℍ(1 m + 2 m i - 3 m j - 4 m k)");
    EXPECT_EQ(v2.repr(true), "ℍ(- 1 m - 2 m i + 3 m j + 4 m k)");
}

TEST(Quaternion, exponential)
{
//    TODO: Michele
}

TEST(Quaternion, power)
{
//    TODO: Michele
}

TEST(Quaternion, logarithm)
{
//    TODO: Michele
}

TEST(Quaternion, cos)
{
//    TODO: Michele
}

TEST(Quaternion, sin)
{
//    TODO: Michele
}

