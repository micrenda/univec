#include <iostream>
#include <limits>
#include <boost/units/quantity.hpp>

#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/angle/degrees.hpp>

#include "univec/VectorC2D.hpp"
#include "univec/VectorP2D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

#include <gtest/gtest.h>

using namespace std;
using namespace boost::units;
using namespace boost::units::si;
using namespace boost::units::degree;
using namespace dfpe;
using namespace testing;

TEST(VectorP2D, norm)
{
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (-3.  * meter,  4. * degrees).norm(), 3.  * si::meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3.5 * meter,  4. * degrees).norm(), 3.5 * si::meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (-3.  * meter, -4. * degrees).norm(), 3.  * si::meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3.  * meter, -4. * degrees).norm(), 3.  * si::meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> ( 0.  * meter,  4. * degrees).norm(), 0.  * si::meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> ( 0.  * meter,  0. * degrees).norm(), 0.  * si::meter);
}

TEST(VectorP2D, normSquared)
{
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3.  * meters,  4. * degrees).normSquared(),  9. * square_meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (-3.  * meters,  4. * degrees).normSquared(),  9. * square_meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (-4.  * meters, -4. * degrees).normSquared(), 16. * square_meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> ( 4.  * meters, -4. * degrees).normSquared(), 16. * square_meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> ( 0.  * meters,  4. * degrees).normSquared(),  0. * square_meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> ( 0.  * meters,  0. * degrees).normSquared(),  0. * square_meter);
}

TEST(VectorP2D, isNull)
{
    EXPECT_FALSE(VectorP2D<QtySiLength> ( 3. * meters, 4. * degrees).isNull());
    EXPECT_FALSE(VectorP2D<QtySiLength> (-4. * meters, 0. * degrees).isNull());
    EXPECT_TRUE(VectorP2D<QtySiLength>  ( 0. * meters, 4. * degrees).isNull());
    EXPECT_TRUE(VectorP2D<QtySiLength>  ( 0. * meters, 0. * degrees).isNull());
}

TEST(VectorP2D, versor)
{
    VEC_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3. * meters, 400. * degrees).versor(), VectorP2D<QtySiLength> (1. * meter,  40. *degrees));
    VEC_EXPECT_NEAR(VectorP2D<QtySiLength> (-3. * meters, 400. * degrees).versor(), VectorP2D<QtySiLength> (1. * meter, 220. *degrees));
    VEC_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3. * meters, 360. * degrees).versor(), VectorP2D<QtySiLength> (1. * meter,   0. *degrees));
    VEC_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3. * meters, -60. * degrees).versor(), VectorP2D<QtySiLength> (1. * meter, 300. *degrees));

    VectorP2D<QtySiLength> v1(-3. * meters, 400. * degrees);
    v1.doVersor();
    VEC_EXPECT_NEAR(v1, VectorP2D<QtySiLength> (1. * meter, 220. *degrees));

}

TEST(VectorP2D, versorDl)
{
    VEC_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3. * meters, 400. * degrees).versorDl(), VectorP2D<QtySiDimensionless> (1.,  40. *degrees));
    VEC_EXPECT_NEAR(VectorP2D<QtySiLength> (-3. * meters, 400. * degrees).versorDl(), VectorP2D<QtySiDimensionless> (1., 220. *degrees));
    VEC_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3. * meters, 360. * degrees).versorDl(), VectorP2D<QtySiDimensionless> (1.,   0. *degrees));
    VEC_EXPECT_NEAR(VectorP2D<QtySiLength> ( 3. * meters, -60. * degrees).versorDl(), VectorP2D<QtySiDimensionless> (1., 300. *degrees));
}

TEST(VectorP2D, dot)
{
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (   1. * meters, 430. * degrees).dot( VectorP2D<QtySiLength> ( 2. * meter,   10. *degrees)),  1. * square_meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (  -1. * meters, 430. * degrees).dot( VectorP2D<QtySiLength> ( 2. * meter,   10. *degrees)), -1. * square_meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (  -1. * meters, 430. * degrees).dot( VectorP2D<QtySiLength> ( 2. * meter,  430. *degrees)), -2. * square_meter);
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (   0. * meters, 430. * degrees).dot( VectorP2D<QtySiLength> (69. * meter,  420. *degrees)),  0. * square_meter);
}

TEST(VectorP2D, angle)
{
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (  1. * meters,   0. * degrees).angle(VectorP2D<QtySiLength> (  1. * meters,   0. * degrees)), QtySiPlaneAngle(  0. * degrees));
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (  1. * meters,   0. * degrees).angle(VectorP2D<QtySiLength> ( -1. * meters,   0. * degrees)), QtySiPlaneAngle(180. * degrees));
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (  1. * meters,   0. * degrees).angle(VectorP2D<QtySiLength> (  1. * meters,  90. * degrees)), QtySiPlaneAngle( 90. * degrees));
    QTY_EXPECT_NEAR(VectorP2D<QtySiLength> (  1. * meters,   0. * degrees).angle(VectorP2D<QtySiLength> (  1. * meters, -90. * degrees)), QtySiPlaneAngle(270. * degrees));

}

TEST(VectorP2D, isFinite)
{
    EXPECT_TRUE(  VectorP2D<QtySiLength> (    1. * meters,   0. * degrees).isFinite());
    EXPECT_TRUE(  VectorP2D<QtySiLength> (  999. * meters, 430. * degrees).isFinite());
    EXPECT_TRUE(  VectorP2D<QtySiLength> (    0. * meters, 430. * degrees).isFinite());
    EXPECT_FALSE( VectorP2D<QtySiLength> (   inf_val * meters,   2. * degrees).isFinite());
    EXPECT_FALSE( VectorP2D<QtySiLength> (    2. * meters,  inf_val * degrees).isFinite());
    EXPECT_FALSE( VectorP2D<QtySiLength> (   inf_val * meters,  inf_val * degrees).isFinite());
}

TEST(VectorP2D, isNormal)
{
    EXPECT_FALSE(  VectorP2D<QtySiLength> (     0. * meters,      0. * degrees).isNormal()); // 0. is not a normal value
    EXPECT_FALSE(  VectorP2D<QtySiLength> (     1. * meters,      0. * degrees).isNormal()); // 0. is not a normal value
    EXPECT_TRUE(  VectorP2D<QtySiLength> (   999. * meters,    430. * degrees).isNormal());
    EXPECT_FALSE( VectorP2D<QtySiLength> (nan_val * meters,    430. * degrees).isNormal());
    EXPECT_FALSE( VectorP2D<QtySiLength> (     2. * meters, nan_val * degrees).isNormal());
    EXPECT_FALSE( VectorP2D<QtySiLength> (nan_val * meters, nan_val * degrees).isNormal());
    EXPECT_FALSE( VectorP2D<QtySiLength> (     2. * meters,     inf_val * degrees).isNormal());
    EXPECT_FALSE( VectorP2D<QtySiLength> (    inf_val * meters,     inf_val * degrees).isNormal());
    EXPECT_FALSE( VectorP2D<QtySiLength> (    inf_val * meters,      2. * degrees).isNormal());
}

TEST(VectorP2D, isNan)
{
    EXPECT_FALSE( VectorP2D<QtySiLength> (     1. * meters,      0. * degrees).isNan());
    EXPECT_FALSE( VectorP2D<QtySiLength> (     0. * meters,      0. * degrees).isNan());
    EXPECT_FALSE( VectorP2D<QtySiLength> (   999. * meters,    430. * degrees).isNan());
    EXPECT_TRUE(  VectorP2D<QtySiLength> (nan_val * meters,    430. * degrees).isNan());
    EXPECT_TRUE(  VectorP2D<QtySiLength> (     2. * meters, nan_val * degrees).isNan());
    EXPECT_TRUE(  VectorP2D<QtySiLength> (nan_val * meters, nan_val * degrees).isNan());
    EXPECT_FALSE( VectorP2D<QtySiLength> (     2. * meters,     inf_val * degrees).isNan());
    EXPECT_FALSE( VectorP2D<QtySiLength> (    inf_val * meters,     inf_val * degrees).isNan());
    EXPECT_FALSE( VectorP2D<QtySiLength> (    inf_val * meters,      2. * degrees).isNan());
    EXPECT_TRUE(  VectorP2D<QtySiLength> (    inf_val * meters, nan_val * degrees).isNan());
}

TEST(VectorP2D, isInf)
{
    EXPECT_FALSE(VectorP2D<QtySiLength>(1. * meters, 0. * degrees).isInfinite());
    EXPECT_FALSE(VectorP2D<QtySiLength>(0. * meters, 0. * degrees).isInfinite());
    EXPECT_FALSE(VectorP2D<QtySiLength>(999. * meters, 430. * degrees).isInfinite());
    EXPECT_FALSE(VectorP2D<QtySiLength>(nan_val * meters, 430. * degrees).isInfinite());
    EXPECT_FALSE(VectorP2D<QtySiLength>(2. * meters, nan_val * degrees).isInfinite());
    EXPECT_FALSE(VectorP2D<QtySiLength>(nan_val * meters, nan_val * degrees).isInfinite());
    EXPECT_TRUE(VectorP2D<QtySiLength>(2. * meters, inf_val * degrees).isInfinite());
    EXPECT_TRUE(VectorP2D<QtySiLength>(inf_val * meters, inf_val * degrees).isInfinite());
    EXPECT_TRUE(VectorP2D<QtySiLength>(inf_val * meters, 2. * degrees).isInfinite());
    EXPECT_TRUE(VectorP2D<QtySiLength>(inf_val * meters, nan_val * degrees).isInfinite());
}

TEST(VectorP2D, addition)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(0. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_d(1. * meters, 0. * degrees);

    VEC_EXPECT_NEAR(vec_a + vec_b, VectorP2D<QtySiLength> (3. * si::meter, 90. * degrees));
    VEC_EXPECT_NEAR(vec_a + vec_c, VectorP2D<QtySiLength> (1. * si::meter, 90. * degrees));
    VEC_EXPECT_NEAR(vec_a + vec_d, VectorP2D<QtySiLength> (sqrt(2) * si::meter, 45. * degrees));
}

TEST(VectorP2D, additionEqual)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(0. * meters, 90. * degrees);

    vec_a += vec_b;
    VEC_EXPECT_NEAR(vec_a, VectorP2D<QtySiLength> (3. * si::meter, 90. * degrees));

    vec_a += vec_c;
    VEC_EXPECT_NEAR(vec_a, VectorP2D<QtySiLength> (3. * si::meter, 90. * degrees));

    VectorP2D<QtySiLength> vec_e(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_d(1. * meters,  0. * degrees);
    vec_e += vec_d;
    VEC_EXPECT_NEAR(vec_e, VectorP2D<QtySiLength> (sqrt(2) * si::meter, 45. * degrees));
}

TEST(VectorP2D, subtraction)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(0. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_d(1. * meters, 0. * degrees);

    VEC_EXPECT_NEAR(vec_a - vec_b, VectorP2D<QtySiLength> (1. * si::meter, 270. * degrees));
    VEC_EXPECT_NEAR(vec_a - vec_c, VectorP2D<QtySiLength> (1. * si::meter, 90. * degrees));
    VEC_EXPECT_NEAR(vec_a - vec_d, VectorP2D<QtySiLength> (sqrt(2) * si::meter, 135. * degrees));
}

TEST(VectorP2D, subtractionEqual)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(0. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_d(1. * meters, 0. * degrees);

    vec_a -= vec_b;
    VEC_EXPECT_NEAR(vec_a, VectorP2D<QtySiLength> (1. * si::meter, 270. * degrees));

    vec_a -= vec_c;
    VEC_EXPECT_NEAR(vec_a, VectorP2D<QtySiLength> (1. * si::meter,  270. * degrees));

    vec_a -= vec_d;
    VEC_EXPECT_NEAR(vec_a, VectorP2D<QtySiLength> (sqrt(2) * si::meter, 225. * degrees));
}

TEST(VectorP2D, multiplication)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(0. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_d(1. * meters, 0. * degrees);

    VEC_EXPECT_NEAR(vec_a * 5.,   VectorP2D<QtySiLength> (5. * si::meter, 90. * degrees));
    VEC_EXPECT_NEAR(vec_b * 105., VectorP2D<QtySiLength> (210. * si::meter, 90. * degrees));
    // All zero radius polar points are normalized to zero angle
    VEC_EXPECT_NEAR(vec_c * 200., VectorP2D<QtySiLength> (0. * si::meter, 0. * degrees));
    VEC_EXPECT_NEAR(vec_d * 10.,  VectorP2D<QtySiLength> (10. * si::meter, 0. * degrees));
}

TEST(VectorP2D, multiplicationEqual)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(0. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_d(1. * meters, 0. * degrees);

    //MultiplicationEqual with scalar
    vec_a *= 5;
    VEC_EXPECT_NEAR(vec_a,   VectorP2D<QtySiLength> (5. * si::meter, 90. * degrees));

    vec_b *= 105.;
    VEC_EXPECT_NEAR(vec_b, VectorP2D<QtySiLength> (210. * si::meter, 90. * degrees));

    // All zero radius polar points are normalized to zero angle
    vec_c *= 200.;
    VEC_EXPECT_NEAR(vec_c, VectorP2D<QtySiLength> (0. * si::meter, 0. * degrees));

    vec_d *= 10.;
    VEC_EXPECT_NEAR(vec_d, VectorP2D<QtySiLength> (10. * si::meter, 0. * degrees));
}

TEST(VectorP2D, divisionEqual)
{
    VectorP2D<QtySiLength> vec_a(3. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(0. * meters, 90. * degrees);

    vec_a /= 3;
    vec_b /= 7;
    VEC_EXPECT_NEAR(vec_a, VectorP2D<QtySiLength> (1. * si::meter, 90. * degrees));
    VEC_EXPECT_NEAR(vec_b, VectorP2D<QtySiLength> (0. * si::meter, 90. * degrees));
}

TEST(VectorP2D, greaterThan)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(3. * meters,  0. * degrees);

    EXPECT_GT(vec_b, vec_a);
    EXPECT_GT(vec_c, vec_b);
}

TEST(VectorP2D, greaterThanOrEqualTo)
{
    VectorP2D<QtySiLength> vec_a(1.  * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2.  * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(3.  * meters, 55. * degrees);
    VectorP2D<QtySiLength> vec_d(3   * meters, 54. * degrees);

    EXPECT_GE(vec_b, vec_a);
    EXPECT_GE(vec_c, vec_b);
    EXPECT_GE(vec_c, vec_d);
}

TEST(VectorP2D, lessThan)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(3. * meters,  0. * degrees);

    EXPECT_LT(vec_a, vec_b);
    EXPECT_LT(vec_b, vec_c);
}

TEST(VectorP2D, lessThanOrEqualTo)
{
    VectorP2D<QtySiLength> vec_a(1. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_b(2. * meters, 90. * degrees);
    VectorP2D<QtySiLength> vec_c(3. * meters,  0. * degrees);
    VectorP2D<QtySiLength> vec_d(3. * meters, 55. * degrees);

    EXPECT_LE(vec_a, vec_b);
    EXPECT_LE(vec_b, vec_c);
    EXPECT_LE(vec_c, vec_d);

}

TEST(VectorP2D, isEqualTo)
{
    VectorP2D<QtySiLength> vec_a(3. * meters,  0. * degrees);
    VectorP2D<QtySiLength> vec_b(3. * meters,  0. * degrees);

    VEC_EXPECT_NEAR(vec_a, vec_b);
}

TEST(VectorP2D, notEqualTo)
{
    VectorP2D<QtySiLength> vec_a(5. * meters,  0. * degrees);
    VectorP2D<QtySiLength> vec_b(3. * meters,  0. * degrees);
    VectorP2D<QtySiLength> vec_c(9. * meters, 55. * degrees);

    EXPECT_NE(vec_a, vec_b);
    EXPECT_NE(vec_b, vec_c);
}

TEST(VectorP2D, isNear)
{
    VectorP2D<QtySiLength> vec_a(5. * meters,  2. * degrees);
    VectorP2D<QtySiLength> vec_b(3. * meters,  1. * degrees);
    VectorP2D<QtySiLength> vec_c(5. * meters,  2.00000000001 * degrees);
    VectorP2D<QtySiLength> vec_d(5.00000000001 * meters,  2. * degrees);

    EXPECT_FALSE(vec_a.isNear(vec_b));
    EXPECT_TRUE(vec_a.isNear(vec_c));
    EXPECT_TRUE(vec_a.isNear(vec_d));
}

TEST(VectorP2D, toCartesian)
{
    VectorP2D<QtySiLength> vec_a(5. * meters,  0. * degrees);

    VEC_EXPECT_NEAR(VectorC2D<QtySiLength>(vec_a), VectorC2D<QtySiLength>(5. * meters, 0. * meters));
}



TEST(VectorP2D, isParallel)
{
    EXPECT_TRUE(VectorP2D<QtySiLength>  (1.*meter, 0.*degrees).isParallel(VectorP2D<QtySiLength> ( 2.*meter, 0.*degrees)));
    EXPECT_TRUE(VectorP2D<QtySiLength>  (1.*meter, 0.*degrees).isParallel(VectorP2D<QtySiLength> (-2.*meter, 0.*degrees)));
    EXPECT_TRUE(VectorP2D<QtySiLength>  (1.*meter, 90.*degrees).isParallel(VectorP2D<QtySiLength> ( 2.*meter, 90.*degrees)));
    EXPECT_TRUE(VectorP2D<QtySiLength>  (1.*meter, 90.*degrees).isParallel(VectorP2D<QtySiLength> ( 2.*meter,-90.*degrees)));

    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isParallel(VectorP2D<QtySiLength> ( 2.*meter,  45.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isParallel(VectorP2D<QtySiLength> ( 2.*meter, 135.*degrees)));

    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isParallel(VectorP2D<QtySiLength> ( 0.*meter, 0.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (0.*meter, 0.*degrees).isParallel(VectorP2D<QtySiLength> ( 1.*meter, 0.*degrees)));
}

TEST(VectorP2D, isSameDirection)
{
    EXPECT_TRUE (VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isSameDirection(VectorP2D<QtySiLength> ( 2.*meter, 0.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isSameDirection(VectorP2D<QtySiLength> (-2.*meter, 0.*degrees)));
    EXPECT_TRUE (VectorP2D<QtySiLength> (1.*meter, 90.*degrees).isSameDirection(VectorP2D<QtySiLength> ( 2.*meter, 90.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 90.*degrees).isSameDirection(VectorP2D<QtySiLength> ( 2.*meter,-90.*degrees)));

    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isSameDirection(VectorP2D<QtySiLength> ( 2.*meter,  45.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isSameDirection(VectorP2D<QtySiLength> ( 2.*meter, 135.*degrees)));

    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isSameDirection(VectorP2D<QtySiLength> ( 0.*meter, 0.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (0.*meter, 0.*degrees).isSameDirection(VectorP2D<QtySiLength> ( 1.*meter, 0.*degrees)));
}

TEST(VectorP2D, isOppositeDirection)
{
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isOppositeDirection(VectorP2D<QtySiLength>  ( 2.*meter,   0.*degrees)));
    EXPECT_TRUE (VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isOppositeDirection(VectorP2D<QtySiLength>  (-2.*meter,   0.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 90.*degrees).isOppositeDirection(VectorP2D<QtySiLength> ( 2.*meter,  90.*degrees)));
    EXPECT_TRUE (VectorP2D<QtySiLength> (1.*meter, 90.*degrees).isOppositeDirection(VectorP2D<QtySiLength> ( 2.*meter, -90.*degrees)));

    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isOppositeDirection(VectorP2D<QtySiLength> ( 2.*meter, 45.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isOppositeDirection(VectorP2D<QtySiLength> (2.*meter, 135.*degrees)));

    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isOppositeDirection(VectorP2D<QtySiLength> ( 0.*meter, 0.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (0.*meter, 0.*degrees).isOppositeDirection(VectorP2D<QtySiLength> ( 1.*meter, 0.*degrees)));
}

TEST(VectorP2D, isPerpendicular)
{
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isPerpendicular(VectorP2D<QtySiLength> ( 1.*meter,   0.*degrees)));
    EXPECT_TRUE (VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isPerpendicular(VectorP2D<QtySiLength> ( 1.*meter,  90.*degrees)));
    EXPECT_TRUE (VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isPerpendicular(VectorP2D<QtySiLength> ( 1.*meter, -90.*degrees)));

    EXPECT_TRUE (VectorP2D<QtySiLength> (1.*meter, 90.*degrees).isPerpendicular(VectorP2D<QtySiLength> ( 1.*meter,   0.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 90.*degrees).isPerpendicular(VectorP2D<QtySiLength> ( 1.*meter,  90.*degrees)));
    EXPECT_TRUE (VectorP2D<QtySiLength> (1.*meter, 90.*degrees).isPerpendicular(VectorP2D<QtySiLength> ( 1.*meter, 180.*degrees)));

    EXPECT_FALSE(VectorP2D<QtySiLength> (0.*meter, 0.*degrees).isPerpendicular(VectorP2D<QtySiLength> ( 2.*meter, 0.*degrees)));
    EXPECT_FALSE(VectorP2D<QtySiLength> (1.*meter, 0.*degrees).isPerpendicular(VectorP2D<QtySiLength> ( 0.*meter, 0.*degrees)));
}

TEST(VectorP2D, elementAccess)
{
#if !defined(GCC) || defined(ENABLE_GCC_PROPERTY)
    EXPECT_EQ(VectorP2D<QtySiLength> (1.*meter, 90.*degrees).r,    1.*meter);   // name-based
    EXPECT_EQ(VectorP2D<QtySiLength> (1.*meter, 90.*degrees).phi, QtySiPlaneAngle(90.*degrees)); // name-based
#endif
}
