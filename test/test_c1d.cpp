#include <iostream>

#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <limits>
#include <cmath>
#include <gtest/gtest.h>

#include "univec/VectorC1D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;

using namespace boost::units;
using namespace boost::units::degree;
using namespace dfpe;
using namespace si;

TEST(VectorC1D, conversion)
{
    QtySiLength v1 = VectorC1D<QtySiLength> (1.*meter);
    EXPECT_EQ(v1, 1.*meter);

    VectorC1D<QtySiLength> v2 = 1.*meter;
    EXPECT_EQ(v2, VectorC1D<QtySiLength>(1.*meter));
}

/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC1D, addition)
{
    VectorC1D<QtySiLength> vec_a(3.*meter);
    VectorC1D<QtySiLength> vec_b(2.*meter);

    VEC_EXPECT_NEAR(vec_a + vec_b, VectorC1D<QtySiLength> (5.*meter));
}

TEST(VectorC1D, additionEqual)
{
    VectorC1D<QtySiLength> vec_a(3.*meter);
    VectorC1D<QtySiLength> vec_b(2.*meter);
    vec_a += vec_b;

    VEC_EXPECT_NEAR(vec_a, VectorC1D<QtySiLength> (5.*meter));
}

/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC1D, subtraction)
{
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (3.*meter) - VectorC1D<QtySiLength> (2.*meter), VectorC1D<QtySiLength> (1.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (2.*meter) - VectorC1D<QtySiLength> (3.*meter), VectorC1D<QtySiLength> (-1.*meter));
}

TEST(VectorC1D, subtractionEqual)
{
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (3.*meter) -= VectorC1D<QtySiLength> (2.*meter), VectorC1D<QtySiLength> (1.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (2.*meter) -= VectorC1D<QtySiLength> (3.*meter), VectorC1D<QtySiLength> (-1.*meter));
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC1D, multiplication)
{
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (3.*meter) * 2, VectorC1D<QtySiLength> (6.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (-2.*meter) * 0.5, VectorC1D<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (2.*meter) * 2, VectorC1D<QtySiLength> (4.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (0.*meter) * 2, VectorC1D<QtySiLength> (0.*meter));

    VEC_EXPECT_NEAR(2 * VectorC1D<QtySiLength> (3.*meter), VectorC1D<QtySiLength> (6.*meter));
    VEC_EXPECT_NEAR(0.5 * VectorC1D<QtySiLength> (-2.*meter), VectorC1D<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(2 * VectorC1D<QtySiLength> (2.*meter), VectorC1D<QtySiLength> (4.*meter));
    VEC_EXPECT_NEAR(2 * VectorC1D<QtySiLength> (0.*meter), VectorC1D<QtySiLength> (0.*meter));
}

TEST(VectorC1D, multiplicationEqual)
{
    // Multiplication with a scalar
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (3.*meter) *= 2, VectorC1D<QtySiLength> (6.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (-2.*meter) *= 0.5, VectorC1D<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (2.*meter) *= 2, VectorC1D<QtySiLength> (4.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (0.*meter) *= 2, VectorC1D<QtySiLength> (0.*meter));
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC1D, division)
{
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (3.*meter) / 0.5, VectorC1D<QtySiLength> ( 6.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (-2.*meter) / 2, VectorC1D<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (6.*meter) / 3., VectorC1D<QtySiLength> (2.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (0.*meter) / -4., VectorC1D<QtySiLength> (0.*meter));
}

TEST(VectorC1D, divisionEqual)
{
    // Division with a scalar
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (3.*meter) /= 0.5, VectorC1D<QtySiLength> ( 6. * meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (-2.*meter) /= 2., VectorC1D<QtySiLength> (-1. * meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (6.*meter) /= 3., VectorC1D<QtySiLength> (2. * meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (0.*meter) /= -4., VectorC1D<QtySiLength> (0. * meter));
}

/* Comparative tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC1D, greaterThan)
{
    VectorC1D<QtySiLength> vec_a(3.*meter);
    VectorC1D<QtySiLength> vec_b(2.*meter);

    EXPECT_GT(vec_a, vec_b);
}

TEST(VectorC1D, greaterThanOrEqualTo)
{
    VectorC1D<QtySiLength> vec_a(3.*meter);
    VectorC1D<QtySiLength> vec_b(3.*meter);
    VectorC1D<QtySiLength> vec_c(2.*meter);

    EXPECT_GE(vec_a, vec_b);
    EXPECT_GE(vec_b, vec_c);
}

TEST(VectorC1D, lessThan)
{
    VectorC1D<QtySiLength> vec_a(3.*meter);
    VectorC1D<QtySiLength> vec_b(2.*meter);

    EXPECT_LT(vec_b, vec_a);
}

TEST(VectorC1D, lessThanOrEqualTo)
{
    VectorC1D<QtySiLength> vec_a(3.*meter);
    VectorC1D<QtySiLength> vec_b(4.*meter);
    VectorC1D<QtySiLength> vec_c(4.*meter);

    EXPECT_LE(vec_a, vec_b);
    EXPECT_LE(vec_b, vec_c);
}

TEST(VectorC1D, isEqualTo)
{
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (2.*meter), VectorC1D<QtySiLength> (2.*meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (0.*meter), VectorC1D<QtySiLength> (0.*meter));
}

TEST(VectorC1D, notEqualTo)
{
    VectorC1D<QtySiLength> vec_a(3.*meter);
    VectorC1D<QtySiLength> vec_b(1.*meter);
    VectorC1D<QtySiLength> vec_c(0.*meter);
    VectorC1D<QtySiLength> vec_d(-1.*meter);

    EXPECT_NE(vec_a, vec_b);
    EXPECT_NE(vec_a, vec_c);
    EXPECT_NE(vec_a, vec_d);
}

TEST(VectorC1D, isNear)
{
    VectorC1D<QtySiLength> vec_a(3.*meter);
    VectorC1D<QtySiLength> vec_b(4.*meter);
    VectorC1D<QtySiLength> vec_c(3.00000000001*meter);

    EXPECT_FALSE(vec_a.isNear(vec_b));
    EXPECT_TRUE(vec_a.isNear(vec_c));
}

/* Common vector operations tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC1D, norm)
{
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (3.*meter).norm(), 3.*meter);
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (-3.*meter).norm(), 3.*meter);
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (0.*meter).norm(), 0.*meter);
}

TEST(VectorC1D, normL1)
{
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (3*meter).normL1(), 3*meter);
}

TEST(VectorC1D, normL)
{
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (3*meter).normL(1), 3*meter);
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (3*meter).normL(2), 3*meter);
}

TEST(VectorC1D, normLInf)
{
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (2*meter).normLInf(), 2*meter);
}

TEST(VectorC1D, normSquared)
{
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (2.*meter).normSquared(), 4.*square_meter);
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (-2.*meter).normSquared(), 4.*square_meter);
    QTY_EXPECT_NEAR(VectorC1D<QtySiLength> (0.*meter).normSquared(), 0.*square_meter);
}

TEST(VectorC1D, versor) {
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength>(3. * meter).versor(), VectorC1D<QtySiLength>(1 * meter));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength>(-3. * meter).versor(), VectorC1D<QtySiLength>(-1 * meter));
}

TEST(VectorC1D, versorDl)
{
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> (  3. * meter).versorDl(), VectorC1D<QtySiDimensionless> ( 1.));
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength> ( -3. * meter).versorDl(), VectorC1D<QtySiDimensionless> (-1.));
}

TEST(VectorC1D, dot)
{
    VectorC1D<QtySiLength> vec_a(1.*meter);
    VectorC1D<QtySiLength> vec_b(-5.*meter);
    QTY_EXPECT_NEAR(vec_a.dot(vec_b), -5.*square_meter);

    VectorC1D<QtySiLength> vec_c1(1.*meter);
    VectorC1D<QtySiLength> vec_c2(0.*meter);
    VectorC1D<QtySiLength> vec_c3(-1.*meter);

    QTY_EXPECT_NEAR(vec_c1.dot(vec_c2), 0.*square_meter);
    QTY_EXPECT_NEAR(vec_c1.dot(vec_c3), -1.*square_meter);
    QTY_EXPECT_NEAR(vec_c1.dot(vec_c1), 1.*square_meter);
}

TEST(VectorC1D, dotPipe)
{
    VectorC1D<QtySiLength> vec_a(1.*meter);
    VectorC1D<QtySiLength> vec_b(-5.*meter);
    QTY_EXPECT_NEAR(vec_a.dot(vec_b), -5.*square_meter);

    VectorC1D<QtySiLength> vec_c1(1.*meter);
    VectorC1D<QtySiLength> vec_c2(0.*meter);
    VectorC1D<QtySiLength> vec_c3(-1.*meter);

    QTY_EXPECT_NEAR(vec_c1|vec_c2, 0.*square_meter);
    QTY_EXPECT_NEAR(vec_c1|vec_c3, -1.*square_meter);
    QTY_EXPECT_NEAR(vec_c1|vec_c1, 1.*square_meter);
}

TEST(VectorC1D, directSum)
{
    VectorC<2,QtySiLength> res = VectorC1D<QtySiLength>(1.*meter).directSum(VectorC1D<QtySiLength>(2.*meter));
    VectorC<2,QtySiLength> ref (1.*meter, 2.*meter);

    VEC_EXPECT_NEAR(res,ref);
}

TEST(VectorC1D, elementwiseProduct)
{
    VectorC1D<QtySiLength> v1 (2. * meter);
    VectorC1D<QtySiForce>  v2 (1. * newton);
    VectorC1D<QtySiEnergy> v3 (2. * meter * newton);

    MAT_EXPECT_NEAR(v1.elementwiseProduct(v2), v3);
}

TEST(VectorC1D, elementwiseDivision)
{
    VectorC1D<QtySiLength>   v1 (2. * meter);
    VectorC1D<QtySiTime>     v2 (1. * second);
    VectorC1D<QtySiVelocity> v3 (2. * meter / second);

    MAT_EXPECT_NEAR(v1.elementwiseDivision(v2), v3);
}


TEST(VectorC1D, isParallel)
{
    EXPECT_TRUE(VectorC1D<QtySiLength>  (1.*meter).isParallel(VectorC1D<QtySiLength> ( 2.*meter)));
    EXPECT_TRUE(VectorC1D<QtySiLength>  (1.*meter).isParallel(VectorC1D<QtySiLength> (-2.*meter)));

    EXPECT_FALSE(VectorC1D<QtySiLength>  (0.*meter).isParallel(VectorC1D<QtySiLength> ( 0.*meter)));
    EXPECT_FALSE(VectorC1D<QtySiLength>  (0.*meter).isParallel(VectorC1D<QtySiLength> ( 1.*meter)));
}

TEST(VectorC1D, isSameDirection)
{
    EXPECT_TRUE (VectorC1D<QtySiLength> (1.*meter).isSameDirection(VectorC1D<QtySiLength> ( 2.*meter)));
    EXPECT_FALSE(VectorC1D<QtySiLength> (1.*meter).isSameDirection(VectorC1D<QtySiLength> (-2.*meter)));

    EXPECT_FALSE(VectorC1D<QtySiLength> (0.*meter).isSameDirection(VectorC1D<QtySiLength> ( 0.*meter)));
    EXPECT_FALSE(VectorC1D<QtySiLength> (0.*meter).isSameDirection(VectorC1D<QtySiLength> ( 1.*meter)));
    EXPECT_FALSE(VectorC1D<QtySiLength> (1.*meter).isSameDirection(VectorC1D<QtySiLength> ( 0.*meter)));
}

TEST(VectorC1D, isOppositeDirection)
{
    EXPECT_FALSE (VectorC1D<QtySiLength> (1.*meter).isOppositeDirection(VectorC1D<QtySiLength> ( 2.*meter)));
    EXPECT_TRUE  (VectorC1D<QtySiLength> (1.*meter).isOppositeDirection(VectorC1D<QtySiLength> (-2.*meter)));

    EXPECT_FALSE(VectorC1D<QtySiLength> (0.*meter).isOppositeDirection(VectorC1D<QtySiLength> ( 0.*meter)));
    EXPECT_FALSE(VectorC1D<QtySiLength> (0.*meter).isOppositeDirection(VectorC1D<QtySiLength> ( 1.*meter)));
    EXPECT_FALSE(VectorC1D<QtySiLength> (1.*meter).isOppositeDirection(VectorC1D<QtySiLength> ( 0.*meter)));
}

TEST(VectorC1D, isPerpendicular)
{
    EXPECT_FALSE (VectorC1D<QtySiLength> (1.*meter).isPerpendicular(VectorC1D<QtySiLength> ( 2.*meter)));
    EXPECT_FALSE  (VectorC1D<QtySiLength> (1.*meter).isPerpendicular(VectorC1D<QtySiLength> (-2.*meter)));

    EXPECT_FALSE(VectorC1D<QtySiLength> (0.*meter).isPerpendicular(VectorC1D<QtySiLength> ( 0.*meter)));
    EXPECT_FALSE(VectorC1D<QtySiLength> (0.*meter).isPerpendicular(VectorC1D<QtySiLength> ( 1.*meter)));
    EXPECT_FALSE(VectorC1D<QtySiLength> (1.*meter).isPerpendicular(VectorC1D<QtySiLength> ( 0.*meter)));
}


/* Angle and angle related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC1D, angle)
{
    QtyAngleDegree result1 (VectorC1D<QtySiLength> (1.*meter).angle(VectorC1D<QtySiLength> (-2.*meter)));
    QTY_EXPECT_NEAR(result1, 180. * degrees);

    QtyAngleDegree result2 (VectorC1D<QtySiLength> (1.*meter).angle(VectorC1D<QtySiLength> (2.*meter)));
    QTY_EXPECT_NEAR(result2, 0. * degrees);
}

/* Validity check operations */
// ----------------------------------------------------------------------------------------------------------------
TEST(VectorC1D, isNull)
{
    EXPECT_TRUE(VectorC1D<QtySiLength> (0.*meter).isNull());
    EXPECT_FALSE(VectorC1D<QtySiLength> (1.*meter).isNull());
    EXPECT_FALSE(VectorC1D<QtySiLength> (-1.*meter).isNull());
}

TEST(VectorC1D, isFinite)
{
    EXPECT_TRUE(VectorC1D<QtySiLength> (1.*meter).isFinite());
    EXPECT_FALSE(VectorC1D<QtySiLength> (inf_val*meter).isFinite());
}

TEST(VectorC1D, isNormal)
{
    EXPECT_TRUE(VectorC1D<QtySiLength> (1.*meter).isNormal());
    EXPECT_FALSE(VectorC1D<QtySiLength> (nan_val*meter).isNormal());
}

TEST(VectorC1D, isNan)
{
    EXPECT_FALSE(VectorC1D<QtySiLength> (1.*meter).isNan());
    EXPECT_TRUE(VectorC1D<QtySiLength> (nan_val*meter).isNan());
}

TEST(VectorC1D, isInf)
{
    EXPECT_FALSE(VectorC1D<QtySiLength>(1. * meter).isInfinite());
    EXPECT_TRUE(VectorC1D<QtySiLength>(inf_val * meter).isInfinite());
}

TEST(VectorC1D, elementAccess)
{
    EXPECT_EQ(VectorC1D<QtySiLength> (1.*meter)[0], 1*meter); // 0-based

    EXPECT_EQ(VectorC1D<QtySiLength> (1.*meter)(1), 1*meter); // 1-based

#ifndef GCC
    EXPECT_EQ(VectorC1D<QtySiLength> (1.*meter).x, 1*meter); // name-based
#endif
}

TEST(VectorC1D, symbol)
{
    EXPECT_EQ(VectorC1D<QtySiLength> (1.*meter).symbol(), "ℝ¹");
}

TEST(VectorC1D, cout)
{
    std::stringstream ss1, ss2;
    ss1 << VectorC1D<QtySiLength> (  1.*meter);
    ss2 << VectorC1D<QtySiLength> ( -1.*meter);

    EXPECT_EQ(ss1.str(), "(1 m)");
    EXPECT_EQ(ss2.str(), "(-1 m)");
}

TEST(VectorC1D, kUnit)
{
    VEC_EXPECT_NEAR(VectorC1D<QtySiLength>::kX(), VectorC1D<QtySiLength>(1.*meter));
}


TEST(VectorC1D, repr)
{
    VectorC1D<QtySiLength> v1(  1.*meter);
    VectorC1D<QtySiLength> v2( -1.*meter);

    EXPECT_EQ(v1.repr(false), "(1 m)");
    EXPECT_EQ(v2.repr(false), "(-1 m)");

    EXPECT_EQ(v1.repr(true), "ℝ¹(1 m)");
    EXPECT_EQ(v2.repr(true), "ℝ¹(-1 m)");
}
