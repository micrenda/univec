#include <iostream>

#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <limits>
#include <cmath>
#include <gtest/gtest.h>

#include "univec/Real.hpp"
#include "univec/VectorC0D.hpp"
#include "univec/VectorC1D.hpp"
#include "univec/VectorC2D.hpp"
#include "univec/Matrix1.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace boost::units::degree;
using namespace dfpe;
using namespace si;

TEST(Real, conversion)
{
    QtySiLength v1 = Real<QtySiLength> (1.*meter);
    EXPECT_EQ(v1, 1.*meter);

    Real<QtySiLength> v2 = 1.*meter;
    EXPECT_EQ(v2, Real<QtySiLength>(1.*meter));
}

TEST(Real, conj)
{
    EXPECT_EQ(Real<QtySiLength> (1.*meter).conj(), Real<QtySiLength> (1.*meter));
    EXPECT_NE(Real<QtySiLength> (-1.*meter).conj(), Real<QtySiLength> (1.*meter));
    EXPECT_EQ(Real<QtySiLength> (1.*meter).conj(), Real<QtySiLength> (1.*meter));
}

TEST(Real, vector)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (1.*meter).vector(),   VectorC0D<QtySiLength> ());
    VEC_EXPECT_NEAR(Real<QtySiLength> (0.*meter).vector(),   VectorC0D<QtySiLength> ());
    VEC_EXPECT_NEAR(Real<QtySiLength> (-4.*meter).vector(),  VectorC0D<QtySiLength> ());
}

TEST(Real, scalar)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (1.*meter).scalar(),  Real<QtySiLength> (1.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (0.*meter).scalar(),  Real<QtySiLength> (0.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (-4.*meter).scalar(), Real<QtySiLength> (-4.*meter));
}

TEST(Real, angle)
{
    // Angle is defined only for quaternions (see angle/axis quaternion representation
}

TEST(Real, axis)
{
    // Axis is defined only for quaternions (see angle/axis quaternion representation
}

TEST(Real, inv)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (1.*meter).inv(), Real<QtySiWavenumber> (1.*reciprocal_meter));
}

TEST(Real, isPure)
{
    EXPECT_FALSE(Real<QtySiLength> (-4.*meter).isPure());
    EXPECT_TRUE (Real<QtySiLength> (-0.*meter).isPure());
}

TEST(Real, isScalar)
{
    EXPECT_TRUE(Real<QtySiLength> (-4.*meter).isScalar());
    EXPECT_TRUE(Real<QtySiLength> (-0.*meter).isScalar());
}

TEST(Real, matrix)
{
    // Disabled for now
    //VEC_EXPECT_NEAR(Real<QtySiLength> (2.*meter).matrix(), RMatrix1<QtySiLength> ( 1.*meter ));
}


TEST(Real, elementAccess)
{
    EXPECT_EQ(Real<QtySiLength> (1.*meter)[0], 1*meter); // 0-based
    EXPECT_EQ(Real<QtySiLength> (1.*meter)(1), 1*meter); // 1-based
}

TEST(Real, symbol)
{
    EXPECT_EQ(Real<QtySiLength> (1.*meter).symbol(), "ℝ");
}

TEST(Real, cout)
{
    std::stringstream ss1, ss2;
    ss1 << Real<QtySiLength> (  1.*meter);
    ss2 << Real<QtySiLength> ( -1.*meter);

    EXPECT_EQ(ss1.str(), "1 m");
    EXPECT_EQ(ss2.str(), "- 1 m");
}

TEST(Real, repr)
{
    Real<QtySiLength> v1(  1.*meter);
    Real<QtySiLength> v2( -1.*meter);

    EXPECT_EQ(v1.repr(false), "(1 m)");
    EXPECT_EQ(v2.repr(false), "(- 1 m)");

    EXPECT_EQ(v1.repr(true), "ℝ(1 m)");
    EXPECT_EQ(v2.repr(true), "ℝ(- 1 m)");
}


/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Real, addition)
{
    Real<QtySiLength> vec_a(3.*meter);
    Real<QtySiLength> vec_b(2.*meter);

    VEC_EXPECT_NEAR(vec_a + vec_b, Real<QtySiLength> (5.*meter));
}

TEST(Real, additionEqual)
{
    Real<QtySiLength> vec_a(3.*meter);
    Real<QtySiLength> vec_b(2.*meter);
    vec_a += vec_b;

    VEC_EXPECT_NEAR(vec_a, Real<QtySiLength> (5.*meter));
}

/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Real, subtraction)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (3.*meter) - Real<QtySiLength> (2.*meter), Real<QtySiLength> (1.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (2.*meter) - Real<QtySiLength> (3.*meter), Real<QtySiLength> (-1.*meter));
}

TEST(Real, subtractionEqual)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (3.*meter) -= Real<QtySiLength> (2.*meter), Real<QtySiLength> (1.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (2.*meter) -= Real<QtySiLength> (3.*meter), Real<QtySiLength> (-1.*meter));
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Real, scalarMutiplication)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (3.*meter) * 2, Real<QtySiLength> (6.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (-2.*meter) * 0.5, Real<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (2.*meter) * 2, Real<QtySiLength> (4.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (0.*meter) * 2, Real<QtySiLength> (0.*meter));

    VEC_EXPECT_NEAR(2 * Real<QtySiLength> (3.*meter), Real<QtySiLength> (6.*meter));
    VEC_EXPECT_NEAR(0.5 * Real<QtySiLength> (-2.*meter), Real<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(2 * Real<QtySiLength> (2.*meter), Real<QtySiLength> (4.*meter));
    VEC_EXPECT_NEAR(2 * Real<QtySiLength> (0.*meter), Real<QtySiLength> (0.*meter));
}

TEST(Real, scalarMultiplicationEqual)
{
    // Multiplication with a scalar
    VEC_EXPECT_NEAR(Real<QtySiLength> (3.*meter) *= 2, Real<QtySiLength> (6.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (-2.*meter) *= 0.5, Real<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (2.*meter) *= 2, Real<QtySiLength> (4.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (0.*meter) *= 2, Real<QtySiLength> (0.*meter));
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Real, scalarDivision)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (3.*meter) / 0.5, Real<QtySiLength> ( 6.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (-2.*meter) / 2, Real<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (6.*meter) / 3., Real<QtySiLength> (2.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (0.*meter) / -4., Real<QtySiLength> (0.*meter));
}

TEST(Real, scalarDivisionEqual)
{
    // Division with a scalar
    VEC_EXPECT_NEAR(Real<QtySiLength> (3.*meter) /= 0.5, Real<QtySiLength> ( 6. * meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (-2.*meter) /= 2., Real<QtySiLength> (-1. * meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (6.*meter) /= 3., Real<QtySiLength> (2. * meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (0.*meter) /= -4., Real<QtySiLength> (0. * meter));
}

TEST(Real, mutiplication)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (3.*meter)   * Real<>(2), Real<QtySiLength> (6.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (-2.*meter) * Real<>(0.5), Real<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (2.*meter)   * Real<>(0), Real<QtySiLength> (0.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (0.*meter)  * Real<>(), Real<QtySiLength> ());
}

TEST(Real, multiplicationEqual)
{
    VEC_EXPECT_NEAR(Real<QtySiLength> (3.*meter)   *= Real<>(2), Real<QtySiLength> (6.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (-2.*meter) *= Real<>(0.5), Real<QtySiLength> (-1.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (2.*meter)   *= Real<>(0), Real<QtySiLength> (0.*meter));
    VEC_EXPECT_NEAR(Real<QtySiLength> (0.*meter)  *= Real<>(), Real<QtySiLength> ());
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Real, division)
{
    VEC_EXPECT_NEAR(Real<QtySiLength>(2. * meter) / Real<>(2), Real<QtySiLength>(1. * meter));
    VEC_EXPECT_NEAR(Real<QtySiLength>(-1. * meter) / Real<>(0.5), Real<QtySiLength>(-2. * meter));
    EXPECT_TRUE((Real<QtySiLength>(1. * meter) / Real<>()).isInfinite());
    EXPECT_TRUE((Real<QtySiLength>() / Real<>()).isNan());
}

TEST(Real, divisionEqual)
{
    VEC_EXPECT_NEAR(Real<QtySiLength>(2. * meter) /= Real<>(2), Real<QtySiLength>(1. * meter));
    VEC_EXPECT_NEAR(Real<QtySiLength>(-1. * meter) /= Real<>(0.5), Real<QtySiLength>(-2. * meter));
    EXPECT_TRUE((Real<QtySiLength>(1. * meter) /= Real<>()).isInfinite());
    EXPECT_TRUE((Real<QtySiLength>() /= Real<>()).isNan());
}

// exp, log, pow, cos, sin, tan, cosh, sinh, tanh