#include <iostream>

#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <cmath>
#include <gtest/gtest.h>

#include "univec/VectorC7D.hpp"
#include "univec/Octonion.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace boost::units::degree;
using namespace dfpe;
using namespace si;


TEST(Octonion, conjugate)
{
    Octonion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter, 5*meter, 6 * meter, 7 * meter, 8 * meter);
    Octonion<QtySiLength> result(1*meter, -2 * meter, -3 * meter, -4 * meter, -5*meter, -6 * meter, -7 * meter, -8 * meter);

    EXPECT_EQ(q.conj(), result);
    EXPECT_EQ(q.conj().conj(), q);
}

TEST(Octonion, pure)
{
    Octonion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter, 5*meter, 6 * meter, 7 * meter, 8 * meter);
    Octonion<QtySiLength> result(0*meter, 2 * meter, 3 * meter, 4 * meter, 5*meter, 6 * meter, 7 * meter, 8 * meter);

    EXPECT_EQ(q.pure(), result);
}

TEST(Octonion, scalar)
{
    Octonion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter, 5*meter, 6 * meter, 7 * meter, 8 * meter);
    Octonion<QtySiLength> result(1*meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter);

    EXPECT_EQ(q.scalar(), result);
}

TEST(Octonion, vector)
{
    Octonion<QtySiLength> q(1*meter, 2 * meter, 3 * meter, 4 * meter, 5*meter, 6 * meter, 7 * meter, 8 * meter);
    VectorC7D<QtySiLength> result(2 * meter, 3 * meter, 4 * meter, 5*meter, 6 * meter, 7 * meter, 8 * meter);

    VEC_EXPECT_NEAR(q.vector(), result);
}


TEST(Octonion, inverse)
{
    Octonion<QtySiLength> q(1 * meter, 2 * meter, 3 * meter, 4 * meter, 5 * meter, 6 * meter, 7 * meter, 8 * meter);
    Octonion<QtySiWavenumber> result(
             1./204.  * reciprocal_meter,
            -1./102.  * reciprocal_meter,
            -1./68.   * reciprocal_meter,
            -1./51.   * reciprocal_meter,
             -5/204.  * reciprocal_meter,
             -1./34.  * reciprocal_meter,
             -7./204. * reciprocal_meter,
             -2./51. * reciprocal_meter);

    EXPECT_EQ(q.inv(), result);
}

TEST(Octonion, isPure)
{
    Octonion<QtySiLength> q1(0*meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter);
    Octonion<QtySiLength> q2(1*meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter);
    Octonion<QtySiLength> q3(0*meter, 2 * meter, 3 * meter, 4 * meter, 5 * meter, 6 * meter, 7 * meter, 8 * meter);
    Octonion<QtySiLength> q4(1*meter, 2 * meter, 3 * meter, 4 * meter, 5 * meter, 6 * meter, 7 * meter, 8 * meter);

    EXPECT_TRUE(q1.isPure());
    EXPECT_FALSE(q2.isPure());
    EXPECT_TRUE(q3.isPure());
    EXPECT_FALSE(q4.isPure());
}

TEST(Octonion, isScalar)
{
    Octonion<QtySiLength> q1(0*meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter);
    Octonion<QtySiLength> q2(1*meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter, 0 * meter);
    Octonion<QtySiLength> q3(0*meter, 2 * meter, 3 * meter, 4 * meter, 5 * meter, 6 * meter, 7 * meter, 8 * meter);
    Octonion<QtySiLength> q4(1*meter, 2 * meter, 3 * meter, 4 * meter, 5 * meter, 6 * meter, 7 * meter, 8 * meter);

    EXPECT_TRUE(q1.isScalar());
    EXPECT_TRUE(q2.isScalar());
    EXPECT_FALSE(q3.isScalar());
    EXPECT_FALSE(q4.isScalar());
}
/*
 * TODO
TEST(Octonion, matrix)
{
    Octonion<QtySiLength> q1(1.*meter, 2.*meter, 3.*meter, 4.*meter);
    Matrix<4,4,QtySiLength> result(
        1.*meter, -2.*meter, -3.*meter, -4.*meter,
        2.*meter,  1.*meter, -4.*meter,  3.*meter,
        3.*meter,  4.*meter,  1.*meter, -2.*meter,
        4.*meter, -3.*meter,  2.*meter,  1.*meter
    );

    VEC_EXPECT_NEAR(q1.matrix(), result);
}*/

TEST(Octonion, multiplication)
{
    Octonion<QtySiLength> q1(1.*meter,  1.  * meter, 2. * meter,  3. * meter, 1.  * meter, -2.5 * meter, 1.2 * meter,  3.  * meter);
    Octonion<QtySiLength> q2(0.*meter, -0.5 * meter, 2. * meter, -1. * meter, -1.5 * meter, 3.2 * meter, 0.7 * meter, -0.5 * meter);

    Octonion<QtySiArea> result1(9.66 * square_meter, -6.35 * square_meter, -4.35  * square_meter, 11.59 * square_meter, -3.95 * square_meter, 12.5 * square_meter, -10.4 * square_meter, 6.1 * square_meter);
    Octonion<QtySiArea> result2(9.66 *square_meter, 5.35   * square_meter, 8.35 * square_meter, -13.59 * square_meter,  0.95  * square_meter, -6.1 * square_meter,  11.8 * square_meter,  -7.1 * square_meter);


    VEC_EXPECT_NEAR(q1 * q2, result1);
    VEC_EXPECT_NEAR(q2 * q1, result2);
}

TEST(Octonion, division)
{
    Octonion<QtySiLength> q1(1. * meter, 2. * meter, 3. * meter, 4. * meter,  5. * meter,  6. * meter,  7. * meter,   8. * meter);
    Octonion<QtySiLength> q2(2. * meter, 4. * meter, 6. * meter, 8. * meter, 10. * meter, 12. * meter, 14. * meter,  16. * meter);

    Octonion<QtySiDimensionless> result1(0.5, 0., 0., 0., 0., 0., 0., 0.);
    Octonion<QtySiDimensionless> result2(2.,  0., 0., 0., 0., 0., 0., 0.);

    VEC_EXPECT_NEAR(q1 / q2, result1);
    VEC_EXPECT_NEAR(q2 / q1, result2);
}


TEST(Octonion, elementAccess)
{
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)[0], 1*meter); // 0-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)[1], 2*meter); // 0-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)[2], 3*meter); // 0-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)[3], 4*meter); // 0-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)[4], 5*meter); // 0-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)[5], 6*meter); // 0-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)[6], 7*meter); // 0-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)[7], 8*meter); // 0-based


    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)(1), 1*meter); // 1-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)(2), 2*meter); // 1-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)(3), 3*meter); // 1-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)(4), 4*meter); // 1-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)(5), 5*meter); // 1-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)(6), 6*meter); // 1-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)(7), 7*meter); // 1-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter)(8), 8*meter); // 1-based

#ifndef GCC
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).a, 1*meter); // name-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).b, 2*meter); // name-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).c, 3*meter); // name-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).d, 4*meter); // name-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).e, 5*meter); // name-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).f, 6*meter); // name-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).g, 7*meter); // name-based
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).h, 8*meter); // name-based
#endif
}

TEST(Octonion, symbol)
{
    EXPECT_EQ(Octonion<QtySiLength> (1.*meter, 2.*meter, 3.*meter, 4.*meter, 5.*meter, 6.*meter, 7.*meter, 8.*meter).symbol(), "𝕆");
}

TEST(Octonion, cout)
{
    std::stringstream ss1, ss2;
    ss1 << Octonion<QtySiLength> ( 1.*meter,  2.*meter,  3.*meter,  4.*meter, -5.*meter, -6.*meter, -7.*meter, -8.*meter);
    ss2 << Octonion<QtySiLength> (-1.*meter, -2.*meter, -3.*meter, -4.*meter,  5.*meter,  6.*meter,  7.*meter,  8.*meter);

    EXPECT_EQ(ss1.str(), "1 m e₀ + 2 m e₁ + 3 m e₂ + 4 m e₃ - 5 m e₄ - 6 m e₅ - 7 m e₆ - 8 m e₇");
    EXPECT_EQ(ss2.str(), "- 1 m e₀ - 2 m e₁ - 3 m e₂ - 4 m e₃ + 5 m e₄ + 6 m e₅ + 7 m e₆ + 8 m e₇");
}

TEST(Octonion, repr)
{
    Octonion<QtySiLength> v1( 1.*meter,  2.*meter,  3.*meter,  4.*meter, -5.*meter, -6.*meter, -7.*meter, -8.*meter);
    Octonion<QtySiLength> v2(-1.*meter, -2.*meter, -3.*meter, -4.*meter,  5.*meter,  6.*meter,  7.*meter,  8.*meter);

    EXPECT_EQ(v1.repr(false), "(1 m e₀ + 2 m e₁ + 3 m e₂ + 4 m e₃ - 5 m e₄ - 6 m e₅ - 7 m e₆ - 8 m e₇)");
    EXPECT_EQ(v2.repr(false), "(- 1 m e₀ - 2 m e₁ - 3 m e₂ - 4 m e₃ + 5 m e₄ + 6 m e₅ + 7 m e₆ + 8 m e₇)");

    EXPECT_EQ(v1.repr(true), "𝕆(1 m e₀ + 2 m e₁ + 3 m e₂ + 4 m e₃ - 5 m e₄ - 6 m e₅ - 7 m e₆ - 8 m e₇)");
    EXPECT_EQ(v2.repr(true), "𝕆(- 1 m e₀ - 2 m e₁ - 3 m e₂ - 4 m e₃ + 5 m e₄ + 6 m e₅ + 7 m e₆ + 8 m e₇)");
}
