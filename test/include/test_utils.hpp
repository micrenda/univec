
#pragma once

#define QTY_EXPECT_NEAR(a, b) { \
    EXPECT_NEAR( ( a ).value(), ( b ).value(), 1e-10 ); \
}

#define VEC_EXPECT_NEAR(v1, v2) { \
    EXPECT_TRUE((v1).isNear((v2))); \
}

#define MAT_EXPECT_NEAR(m1, m2) { \
    EXPECT_TRUE((m1).isNear((m2))); \
}


extern double inf_val;
extern double nan_val;