#include <iostream>
#include <boost/units/quantity.hpp>

#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <gtest/gtest.h>

#include "qtydef/QtyDefinitions.hpp"
#include "univec/VectorC1D.hpp"
#include "univec/VectorC2D.hpp"
#include "univec/VectorC3D.hpp"
#include "univec/frame/TranslateFrame.hpp"
#include "univec/frame/TranslateFrame1D.hpp"
#include "univec/frame/TranslateFrame2D.hpp"
#include "univec/frame/TranslateFrame3D.hpp"
#include "univec/frame/ScaleFrame.hpp"
#include "univec/frame/ScaleFrame1D.hpp"
#include "univec/frame/ScaleFrame2D.hpp"
#include "univec/frame/ScaleFrame3D.hpp"
#include "univec/frame/RotateFrame2D.hpp"
#include "univec/frame/RotateFrame3D.hpp"
#include "univec/frame/CompositeFrame.hpp"
#include "test_utils.hpp"



using namespace std;
using namespace boost::units;
using namespace boost::units::si;
using namespace boost::units::degree;
using namespace testing;
using namespace dfpe;

TEST(TranslateFrame, translateNull)
{
   TranslateFrame<3,QtySiLength> frame; // Null translation

   VectorC3D<QtySiLength> v1 (1. * meter, 2. * meter, 3. * meter);

   VEC_EXPECT_NEAR(frame.forward(v1),  v1);
   VEC_EXPECT_NEAR(frame.backward(v1), v1);
}


TEST(TranslateFrame, translate1D)
{
   VectorC1D<QtySiLength> v1 (1. * meter);
   VectorC1D<QtySiLength> v2 (4. * meter);
   VectorC1D<QtySiLength> v3 (3. * meter);

   TranslateFrame<1,QtySiLength> frame(v1);

   VEC_EXPECT_NEAR(frame.forward(v2),  v3);
   VEC_EXPECT_NEAR(frame.backward(v3), v2);
}

TEST(TranslateFrame, translate2D)
{
   VectorC2D<QtySiLength> v1 (1. * meter,  2. * meter);
   VectorC2D<QtySiLength> v2 (4. * meter,  5. * meter);
   VectorC2D<QtySiLength> v3 (3. * meter,  3. * meter);

   TranslateFrame2D<QtySiLength> frame(v1);

   VEC_EXPECT_NEAR(frame.forward(v2),  v3);
   VEC_EXPECT_NEAR(frame.backward(v3), v2);
}

TEST(TranslateFrame, translate3D)
{
   VectorC3D<QtySiLength> v1 (1. * meter, 2. * meter, 3. * meter);
   VectorC3D<QtySiLength> v2 (4. * meter, 5. * meter, 6. * meter);
   VectorC3D<QtySiLength> v3 (3. * meter, 3. * meter, 3. * meter);

   TranslateFrame<3,QtySiLength> frame(v1);

   VEC_EXPECT_NEAR(frame.forward(v2),  v3);
   VEC_EXPECT_NEAR(frame.backward(v3), v2);
}

TEST(TranslateFrame, translate3DRotOnly)
{
   VectorC3D<QtySiLength> v1 (1. * meter, 2. * meter, 3. * meter);

   TranslateFrame3D<QtySiLength> frame(v1);

   VEC_EXPECT_NEAR(frame.forwardRotOnly(v1),  v1);
   VEC_EXPECT_NEAR(frame.backwardRotOnly(v1), v1);
}


TEST(ScaleFrame, scaleUnit)
{
    ScaleFrame3D<QtySiLength> frame; // Unit scaling

    VectorC3D<QtySiLength> v1 (1. * meter, 2. * meter, 3. * meter);

    VEC_EXPECT_NEAR(frame.forward(v1),  v1);
    VEC_EXPECT_NEAR(frame.backward(v1), v1);
}


TEST(ScaleFrame, scale1D)
{
    VectorC1D<> v1 (2.);
    VectorC1D<QtySiLength> v2 (20. * meter);
    VectorC1D<QtySiLength> v3 (10 * meter);

    ScaleFrame1D<QtySiLength> frame(v1);

    VEC_EXPECT_NEAR(frame.forward(v2),  v3);
    VEC_EXPECT_NEAR(frame.backward(v3), v2);
}

TEST(ScaleFrame, scale2D)
{
    VectorC2D<>            v1 (2.,  4.);
    VectorC2D<QtySiLength> v2 (8. * meter,  20. * meter);
    VectorC2D<QtySiLength> v3 (4. * meter,   5. * meter);

    ScaleFrame2D<QtySiLength> frame(v1);

    VEC_EXPECT_NEAR(frame.forward(v2),  v3);
    VEC_EXPECT_NEAR(frame.backward(v3), v2);
}

TEST(ScaleFrame, scale3D)
{
    VectorC3D<> v1 (2.,  4.,  6.);
    VectorC3D<QtySiLength> v2 (8. * meter, 20. * meter, 36. * meter);
    VectorC3D<QtySiLength> v3 (4. * meter,  5. * meter,  6. * meter);

    ScaleFrame3D<QtySiLength> frame(v1);

    VEC_EXPECT_NEAR(frame.forward(v2),  v3);
    VEC_EXPECT_NEAR(frame.backward(v3), v2);
}

TEST(ScaleFrame, scale3DRotOnly)
{
    VectorC3D<> v1 (2.,  4.,  6.);

    ScaleFrame3D<QtySiLength> frame(v1);

    VEC_EXPECT_NEAR(frame.forwardRotOnly(v1),  v1);
    VEC_EXPECT_NEAR(frame.backwardRotOnly(v1), v1);
}

TEST(RotateFrame2D, rotateNull)
{
    RotateFrame2D<QtySiLength> frame; // Null rotation

    VectorC2D<QtySiLength> v1 (1. * meter, 2. * meter);

    VEC_EXPECT_NEAR(frame.forward(v1),  v1);
    VEC_EXPECT_NEAR(frame.backward(v1), v1);
}

TEST(RotateFrame2D, rotate)
{
    VectorC2D<QtySiLength> v1 (1. * meter,  1. * meter);
    VectorC2D<QtySiLength> v2 (1. * meter, -1. * meter);
    VectorC2D<QtySiLength> v3 (sqrt(2) * meter, 0. * meter);

    RotateFrame2D<QtySiLength> frame1(QtySiPlaneAngle(90. * degrees));
    RotateFrame2D<QtySiLength> frame2(QtySiPlaneAngle(45. * degrees));

    VEC_EXPECT_NEAR(frame1.forward(v1),  v2);
    VEC_EXPECT_NEAR(frame1.backward(v2), v1);

    VEC_EXPECT_NEAR(frame2.forward(v1),  v3);
    VEC_EXPECT_NEAR(frame2.backward(v3), v1);
}

TEST(RotateFrame2D, rotateRotOnly)
{
    VectorC2D<QtySiLength> v1 (1. * meter,  1. * meter);
    VectorC2D<QtySiLength> v2 (1. * meter, -1. * meter);
    VectorC2D<QtySiLength> v3 (sqrt(2) * meter, 0. * meter);

    RotateFrame2D<QtySiLength> frame1(QtySiPlaneAngle(90. * degrees));
    RotateFrame2D<QtySiLength> frame2(QtySiPlaneAngle(45. * degrees));

    VEC_EXPECT_NEAR(frame1.forwardRotOnly(v1),  v2);
    VEC_EXPECT_NEAR(frame1.backwardRotOnly(v2), v1);

    VEC_EXPECT_NEAR(frame2.forwardRotOnly(v1),  v3);
    VEC_EXPECT_NEAR(frame2.backwardRotOnly(v3), v1);
}


TEST(RotateFrame3D, rotateNull)
{
    RotateFrame3D<QtySiLength> frame; // Null rotation

    VectorC3D<QtySiLength> v1 (1. * meter, 2. * meter, 3. * meter);

    VEC_EXPECT_NEAR(frame.forward(v1),  v1);
    VEC_EXPECT_NEAR(frame.backward(v1), v1);
}

TEST(RotateFrame3D, rotateX)
{
    EulerRotation3D<QtySiLength> rotation(
            45. * degrees, 0. * degrees, 0. * degrees,
            RotationMode::INTRINSIC,
            RotationAxis::X, RotationAxis::Y, RotationAxis::Z);

    VectorC3D<QtySiLength> v1 (0. * meter,      1. * meter, 1. * meter);
    VectorC3D<QtySiLength> v2 (0. * meter, sqrt(2) * meter, 0. * meter);

    RotateFrame3D<QtySiLength> frame(rotation);

    VEC_EXPECT_NEAR(frame.forward(v1),  v2);
    VEC_EXPECT_NEAR(frame.backward(v2), v1);
}

TEST(RotateFrame3D, rotateY)
{
    EulerRotation3D<QtySiLength> rotation(
            0. * degrees, 45. * degrees, 0. * degrees,
            RotationMode::INTRINSIC,
            RotationAxis::X, RotationAxis::Y, RotationAxis::Z);

    VectorC3D<QtySiLength> v1 (1. * meter, 0. * meter, 1. * meter);
    VectorC3D<QtySiLength> v2 (0. * meter, 0. * meter, sqrt(2) * meter);

    RotateFrame3D<QtySiLength> frame(rotation);

    VEC_EXPECT_NEAR(frame.forward(v1),  v2);
    VEC_EXPECT_NEAR(frame.backward(v2), v1);
}

TEST(RotateFrame3D, rotateZ)
{
    EulerRotation3D<QtySiLength> rotation(
            0. * degrees, 0. * degrees, 45. * degrees,
            RotationMode::INTRINSIC,
            RotationAxis::X, RotationAxis::Y, RotationAxis::Z);

    VectorC3D<QtySiLength> v1 (1. * meter, 1. * meter, 0. * meter);
    VectorC3D<QtySiLength> v2 (sqrt(2) * meter, 0. * meter, 0. * meter);

    RotateFrame3D<QtySiLength> frame(rotation);

    VEC_EXPECT_NEAR(frame.forward(v1),  v2);
    VEC_EXPECT_NEAR(frame.backward(v2), v1);
}


TEST(RotateFrame3D, rotateRotOnly)
{
    EulerRotation3D<QtySiLength> rotation(
            0. * degrees, 0. * degrees, 45. * degrees,
            RotationMode::INTRINSIC,
            RotationAxis::X, RotationAxis::Y, RotationAxis::Z);

    VectorC3D<QtySiLength> v1 (1. * meter, 1. * meter, 0. * meter);
    VectorC3D<QtySiLength> v2 (sqrt(2) * meter, 0. * meter, 0. * meter);

    RotateFrame3D<QtySiLength> frame(rotation);

    VEC_EXPECT_NEAR(frame.forwardRotOnly(v1),  v2);
    VEC_EXPECT_NEAR(frame.backwardRotOnly(v2), v1);
}


TEST(CompositeFrame, multiple)
{
    VectorC3D<QtySiLength> v1; // (0,0,0) m
    VectorC3D<QtySiLength> v2(0. * meter, -sqrt(2) * meter, sqrt(2) * meter);

    VectorC3D<QtySiLength> translation(0. * meter, 1. * meter, 0. * meter);

    EulerRotation3D<QtySiLength> rotation(
            45. * degrees, 0. * degrees, 0. * degrees,
            RotationMode::INTRINSIC,
            RotationAxis::X, RotationAxis::Y, RotationAxis::Z);

    QtySiDimensionless scale(0.5);

    auto frame = TranslateFrame3D<QtySiLength>(translation)
            >> RotateFrame3D<QtySiLength>(rotation)
            >> ScaleFrame3D<QtySiLength>(scale);

    VEC_EXPECT_NEAR(frame.forward(v1),  v2);
    VEC_EXPECT_NEAR(frame.backward(v2), v1);
}

