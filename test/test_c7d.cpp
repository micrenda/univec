#include <boost/units/quantity.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <boost/units/systems/si/length.hpp>
#include <gtest/gtest.h>

#include "univec/VectorC7D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace boost::units::degree;
using namespace dfpe;
using namespace si;

TEST(VectorC7D, dot)
{
    QTY_EXPECT_NEAR(VectorC7D<QtySiLength> (1.*meter, 2.*meter, 3*meter, 4*meter, 5*meter, 6*meter, 7*meter).dot(
                    VectorC7D<QtySiLength> (7.*meter, 6.*meter, 5*meter, 4*meter, 3*meter, 2*meter, 1*meter)), 84.*square_meter);

    QTY_EXPECT_NEAR(VectorC7D<QtySiLength> ( 1.*meter, -2.*meter,  3*meter, -4*meter,  5*meter, -6*meter,  7*meter).dot(
                    VectorC7D<QtySiLength> (-7.*meter,  6.*meter, -5*meter,  4*meter, -3*meter,  2*meter, -1*meter)), -84.*square_meter);
}


TEST(VectorC7D, cross)
{
    VEC_EXPECT_NEAR(
            VectorC7D<QtySiLength> ( 1.*meter,   2.*meter,   3*meter,   4*meter,    5*meter,   6*meter,    7*meter).cross(
            VectorC7D<QtySiLength> ( 7.*meter,   6.*meter,   5*meter,   4*meter,    3*meter,    2*meter,   1*meter)),
            VectorC7D<QtySiArea>   (-8.*square_meter, -16.*square_meter, -24.*square_meter, 96.*square_meter,  -8.*square_meter, -48.*square_meter,  8.*square_meter));

    VEC_EXPECT_NEAR(
            VectorC7D<QtySiLength> (  1.*meter,   -2.*meter,    3*meter,   -4*meter,     5*meter,   -6*meter,    7*meter).cross(
            VectorC7D<QtySiLength> ( -7.*meter,    6.*meter,   -5*meter,    4*meter,    -3*meter,    2*meter,   -1*meter)),
            VectorC7D<QtySiArea>   (-8.*square_meter, 16.*square_meter, -24.*square_meter, -96.*square_meter,  -8.*square_meter,  48.*square_meter,  8.*square_meter));

    VEC_EXPECT_NEAR(
            VectorC7D<QtySiLength> (  1.*meter,    2.*meter,    3*meter,    4*meter,     5*meter,    6*meter,    7*meter).cross(
            VectorC7D<QtySiLength> (  1.*meter,    2.*meter,    3*meter,    4*meter,     5*meter,    6*meter,    7*meter)),
            VectorC7D<QtySiArea>   ());
}
