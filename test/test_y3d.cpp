#include <iostream>
#include <boost/units/quantity.hpp>

#include <boost/units/systems/si/length.hpp>
#include <boost/units/systems/angle/degrees.hpp>
#include <gtest/gtest.h>

#include "qtydef/QtyDefinitions.hpp"
#include "univec/VectorY3D.hpp"
#include "test_utils.hpp"



using namespace std;
using namespace boost::units;
using namespace boost::units::si;
using namespace boost::units::degree;
using namespace testing;
using namespace dfpe;

TEST(VectorY3D, norm)
{
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> (-3.  * meter,  4. * degrees,  3. * meter).norm(), 3. * sqrt(2.)  * si::meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> ( 3.5 * meter,  4. * degrees, 3.5 * meter).norm(), 3.5 * sqrt(2.) * si::meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> ( 0.  * meter,  4. * degrees, 0. * meter).norm(), 0.  * si::meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> ( 0.  * meter,  0. * degrees, 3. * meter).norm(), 3.  * si::meter);
}

TEST(VectorY3D, normSquared)
{
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> ( 0.  * meters,  4. * degrees, 3.  * meter).normSquared(),   9. * square_meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> (-3.  * meters,  4. * degrees, 3.  * meter).normSquared(),  18. * square_meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> ( 0.  * meters,  4. * degrees, 3.  * meter).normSquared(),   9. * square_meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> ( 3.  * meters,  0. * degrees, 0.  * meter).normSquared(),   9. * square_meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> ( 0.  * meters,  0. * degrees, 0.  * meter).normSquared(),   0. * square_meter);
}

TEST(VectorY3D, isNull)
{
    EXPECT_FALSE(VectorY3D<QtySiLength> ( 3. * meters, 4. * degrees, 3.5 * meter).isNull());
    EXPECT_FALSE(VectorY3D<QtySiLength> ( 0. * meters, 4. * degrees, 3.5 * meter).isNull());
    EXPECT_FALSE(VectorY3D<QtySiLength> ( 3. * meters, 0. * degrees, 0.  * meter).isNull());
    EXPECT_TRUE(VectorY3D<QtySiLength>  ( 0. * meters, 0. * degrees, 0.  * meter).isNull());
}

TEST(VectorY3D, versor)
{
    VEC_EXPECT_NEAR(VectorY3D<QtySiLength> ( 3. * meters, 400. * degrees, 0. * meter).versor(), VectorY3D<QtySiLength> (1. * meter,  40. *degrees, 0. * meter));
    VEC_EXPECT_NEAR(VectorY3D<QtySiLength> (-3. * meters, 400. * degrees, 4. * meter).versor(), VectorY3D<QtySiLength> (0.6 * meter, 220. *degrees, 0.8 * meter));

    VectorY3D<QtySiLength> v1( 3. * meters, 400. * degrees, 0. * meter);
    VectorY3D<QtySiLength> result(1. * meter,  40. *degrees, 0. * meter);

    v1.doVersor();
    VEC_EXPECT_NEAR(v1, result);
}

TEST(VectorY3D, versorDl)
{
    VEC_EXPECT_NEAR(VectorY3D<QtySiLength> ( 3. * meters, 400. * degrees, 0. * meter).versorDl(), VectorY3D<QtySiDimensionless> (1.,  40. *degrees, 0.));
    VEC_EXPECT_NEAR(VectorY3D<QtySiLength> (-3. * meters, 400. * degrees, 4. * meter).versorDl(), VectorY3D<QtySiDimensionless> (0.6, 220. *degrees, 0.8));
}

TEST(VectorY3D, dot)
{
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> (   1. * meters, 430. * degrees, 0.  * meter).dot( VectorY3D<QtySiLength> ( 2. * meter,   10. *degrees, 0.  * meter)),  1. * square_meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> (   1. * meters, 90.  * degrees, 5.  * meter).dot( VectorY3D<QtySiLength> (-1. * meter,  180. *degrees, 5.  * meter)), 25. * square_meter);
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> (   5. * meters, 30.  * degrees, 3.  * meter).dot( VectorY3D<QtySiLength> (11. * meter,  90. *degrees, 8.  * meter)), 51.5 * square_meter);
}

TEST(VectorY3D, cross)
{
    VEC_EXPECT_NEAR(VectorY3D<QtySiLength> (   1. * meters, 0. * degrees, 0.  * meter).cross( VectorY3D<QtySiLength> ( 1. * meter,   90. *degrees, 0.  * meter)), VectorY3D<QtySiArea> ( 0. * square_meter,   0. *degrees, 1.  * square_meter) );
    VEC_EXPECT_NEAR(VectorY3D<QtySiLength> (   0. * meters, 0.  * degrees, 1.  * meter).cross( VectorY3D<QtySiLength> (1. * meter,  0. *degrees, 0.  * meter)), VectorY3D<QtySiArea> ( 1. * square_meter,   90. *degrees, 0.  * square_meter));
}

TEST(VectorY3D, angle)
{
    // TODO: Michele: this has a problem with the number of decimals. Investigate it.
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> (  1. * meters,   45. * degrees, 1.5 * meter).angle(VectorY3D<QtySiLength> (  1. * meters,   45. * degrees, 1.5 * meter)), QtySiPlaneAngle(  0. * degrees));
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> (  1. * meters,   0. * degrees, 0. * meter).angle(VectorY3D<QtySiLength> ( -1. * meters,   0. * degrees, 0. * meter)),     QtySiPlaneAngle(180. * degrees));
    QTY_EXPECT_NEAR(VectorY3D<QtySiLength> (  1. * meters,   0. * degrees, 0. * meter).angle(VectorY3D<QtySiLength> (  0. * meters,  90. * degrees, 1. * meter)),     QtySiPlaneAngle( 90. * degrees));
}

TEST(VectorY3D, isFinite)
{
    EXPECT_TRUE( VectorY3D<QtySiLength> (     1. * meters,      0. * degrees, 3.5 * meter).isFinite());
    EXPECT_TRUE( VectorY3D<QtySiLength> (     0. * meters,      0. * degrees, 3.5 * meter).isFinite());
    EXPECT_TRUE( VectorY3D<QtySiLength> (   999. * meters,    430. * degrees, 3.5 * meter).isFinite());
    EXPECT_FALSE( VectorY3D<QtySiLength> (nan_val * meters,    430. * degrees, 3.5 * meter).isFinite());
    EXPECT_FALSE( VectorY3D<QtySiLength> (     2. * meters, nan_val * degrees, 3.5 * meter).isFinite());
    EXPECT_FALSE( VectorY3D<QtySiLength> (nan_val * meters, nan_val * degrees, 3.5 * meter).isFinite());
    EXPECT_FALSE(  VectorY3D<QtySiLength> (     2. * meters,     inf_val * degrees, 3.5 * meter).isFinite());
    EXPECT_FALSE(  VectorY3D<QtySiLength> (    inf_val * meters,     inf_val * degrees, 3.5 * meter).isFinite());
    EXPECT_FALSE(  VectorY3D<QtySiLength> (    inf_val * meters,      2. * degrees, 3.5 * meter).isFinite());
    EXPECT_FALSE(  VectorY3D<QtySiLength> (    inf_val * meters, nan_val * degrees, 3.5 * meter).isFinite());
}

TEST(VectorY3D, isNormal)
{
    EXPECT_FALSE( VectorY3D<QtySiLength> (     1. * meters,      0. * degrees, 3.5 * meter).isNormal()); // 0 is not normal
    EXPECT_FALSE( VectorY3D<QtySiLength> (     0. * meters,      0. * degrees, 3.5 * meter).isNormal()); // 0 is not normal
    EXPECT_TRUE( VectorY3D<QtySiLength> (   999. * meters,    430. * degrees, 3.5 * meter).isNormal());
    EXPECT_FALSE( VectorY3D<QtySiLength> (nan_val * meters,    430. * degrees, 3.5 * meter).isNormal());
    EXPECT_FALSE( VectorY3D<QtySiLength> (     2. * meters, nan_val * degrees, 3.5 * meter).isNormal());
    EXPECT_FALSE( VectorY3D<QtySiLength> (nan_val * meters, nan_val * degrees, 3.5 * meter).isNormal());
    EXPECT_FALSE(  VectorY3D<QtySiLength> (     2. * meters,     inf_val * degrees, 3.5 * meter).isNormal());
    EXPECT_FALSE(  VectorY3D<QtySiLength> (    inf_val * meters,     inf_val * degrees, 3.5 * meter).isNormal());
    EXPECT_FALSE(  VectorY3D<QtySiLength> (    inf_val * meters,      2. * degrees, 3.5 * meter).isNormal());
    EXPECT_FALSE(  VectorY3D<QtySiLength> (    inf_val * meters, nan_val * degrees, 3.5 * meter).isNormal());
}

TEST(VectorY3D, isNan)
{
    EXPECT_FALSE( VectorY3D<QtySiLength> (     1. * meters,      0. * degrees, 3.5 * meter).isNan());
    EXPECT_FALSE( VectorY3D<QtySiLength> (     0. * meters,      0. * degrees, 3.5 * meter).isNan());
    EXPECT_FALSE( VectorY3D<QtySiLength> (   999. * meters,    430. * degrees, 3.5 * meter).isNan());
    EXPECT_TRUE(  VectorY3D<QtySiLength> (nan_val * meters,    430. * degrees, 3.5 * meter).isNan());
    EXPECT_TRUE(  VectorY3D<QtySiLength> (     2. * meters, nan_val * degrees, 3.5 * meter).isNan());
    EXPECT_TRUE(  VectorY3D<QtySiLength> (nan_val * meters, nan_val * degrees, 3.5 * meter).isNan());
    EXPECT_FALSE( VectorY3D<QtySiLength> (     2. * meters,     inf_val * degrees, 3.5 * meter).isNan());
    EXPECT_FALSE( VectorY3D<QtySiLength> (    inf_val * meters,     inf_val * degrees, 3.5 * meter).isNan());
    EXPECT_FALSE( VectorY3D<QtySiLength> (    inf_val * meters,      2. * degrees, 3.5 * meter).isNan());
    EXPECT_TRUE(  VectorY3D<QtySiLength> (    inf_val * meters, nan_val * degrees, 3.5 * meter).isNan());
}

TEST(VectorY3D, isInfinite)
{
    EXPECT_FALSE(VectorY3D<QtySiLength>(1. * meters, 0. * degrees, 3.5 * meter).isInfinite());
    EXPECT_FALSE(VectorY3D<QtySiLength>(0. * meters, 0. * degrees, 3.5 * meter).isInfinite());
    EXPECT_FALSE(VectorY3D<QtySiLength>(999. * meters, 430. * degrees, 3.5 * meter).isInfinite());
    EXPECT_FALSE(VectorY3D<QtySiLength>(nan_val * meters, 430. * degrees, 3.5 * meter).isInfinite());
    EXPECT_FALSE(VectorY3D<QtySiLength>(2. * meters, nan_val * degrees, 3.5 * meter).isInfinite());
    EXPECT_FALSE(VectorY3D<QtySiLength>(nan_val * meters, nan_val * degrees, 3.5 * meter).isInfinite());
    EXPECT_TRUE(VectorY3D<QtySiLength>(2. * meters, inf_val * degrees, 3.5 * meter).isInfinite());
    EXPECT_TRUE(VectorY3D<QtySiLength>(inf_val * meters, inf_val * degrees, 3.5 * meter).isInfinite());
    EXPECT_TRUE(VectorY3D<QtySiLength>(inf_val * meters, 2. * degrees, 3.5 * meter).isInfinite());
    EXPECT_TRUE(VectorY3D<QtySiLength>(inf_val * meters, nan_val * degrees, 3.5 * meter).isInfinite());
}


TEST(VectorY3D, addition)
{
    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  1. * meter);

    VectorY3D<QtySiLength> vec_result_1 = vec_a + vec_b;
    VectorY3D<QtySiLength> vec_result_2 = vec_a + vec_c;
    VectorY3D<QtySiLength> vec_result_3 = vec_c + vec_d;
    VectorY3D<QtySiLength> vec_result_4 = vec_a + vec_d;

    VEC_EXPECT_NEAR(vec_result_1, VectorY3D<QtySiLength> (3. * meter, 90. * degrees, 0. * meter));
    VEC_EXPECT_NEAR(vec_result_2, VectorY3D<QtySiLength> (1. * meter, 90. * degrees, 0. * meter));
    VEC_EXPECT_NEAR(vec_result_3, VectorY3D<QtySiLength> (1. * meter, 0. * degrees, 1. * meter));
    VEC_EXPECT_NEAR(vec_result_4, VectorY3D<QtySiLength> (sqrt(2.) * meter, 45. * degrees, 1. * meter));
}

TEST(VectorY3D, additionEqual)
{

    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_c_1(0. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  1. * meter);

    vec_b += vec_a;
    VEC_EXPECT_NEAR(vec_b, VectorY3D<QtySiLength> (3. * meter, 90. * degrees, 0. * meter));

    vec_c += vec_a;
    VEC_EXPECT_NEAR(vec_c, VectorY3D<QtySiLength> (1. * meter, 90. * degrees, 0. * meter));

    vec_c_1 += vec_d;
    VEC_EXPECT_NEAR(vec_c_1, VectorY3D<QtySiLength> (1. * meter, 0. * degrees, 1. * meter));

    vec_a += vec_d;
    VEC_EXPECT_NEAR(vec_a, VectorY3D<QtySiLength> (sqrt(2.) * meter, 45. * degrees, 1. * meter));
}

TEST(VectorY3D, subtraction)
{

    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  1. * meter);

    VectorY3D<QtySiLength> vec_result_1 = vec_a - vec_b;
    VectorY3D<QtySiLength> vec_result_2 = vec_a - vec_c;
    VectorY3D<QtySiLength> vec_result_3 = vec_c - vec_d;
    VectorY3D<QtySiLength> vec_result_4 = vec_a - vec_d;

    VEC_EXPECT_NEAR(vec_result_1, VectorY3D<QtySiLength> (1. * meter, 270. * degrees, 0. * meter));
    VEC_EXPECT_NEAR(vec_result_2, VectorY3D<QtySiLength> (1. * meter, 90. * degrees, 0. * meter));
    VEC_EXPECT_NEAR(vec_result_3, VectorY3D<QtySiLength> (1. * meter, 180. * degrees, -1. * meter));
    VEC_EXPECT_NEAR(vec_result_4, VectorY3D<QtySiLength> (sqrt(2.) * meter, 135. * degrees, -1. * meter));
}

TEST(VectorY3D, subtractionEqual)
{
    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_c_1(0. * meters, 90. * degrees, 0. * meter);
    VectorY3D<QtySiLength> vec_d(1. * meters, 0. * degrees,  1. * meter);

    vec_b -= vec_a;
    VEC_EXPECT_NEAR(vec_b, VectorY3D<QtySiLength> (1. * meter, 90. * degrees, 0. * meter));

    vec_c -= vec_a;
    VEC_EXPECT_NEAR(vec_c, VectorY3D<QtySiLength> (1. * meter, 270. * degrees, 0. * meter));

    vec_c_1 -= vec_d;
    VEC_EXPECT_NEAR(vec_c_1, VectorY3D<QtySiLength> (1. * meter, 180. * degrees, -1. * meter));

    vec_a -= vec_d;
    VEC_EXPECT_NEAR(vec_a, VectorY3D<QtySiLength> (sqrt(2.) * meter, 135. * degrees, -1. * meter));
}

TEST(VectorY3D, multiplication)
{
    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_d(1. * meters, 0. * degrees, 3.5 * meter);

    VEC_EXPECT_NEAR(vec_a * 5.,   VectorY3D<QtySiLength> (5. * si::meter, 90. * degrees, 17.5 * meter));
    VEC_EXPECT_NEAR(vec_b * 105., VectorY3D<QtySiLength> (210. * si::meter, 90. * degrees, 367.5 * meter));

    // All zero radius polar points are normalized to zero angle
    VEC_EXPECT_NEAR(vec_c * 200., VectorY3D<QtySiLength> (0. * si::meter, 0. * degrees, 700.* meter));
    VEC_EXPECT_NEAR(vec_d * 10., VectorY3D<QtySiLength> (10. * si::meter, 0. * degrees, 35.* meter));
}

TEST(VectorY3D, multiplicationEqual)
{
    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_c(0. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_d(1. * meters, 0. * degrees, 3.5 * meter);

    //MultiplicationEqual with scalar
    vec_a *= 5;
    VEC_EXPECT_NEAR(vec_a,   VectorY3D<QtySiLength> (5. * si::meter, 90. * degrees, 17.5 * meter));

    vec_b *= 105.;
    VEC_EXPECT_NEAR(vec_b, VectorY3D<QtySiLength> (210. * si::meter, 90. * degrees, 367.5 * meter));

    // All zero radius polar points are normalized to zero angle
    vec_c *= 200.;
    VEC_EXPECT_NEAR(vec_c, VectorY3D<QtySiLength> (0. * si::meter, 0. * degrees, 700. * meter));

    vec_d *= 10.;
    VEC_EXPECT_NEAR(vec_d, VectorY3D<QtySiLength> (10. * si::meter, 0. * degrees, 35. * meter));
}

TEST(VectorY3D, divisionEqual)
{
    VectorY3D<QtySiLength> vec_a(3. * meters, 90. * degrees, 3 * meter);
    VectorY3D<QtySiLength> vec_b(0. * meters, 90. * degrees, 7 * meter);

    vec_a /= 3;
    vec_b /= 7;
    VEC_EXPECT_NEAR(vec_a, VectorY3D<QtySiLength> (1. * si::meter, 90. * degrees, 1. * meter));
    VEC_EXPECT_NEAR(vec_b, VectorY3D<QtySiLength> (0. * si::meter, 90. * degrees, 1. * meter));
}



TEST(VectorY3D, greaterThan)
{
    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_c(3. * meters,  0. * degrees, 3.5 * meter);

    EXPECT_GT(vec_b, vec_a);
    EXPECT_GT(vec_c, vec_b);
}

TEST(VectorY3D, greaterThanOrEqualTo)
{
    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_c(3. * meters,  0. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_d(3. * meters, 55. * degrees, 3.5 * meter);

    EXPECT_GE(vec_b, vec_a);
    EXPECT_GE(vec_c, vec_b);
    EXPECT_GE(vec_d, vec_c);
}

TEST(VectorY3D, lessThan)
{
    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_c(3. * meters,  0. * degrees, 3.5 * meter);

    EXPECT_LT(vec_a, vec_b);
    EXPECT_LT(vec_b, vec_c);
}

TEST(VectorY3D, lessThanOrEqualTo)
{
    VectorY3D<QtySiLength> vec_a(1. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(2. * meters, 90. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_c(3. * meters,  0. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_d(3. * meters, 55. * degrees, 3.5 * meter);

    EXPECT_LE(vec_a, vec_b);
    EXPECT_LE(vec_b, vec_c);
    EXPECT_LE(vec_c, vec_d);
}

TEST(VectorY3D, isEqualTo)
{
    VectorY3D<QtySiLength> vec_a(3. * meters,  0. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(3. * meters,  0. * degrees, 3.5 * meter);

    VEC_EXPECT_NEAR(vec_a, vec_b);
}

TEST(VectorY3D, notEqualTo)
{
    VectorY3D<QtySiLength> vec_a(5. * meters,  0. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(3. * meters,  0. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_c(9. * meters, 55. * degrees, 3.5 * meter);

    EXPECT_NE(vec_a, vec_b);
    EXPECT_NE(vec_b, vec_c);
}

TEST(VectorY3D, isNear)
{
    VectorY3D<QtySiLength> vec_a(5. * meters,  0. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_b(3. * meters,  0. * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_c(5. * meters,  0.000000000001 * degrees, 3.5 * meter);
    VectorY3D<QtySiLength> vec_d(5.00000000000001 * meters,  0. * degrees, 3.5 * meter);

    EXPECT_FALSE(vec_a.isNear(vec_b));
    EXPECT_TRUE(vec_a.isNear(vec_c));
    EXPECT_TRUE(vec_a.isNear(vec_d));
}

TEST(VectorY3D, toCartesian)
{
    VectorY3D<QtySiLength> vec_a(5. * meters,  0. * degrees, 3.5 * meter);
    VectorC<3,QtySiLength> vec_b(vec_a);
    VectorC<3,QtySiLength> result(5. * meters, 0. * meters, 3.5 * meter);

    VEC_EXPECT_NEAR(vec_b, result);
}

TEST(VectorY3D, elementAccess)
{
#if !defined(GCC) || defined(ENABLE_GCC_PROPERTY)
    EXPECT_EQ(VectorY3D<QtySiLength> (1.*meter, 90.*degrees, 2.*meter).rho,   1.*meter);   // name-based
    EXPECT_EQ(VectorY3D<QtySiLength> (1.*meter, 90.*degrees, 2.*meter).phi,  QtySiPlaneAngle(90.*degrees)); // name-based
    EXPECT_EQ(VectorY3D<QtySiLength> (1.*meter, 90.*degrees, 2.*meter).z,     2.*meter);   // name-based
#endif
}