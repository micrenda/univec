#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <gtest/gtest.h>

#include "univec/Matrix1.hpp"
#include "univec/Matrix2.hpp"
#include "univec/Matrix4.hpp"
#include "univec/VectorC2D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace dfpe;
using namespace si;

/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix2, addition) {

    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            3. * meter, -3. * meter,
            5. * meter, 0. * meter
    );
    RMatrix2<QtySiLength> result(
            4. * meter, -1. * meter,
            8. * meter, 4. * meter
    );

    EXPECT_EQ(m1 + m2, result);
}

TEST(RMatrix2, additionEqual) {
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            3. * meter, -3. * meter,
            5. * meter, 0. * meter
    );
    RMatrix2<QtySiLength> result(
            4. * meter, -1. * meter,
            8. * meter, 4. * meter
    );

    EXPECT_EQ(m1 += m2, result);
}

/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix2, subtraction) {
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            3. * meter, -3. * meter,
            5. * meter, 0. * meter
    );
    RMatrix2<QtySiLength> result(
            -2. * meter, 5. * meter,
            -2. * meter, 4. * meter
    );

    EXPECT_EQ(m1 - m2, result);
}

TEST(RMatrix2, subtractionEqual) {
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            3. * meter, -3. * meter,
            5. * meter, 0. * meter
    );
    RMatrix2<QtySiLength> result(
            -2. * meter, 5. * meter,
            -2. * meter, 4. * meter
    );

    EXPECT_EQ(m1 -= m2, result);
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix2, multiplicationByScalar) {
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> result1(
            2. * meter, 4. * meter,
            6. * meter, 8. * meter
    );
    RMatrix2<QtySiLength> result2(
            0. * meter, 0. * meter,
            0. * meter, 0. * meter
    );

    EXPECT_EQ(m1 * 2., result1);
    EXPECT_EQ(m1 * 0., result2);

    EXPECT_EQ(2. * m1, result1);
    EXPECT_EQ(0. * m1, result2);
}

TEST(RMatrix2, multiplicationByMatrix) {

    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            2. * meter, 4. * meter,
            6. * meter, 8. * meter
    );
    RMatrix2<QtySiArea> result(
            14. * square_meter, 20. * square_meter,
            30. * square_meter, 44. * square_meter
    );

    EXPECT_EQ(m1 * m2, result);
}

TEST(RMatrix2, multiplicationByVector) {
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    VectorC2D<QtySiLength> v1(2. * meter, 4. * meter);
    VectorC2D<QtySiArea> result(10. * square_meter, 22. * square_meter);

    EXPECT_EQ(m1 * v1, result);
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix2, scalardivision) {
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> result(
            0.5 * meter, 1. * meter,
            1.5 * meter, 2. * meter
    );

    EXPECT_EQ(m1 / 2., result);
//    TODO: division with an integer
//    EXPECT_EQ(m1 / 2, result);
}

TEST(RMatrix2, divisionByZero)
{
    // Matrix division by zero scalar is not possible!
    RMatrix2<QtySiLength> m1 (
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_TRUE((m1 / 0.).isInfinite());
}

/* Comparative tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix2, isEqual)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_EQ(m1, m2);
}

TEST(RMatrix2, isNotEqual)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            2. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_NE(m1, m2);
}

/* Common matrix operations tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix2, transpose)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> result(
            1. * meter, 3. * meter,
            2. * meter, 4. * meter
    );

    EXPECT_EQ(m1.t(), result);
}

TEST(RMatrix2, replaceWithTranspose)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> result(
            1. * meter, 3. * meter,
            2. * meter, 4. * meter
    );

    m1.doT();
    EXPECT_EQ(m1, result);
}

TEST(RMatrix2, square)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_TRUE(m1.square);
}

TEST(RMatrix2, orderOfSquareMatrix)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_EQ(m1.order(), 2);
}

TEST(RMatrix2, order)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    pair<unsigned int,unsigned int> result(2,2);
    EXPECT_EQ(m1.orders(), result);
}

TEST(RMatrix2, isIdentity)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 0. * meter,
            0. * meter, 1. * meter
    );
    RMatrix2<QtySiLength> m2(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_TRUE(m1.isIdentity());
    EXPECT_FALSE(m2.isIdentity());
}

TEST(RMatrix2, trace)
{
    // The Sum of the main diagonal elements
    RMatrix2<QtySiLength> m1(
            1. * meter, 0. * meter,
            0. * meter, 1. * meter
    );

    EXPECT_EQ(m1.tr(), Real<QtySiLength>(2 * meter));
}

TEST(RMatrix2, isDiagonal)
{
    // True if all matrix elements are 0 except of those from the main diagonal
    RMatrix2<QtySiLength> m1(
            -1. * meter, 0. * meter,
            0. * meter, 3. * meter
    );
    RMatrix2<QtySiLength> m2(
            -1. * meter, 0. * meter,
            2 * meter, 3. * meter
    );

    EXPECT_TRUE(m1.isDiagonal());
    EXPECT_FALSE(m2.isDiagonal());
}

TEST(RMatrix2, isUpperTriangular)
{
    RMatrix2<QtySiLength> m1(
            -1. * meter, 0. * meter,
             0. * meter, 3. * meter
    );
    RMatrix2<QtySiLength> m2(
            -1. * meter, 0. * meter,
             2. * meter, 3. * meter
    );

    EXPECT_TRUE(m1.isUpperTriangular());
    EXPECT_FALSE(m2.isUpperTriangular());
}

TEST(RMatrix2, isLowerTriangular)
{
    RMatrix2<QtySiLength> m1(
            -1. * meter, 0. * meter,
            0. * meter, 3. * meter
    );
    RMatrix2<QtySiLength> m2(
            -1. * meter, 2. * meter,
            0 * meter, 3. * meter
    );

    EXPECT_TRUE(m1.isLowerTriangular());
    EXPECT_FALSE(m2.isLowerTriangular());
}


TEST(RMatrix2, isTriangular)
{
    RMatrix2<QtySiLength> m0 (
        1. * meter, 0. * meter,
        0. * meter, 5. * meter
    );

    RMatrix2<QtySiLength> m1 (
        1. * meter, 2. * meter,
        0. * meter, 5. * meter
    );

    RMatrix2<QtySiLength> m2 (
        1. * meter, 0. * meter,
        4. * meter, 5. * meter
    );

    RMatrix2<QtySiLength> m3 (
        1. * meter, 2. * meter,
        4. * meter, 5. * meter
    );

    EXPECT_TRUE(m0.isTriangular());
    EXPECT_TRUE(m1.isTriangular());
    EXPECT_TRUE(m2.isTriangular());
    EXPECT_FALSE(m3.isTriangular());
}

TEST(RMatrix2, isStrictTriangular)
{
    RMatrix2<QtySiLength> m1 (
        1. * meter, 0. * meter,
        4. * meter, 1. * meter
    );

    RMatrix2<QtySiLength> m2 (
        1. * meter, 2. * meter,
        0. * meter, 5. * meter
    );

    RMatrix2<QtySiLength> m3 (
        0. * meter, 0. * meter,
        4. * meter, 0. * meter
    );

    RMatrix2<QtySiLength> m4 (
        0. * meter, 2. * meter,
        0. * meter, 0. * meter
    );
    EXPECT_FALSE(m1.isStrictTriangular());
    EXPECT_FALSE(m2.isStrictTriangular());
    EXPECT_TRUE(m3.isStrictTriangular());
    EXPECT_TRUE(m4.isStrictTriangular());
}

TEST(RMatrix2, isUniTriangular)
{
    RMatrix2<QtySiLength> m1 (
        1. * meter, 0. * meter,
        4. * meter, 5. * meter
    );

    RMatrix2<QtySiLength> m2 (
        1. * meter, 2. * meter,
        0. * meter, 5. * meter
    );

    RMatrix2<QtySiLength> m3 (
        1. * meter, 0. * meter,
        4. * meter, 1. * meter
    );

    RMatrix2<QtySiLength> m4 (
        1. * meter, 2. * meter,
        0. * meter, 1. * meter
    );

    EXPECT_FALSE(m1.isUniTriangular());
    EXPECT_FALSE(m2.isUniTriangular());
    EXPECT_TRUE(m3.isUniTriangular());
    EXPECT_TRUE(m4.isUniTriangular());
}


TEST(RMatrix2, isSymmetric)
{
    RMatrix2<QtySiLength> m1(
            -1. * meter, 0. * meter,
            0. * meter, 3. * meter
    );
    RMatrix2<QtySiLength> m2(
            -1. * meter, 2. * meter,
            0 * meter, 3. * meter
    );

    EXPECT_TRUE(m1.isSymmetric());
    EXPECT_FALSE(m2.isSymmetric());
}

TEST(RMatrix2, isSkewSymmetric)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 4. * meter,
            0. * meter, -1. * meter
    );
    RMatrix2<QtySiLength> m2(
            0. * meter, 2. * meter,
            -2. * meter, 0. * meter
    );

    EXPECT_FALSE(m1.isSkewSymmetric());
    EXPECT_TRUE(m2.isSkewSymmetric());
}

TEST(RMatrix2, submatrix)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    RMatrix1<QtySiLength> result(3. * meter);
    EXPECT_EQ(m1.sub(1,2), result);
}

TEST(RMatrix2, determinantLeibniz)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::LEIBNIZ_FORMULA), Real<QtySiArea>(-2. * square_meter));
}

TEST(RMatrix2, determinantLaplace)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::LAPLACE_FORMULA), Real<QtySiArea>(-2. * square_meter));
}

TEST(RMatrix2, determinantGauss)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::GAUSSIAN_ELIMINATION), Real<QtySiArea>(-2. * square_meter));
}

TEST(RMatrix2, isOrthogonal)
{
//    Is orthogonal if the inverse matrix is equal with its transpose.
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    RMatrix2<QtySiLength> m2(
            1. * meter, 0. * meter,
            0. * meter, -1. * meter
    );

    EXPECT_FALSE(m1.isOrthogonal());
    EXPECT_TRUE(m2.isOrthogonal());
}

TEST(RMatrix2, isSingular)
{
// a matrix is singular if its determinant is equal with 0 or doesn't have an inverse matrix.
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            3. * meter, 6. * meter,
            2. * meter, 4. * meter
    );

    EXPECT_FALSE(m1.isSingular());
    EXPECT_TRUE(m2.isSingular());
}

TEST(RMatrix2, isHermitian)
{
// a matrix is singular if its determinant is equal with 0 or doesn't have an inverse matrix.
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            3. * meter, 6. * meter,
            2. * meter, 4. * meter
    );

    EXPECT_FALSE(m1.isSingular());
    EXPECT_TRUE(m2.isSingular());
}

TEST(RMatrix2, minor)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_EQ(m1.minor(1,1), 4. * meter);
    EXPECT_EQ(m1.minor(1,2), 3. * meter);
    EXPECT_EQ(m1.minor(2,1), 2. * meter);
    EXPECT_EQ(m1.minor(2,2), 1. * meter);
}

TEST(RMatrix2, minors)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    RMatrix2<QtySiLength> result (
            4. * meter, 3. * meter,
            2. * meter, 1. * meter
    );

    EXPECT_EQ(m1.minors(), result);
}

TEST(RMatrix2, cofactor)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    EXPECT_EQ(m1.cofactor(1,1), 4. * meter);
    EXPECT_EQ(m1.cofactor(1,2), -3. * meter);
    EXPECT_EQ(m1.cofactor(2,1), -2. * meter);
    EXPECT_EQ(m1.cofactor(2,2), 1. * meter);
}

TEST(RMatrix2, cofactors) {
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> result(
            4. * meter, -3. * meter,
            -2. * meter, 1. * meter
    );

    EXPECT_EQ(m1.cofactors(), result);
}

TEST(RMatrix2, adjugate)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> result(
            4. * meter, -2. * meter,
            -3. * meter, 1. * meter
    );

    EXPECT_EQ(m1.adj(), result);
}

TEST(RMatrix2, inv)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiWavenumber> result(
            -2. * reciprocal_meter, 1. * reciprocal_meter,
            1.5 * reciprocal_meter, -0.5 * reciprocal_meter
    );

    VEC_EXPECT_NEAR(m1.inv(), result);
}

TEST(RMatrix2, diagonal)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );

    RMatrix<2,1,QtySiLength> result(1. * meter, 4. * meter);

    VEC_EXPECT_NEAR(m1.diagonal(), result);
}


TEST(RMatrix2, row)
{
    RMatrix2<QtySiLength> m1 (
            1. * meter, 2 * meter,
            3. * meter, 4 * meter
    );

    RMatrix<1,2,QtySiLength> result1(1. * meter, 2. * meter);
    RMatrix<1,2,QtySiLength> result2(3. * meter, 4. * meter);

    MAT_EXPECT_NEAR(m1.row(1), result1);
    MAT_EXPECT_NEAR(m1.row(2), result2);
}

TEST(RMatrix2, col)
{
    RMatrix2<QtySiLength> m1 (
            1. * meter, 2 * meter,
            3. * meter, 4 * meter
    );

    RMatrix<2,1,QtySiLength> result1(1. * meter,  3. * meter);
    RMatrix<2,1,QtySiLength> result2(2. * meter,  4. * meter);

    MAT_EXPECT_NEAR(m1.col(1), result1);
    MAT_EXPECT_NEAR(m1.col(2), result2);
}

TEST(RMatrix2, normL1)
{
    RMatrix2<QtySiLength> m1 (
             1. * meter, -2 * meter,
            -3. * meter,  4 * meter
    );

    QTY_EXPECT_NEAR(m1.normL1(), 6. * meter);
}

TEST(RMatrix2, normLInf)
{
    RMatrix2<QtySiLength> m1 (
             1. * meter, -2 * meter,
            -3. * meter,  4 * meter
    );

    QTY_EXPECT_NEAR(m1.normLInf(), 7. * meter);
}

TEST(RMatrix2, normLMax)
{
    RMatrix2<QtySiLength> m1 (
             -1. * meter,  2 * meter,
              3. * meter, -4 * meter
    );

    QTY_EXPECT_NEAR(m1.normLMax(), 4. * meter);
}

TEST(RMatrix2, normL)
{
    //TODO
}

TEST(RMatrix2, normF)
{
    RMatrix2<QtySiLength> m1 (
            -1. * meter, -2 * meter,
             2. * meter,  4 * meter
    );

    QTY_EXPECT_NEAR(m1.normF(), 5. * meter);
}



/* Validity check operations */
// ----------------------------------------------------------------------------------------------------------------
TEST(RMatrix2, isNull)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            0. * meter, 0. * meter,
            0. * meter, 0. * meter
    );

    EXPECT_FALSE(m1.isNull());
    EXPECT_TRUE(m2.isNull());
}

TEST(RMatrix2, isFinite)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter
    );

    EXPECT_TRUE(m1.isFinite());
    EXPECT_FALSE(m2.isFinite());
}

TEST(RMatrix2, isInfinite)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter
    );

    EXPECT_FALSE(m1.isInfinite());
    EXPECT_TRUE(m2.isInfinite());
}

TEST(RMatrix2, isNormal)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 0. * meter,
            3. * meter, 4. * meter
    );
    RMatrix2<QtySiLength> m2(
            -1. * meter, 0 * meter,
            nan_val * meter, 4. * meter
    );

    EXPECT_FALSE(m1.isNormal());
    EXPECT_FALSE(m2.isNormal());
}

TEST(RMatrix2, isNan)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 0. * meter,
            3. * meter, 4. * meter
    );

    RMatrix2<QtySiLength> m2(
            -1. * meter, 0 * meter,
            nan_val * meter, 4. * meter
    );

    EXPECT_FALSE(m1.isNan());
    EXPECT_TRUE(m2.isNan());
}


TEST(RMatrix2, kroneckerProduct)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3 * meter,  4. * meter
    );

    RMatrix2<QtySiLength> m2(
            0. * meter, 5. * meter,
            6. * meter, 7. * meter
    );

    RMatrix4<QtySiArea> result(
             0. * square_meter,  5. * square_meter,  0. * square_meter, 10. * square_meter,
             6. * square_meter,  7. * square_meter, 12. * square_meter, 14. * square_meter,
             0. * square_meter, 15. * square_meter,  0. * square_meter, 20. * square_meter,
            18. * square_meter, 21. * square_meter, 24. * square_meter, 28. * square_meter
    );

    MAT_EXPECT_NEAR(m1.kroneckerProduct(m2), result);
}

TEST(RMatrix2, kroneckerSum)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3 * meter,  4. * meter
    );

    RMatrix2<QtySiLength> m2(
            5. * meter, 6. * meter,
            7. * meter, 8. * meter
    );

    RMatrix4<QtySiLength> result(
              6. * meter,  6. * meter,  2. * meter,  0. * meter,
              7. * meter,  9. * meter,  0. * meter,  2. * meter,
              3. * meter,  0. * meter,  9. * meter,  6. * meter,
              0. * meter,  3. * meter,  7. * meter, 12. * meter
    ); 

    MAT_EXPECT_NEAR(m1.kroneckerSum(m2), result);
}

TEST(RMatrix2, directSum)
{
    RMatrix2<QtySiLength> m1(
            1. * meter, 2. * meter,
            3 * meter,  4. * meter
    );

    RMatrix2<QtySiLength> m2(
            5. * meter, 6. * meter,
            7. * meter, 8. * meter
    );

    RMatrix4<QtySiLength> result(
             1. * meter,  2. * meter,  0. * meter, 0. * meter,
             3. * meter,  4. * meter,  0. * meter, 0. * meter,
             0. * meter,  0. * meter,  5. * meter, 6. * meter,
             0. * meter,  0. * meter,  7. * meter, 8. * meter
    );

    MAT_EXPECT_NEAR(m1.directSum(m2), result);
}


TEST(RMatrix2, elementrwiseProduct)
{
    RMatrix2<QtySiLength> m1 (
            2. * meter,  4. * meter,
            8. * meter, 10. * meter
    );

    RMatrix2<QtySiForce> m2 (
            1. * newton, 2. * newton,
            4. * newton, 5. * newton
    );

    RMatrix2<QtySiEnergy> m3 (
            2. * meter * newton,    8. * meter * newton,
            32. * meter * newton,  50. * meter * newton

    );

    MAT_EXPECT_NEAR(m1.elementwiseProduct(m2), m3);
}

TEST(RMatrix2, elementrwiseDivision)
{
    RMatrix2<QtySiLength> m1 (
            2. * meter,  4. * meter,
            8. * meter, 10. * meter
    );

    RMatrix2<QtySiTime> m2 (
            1. * second, 2. * second,
            4. * second, 5. * second
    );

    RMatrix2<QtySiVelocity> m3 (
            2. * meter / second, 2. * meter / second,
            2. * meter / second, 2. * meter / second
    );

    MAT_EXPECT_NEAR(m1.elementwiseDivision(m2), m3);
}


TEST(RMatrix2, rowMajor)
{
    RMatrix2<QtySiLength> m1 (
            0. * meter,  1. * meter,
            2. * meter,  3. * meter
    );

    EXPECT_EQ(m1.rowMajor(0), 0. * meter);
    EXPECT_EQ(m1.rowMajor(1), 1. * meter);
    EXPECT_EQ(m1.rowMajor(2), 2. * meter);
    EXPECT_EQ(m1.rowMajor(3), 3. * meter);
}

TEST(RMatrix2, colMajor)
{
    RMatrix2<QtySiLength> m1 (
            0. * meter,  2. * meter,
            1. * meter,  3. * meter
    );

    EXPECT_EQ(m1.colMajor(0), 0. * meter);
    EXPECT_EQ(m1.colMajor(1), 1. * meter);
    EXPECT_EQ(m1.colMajor(2), 2. * meter);
    EXPECT_EQ(m1.colMajor(3), 3. * meter);
}