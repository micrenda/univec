#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <gtest/gtest.h>

#include "univec/constants.hpp"
#include "univec/Matrix4.hpp"

#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace dfpe;
using namespace si;

/* Constants related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(Constants, pauliSigma)
{

    VEC_EXPECT_NEAR(pauliSigma<0>, CI<2>);

    VEC_EXPECT_NEAR(pauliSigma<1>.det(), Complex<QtySiDimensionless>(-1.));
    VEC_EXPECT_NEAR(pauliSigma<2>.det(), Complex<QtySiDimensionless>(-1.));
    VEC_EXPECT_NEAR(pauliSigma<3>.det(), Complex<QtySiDimensionless>(-1.));

    VEC_EXPECT_NEAR(pauliSigma<1>.tr(), Complex<QtySiDimensionless>(0.));
    VEC_EXPECT_NEAR(pauliSigma<2>.tr(), Complex<QtySiDimensionless>(0.));
    VEC_EXPECT_NEAR(pauliSigma<3>.tr(), Complex<QtySiDimensionless>(0.));

    EXPECT_TRUE(pauliSigma<1>.isHermitian());
    EXPECT_TRUE(pauliSigma<2>.isHermitian());
    EXPECT_TRUE(pauliSigma<3>.isHermitian());

    MAT_EXPECT_NEAR(pauliSigma<1> * pauliSigma<1>, CI<2>);
    MAT_EXPECT_NEAR(pauliSigma<2> * pauliSigma<2>, CI<2>);
    MAT_EXPECT_NEAR(pauliSigma<3> * pauliSigma<3>, CI<2>);

    MAT_EXPECT_NEAR(pauliSigma<1>, pauliSigmaK(1));
    MAT_EXPECT_NEAR(pauliSigma<2>, pauliSigmaK(2));
    MAT_EXPECT_NEAR(pauliSigma<3>, pauliSigmaK(3));

    MAT_EXPECT_NEAR(Complex<QtySiDimensionless>(0,-1) * pauliSigma<1> * pauliSigma<2> * pauliSigma<3>, CI<2>);
}

TEST(Constants, diracGamma)
{

    VEC_EXPECT_NEAR(diracGamma<0>.det(), Complex<QtySiDimensionless>(1.));
    VEC_EXPECT_NEAR(diracGamma<1>.det(), Complex<QtySiDimensionless>(1.));
    VEC_EXPECT_NEAR(diracGamma<2>.det(), Complex<QtySiDimensionless>(1.));
    VEC_EXPECT_NEAR(diracGamma<3>.det(), Complex<QtySiDimensionless>(1.));
    VEC_EXPECT_NEAR(diracGamma<5>.det(), Complex<QtySiDimensionless>(1.));

    VEC_EXPECT_NEAR(diracGamma<0>.tr(), Complex<QtySiDimensionless>(0.));
    VEC_EXPECT_NEAR(diracGamma<1>.tr(), Complex<QtySiDimensionless>(0.));
    VEC_EXPECT_NEAR(diracGamma<2>.tr(), Complex<QtySiDimensionless>(0.));
    VEC_EXPECT_NEAR(diracGamma<3>.tr(), Complex<QtySiDimensionless>(0.));
    VEC_EXPECT_NEAR(diracGamma<5>.tr(), Complex<QtySiDimensionless>(0.));

    EXPECT_TRUE(diracGamma<0>.isHermitian());
    EXPECT_TRUE(diracGamma<1>.isSkewHermitian());
    EXPECT_TRUE(diracGamma<2>.isSkewHermitian());
    EXPECT_TRUE(diracGamma<3>.isSkewHermitian());
    EXPECT_TRUE(diracGamma<5>.isHermitian());

    MAT_EXPECT_NEAR(diracGamma<0> * diracGamma<0>, CI<4>);
    MAT_EXPECT_NEAR(diracGamma<1> * diracGamma<1>, -CI<4>);
    MAT_EXPECT_NEAR(diracGamma<2> * diracGamma<2>, -CI<4>);
    MAT_EXPECT_NEAR(diracGamma<3> * diracGamma<3>, -CI<4>);
    MAT_EXPECT_NEAR(diracGamma<5> * diracGamma<5>, CI<4>);

    MAT_EXPECT_NEAR(diracGamma<0>, diracGammaMu(0));
    MAT_EXPECT_NEAR(diracGamma<1>, diracGammaMu(1));
    MAT_EXPECT_NEAR(diracGamma<2>, diracGammaMu(2));
    MAT_EXPECT_NEAR(diracGamma<3>, diracGammaMu(3));
    MAT_EXPECT_NEAR(diracGamma<5>, diracGammaMu(5));

    MAT_EXPECT_NEAR(Complex<QtySiDimensionless>(0,1) * diracGamma<0> * diracGamma<1> * diracGamma<2> * diracGamma<3>, diracGamma<5>);
}

TEST(Constants, MetricTensorG)
{
    VEC_EXPECT_NEAR(MetricTensorG<>.det(), Real<>(-1.));
    VEC_EXPECT_NEAR(MetricTensorG<>.tr(), Real<>(-2.));
    EXPECT_TRUE(MetricTensorG<>.isDiagonal());

    RMatrix4<> result (
            1, 0, 0, 0,
            0,-1, 0, 0,
            0, 0,-1, 0,
            0, 0, 0,-1);

    MAT_EXPECT_NEAR(MetricTensorG<>, result);

    MAT_EXPECT_NEAR(CMetricTensorG<>, CMatrix4<>(MetricTensorG<>));
    MAT_EXPECT_NEAR(HMetricTensorG<>, HMatrix4<>(MetricTensorG<>));
    MAT_EXPECT_NEAR(OMetricTensorG<>, OMatrix4<>(MetricTensorG<>));
    MAT_EXPECT_NEAR(SMetricTensorG<>, SMatrix4<>(MetricTensorG<>));

}

TEST(Constants, MetricTensorEta)
{
    VEC_EXPECT_NEAR(MetricTensorEta<>.det(), Real<>(-1.));
    VEC_EXPECT_NEAR(MetricTensorEta<>.tr(), Real<>(2.));
    EXPECT_TRUE(MetricTensorEta<>.isDiagonal());

    RMatrix4<> result (
           -1, 0, 0, 0,
            0, 1, 0, 0,
            0, 0, 1, 0,
            0, 0, 0, 1);

    VEC_EXPECT_NEAR(MetricTensorEta<>, result);

    MAT_EXPECT_NEAR(CMetricTensorEta<>, CMatrix4<>(MetricTensorEta<>));
    MAT_EXPECT_NEAR(HMetricTensorEta<>, HMatrix4<>(MetricTensorEta<>));
    MAT_EXPECT_NEAR(OMetricTensorEta<>, OMatrix4<>(MetricTensorEta<>));
    MAT_EXPECT_NEAR(SMetricTensorEta<>, SMatrix4<>(MetricTensorEta<>));
}
