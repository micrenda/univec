#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <gtest/gtest.h>

#include "univec/Matrix.hpp"
#include "univec/Matrix2.hpp"
#include "univec/Matrix3.hpp"
#include "univec/VectorC2D.hpp"
#include "univec/VectorC3D.hpp"
#include "qtydef/QtyDefinitions.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace dfpe;
using namespace si;

TEST(CMatrix, isScalar)
{
    RMatrix<3,2, QtySiLength> m1;
    CMatrix<3,2, QtySiLength> m2;

    EXPECT_TRUE(m1.real);
    EXPECT_FALSE(m2.real);
}

TEST(CMatrix, isHermitian)
{
    CMatrix2<QtySiLength> m1 ({
        Complex<QtySiLength>(3. * meter,  0. * meter),
        Complex<QtySiLength>(3. * meter, -2. * meter),
        Complex<QtySiLength>(3. * meter,  2. * meter),
        Complex<QtySiLength>(2. * meter,  0. * meter),
    });

    CMatrix3<QtySiLength> m2 ({
        Complex<QtySiLength>( 1. * meter,  0. * meter),
        Complex<QtySiLength>( 2. * meter,  1. * meter),
        Complex<QtySiLength>( 5. * meter, -4. * meter),
        Complex<QtySiLength>( 2. * meter, -1. * meter),
        Complex<QtySiLength>( 4. * meter,  0. * meter),
        Complex<QtySiLength>( 0. * meter,  6. * meter),
        Complex<QtySiLength>( 5. * meter,  4. * meter),
        Complex<QtySiLength>( 0. * meter, -6. * meter),
        Complex<QtySiLength>( 2. * meter,  0. * meter),
    });

    CMatrix3<QtySiLength> m3 ({
        Complex<QtySiLength>( 1. * meter,  1. * meter),
        Complex<QtySiLength>( 2. * meter,  2. * meter),
        Complex<QtySiLength>( 3. * meter,  3. * meter),
        Complex<QtySiLength>( 4. * meter,  4. * meter),
        Complex<QtySiLength>( 5. * meter,  5. * meter),
        Complex<QtySiLength>( 6. * meter,  6. * meter),
        Complex<QtySiLength>( 7. * meter,  7. * meter),
        Complex<QtySiLength>( 8. * meter,  8. * meter),
        Complex<QtySiLength>( 9. * meter,  9. * meter),
    });

    EXPECT_TRUE(m1.isHermitian());
    EXPECT_TRUE(m2.isHermitian());
    EXPECT_FALSE(m3.isHermitian());

}

TEST(CMatrix, isSkewHermitian)
{
    CMatrix2<QtySiLength> m1 ({
        Complex<QtySiLength>( 0. * meter,  5. * meter),
        Complex<QtySiLength>( 3. * meter, -2. * meter),
        Complex<QtySiLength>(-3. * meter, -2. * meter),
        Complex<QtySiLength>( 0. * meter,  6. * meter),
    });

    CMatrix3<QtySiLength> m2 ({
        Complex<QtySiLength>( 0. * meter,  0. * meter),
        Complex<QtySiLength>( 2. * meter,  1. * meter),
        Complex<QtySiLength>( 5. * meter,  4. * meter),
        Complex<QtySiLength>(-2. * meter,  1. * meter),
        Complex<QtySiLength>( 0. * meter,  0. * meter),
        Complex<QtySiLength>( 0. * meter,  6. * meter),
        Complex<QtySiLength>(-5. * meter,  4. * meter),
        Complex<QtySiLength>( 0. * meter,  6. * meter),
        Complex<QtySiLength>( 0. * meter,  0. * meter),
    });

    CMatrix3<QtySiLength> m3 ({
        Complex<QtySiLength>( 1. * meter,  1. * meter),
        Complex<QtySiLength>( 2. * meter,  2. * meter),
        Complex<QtySiLength>( 3. * meter,  3. * meter),
        Complex<QtySiLength>( 4. * meter,  4. * meter),
        Complex<QtySiLength>( 5. * meter,  5. * meter),
        Complex<QtySiLength>( 6. * meter,  6. * meter),
        Complex<QtySiLength>( 7. * meter,  7. * meter),
        Complex<QtySiLength>( 8. * meter,  8. * meter),
        Complex<QtySiLength>( 9. * meter,  9. * meter),
    });

    EXPECT_TRUE(m1.isSkewHermitian());
    EXPECT_TRUE(m2.isSkewHermitian());
    EXPECT_FALSE(m3.isSkewHermitian());

}

TEST(CMatrix, isUnitary)
{
    CMatrix2<QtySiDimensionless> m1 ({
        Complex<QtySiDimensionless>( 1.,  0.),
        Complex<QtySiDimensionless>( 0.,  0.),
        Complex<QtySiDimensionless>( 0.,  0.),
        Complex<QtySiDimensionless>( 0.,  1.),
    });

    CMatrix3<QtySiDimensionless> m2 ({
        Complex<QtySiDimensionless>( 0.,  1.),
        Complex<QtySiDimensionless>( 0.,  0.),
        Complex<QtySiDimensionless>( 0.,  0.),
        Complex<QtySiDimensionless>( 0.,  0.),
        Complex<QtySiDimensionless>( 0.,  1.),
        Complex<QtySiDimensionless>( 0.,  0.),
        Complex<QtySiDimensionless>( 0.,  0.),
        Complex<QtySiDimensionless>( 0.,  0.),
        Complex<QtySiDimensionless>( 1.,  0.),
    });

    CMatrix3<QtySiDimensionless> m3 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
        Complex<QtySiDimensionless>( 5.,  5.),
        Complex<QtySiDimensionless>( 6.,  6.),
        Complex<QtySiDimensionless>( 7.,  7.),
        Complex<QtySiDimensionless>( 8.,  8.),
        Complex<QtySiDimensionless>( 9.,  9.),
    });

    EXPECT_TRUE(m1.isUnitary());
    EXPECT_TRUE(m2.isUnitary());
    EXPECT_FALSE(m3.isUnitary());

}

TEST(CMatrix, conj)
{
    CMatrix2<QtySiDimensionless> m1 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
    });

    CMatrix2<QtySiDimensionless> result1 ({
        Complex<QtySiDimensionless>( 1., -1.),
        Complex<QtySiDimensionless>( 2., -2.),
        Complex<QtySiDimensionless>( 3., -3.),
        Complex<QtySiDimensionless>( 4., -4.),
    });

    CMatrix3<QtySiDimensionless> m2 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
        Complex<QtySiDimensionless>( 5.,  5.),
        Complex<QtySiDimensionless>( 6.,  6.),
        Complex<QtySiDimensionless>( 7.,  7.),
        Complex<QtySiDimensionless>( 8.,  8.),
        Complex<QtySiDimensionless>( 9.,  9.),
    });

    CMatrix3<QtySiDimensionless> result2 ({
        Complex<QtySiDimensionless>( 1., -1.),
        Complex<QtySiDimensionless>( 2., -2.),
        Complex<QtySiDimensionless>( 3., -3.),
        Complex<QtySiDimensionless>( 4., -4.),
        Complex<QtySiDimensionless>( 5., -5.),
        Complex<QtySiDimensionless>( 6., -6.),
        Complex<QtySiDimensionless>( 7., -7.),
        Complex<QtySiDimensionless>( 8., -8.),
        Complex<QtySiDimensionless>( 9., -9.),
    });

    MAT_EXPECT_NEAR(m1.conj(), result1);
    MAT_EXPECT_NEAR(m2.conj(), result2);

}

TEST(CMatrix, doConj)
{
    CMatrix2<QtySiDimensionless> m1 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
    });

    CMatrix2<QtySiDimensionless> result1 ({
        Complex<QtySiDimensionless>( 1., -1.),
        Complex<QtySiDimensionless>( 2., -2.),
        Complex<QtySiDimensionless>( 3., -3.),
        Complex<QtySiDimensionless>( 4., -4.),
    });

    CMatrix3<QtySiDimensionless> m2 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
        Complex<QtySiDimensionless>( 5.,  5.),
        Complex<QtySiDimensionless>( 6.,  6.),
        Complex<QtySiDimensionless>( 7.,  7.),
        Complex<QtySiDimensionless>( 8.,  8.),
        Complex<QtySiDimensionless>( 9.,  9.),
    });

    CMatrix3<QtySiDimensionless> result2 ({
        Complex<QtySiDimensionless>( 1., -1.),
        Complex<QtySiDimensionless>( 2., -2.),
        Complex<QtySiDimensionless>( 3., -3.),
        Complex<QtySiDimensionless>( 4., -4.),
        Complex<QtySiDimensionless>( 5., -5.),
        Complex<QtySiDimensionless>( 6., -6.),
        Complex<QtySiDimensionless>( 7., -7.),
        Complex<QtySiDimensionless>( 8., -8.),
        Complex<QtySiDimensionless>( 9., -9.),
    });
    m1.doConj();
    m2.doConj();
    MAT_EXPECT_NEAR(m1, result1);
    MAT_EXPECT_NEAR(m2, result2);

}

TEST(CMatrix, conjT)
{
    CMatrix2<QtySiDimensionless> m1 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
    });

    CMatrix2<QtySiDimensionless> result1 ({
        Complex<QtySiDimensionless>( 1.,  -1.),
        Complex<QtySiDimensionless>( 3.,  -3.),
        Complex<QtySiDimensionless>( 2.,  -2.),
        Complex<QtySiDimensionless>( 4.,  -4.),
    });

    CMatrix3<QtySiDimensionless> m2 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
        Complex<QtySiDimensionless>( 5.,  5.),
        Complex<QtySiDimensionless>( 6.,  6.),
        Complex<QtySiDimensionless>( 7.,  7.),
        Complex<QtySiDimensionless>( 8.,  8.),
        Complex<QtySiDimensionless>( 9.,  9.),
    });

    CMatrix3<QtySiDimensionless> result2 ({
        Complex<QtySiDimensionless>( 1., -1.),
        Complex<QtySiDimensionless>( 4., -4.),
        Complex<QtySiDimensionless>( 7., -7.),
        Complex<QtySiDimensionless>( 2., -2.),
        Complex<QtySiDimensionless>( 5., -5.),
        Complex<QtySiDimensionless>( 8., -8.),
        Complex<QtySiDimensionless>( 3., -3.),
        Complex<QtySiDimensionless>( 6., -6.),
        Complex<QtySiDimensionless>( 9., -9.),
    });

    MAT_EXPECT_NEAR(m1.conjT(), result1);
    MAT_EXPECT_NEAR(m2.conjT(), result2);

}

TEST(CMatrix, doConjT)
{
    CMatrix2<QtySiDimensionless> m1 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
    });

    CMatrix2<QtySiDimensionless> result1 ({
        Complex<QtySiDimensionless>( 1.,  -1.),
        Complex<QtySiDimensionless>( 3.,  -3.),
        Complex<QtySiDimensionless>( 2.,  -2.),
        Complex<QtySiDimensionless>( 4.,  -4.),
    });

    CMatrix3<QtySiDimensionless> m2 ({
        Complex<QtySiDimensionless>( 1.,  1.),
        Complex<QtySiDimensionless>( 2.,  2.),
        Complex<QtySiDimensionless>( 3.,  3.),
        Complex<QtySiDimensionless>( 4.,  4.),
        Complex<QtySiDimensionless>( 5.,  5.),
        Complex<QtySiDimensionless>( 6.,  6.),
        Complex<QtySiDimensionless>( 7.,  7.),
        Complex<QtySiDimensionless>( 8.,  8.),
        Complex<QtySiDimensionless>( 9.,  9.),
    });

    CMatrix3<QtySiDimensionless> result2 ({
        Complex<QtySiDimensionless>( 1., -1.),
        Complex<QtySiDimensionless>( 4., -4.),
        Complex<QtySiDimensionless>( 7., -7.),
        Complex<QtySiDimensionless>( 2., -2.),
        Complex<QtySiDimensionless>( 5., -5.),
        Complex<QtySiDimensionless>( 8., -8.),
        Complex<QtySiDimensionless>( 3., -3.),
        Complex<QtySiDimensionless>( 6., -6.),
        Complex<QtySiDimensionless>( 9., -9.),
    });

    m1.doConjT();
    m2.doConjT();

    MAT_EXPECT_NEAR(m1, result1);
    MAT_EXPECT_NEAR(m2, result2);

}


//--------------------------------------------------------------------------------
//--------------------------------------------------------------------------------
/* Addition and addition related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(CMatrix, addition)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );

    CMatrix3<QtySiLength> result (
            4. * meter, 4. * meter, 4. * meter,
            0 * meter, 4 * meter, 13 * meter,
            2. * meter, -3 * meter, 10. * meter
    );

    EXPECT_EQ(m1 + m2, result);
}

TEST(CMatrix, additionEqual)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> result (
            4. * meter, 4. * meter, 4. * meter,
            0 * meter, 4 * meter, 13 * meter,
            2. * meter, -3 * meter, 10. * meter
    );

    EXPECT_EQ(m1 += m2, result);
}

/* Subtraction and subtraction related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(CMatrix, subtraction)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> result (
            -2. * meter, -6. * meter, -4. * meter,
            4 * meter, 10 * meter, -1 * meter,
            2. * meter, -5 * meter, 0. * meter
    );

    EXPECT_EQ(m1 - m2, result);
}

TEST(CMatrix, subtractionEqual)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> result (
            -2. * meter, -6. * meter, -4. * meter,
            4 * meter, 10 * meter, -1 * meter,
            2. * meter, -5 * meter, 0. * meter
    );

    EXPECT_EQ(m1 -= m2, result);
}

/* Multiplication and multiplication related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(CMatrix, multiplicationByScalar)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> result (
            2. * meter, -2. * meter, 0. * meter,
            4. * meter, 14 * meter, 12 * meter,
            4. * meter, -8 * meter, 10. * meter
    );

    EXPECT_EQ(m1 * 2., result);
}

TEST(CMatrix, multiplicationByMatrix)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            3. * meter, 5. * meter, 4. * meter,
            -2 * meter, -3 * meter, 7 * meter,
            0. * meter, 1 * meter, 5. * meter
    );
    CMatrix3<QtySiArea> result (
            5. * square_meter, 8 * square_meter, -3. * square_meter,
            -8. * square_meter, -5 * square_meter, 87 * square_meter,
            14. * square_meter, 27 * square_meter, 5. * square_meter
    );

    EXPECT_EQ(m1*m2, result);
}

/* Division and division related tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(CMatrix, scalarDivision)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> result (
            0.5 * meter, -0.5 * meter, 0. * meter,
            1. * meter, 3.5 * meter, 3 * meter,
            1. * meter, -2 * meter, 2.5 * meter
    );

    EXPECT_EQ(m1 / 2., result);
}

/* Comparative tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(CMatrix, isEqual)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter,  7. * meter, 6. * meter,
            2. * meter, -4. * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter,  7. * meter, 6. * meter,
            2. * meter, -4. * meter, 5. * meter
    );

    EXPECT_EQ(m1, m2);
}

TEST(CMatrix, isNotEqual)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            2. * meter, -1. * meter, 0. * meter,
            3. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    EXPECT_NE(m1, m2);
}

/* Common matrix operations tests */
// ----------------------------------------------------------------------------------------------------------------
TEST(CMatrix, transpose)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> result (
            1. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_EQ(m1.t(), result);
}

TEST(CMatrix, replaceWithTranspose)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> result (
            1. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    m1.doT();
    EXPECT_EQ(m1, result);
}

TEST(CMatrix, square)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.square);
}

TEST(CMatrix, orderOfSquareMatrix)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    EXPECT_EQ(m1.order(), 3);
}

TEST(CMatrix, order)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    pair<unsigned int,unsigned int> result(3,3);
    EXPECT_EQ(m1.orders(), result);
}


TEST(CMatrix, normL1)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter,
            7. * meter, -8 * meter,  9 * meter
    );

    QTY_EXPECT_NEAR(m1.normL1(), 18. * meter);
}

TEST(CMatrix, normLInf)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter,
            7. * meter, -8 * meter,  9 * meter
    );

    QTY_EXPECT_NEAR(m1.normLInf(), 24. * meter);
}

TEST(CMatrix, normLMax)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -2 * meter,  3 * meter,
            -4. * meter,  5 * meter, -6 * meter,
            7. * meter, -8 * meter,  9 * meter
    );

    QTY_EXPECT_NEAR(m1.normLMax(), 9. * meter);
}

TEST(CMatrix, normL)
{
    // TODO
}

TEST(CMatrix, normF)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1 * meter,  1 * meter,
            -1. * meter,  1 * meter, -1 * meter,
            1. * meter, -1 * meter,  1 * meter
    );

    QTY_EXPECT_NEAR(m1.normF(), 3. * meter);
}

TEST(CMatrix, isIdentity)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, 0. * meter, 0. * meter,
            0. * meter, 1. * meter, 0. * meter,
            0. * meter, 0. * meter, 1. * meter
    );
    CMatrix3<QtySiLength> m2 (
            1. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.isIdentity());
    EXPECT_FALSE(m2.isIdentity());
}

TEST(CMatrix, trace)
{
    CMatrix3<QtySiLength> m1 (
            -3. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_EQ(m1.tr(), Complex<QtySiLength>(9 * meter));
}

TEST(CMatrix, isDiagonal)
{
    CMatrix3<QtySiLength> m1 (
            -3. * meter, 0. * meter, 0 * meter,
            0 * meter, 7. * meter, 0. * meter,
            0. * meter, 0 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            -3. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.isDiagonal());
    EXPECT_FALSE(m2.isDiagonal());
}

TEST(CMatrix, isUpperTriangular)
{
    CMatrix3<QtySiLength> m1 (
            -3. * meter, 0. * meter, 0 * meter,
            0 * meter, 7. * meter, 0. * meter,
            0. * meter, 0 * meter, 5. * meter
    );

    CMatrix3<QtySiLength> m2 (
            -3. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.isUpperTriangular());
    EXPECT_FALSE(m2.isUpperTriangular());
}

TEST(CMatrix, isLowerTriangular)
{
    CMatrix3<QtySiLength> m1 (
            -3. * meter, 0. * meter, 0 * meter,
            0 * meter, 7. * meter, 0. * meter,
            0. * meter, 0 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            -3. * meter, 2. * meter, 2. * meter,
            -1. * meter, 7. * meter, -4. * meter,
            0. * meter, 6 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.isLowerTriangular());
    EXPECT_FALSE(m2.isLowerTriangular());
}

TEST(CMatrix, isSymmetric)
{
    CMatrix3<QtySiLength> m1 (
            -3. * meter, 2. * meter, 3. * meter,
            2. * meter, 7. * meter, 6. * meter,
            3. * meter, 6 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            -3. * meter, 2. * meter, 3. * meter,
            2. * meter, 7. * meter, -4. * meter,
            3. * meter, 6 * meter, 5. * meter
    );

    EXPECT_TRUE(m1.isSymmetric());
    EXPECT_FALSE(m2.isSymmetric());
}

TEST(CMatrix, isSkewSymmetric)
{
    CMatrix3<QtySiLength> m1 (
            -3. * meter, 2. * meter, 3. * meter,
            2. * meter, 7. * meter, 6. * meter,
            3. * meter, 6 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            0. * meter, 1. * meter, 3. * meter,
            -1. * meter, 0. * meter, 2. * meter,
            -3. * meter, -2 * meter, 0. * meter
    );

    EXPECT_FALSE(m1.isSkewSymmetric());
    EXPECT_TRUE(m2.isSkewSymmetric());
}

TEST(CMatrix, submatrix)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix2<QtySiLength> result = RMatrix2<QtySiLength> (
            7. * meter, 6. * meter,
            -4. * meter, 5. * meter
    );

    EXPECT_EQ(m1.sub(1,1), result);
}

TEST(CMatrix, determinantLaplace)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::LAPLACE_FORMULA), Complex<QtySiVolume>(57 * cubic_meter));
}

TEST(CMatrix, determinantGauss)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::GAUSSIAN_ELIMINATION), Complex<QtySiVolume>(57 * cubic_meter));
}

TEST(CMatrix, isOrthogonal)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );

    CMatrix3<QtySiLength> m2 (
            1. * meter, 0. * meter, 0. * meter,
            0. * meter, -1. * meter, 0. * meter,
            0. * meter, 0 * meter, 1. * meter
    );

    CMatrix3<QtySiLength> m3 (
             1./3. * meter, 2./3. * meter, -2./3. * meter,
            -2./3. * meter, 2./3. * meter,  1./3. * meter,
             2./3. * meter, 1./3. * meter,  2./3. * meter
    );
    
    EXPECT_FALSE(m1.isOrthogonal());
    EXPECT_TRUE(m2.isOrthogonal());
    EXPECT_TRUE(m3.isOrthogonal());
}

TEST(CMatrix, isSingular)
{
    CMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    
    CMatrix3<QtySiLength> m2(
            1. * meter, 2 * meter, 2. * meter,
            1. * meter, 2 * meter, 2 * meter,
            3. * meter, 2 * meter, -1. * meter
    );

    EXPECT_FALSE(m1.isSingular());
    EXPECT_TRUE(m2.isSingular());
}

TEST(CMatrix, minor)
{
    CMatrix3<QtySiLength> m1 (
            1. * meter, 0 * meter, 2. * meter,
            2. * meter, -2 * meter, 1 * meter,
            3. * meter, 2 * meter, 4. * meter
    );

    EXPECT_EQ(m1.minor(2,2),Complex<QtySiArea>( -2. * square_meter ));
}

TEST(CMatrix, minors)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    CMatrix3<QtySiArea> matrixOfMinors (
            2. * square_meter, 2 * square_meter, 2. * square_meter,
            -2. * square_meter, 3 * square_meter, 3 * square_meter,
            0. * square_meter, -10 * square_meter, 0. * square_meter
    );

    EXPECT_EQ(m1.minors(), matrixOfMinors);
}

TEST(CMatrix, cofactor)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    EXPECT_EQ(m1.cofactor(1,1), Complex<QtySiArea>( 2. * square_meter));
    EXPECT_EQ(m1.cofactor(1,2), Complex<QtySiArea>(-2. * square_meter));
    EXPECT_EQ(m1.cofactor(1,3), Complex<QtySiArea>( 2. * square_meter));
    EXPECT_EQ(m1.cofactor(2,1), Complex<QtySiArea>( 2. * square_meter));
    EXPECT_EQ(m1.cofactor(2,2), Complex<QtySiArea>( 3. * square_meter));
    EXPECT_EQ(m1.cofactor(2,3), Complex<QtySiArea>(-3. * square_meter));
    EXPECT_EQ(m1.cofactor(3,1), Complex<QtySiArea>( 0. * square_meter));
    EXPECT_EQ(m1.cofactor(3,2), Complex<QtySiArea>(10. * square_meter));
    EXPECT_EQ(m1.cofactor(3,3), Complex<QtySiArea>( 0. * square_meter));
}

TEST(CMatrix, cofactors)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    CMatrix3<QtySiArea> result(
            2. * square_meter, -2. * square_meter, 2. * square_meter,
            2. * square_meter, 3. * square_meter, -3 * square_meter,
            0. * square_meter, 10. * square_meter, 0. * square_meter
    );

    EXPECT_EQ(m1.cofactors(), result);
}

TEST(CMatrix, adjugate)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    CMatrix3<QtySiArea> result (
            2. * square_meter, 2 * square_meter, 0. * square_meter,
            -2. * square_meter, 3 * square_meter, 10 * square_meter,
            2. * square_meter, -3 * square_meter, 0. * square_meter
    );

    EXPECT_EQ(m1.adj(), result);
}

TEST(CMatrix, inv)
{
    CMatrix3<QtySiLength> m1(
            3. * meter, 0 * meter,  2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter,  1. * meter
    );
    CMatrix3<QtySiWavenumber> result(
             0.2 * reciprocal_meter,  0.2 * reciprocal_meter, 0. * reciprocal_meter,
            -0.2 * reciprocal_meter,  0.3 * reciprocal_meter, 1  * reciprocal_meter,
             0.2 * reciprocal_meter, -0.3 * reciprocal_meter, 0  * reciprocal_meter
    );

    MAT_EXPECT_NEAR(m1.inv(), result);
}

TEST(CMatrix, diagonal)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    CMatrix<3,1,QtySiLength> result(3. * meter, 0. * meter, 1. * meter);

    MAT_EXPECT_NEAR(m1.diagonal(), result);
}

TEST(CMatrix, row)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    CMatrix<1,3,QtySiLength> result1(3. * meter, 0. * meter,  2. * meter);
    CMatrix<1,3,QtySiLength> result2(2. * meter, 0. * meter, -2. * meter);
    CMatrix<1,3,QtySiLength> result3(0. * meter, 1. * meter,  1. * meter);

    MAT_EXPECT_NEAR(m1.row(1), result1);
    MAT_EXPECT_NEAR(m1.row(2), result2);
    MAT_EXPECT_NEAR(m1.row(3), result3);
}

TEST(CMatrix, column)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    CMatrix<3,1,QtySiLength> result1(3. * meter,  2. * meter,  0. * meter);
    CMatrix<3,1,QtySiLength> result2(0. * meter,  0. * meter,  1. * meter);
    CMatrix<3,1,QtySiLength> result3(2. * meter, -2. * meter,  1. * meter);

    MAT_EXPECT_NEAR(m1.col(1), result1);
    MAT_EXPECT_NEAR(m1.col(2), result2);
    MAT_EXPECT_NEAR(m1.col(3), result3);
}

TEST(CMatrixMN, norm)
{
    CMatrix<3,1,QtySiLength> m1(
            3. * meter, 6. * meter,	22. * meter
    );
    CMatrix<1,3,QtySiLength> m2(
            3. * meter, 6. * meter,	22. * meter
    );

    QTY_EXPECT_NEAR(m1.norm(), 23. * meter);
    QTY_EXPECT_NEAR(m2.norm(), 23. * meter);
}

TEST(CMatrixMN, normSquared)
{
    CMatrix<3,1,QtySiLength> m1(
            3. * meter, 6. * meter,	22. * meter
    );
    CMatrix<1,3,QtySiLength> m2(
            3. * meter, 6. * meter,	22. * meter
    );

    QTY_EXPECT_NEAR(m1.normSquared(), 529. * square_meter);
    QTY_EXPECT_NEAR(m2.normSquared(), 529. * square_meter);
}


/* Validity check operations */
// ----------------------------------------------------------------------------------------------------------------
TEST(CMatrix, isNull)
{
    CMatrix3<QtySiLength> m1  (
            1. * meter, -1. * meter, 0. * meter,
            2. * meter, 7. * meter, 6. * meter,
            2. * meter, -4 * meter, 5. * meter
    );
    CMatrix3<QtySiLength> m2 (
            0. * meter, 0. * meter, 0. * meter,
            0. * meter, 0. * meter, 0. * meter,
            0. * meter, 0. * meter, 0. * meter
    );

    EXPECT_FALSE(m1.isNull());
    EXPECT_TRUE(m2.isNull());
}

TEST(CMatrix, isFinite)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    CMatrix3<QtySiLength> m2 (
            inf_val * meter, inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter, inf_val * meter
    );

    EXPECT_TRUE(m1.isFinite());
    EXPECT_FALSE(m2.isFinite());
}

TEST(CMatrix, isInf)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    CMatrix3<QtySiLength> m2 (
            inf_val * meter, inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter, inf_val * meter,
            inf_val * meter, inf_val * meter, inf_val * meter
    );

    EXPECT_FALSE(m1.isInfinite());
    EXPECT_TRUE(m2.isInfinite());
}

TEST(CMatrix, isNormal)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    CMatrix3<QtySiLength> m2 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, nan_val * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    EXPECT_FALSE(m1.isNormal());
    EXPECT_FALSE(m2.isNormal());
}

TEST(CMatrix, isNan)
{
    CMatrix3<QtySiLength> m1 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, 0 * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );
    CMatrix3<QtySiLength> m2 (
            3. * meter, 0 * meter, 2. * meter,
            2. * meter, nan_val * meter, -2 * meter,
            0. * meter, 1 * meter, 1. * meter
    );

    EXPECT_FALSE(m1.isNan());
    EXPECT_TRUE(m2.isNan());
}