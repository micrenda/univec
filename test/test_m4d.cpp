#include <boost/units/quantity.hpp>
#include <boost/units/systems/si/length.hpp>
#include <gtest/gtest.h>

#include "qtydef/QtyDefinitions.hpp"

#include "univec/Matrix4.hpp"
#include "test_utils.hpp"

using namespace std;
using namespace boost::units;
using namespace dfpe;
using namespace si;

TEST(RMatrix4, determinantLeibniz)
{
    RMatrix4<QtySiLength> m1 (
            1. * meter, 2. * meter, 3. * meter, 4. * meter,
            4. * meter, 3. * meter, 2. * meter, 1. * meter,
            1. * meter, 2. * meter, 4. * meter, 3. * meter,
            2. * meter, 1. * meter, 3. * meter, 4. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::LEIBNIZ_FORMULA), -40. * square_meter * square_meter);
}

TEST(RMatrix4, determinantLaplace)
{
    RMatrix4<QtySiLength> m1 (
            1. * meter, 2. * meter, 3. * meter, 4. * meter,
            4. * meter, 3. * meter, 2. * meter, 1. * meter,
            1. * meter, 2. * meter, 4. * meter, 3. * meter,
            2. * meter, 1. * meter, 3. * meter, 4. * meter
    );


    EXPECT_EQ(m1.det(DeterminantMethod::LAPLACE_FORMULA), -40. * square_meter * square_meter);
}

TEST(RMatrix4, determinantGauss)
{
    RMatrix4<QtySiLength> m1 (
            1. * meter, 2. * meter, 3. * meter, 4. * meter,
            4. * meter, 3. * meter, 2. * meter, 1. * meter,
            1. * meter, 2. * meter, 4. * meter, 3. * meter,
            2. * meter, 1. * meter, 3. * meter, 4. * meter
    );

    EXPECT_EQ(m1.det(DeterminantMethod::GAUSSIAN_ELIMINATION), -40. * square_meter * square_meter);
}
